﻿namespace UnitTestProject
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Web.Html;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// This demo how to use the library
    /// </summary>
    [TestClass]
    public partial class UnitTests
    {
        /// <summary>
        /// The render section demo method.
        /// </summary>
        [TestMethod]
        public void RenderSectionDemoMethod()
        {
            try
            {
                /* Mimic Html writer for Control */
                using (var output = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                {
                    using (output.RenderSection())
                    {
                        output.WriteLine("My Section!");
                    }

                    using (output.RenderSection(new CssClassAttribute("articles")))
                    {
                        output.WriteLine("My Section!");
                    }

                    /* This way is use in Razor Templates mostly */
                    using (output.RenderSection(new CssClassAttribute("articles"), new DataAttribute("module", "articles")))
                    {
                        output.WriteLine("My Section!");
                    }

                    /* This way is use in WebControls mostly as attributes are passed as array from system  */
                    var attr = new[]
                                  { 
                                      new CssClassAttribute("articles"), 
                                      new DataAttribute("module", "articles"),
                                      new XAttribute("custom", "my attribute") 
                                   };

                    using (output.RenderSection(attr))
                    {
                        output.WriteLine("My Section!");
                    }

                    var html = output.InnerWriter.ToString();
                    Assert.IsTrue(!string.IsNullOrWhiteSpace(html));
                }
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        /// <summary>
        /// Tags the demo method.
        /// </summary>
        [TestMethod]
        public void TagDemoMethod()
        {
            try
            {
                /* The library contains all major html tags. This is just a scratch of surface */
                var js = new Script { Src = "/js/main.js" };
                Page page = new Page();
                page.Controls.Add(js);

                /* Conversions */
                Control control = js;
                Assert.IsNotNull(control);
                string tag = js;
                Assert.IsNotNull(tag);
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }

        /// <summary>
        /// Tags the with HTML writer demo method.
        /// </summary>
        [TestMethod]
        public void TagWithHtmlWriterDemoMethod()
        {
            try
            {
                /* Mimic Html writer for Control */
                using (var output = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                {
                    /* The library contains all major html tags. This is just a scratch of surface */

                    /* Note: The same results can be achieved same way in method above. 
                     * Main difference the inned html encoding. 
                     * The HtmlTags(XElement's) will always html encode inner string as method above not, also it can be converted to webform controls or IHtmlString.  
                     */
                    var js = new Script { Src = "/js/main.js" };

                    output.WriteLineNoTabs(js);

                    var link = new A("Vikings & Sons")
                                      {
                                          Href = "/",
                                          Class = "main",
                                          Target = "_blank"
                                      };

                    link.Add(new DataAttribute("io-segment-track", "true"));
                    link.Add(new DataAttribute("io-segment-label", "header link"));

                    output.WriteLineNoTabs(link);
                    output.Hr();

                    output.A(href: "/", innerHtml: "Vikings & Sons", cssClass: "main", target: "_blank", attributes: new XAttribute[] { new DataAttribute("io-segment-track", "true"), new DataAttribute("io-segment-label", "header link") });

                    var html = output.InnerWriter.ToString();
                    /* Something like this:
                        <script src="/js/main.js"></script>
                        <a href="/" class="main" target="_blank" data-io-segment-track="true" data-io-segment-label="header link">Vikings &amp; Sons</a>
                        <hr />
                        <a data-io-segment-track="true" data-io-segment-label="header link" href="/" class="main" target="_blank">Vikings & Sons</a>   
                     */
                    Assert.IsTrue(!string.IsNullOrWhiteSpace(html));
                }
            }
            catch (Exception exception)
            {
                Assert.Fail(exception.Message);
            }
        }
    }
}