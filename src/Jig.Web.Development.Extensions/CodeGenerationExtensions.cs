﻿namespace Jig.Web.CodeGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// String object extensions used during T4 code generation.
    /// </summary>
    /// <remarks>
    /// This extension library was forked from Hedgehog Development's T4 starter kit. 
    /// https://github.com/HedgehogDevelopment/tds-codegen.git
    /// I changed the namespace to ensure it doesn't conflict with other TDS projects
    /// you may have running.; I have made some slight modifications to produce class 
    /// names that I find a bit more pleasing.
    /// </remarks>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed. Suppression is OK here.")]
    public static class CodeGenerationExtensions
    {
        /// <summary>
        /// The title case regex
        /// </summary>
        private static readonly Regex TitleCaseRegex = new Regex("([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", RegexOptions.Compiled);

        /// <summary>
        /// As valid word regex
        /// </summary>
        private static readonly Regex AsValidWordRegex = new Regex(@"[^\p{Ll}\p{Lu}\p{Lt}\p{Lo}\p{Nd}\p{Nl}\p{Mn}\p{Mc}\p{Cf}\p{Pc}\p{Lm}]", RegexOptions.Compiled);

        /// <summary>
        /// Titles the case.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word as title case</returns>
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
        public static string TitleCase(this string word)
        {
            try
            {
                if (string.IsNullOrEmpty(word))
                {
                    return string.Empty;
                }

                var newWord = TitleCaseRegex.Replace(word, "$1+");
                newWord = CultureInfo.InvariantCulture.TextInfo.ToTitleCase(newWord);
                newWord = newWord.Replace("+", string.Empty);
                return newWord;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method TitleCase: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Camels the case.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word in Camel Case</returns>
        public static string CamelCase(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                /*
                 * Something -> something
                 * ISomething -> iSomething
                 * SomethingElse -> somethingElse
                 * ISomethingElse -> iSomethingElse
                 */

                var titleCase = word.TitleCase();
                return titleCase.Substring(0, 1).ToLowerInvariant() + titleCase.Substring(1);
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method CamelCase: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Determines whether [is interface word] [the specified word].
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word as interface</returns>
        public static bool IsInterfaceWord(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return false;
            }

            try
            {
                // looks like an interface if... I[A-Z]
                // proper definition is http://msdn.microsoft.com/en-us/library/8bc1fexb(v=VS.71).aspx
                return word.Length > 2 && !word.Contains(" ") && (word[0] == 'I' && char.IsUpper(word, 1) && char.IsLower(word, 2));
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method IsInterfaceWord: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// As the name of the interface.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word as interface name</returns>
        public static string AsInterfaceName(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                /*
                 * return I[TitleCaseWord]
                 * something -> ISomething
                 * Something -> ISomething
                 * ISomething -> ISomething
                 */

                var interfaceWord = GetFormattedWord(word, TitleCase).Trim('_');

                // Only prefix the word with a 'I' if we don't have a word that already looks like an interface.
                if (!word.IsInterfaceWord())
                {
                    interfaceWord = string.Concat("I", interfaceWord);
                }

                return interfaceWord;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsInterfaceName: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// As the name of the class.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word as class name</returns>
        public static string AsClassName(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                // TitleCase the word
                return GetFormattedWord(word, TitleCase);
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsClassName: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// As the name of the property.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <param name="pluralize">if set to <c>true</c> [pluralize].</param>
        /// <returns>The word as property name</returns>
        public static string AsPropertyName(this string word, bool pluralize = false)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                // TitleCase the word and pluralize it
                if (pluralize)
                {
                    return GetFormattedWord(word, TitleCase, Inflector.Pluralize);
                }

                return GetFormattedWord(word, TitleCase);
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsPropertyName: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// As the name of the field.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>The word as field name</returns>
        public static string AsFieldName(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                // return _someParam. 
                // Note, this isn't MS guideline, but it easier to deal with than using this. everywhere to avoid name collisions
                // ReSharper disable RedundantLambdaParameterType
                return GetFormattedWord(word, CamelCase, (string s) => "_" + s);
                // ReSharper restore RedundantLambdaParameterType
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsFieldName: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Tests whether the words conflicts with reserved or language keywords, and if so, attempts to return 
        /// valid words that do not conflict. Usually the returned words are only slightly modified to differentiate 
        /// the identifier from the keyword; for example, the word might be preceded by the underscore ("_") character.
        /// </summary>
        /// <param name="words">The words.</param>
        /// <returns>The list of words with each word cleaned up into a valid word.</returns>
        public static IEnumerable<string> AsValidWords(this IEnumerable<string> words)
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var word in words)
            {
                // ReSharper restore LoopCanBeConvertedToQuery
                if (string.IsNullOrWhiteSpace(word))
                {
                    yield return string.Empty;
                }

                if (word != null)
                {
                    yield return AsValidWord(word.Trim());
                }
            }
        }

        /// <summary>
        /// Tests whether the word conflicts with reserved or language keywords, and if so, attempts to return a 
        /// valid word that does not conflict. Usually the returned word is only slightly modified to differentiate 
        /// the identifier from the keyword; for example, the word might be preceded by the underscore ("_") character.
        /// <para>
        /// Valid identifiers in C# are defined in the C# Language Specification, item 2.4.2. The rules are very simple:
        /// - An identifier must start with a letter or an underscore
        /// - After the first character, it may contain numbers, letters, connectors, etc
        /// - If the identifier is a keyword, it must be perpended with “@”
        /// </para>
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>A valid word for the specified word.</returns>
        public static string AsValidWord(this string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                var identifier = word;

                if (identifier == "*")
                {
                    identifier = "Wildcard";
                }

                identifier = RemoveDiacritics(identifier);

                // C# Identifiers - http://msdn.microsoft.com/en-us/library/aa664670(VS.71).aspx
                // removes all illegal characters
                identifier = AsValidWordRegex.Replace(identifier, string.Empty);

                // The identifier must start with a character or '_'
                if (!(char.IsLetter(identifier, 0) || identifier[0] == '_'))
                {
                    identifier = string.Concat("_", identifier);
                }

                // fix language specific reserved words
                identifier = FixReservedWord(identifier);

                return identifier;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsValidWord: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Concatenates all of the <paramref name="words"/> with a '.' separator.
        /// <para>Each word is passed through the <c>AsValidWord</c> method ensuring that it is a valid for a namespace segment.</para>
        /// <para>Leading, trailing, and more than one consecutive '.' are removed.</para>
        /// </summary>
        /// <example> 
        /// This sample shows how to call the <see cref="AsNamespace"/> method.
        /// <code>
        ///     string[] segments = new string[5]{ ".My", "Namespace.", "For", "The...Sample..", "Project."};
        ///     string ns = segments.AsNamespace();
        /// </code>
        /// The <c>ns</c> variable would contain "<c>My.Namespace.For.The.Sample.Project</c>".
        /// </example>
        /// <param name="words">The namespace segments.</param>
        /// <returns>A valid string in valid namespace format.</returns>
        public static string AsNamespace(this IEnumerable<string> words)
        {
            if (words == null)
            {
                return string.Empty;
            }

            var wordCollection = words.ToArray();
            try
            {
                var joinedNamespace = new List<string>();

                foreach (var segment in wordCollection)
                {
                    if (!string.IsNullOrEmpty(segment))
                    {
                        // split apart any strings with a '.' and remove any consecutive multiple '.'
                        var segments = segment.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

                        // being we are making a namespace, make sure the segments are valid
                        var validSegments = segments.AsValidWords();
                        joinedNamespace.AddRange(validSegments);
                    }
                }

                var ns = string.Join(".", joinedNamespace.ToArray());
                return ns;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method AsNamespace: " + string.Join(", ", wordCollection));
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Gets a custom property from the data assuming it is query string format
        /// </summary>
        /// <param name="data">A string in query string format.</param>
        /// <param name="key">The key to get the value for.</param>
        /// <returns>
        /// The value, or an empty string
        /// </returns>
        public static string GetCustomProperty(string data, string key)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }

            try
            {
                var strArray = data.Split('&');
                var keyEquals = key + "=";
                var length = keyEquals.Length;

                foreach (var keyValuePair in strArray)
                {
                    if ((keyValuePair.Length > length) && keyValuePair.StartsWith(keyEquals, StringComparison.OrdinalIgnoreCase))
                    {
                        return keyValuePair.Substring(length);
                    }
                }
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method GetCustomProperty: Data =>" + data + ", Key=>" + key);
                Trace.TraceError(exception.ToString());
                throw;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the calculated namespace for the template
        /// </summary>
        /// <param name="sitePaths">The site mappings to the namespaces.</param>
        /// <param name="itemPath">The item path.</param>
        /// <param name="includeGlobal">The include Global.</param>
        /// <returns>
        /// The namespace of the supplied item.
        /// </returns>
        public static string GetNamespace(Dictionary<string, string> sitePaths, string itemPath, bool includeGlobal = false)
        {
            /* Examples:
                    TDS Item Namespace      Jig.Sitecore.Sites.Shared.Data.sitecore.templates.Sites.Shared.HeadTags.OpenGraph
                    Item Path               /sitecore/templates/Sites/Shared/HeadTags/OpenGraph/OpenGraphImageTag
               Result should be:
                    Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraph
            */
            if (string.IsNullOrWhiteSpace(itemPath) || sitePaths == null)
            {
                return string.Empty;
            }

            string @namespace = itemPath;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var pair in sitePaths)
            {
                @namespace = @namespace.Replace(pair.Key, pair.Value);
            }

            var namespaceSegments = @namespace.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            var list = new List<string>();
            for (int i = 0; i < namespaceSegments.Length; i++)
            {
                if (i + 1 != namespaceSegments.Length)
                {
                    list.Add(namespaceSegments[i]);
                }
            }

            // ReSharper restore LoopCanBeConvertedToQuery
            @namespace = list.AsNamespace(); // use an extension method in the supporting assembly

            return includeGlobal ? string.Concat("global::", @namespace) : @namespace;
        }

        /// <summary>
        /// Gets the calculated namespace for the template
        /// </summary>
        /// <param name="defaultNamespace">The default Namespace.</param>
        /// <param name="itemPath">The item path.</param>
        /// <param name="includeGlobal">The include Global.</param>
        /// <returns>
        /// The namespace of the supplied item.
        /// </returns>
        public static string GetNamespace(string defaultNamespace, string itemPath, bool includeGlobal = false)
        {
            /* Examples:
                    TDS Item Namespace      Jig.Sitecore.Sites.Shared.Data.sitecore.templates.Sites.Shared.HeadTags.OpenGraph
                    Item Path               /sitecore/templates/Sites/Shared/HeadTags/OpenGraph/OpenGraphImageTag
               Result should be:
                    Jig.Sitecore.Sites.Shared.Data.HeadTags.OpenGraph
            */
            var namespaceSegments = new HashSet<string>
                                        {
                                            defaultNamespace
                                        };

            if (!string.IsNullOrWhiteSpace(itemPath))
            {
                var tokens = itemPath.Split('/');
                if (tokens.Length > 4)
                {
                    /* Skip '/sitecore/templates/Sites/{Site Name}' */
                    for (int i = 5; i < tokens.Length - 1; i++)
                    {
                        namespaceSegments.Add(tokens[i]);
                    }
                }
            }

            var @namespace = namespaceSegments.AsNamespace(); // use an extension method in the supporting assembly

            return includeGlobal ? string.Concat("global::", @namespace) : @namespace;
        }

        /// <summary>
        /// Replacement "des caractères accentués"
        /// </summary>
        /// <param name="word">The string to replace diacritics on.</param>
        /// <remarks>A diacritic is a glyph added to a letter, or basic glyph</remarks>
        /// <returns>The string without diacritic marks.</returns>
        private static string RemoveDiacritics(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                var normalizedString = word.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();

                foreach (var c in normalizedString)
                {
                    if (CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(c);
                    }
                }

                return stringBuilder.ToString();
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method RemoveDiacritics: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// Tests whether the word conflicts with reserved or language keywords, and if so, attempts to return a 
        /// valid word that does not conflict. Usually the returned word is only slightly modified to differentiate 
        /// the identifier from the keyword; for example, the word might be preceded by the underscore ("_") character.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <returns>A valid identifier.</returns>
        private static string FixReservedWord(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            // turns keywords into usable words.
            // i.e. class -> _class
            var codeProvider = new Microsoft.CSharp.CSharpCodeProvider();

            try
            {
                var newWord = codeProvider.CreateValidIdentifier(word);
                return newWord;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError("Failed method FixReservedWord: " + word);
                Trace.TraceError(exception.ToString());
                throw;
            }
        }

        /// <summary>
        /// The get formatted word.
        /// </summary>
        /// <param name="word">The word.</param>
        /// <param name="transformations">The transformations.</param>
        /// <returns>
        /// The formatted version of the original string.
        /// </returns>
        private static string GetFormattedWord(this string word, params Func<string, string>[] transformations)
        {
            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }

            try
            {
                var newWord = word;
                // ReSharper disable LoopCanBeConvertedToQuery
                foreach (var item in transformations)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    if (item != null)
                    {
                        newWord = item(newWord);
                    }
                }

                // Now that the basic transforms are done, make sure we have a valid word to use 
                newWord = newWord.AsValidWord();
                return newWord;
            }
            catch (Exception exception)
            {
                /* Improve T4 template debugging */
                Trace.TraceError(exception.ToString());
                throw;
            }
        }
    }
}
