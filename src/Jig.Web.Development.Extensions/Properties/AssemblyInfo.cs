﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Jig.Web.CodeGeneration.Extensions")]

[assembly: Guid("8e417023-c562-445b-bf3a-7b6bd2ada72e")]

[assembly: AssemblyCompany("Jig Team")]
[assembly: AssemblyProduct("Jig Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig Web Team 2014")]
[assembly: AssemblyTrademark("Jig Team")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyFileVersion("2.0.0")]
[assembly: AssemblyVersion("2.0.0")]