﻿namespace System.Web.UI
{
    using System.Web.WebPages;
    using System.Web.WebPages.Instrumentation;

    /// <summary>
    /// Html Writer extension for Razor File Generator
    /// </summary>
    /// <remarks>This is Generic Razor code generation extension.</remarks>
    public static class HtmlTextWriterExtensions
    {
        /// <summary>
        /// Razor 2.0
        /// Writes attribute in situations like &lt;img src="@Model"&gt;.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="attribute">The attribute.</param>
        /// <param name="prefix">The prefix.</param>
        /// <param name="suffix">The suffix.</param>
        /// <param name="values">The values.</param>
        public static void WriteRazorAttribute(
            this HtmlTextWriter writer,
            // ReSharper disable UnusedParameter.Global
            string attribute,
            // ReSharper restore UnusedParameter.Global
            PositionTagged<string> prefix,
            PositionTagged<string> suffix,
            params AttributeValue[] values)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (writer != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                writer.Write(prefix.Value);

                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (values != null)
                {
                    // ReSharper restore ConditionIsAlwaysTrueOrFalse
                    foreach (var attributeValue in values)
                    {
                        writer.Write(attributeValue.Prefix.Value);

                        var value = attributeValue.Value.Value;
                        if (value != null)
                        {
                            writer.Write(value);
                        }
                    }
                }

                writer.Write(suffix.Value);
            }
        }
    }
}
