﻿namespace Jig.Web.CodeGeneration.Views
{
    using System;

    /// <summary>
    /// The Jig razor base.
    /// </summary>
    /// <typeparam name="TModel">The type of the model.</typeparam>
    public partial class CodeGeneratorView<TModel> : CodeGeneratorView where TModel : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CodeGeneratorView{TModel}"/> class.
        /// </summary>
        public CodeGeneratorView()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CodeGeneratorView{TModel}"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        public CodeGeneratorView(TModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            this.Model = model;
        }

        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public TModel Model { get; set; }
    }
}
