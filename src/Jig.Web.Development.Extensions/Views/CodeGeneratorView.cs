﻿namespace Jig.Web.CodeGeneration.Views
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The Jig razor base.
    /// </summary>
    // ReSharper disable ClassWithVirtualMembersNeverInherited.Global
    public partial class CodeGeneratorView
    // ReSharper restore ClassWithVirtualMembersNeverInherited.Global
    {
        #region Public Methods and Operators

        /// <summary>
        /// Performs an implicit conversion from <see cref="CodeGeneratorView"/> to <see cref="System.String"/>.
        /// </summary>
        /// <param name="razorBase">The razor base.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator string(CodeGeneratorView razorBase)
        {
            if (razorBase == null)
            {
                return string.Empty;
            }

            var html = razorBase.Render();
            return html;
        }

        /// <summary>
        /// Outputs server control content.
        /// </summary>
        /// <returns>Outputs server control content(html)</returns>
        public string Render()
        {
            using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
            {
                this.Render(innerWriter);

                var html = innerWriter.InnerWriter.ToString();
                return html;
            }
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores tracing information
        /// about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        public void Render(HtmlTextWriter output)
        {
            try
            {
                /* Do proper exception handling, write to inner writer  */
                using (var innerWriter = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
                {
                    this.RenderView(innerWriter);

                    output.WriteLine(innerWriter.InnerWriter.ToString());
                }
            }
            catch (Exception exception)
            {
                Trace.TraceError(exception.ToString());

                throw;
            }
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="output">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        protected virtual void RenderView(HtmlTextWriter output)
        {
            var code = new XElement("code", string.Empty);
            code.Add(new XAttribute("class", "error"));
            code.Add(
                new XAttribute(
                    "data-error-message",
                    "Override " + this.GetType().FullName + ".RenderView() method"));

            output.WriteLine(code);
        }

        #endregion
    }
}
