// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DynamicSqlDataReader.cs" company="Jig Team">
//   Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// <summary>
//   Dynamically Adding Load Method from DataSource.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
//  Author:         Herbrandson
//  Co-Author:      J.Baltika
//  Date:           12/18/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Data.SqlClient;
    using System.Reflection;
    using System.Reflection.Emit;
    using System.Runtime.InteropServices;
    using System.Web;

    /// <summary>
    ///     Dynamically Adding Load Method from DataSource.
    /// </summary>
    /// <typeparam name="TObject">Generic Object.</typeparam>
    [GuidAttribute("7BEE9AB7-448B-4F55-BE78-5483BADEE984")]
    internal sealed class DynamicSqlDataReader<TObject>
    {
        /// <summary>
        /// Delegate for SqlDataReader.
        /// </summary>
        private Load handler;

        /// <summary>
        ///     Prevents a default instance of the DynamicSqlDataReader class from being created.
        /// </summary>
        private DynamicSqlDataReader()
        {
        }

        /// <summary>
        ///     The delegate for SqlDataReader.
        /// </summary>
        /// <param name="dataRecord">The instance of the SqlDataReader with data.</param>
        /// <returns>The generic Object with Load delegate attached.</returns>
        private delegate TObject Load(SqlDataReader dataRecord);

        /// <summary>
        ///     Dynamically create and compile method at runtime.
        /// </summary>
        /// <param name="dataRecord">IReader with data.</param>
        /// <returns>The generic TObject.</returns>
        /// <remarks>
        /// <pre>
        /// The method like this will be dynamically create and compile code at runtime.
        /// This would gives the best of both worlds. This code could be dynamic(depends on IReader fields), 
        /// but since it was actually compiled, it would be as fast as classical below.
        /// The downside was that the dynamic code needed to be written using IL (intermediate language) instead of C#.
        /// public Person Load(SqlDataReader reader)
        /// {
        ///     Person person = new Person();
        ///     if (!reader.IsDBNull(0))
        ///     {
        ///         person.ID = (Guid)reader[0];
        ///     }
        ///     if (!reader.IsDBNull(1))
        ///     {
        ///         person.Name = (string)reader[1];
        ///     }
        ///     if (!reader.IsDBNull(2))
        ///     {
        ///         person.Kids = (int)reader[2];
        ///     }
        ///     if (!reader.IsDBNull(3))
        ///     {
        ///         person.Active = (bool)reader[3];
        ///     }
        ///     if (!reader.IsDBNull(4))
        ///     {
        ///         person.DateOfBirth = (DateTime)reader[4];
        ///     }
        ///     return person;
        /// }
        /// </pre>
        /// </remarks>
        [NotNull]
        public static DynamicSqlDataReader<TObject> CreateDynamicMethod([NotNull] SqlDataReader dataRecord)
        {
            if (dataRecord == null)
            {
                throw new HttpException("The SqlDataReader can't be null!");
            }

            var dynamicBuilder = new DynamicSqlDataReader<TObject>();

            Type type = typeof(TObject);

            var method = new DynamicMethod(
                "DynamicLoadSqlDataReader",
                MethodAttributes.Public | MethodAttributes.Static,
                CallingConventions.Standard,
                type,
                new[] { DynamicSqlDataReaderProperties.DataRecordType },
                type,
                true);
            ILGenerator generator = method.GetILGenerator();

            // The equivalent in C#
            // Person myPerson;
            LocalBuilder result = generator.DeclareLocal(type);

            // The equivalent in C#
            // myPerson = new Person();
            var constructor = type.GetConstructor(Type.EmptyTypes);

            if (constructor == null)
            {
                throw new HttpException("The constructor can't be null!");
            }

            // ReSharper disable AssignNullToNotNullAttribute
            generator.Emit(OpCodes.Newobj, constructor);
            // ReSharper restore AssignNullToNotNullAttribute
            generator.Emit(OpCodes.Stloc, result);

            // Add  System.Collections.Specialized.ListDictionary cache some day
            // ReSharper disable PossibleNullReferenceException
            for (int i = 0; i < dataRecord.FieldCount; i++) // ReSharper restore PossibleNullReferenceException
            {
                var propertyInfo = SqlPropertyInfoCache.ResolvePropertyInfo(type, dataRecord.GetName(i));

                // Check that TObject has the Setter Property  with the same name as SqlDataReader 
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (propertyInfo != null) // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    Label endIfLabel = generator.DefineLabel();

                    MethodInfo methodInfo = propertyInfo.GetSetMethod(nonPublic: true);
                    if (methodInfo != null)
                    {
                        // The equivalent in C#
                        // if (!mySqlDataReader.IsDBNull(1))
                        // {
                        generator.Emit(OpCodes.Ldarg_0);
                        generator.Emit(OpCodes.Ldc_I4, i);
                        generator.Emit(OpCodes.Callvirt, DynamicSqlDataReaderProperties.IsDbNullMethod);
                        generator.Emit(OpCodes.Brtrue, endIfLabel);

                        // The equivalent in C#
                        // myPerson.Name = (string)mySqlDataReader[1];
                        // OR
                        // myPerson.Age = (byte)mySqlDataReader[1];  
                        // OR
                        // myPerson.ID = (int)mySqlDataReader[1]; 
                        generator.Emit(OpCodes.Ldloc, result);
                        generator.Emit(OpCodes.Ldarg_0);
                        generator.Emit(OpCodes.Ldc_I4, i);
                        generator.Emit(OpCodes.Callvirt, DynamicSqlDataReaderProperties.GetValueMethod);

                        var nullable = propertyInfo.PropertyType.Name.Contains("Nullable") ? TypeOf.GetNullableType(dataRecord.GetFieldType(i)) : dataRecord.GetFieldType(i);

                        generator.Emit(OpCodes.Unbox_Any, nullable ?? typeof(object));

                        generator.Emit(OpCodes.Callvirt, methodInfo);

                        // The equivalent in C#
                        // }
                        generator.MarkLabel(endIfLabel);
                    }
                }
            }

            // The equivalent in C#
            // return myPerson;
            generator.Emit(OpCodes.Ldloc, result);
            generator.Emit(OpCodes.Ret);

            // The code then returns a handler to a delegate. 
            // When this handler is invoked, it calls the dynamically generated code
            Delegate loadDelegate = method.CreateDelegate(typeof(Load));
            dynamicBuilder.handler = (Load)loadDelegate;

            return dynamicBuilder;
        }

        /// <summary>
        ///     Load actual data to the object.
        /// </summary>
        /// <param name="dataRecord">The instance of the SqlDataReader with data.</param>
        /// <returns>Generic Object with loaded data.</returns>
        public TObject Build([NotNull] SqlDataReader dataRecord)
        {
            return this.handler(dataRecord);
        }
    }
}
