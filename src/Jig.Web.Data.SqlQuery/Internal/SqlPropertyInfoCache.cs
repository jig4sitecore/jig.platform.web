﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The sql property info cache.
    /// </summary>
    internal static class SqlPropertyInfoCache
    {
        #region Static Fields

        /// <summary>
        ///     The cache
        /// </summary>
        private static readonly IDictionary<Type, ICollection<KeyValuePair<PropertyInfo, SqlFieldNameAttribute>>> Cache = new Dictionary<Type, ICollection<KeyValuePair<PropertyInfo, SqlFieldNameAttribute>>>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The resolve property info.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="columnName">The column name.</param>
        /// <returns>
        /// The PropertyInfo.
        /// </returns>
        [NotNull]
        public static PropertyInfo ResolvePropertyInfo([NotNull] Type type, [NotNull] string columnName)
        {
            ICollection<KeyValuePair<PropertyInfo, SqlFieldNameAttribute>> collection;

            if (!Cache.TryGetValue(type, out collection))
            {
                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.IgnoreCase | BindingFlags.FlattenHierarchy | BindingFlags.SetProperty);
                collection = new HashSet<KeyValuePair<PropertyInfo, SqlFieldNameAttribute>>();
                foreach (PropertyInfo info in properties)
                {
                    SqlFieldNameAttribute field = info.GetCustomAttributes(inherit: true, attributeType: typeof(SqlFieldNameAttribute)).OfType<SqlFieldNameAttribute>().FirstOrDefault();

                    collection.Add(new KeyValuePair<PropertyInfo, SqlFieldNameAttribute>(info, field));
                }

                /* insert to cache */
                Cache[type] = collection;
            }

            var property =
                collection.FirstOrDefault(
                    x =>
                    string.Equals(x.Key.Name, columnName, StringComparison.InvariantCultureIgnoreCase) || (x.Value != null && string.Equals(x.Value.SqlFieldName, columnName, StringComparison.InvariantCultureIgnoreCase)));

            return property.Key;
        }

        #endregion
    }
}