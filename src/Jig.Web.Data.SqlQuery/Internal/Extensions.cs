﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Web.Caching;

    /// <summary>
    /// The sql query extensions.
    /// </summary>
    /// <remarks>These extensions available in other libraries. They are internal for code/libraries decoupling purposes</remarks>
    internal static partial class SqlQueryExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Case insensitive hash for small strings
        /// </summary>
        /// <param name="input">string to get hash code</param>
        /// <returns>
        /// The hash value of the string
        /// </returns>
        /// <remarks>
        /// This method were modified for .NET
        /// </remarks>
        [DebuggerHidden]
        public static int ConvertAsHashCode([NotNull] this string input)
        {
            unchecked
            {
                if (!string.IsNullOrEmpty(input))
                {
                    return input.ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                return string.Empty.GetHashCode();
            }
        }

        /// <summary>
        /// Clear All cached all items with specific partial key within cache keys
        /// </summary>
        /// <param name="cache">The cache for a Web application</param>
        /// <param name="key">The partial Cache key</param>
        [DebuggerHidden]
        public static void RemoveKeysContaining([NotNull] this Cache cache, [NotNull] string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                var keys = new List<string>();
                IDictionaryEnumerator cacheEnum = cache.GetEnumerator();
                while (cacheEnum.MoveNext())
                {
                    keys.Add(cacheEnum.Key.ToString());
                }

                foreach (string collectionKey in keys)
                {
                    if (collectionKey.IndexOf(key, StringComparison.OrdinalIgnoreCase) != -1)
                    {
                        cache.Remove(collectionKey);
                    }
                }
            }
        }

        #endregion
    }
}