﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Web;
    using System.Web.Configuration;

    using Jig.Web.Data.SqlClient.Configuration;

    /// <summary>
    /// The settings.
    /// </summary>
    [DebuggerDisplay("{ConnectionString}", Name = "{ConnectionStringName}")]
    public sealed class Settings
    {
        /// <summary>
        /// The default SQL connection name
        /// </summary>
        private const string SqlConnectionNamePrefix = "SqlQuery";

        /// <summary>
        /// The sync root
        /// </summary>
        private static readonly object SyncRoot = new object();

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1118:ParameterMustNotSpanMultipleLines", Justification = "Reviewed. Suppression is OK here.")]
        public Settings()
        {
            Trace.TraceInformation("Start constructor Settings()");

            Trace.TraceInformation("Step 1. Try to find default Sql connection by name");
            foreach (ConnectionStringSettings connection in WebConfigurationManager.ConnectionStrings)
            {
                Trace.TraceInformation("Evaluating ConnectionStringSettings: Name={0}, ConnectionString={1}", connection.Name, connection.ConnectionString);
                if (!string.IsNullOrWhiteSpace(connection.Name) && connection.Name.StartsWith(SqlConnectionNamePrefix, StringComparison.InvariantCultureIgnoreCase))
                {
                    this.ConnectionStringName = connection.Name;
                    if (string.IsNullOrWhiteSpace(this.ConnectionStringName))
                    {
                        throw new HttpException(540, "The connection string name cannot be null!");
                    }

                    this.ConnectionString = connection.ConnectionString;
                    if (string.IsNullOrWhiteSpace(this.ConnectionString))
                    {
                        throw new HttpException(540, "The connection string cannot be null!");
                    }

                    Trace.TraceInformation("Setting SQL Query ConnectionString: Name={0}, ConnectionString={1}", connection.Name, connection.ConnectionString);

                    return;
                }
            }

            Trace.TraceInformation("Step 2: Try to find by custom configuration");
            var instance = SqlQueryConnectionSection.Instance;
            if (instance != null)
            {
                this.ConnectionStringName = instance.SqlQueryConnectionSettings.ConnectionStringName;
                Trace.TraceInformation("ConnectionStringName: " + this.ConnectionStringName);
                if (string.IsNullOrWhiteSpace(this.ConnectionStringName))
                {
                    throw new HttpException(540, "The connection string name cannot be null!");
                }

                var connection = WebConfigurationManager.ConnectionStrings[this.ConnectionStringName];
                if (connection == null)
                {
                    throw new HttpException(540, "No connection strings are found in the config file!");
                }

                this.ConnectionString = connection.ConnectionString;
                if (string.IsNullOrWhiteSpace(this.ConnectionString))
                {
                    throw new HttpException(540, "The connection string cannot be null!");
                }

                Trace.TraceInformation("Setting SQL Query ConnectionString: Name={0}, ConnectionString={1}", connection.Name, connection.ConnectionString);

                return;
            }

            Trace.TraceInformation("Step 3. Try to find first non-local connection (machine.config has connection string LocalSqlServer. Skip that)");
            foreach (ConnectionStringSettings connection in WebConfigurationManager.ConnectionStrings)
            {
                Trace.TraceInformation("Evaluating ConnectionStringSettings: Name={0}, ConnectionString={1}", connection.Name, connection.ConnectionString);
                if (!string.IsNullOrWhiteSpace(connection.Name) && !connection.Name.StartsWith("Local", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.ConnectionStringName = connection.Name;
                    if (string.IsNullOrWhiteSpace(this.ConnectionStringName))
                    {
                        throw new HttpException(540, "The connection string name cannot be null!");
                    }

                    this.ConnectionString = connection.ConnectionString;
                    if (string.IsNullOrWhiteSpace(this.ConnectionString))
                    {
                        throw new HttpException(540, "The connection string cannot be null!");
                    }

                    Trace.TraceInformation("Setting SQL Query ConnectionString: Name={0}, ConnectionString={1}", connection.Name, connection.ConnectionString);

                    return;
                }
            }

            Trace.TraceInformation("Didn't found any suitable Sql connection string!");

            throw new HttpException(
                        540,
                        @"
Add default connection string:

    <connectionStrings>
        <add name=""SqlQuery"" connectionString=""{ConnectionString}"" providerName=""System.Data.SqlClient"" />
    </connectionStrings>

Or SqlQueryConnectionSection section:

<configuration>
    <configSections>
        <section name=""sqlQueryConnectionSection"" type=""Jig.Data.SqlClient.Configuration.SqlQueryConnectionSection, Jig.Data.SqlQuery""/>
    </configSections>
    <sqlQueryConnectionSection xmlns=""urn:Jig.Data.SqlClient"">
        <sqlQueryConnectionSettings connectionStringName=""{SqlQueryConnectionName}""/>
    </sqlQueryConnectionSection>
    <connectionStrings>
        <add name=""{SqlQueryConnectionName}"" connectionString=""{ConnectionString}"" providerName=""System.Data.SqlClient"" />
    </connectionStrings>
</configuration>

Or will use first connection string:
    <connectionStrings>
        <add name=""{Custom}"" connectionString=""{ConnectionString}"" providerName=""System.Data.SqlClient"" />
    </connectionStrings>

                ");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the connection string name.
        /// </summary>
        [NotNull]
        public string ConnectionStringName { get; private set; }

        /// <summary>
        /// Gets the connection string settings.
        /// </summary>
        /// <value>
        /// The connection string settings.
        /// </value>
        [NotNull]
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Sets the SQL connection.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="connectionString">The connection string.</param>
        public void SetSqlConnection([NotNull]string name, [NotNull] string connectionString)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new HttpException(540, "The connection string name cannot be null!");
            }

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new HttpException(540, "The connection string cannot be null!");
            }

            lock (SyncRoot)
            {
                this.ConnectionStringName = name;
                this.ConnectionString = connectionString;
            }
        }

        #endregion
    }
}