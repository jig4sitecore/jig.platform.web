﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.Linq;
    using System.Runtime.Caching;
    using System.Text;

    /// <summary>
    /// The sql query cache.
    /// </summary>
    public sealed class SqlQueryCache : IDisposable
    {
        /// <summary>
        /// The cache instance.
        /// </summary>
        internal readonly MemoryCache CacheInstance = new MemoryCache("SqlQuery");

        /// <summary>
        /// Inserts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <returns>The cache instance</returns>
        [NotNull]
        public SqlQueryCache Insert([NotNull] string key, [NotNull] object value, SqlCachingTime cachingTime = SqlCachingTime.Normal)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (value != null && !string.IsNullOrWhiteSpace(key) && cachingTime > SqlCachingTime.None)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.CacheInstance.Set(key, value, DateTimeOffset.Now.AddMinutes((double)cachingTime));
            }

            return this;
        }

        /// <summary>
        /// Inserts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <returns>The cache instance</returns>
        /// <remarks>Doesn't cache empty collections</remarks>
        [NotNull]
        public SqlQueryCache Insert([NotNull] string key, [NotNull] ICollection<object> collection, SqlCachingTime cachingTime = SqlCachingTime.Normal)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Count > 0 && !string.IsNullOrWhiteSpace(key) && cachingTime > SqlCachingTime.None)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.CacheInstance.Set(key, collection, DateTimeOffset.Now.AddMinutes((double)cachingTime));
            }

            return this;
        }

        /// <summary>
        /// Inserts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <returns>The cache instance</returns>
        /// <remarks>Doesn't cache empty collections</remarks>
        [NotNull]
        public SqlQueryCache Insert([NotNull] string key, [NotNull] object[] collection, SqlCachingTime cachingTime = SqlCachingTime.Normal)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Length > 0 && !string.IsNullOrWhiteSpace(key) && cachingTime > SqlCachingTime.None)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.CacheInstance.Set(key, collection, DateTimeOffset.Now.AddMinutes((double)cachingTime));
            }

            return this;
        }

        /// <summary>
        /// Inserts the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="collection">The collection.</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <returns>The cache instance</returns>
        /// <remarks>Doesn't cache empty collections</remarks>
        [NotNull]
        public SqlQueryCache Insert([NotNull] string key, [NotNull] IEnumerable<object> collection, SqlCachingTime cachingTime = SqlCachingTime.Normal)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Any() && !string.IsNullOrWhiteSpace(key) && cachingTime > SqlCachingTime.None)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this.CacheInstance.Set(key, collection, DateTimeOffset.Now.AddMinutes((double)cachingTime));
            }

            return this;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="key">The key.</param>
        /// <returns>The cache instance</returns>
        [NotNull]
        public TObject Get<TObject>([NotNull] string key) where TObject : class
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                var obj = this.CacheInstance.Get(key);

                return obj as TObject;
            }

            return default(TObject);
        }

        /// <summary>
        /// The clear.
        /// </summary>
        /// <returns>
        /// The SqlQueryCache.
        /// </returns>
        [NotNull]
        public SqlQueryCache Clear()
        {
            var keys = this.GetCacheKeys();
            foreach (var key in keys)
            {
                this.CacheInstance.Remove(key);
            }

            return this;
        }

        /// <summary>
        /// The clear.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// The SqlQueryCache.
        /// </returns>
        [NotNull]
        public SqlQueryCache Clear([NotNull] Type type)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var hash = type.FullName.ToString(CultureInfo.InvariantCulture);

                this.RemoveCacheItemsByPartialKey(hash);
            }

            return this;
        }

        /// <summary>
        /// The get cache keys.
        /// </summary>
        /// <returns>
        /// The  collection of cache keys.
        /// </returns>
        [NotNull]
        public IEnumerable<string> GetCacheKeys()
        {
            var keys = this.CacheInstance.Select(x => x.Key).ToArray();
            return keys;
        }

        /// <summary>
        /// The remove.
        /// </summary>
        /// <param name="sql">The sql.</param>
        /// <returns>
        /// The SqlQueryCache.
        /// </returns>
        [NotNull]
        public SqlQueryCache Remove([NotNull] string sql)
        {
            if (string.IsNullOrWhiteSpace(sql))
            {
                var hash = sql.ConvertAsHashCode().ToString(CultureInfo.InvariantCulture);

                this.RemoveCacheItemsByPartialKey(hash);
            }

            return this;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (this.CacheInstance != null)
            {
                this.CacheInstance.Dispose();
            }
        }

        /// <summary>
        /// Generates the cache key.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="executingMethodPrefix">The executing method prefix.</param>
        /// <param name="sql">The SQL statement or stored procedure</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>
        /// The Cache key based on Sql command and parameters
        /// </returns>
        [NotNull]
        internal static string GenerateCacheKey<TObject>([NotNull] string executingMethodPrefix, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            Type type = typeof(TObject);
            var builder = new StringBuilder(64);

            if (parameters != null && parameters.Length > 0)
            {
                // Ensure that sequence of SqlParameters would not have influence
                var sorted = parameters.OrderBy(n => n.ParameterName, StringComparer.OrdinalIgnoreCase);

                foreach (SqlParameter item in sorted)
                {
                    builder.Append(item.ParameterName).Append(':').Append(item.Value).Append('|');
                }
            }

            // Hash code will make key shorter
            return string.Concat(executingMethodPrefix, '#', type.FullName, '#', sql.ConvertAsHashCode(), '#', builder.ToString());
        }

        /// <summary>
        /// Removes the cache items by partial key.
        /// </summary>
        /// <param name="hash">The hash.</param>
        internal void RemoveCacheItemsByPartialKey([NotNull] string hash)
        {
            var keys = (from item in this.CacheInstance where item.Key.IndexOf(hash, StringComparison.InvariantCultureIgnoreCase) > 0 select item.Key).ToArray();
            foreach (var key in keys)
            {
                this.CacheInstance.Remove(key);
            }
        }
    }
}