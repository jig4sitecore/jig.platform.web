﻿namespace Jig.Web.Data.SqlClient
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        ///     Find All records from the selected table. Command Type equals CommandType.StoredProcedure.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="sqlStoredProcedureName">The name of a stored procedure</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IList<TObject> IList<TObject>([NotNull] string sqlStoredProcedureName, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return IList<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlStoredProcedureName, cacheTime, parameters);
        }

        /// <summary>
        /// Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IList<TObject> IList<TObject>(CommandType commandType, [NotNull] string sql, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return IList<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, cacheTime, parameters);
        }

        /// <summary>
        /// Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IList<TObject> IList<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            string cacheKey = SqlQueryCache.GenerateCacheKey<TObject>("SqlQuery.IList<TObject>", sql, parameters);

            var list = Cache.Get<IList<TObject>>(cacheKey);

            if (list == null)
            {
                list = IList<TObject>(connectionString, commandType, sql, parameters);

                Cache.Insert(cacheKey, list, cachingTime);
            }

            return list;
        }

        /* ReSharper restore InconsistentNaming */
    }
}
