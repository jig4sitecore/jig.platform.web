﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Select all records from the Sql table. NO caching involved. Command Type equals CommandType.StoredProcedure.
        /// </summary>  
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>
        ///     The returns SQL DataView.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="ASP.NET C# Page" lang="C#">
        ///     // Get DataView                
        ///     DataView dataView = SqlQuery.ExecuteReader("Sql-StoredProcedure-Name");
        ///     // Do something 
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataViewDemo()
        /// {
        ///     // Get DataView                
        ///     DataView dataView = SqlQuery.ExecuteReader("Sql-StoredProcedure-Name");
        ///     // In the real life always check if dataView is not null 
        ///     return dataView == null ? string.Empty : dataView.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static DataView ExecuteReader([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReader(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Select all records from the Sql table. NO caching involved.
        /// </summary>  
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>
        ///     The returns SQL DataView.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="ASP.NET C# Page" lang="C#">
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     // Get DataView                
        ///     DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql);
        ///     // Do something 
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataViewDemo()
        /// {
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     // Get DataView                
        ///     DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql);
        ///     // In the real life always check if dataView is not null 
        ///     return dataView == null ? string.Empty : dataView.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "This is impossible in this scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static DataView ExecuteReader(CommandType type, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReader(SqlQueryConnection.ConnectionString, type, sql, parameters);
        }

        /// <summary>
        /// Select all records from the Sql table. NO caching involved.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>The returns SQL DataView.</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// const string Sql = @"
        /// SELECT
        /// [ProductID]
        /// ,[ProductName]
        /// FROM
        /// [Current Product List]
        /// ";
        /// // Get DataView
        /// DataView dataView = SqlQuery.ExecuteReader(connectionString, CommandType.Text, Sql);
        /// // Do something
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataViewDemo()
        /// {
        ///     const string Sql = @"
        ///                         SELECT
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM
        ///                             [Current Product List]
        ///                         ";
        ///     // Get DataView
        ///     DataView dataView = SqlQuery.ExecuteReader(connectionStringName, CommandType.Text, Sql);
        ///     // In the real life always check if dataView is not null
        ///     return dataView == null ? string.Empty : dataView.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "This is impossible in this scope"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static DataView ExecuteReader([NotNull] string connectionString, CommandType type, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            // Initialize SQL connection
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);

                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = type;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();
                    using (SqlDataReader reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var dataTable = new DataTable("SqlDataTable");
                        dataTable.Load(reader);

                        return dataTable.DefaultView;
                    }
                }
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Dispose();
                }
            }
        }
    }
}
