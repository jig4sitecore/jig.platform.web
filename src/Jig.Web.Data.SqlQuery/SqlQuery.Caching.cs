﻿namespace Jig.Web.Data.SqlClient
{
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        /// The Sql Query Cache
        /// </summary>
        public static readonly SqlQueryCache Cache = new SqlQueryCache();
    }
}
