﻿namespace Jig.Web.Data.SqlClient
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        ///     Find All records from the selected Sql Object.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="sqlProcedure">The name of a stored procedure.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns HashSet of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static ICollection<TObject> ICollection<TObject>([NotNull] string sqlProcedure, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return ICollection<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, cacheTime, parameters);
        }

        /// <summary>
        /// Find All records from the selected Sql Object.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns HashSet of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static ICollection<TObject> ICollection<TObject>(CommandType commandType, [NotNull] string sql, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return ICollection<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, cacheTime, parameters);
        }

        /// <summary>
        /// Find All records from the selected Sql Object.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The cache time.</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns HashSet of TObject's from selected SQL table.
        /// </returns>
        /// <remarks>Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library!</remarks>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static ICollection<TObject> ICollection<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            string cacheKey = SqlQueryCache.GenerateCacheKey<TObject>("SqlQuery.ICollection<TObject>", sql, parameters);

            var hashSet = Cache.Get<ICollection<TObject>>(cacheKey);

            if (hashSet == null)
            {
                hashSet = ICollection<TObject>(connectionString, commandType, sql, parameters);

                Cache.Insert(cacheKey, hashSet, cachingTime);
            }

            return hashSet;
        }

        /* ReSharper restore InconsistentNaming */
    }
}
