﻿namespace Jig.Web.Data.SqlClient
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        ///     Find All records from the selected Sql table and add all first column  NOT NULL values to the list. Command Type equals CommandType.StoredProcedure.
        /// </summary>
        /// <typeparam name="TObject">The generic and primitive object types like int, string and etc</typeparam>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IEnumerable<TObject> IEnumerableByFirstColumn<TObject>([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters)
        {
            return IListByFirstColumn<TObject>(CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Find All records from the selected Sql table and add all first column  NOT NULL values to the list. 
        /// </summary>
        /// <typeparam name="TObject">The generic and primitive object types like int, string and etc</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns array of TObject's from selected SQL table.</returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IEnumerable<TObject> IEnumerableByFirstColumn<TObject>(CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            return IListByFirstColumn<TObject>(commandType, sql, parameters);
        }

        /* ReSharper restore InconsistentNaming */
    }
}
