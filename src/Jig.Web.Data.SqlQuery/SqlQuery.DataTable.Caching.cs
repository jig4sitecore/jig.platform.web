﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <param name="parameters">Sql Parameter array.</param>
        /// <returns>The returns SQL  DataTable.</returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static DataTable ExecuteReaderAsDataTable([NotNull] string sqlProcedure, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReaderAsDataTable(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, cachingTime, parameters);
        }

        /// <summary>
        /// Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="commandType">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <param name="parameters">Sql Parameter array.</param>
        /// <returns>The returns SQL DataTable.</returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="ASP.NET C# Page" lang="C#">
        /// const string Sql = @"
        /// SELECT
        ///     [ProductID]
        ///     ,[ProductName]
        /// FROM
        ///     [Current Product List]
        /// ";
        ///     // Get DataTable
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, Sql, CacheTime.Normal);
        ///     // Do something
        /// }
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///         SELECT
        ///             [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///             [Current Product List]
        ///     ";
        ///     // Get DataTable
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, Sql, CacheTime.Normal);
        ///     // In the real life always check if dataTable is not null
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static DataTable ExecuteReaderAsDataTable(CommandType commandType, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReaderAsDataTable(SqlQueryConnection.ConnectionString, commandType, sql, cachingTime, parameters);
        }

        /// <summary>
        /// Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The caching time.</param>
        /// <param name="parameters">Sql Parameter array.</param>
        /// <returns>The returns SQL DataTable.</returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="ASP.NET C# Page" lang="C#">
        /// const string Sql = @"
        ///     SELECT
        ///          [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        ///     ";
        ///     // Get DataTable
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionString, CommandType.Text, Sql, CacheTime.Normal);
        ///     // Do something
        /// </code>
        /// <code title="C# source code in System.Web.Services.WebService file" lang="C#">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///         SELECT
        ///              [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///              [Current Product List]
        ///     ";
        ///     // Get DataTable
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionStringName, CommandType.Text, Sql, CacheTime.Normal);
        ///     // In the real life always check if dataTable is not null
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static DataTable ExecuteReaderAsDataTable([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters)
        {
            string cacheKey = SqlQueryCache.GenerateCacheKey<DataTable>("SqlQuery.ExecuteReaderAsDataTable", sql, parameters);

            var dataTable = Cache.Get<DataTable>(cacheKey);

            if (dataTable == null)
            {
                dataTable = ExecuteReaderAsDataTable(connectionString, commandType, sql, parameters);

                Cache.Insert(cacheKey, dataTable, cachingTime);
            }

            return dataTable;
        }
    }
}
