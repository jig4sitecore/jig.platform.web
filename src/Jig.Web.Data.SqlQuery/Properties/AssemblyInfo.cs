﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Jig.Web.Data.SqlQuery")]
[assembly: AssemblyDescription("MS SQL query and access library")]
[assembly: Guid("b9944bb0-005f-4e1c-b87a-5d069043d2e1")]

[assembly: AssemblyCompany("Jig Team")]
[assembly: AssemblyProduct("Jig Framework")]
[assembly: AssemblyCopyright("Copyright © Jig Web Team")]
[assembly: AssemblyTrademark("Jig Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyFileVersion("2.0.0")]
[assembly: AssemblyVersion("2.0.0")]