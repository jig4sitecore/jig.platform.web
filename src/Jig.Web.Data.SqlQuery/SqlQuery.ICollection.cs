﻿namespace Jig.Web.Data.SqlClient
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        ///     Find All records from the selected table. Command Type equals CommandType.StoredProcedure.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns HashSet of TObject's from selected SQL table.</returns>
        /// <remarks>Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library!</remarks>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1644:DocumentationHeadersMustNotContainBlankLines", Justification = "Code sample")]
        public static ICollection<TObject> ICollection<TObject>([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return ICollection<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns HashSet of TObject's from selected SQL table.</returns>
        /// <remarks>Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library!</remarks>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1644:DocumentationHeadersMustNotContainBlankLines", Justification = "Code sample")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static ICollection<TObject> ICollection<TObject>(CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return ICollection<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, parameters);
        }

        /// <summary>
        /// Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns HashSet of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1644:DocumentationHeadersMustNotContainBlankLines", Justification = "Code sample")]
        public static ICollection<TObject> ICollection<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            // Initialize SQL connection
            SqlConnection sqlConnection = null;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = commandType;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();

                    using (SqlDataReader reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        DynamicSqlDataReader<TObject> builder = DynamicSqlDataReader<TObject>.CreateDynamicMethod(reader);

                        var list = new HashSet<TObject>();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TObject entity = builder.Build(reader);
                                list.Add(entity);
                            }
                        }

                        return list;
                    }
                }
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Dispose();
                }
            }
        }

        /* ReSharper restore InconsistentNaming */
    }
}
