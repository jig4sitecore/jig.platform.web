﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;
    using System.Web;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Find all Sql record by primary key
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="sqlProcedure">The sql procedure name.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="primaryKey">The primary and/or foreign key(s)</param>
        /// <returns>Returns a instance of the generic object</returns>
        /// <remarks>Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library!</remarks>
        [NotNull]
        public static TObject ItemByKey<TObject>([NotNull] string sqlProcedure, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] primaryKey) where TObject : class
        {
            return ItemByKey<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, cacheTime, primaryKey);
        }

        /// <summary>
        ///     Find all Sql record by primary key
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="primaryKey">The primary or/and foreign key(s)</param>
        /// <returns>Returns a instance of the generic object</returns>
        [NotNull]
        public static TObject ItemByKey<TObject>(CommandType commandType, [NotNull] string sql, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] primaryKey) where TObject : class
        {
            return ItemByKey<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, cacheTime, primaryKey);
        }

        /// <summary>
        /// Find all Sql record by primary key
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The cache time.</param>
        /// <param name="primaryKey">The primary or/and foreign key(s)</param>
        /// <returns>
        /// Returns a instance of the generic object
        /// </returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# class file">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// </code>
        /// <code title="ASP.NET C# file">
        /// const string Sql = @"
        ///     SELECT
        ///         [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        ///     WHERE
        ///         ProductID <![CDATA[=]]> @ProductID";
        /// // Retrieve object where ProductID = 1
        /// var item = SqlQuery.ItemByKey<![CDATA[<Product>]]>(connectionStringName, CommandType.Text, Sql, CacheTime.Normal, new SqlParameter("@ProductID", 1));
        /// // Do something
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1644:DocumentationHeadersMustNotContainBlankLines", Justification = "Code sample")]
        public static TObject ItemByKey<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] primaryKey) where TObject : class
        {
            string cacheKey = SqlQueryCache.GenerateCacheKey<TObject>("SqlQuery.ItemByKey<TObject>", sql, primaryKey);

            var entity = HttpRuntime.Cache[cacheKey] as TObject;

            if (entity == null)
            {
                entity = ItemByKey<TObject>(connectionString, commandType, sql, primaryKey);

                if (entity != null)
                {
                    Cache.Insert(cacheKey, entity, cachingTime);
                }
            }

            return entity;
        }
    }
}
