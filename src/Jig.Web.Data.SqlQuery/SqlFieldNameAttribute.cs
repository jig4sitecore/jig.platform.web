﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Web;

    /// <summary>
    /// The Sql data set field name attribute.
    /// </summary>
    /// <example>
    /// <code>
    /// Renaming on SQL view or stored procedure (without SqlFieldName attribute on property) :
    ///    SELECT
    ///        Brand_Num AS BrandNumber
    ///       ,Brand_Name AS BrandName
    ///       ,Brand_Group AS BrandGroup
    ///       ,Brand_Logo AS BrandLogo
    ///       ,DOT_Reg_URL AS DOTRegularUrl
    ///    FROM
    ///         [dbo].EXP_TIREBRAND
    /// Using SqlFieldName attributes:
    ///    SELECT
    ///        Brand_Num
    ///       ,Brand_Name
    ///       ,Brand_Group
    ///       ,Brand_Logo
    ///       ,DOT_Reg_URL
    ///    FROM
    ///         [dbo].EXP_TIREBRAND
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlFieldNameAttribute : Attribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlFieldNameAttribute"/> class. 
        /// Initializes a new instance of the SqlFieldNameAttribute"/&gt; class.
        /// </summary>
        /// <param name="sqlFieldName">
        /// Name of the SQL field.
        /// </param>
        public SqlFieldNameAttribute([NotNull] string sqlFieldName)
        {
            if (string.IsNullOrWhiteSpace(sqlFieldName))
            {
                throw new HttpException("The Sql field name can't be null!");
            }

            this.SqlFieldName = sqlFieldName;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="SqlFieldNameAttribute"/> class from being created.
        /// </summary>
        // ReSharper disable UnusedMember.Local
        private SqlFieldNameAttribute()
        // ReSharper restore UnusedMember.Local
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the name of the SQL field.
        /// </summary>
        /// <value>
        /// The name of the SQL field.
        /// </value>
        [NotNull]
        public string SqlFieldName { get; private set; }

        #endregion
    }
}
