﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Executes a Transact-SQL statement against the connection and returns the number of rows affected. 
        /// Use to change the data in a database by executing UPDATE, INSERT, or DELETE statements.
        /// Command Type equals CommandType.StoredProcedure.
        /// </summary>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Represents a parameter array to a SqlCommand</param>
        /// <returns>The number of rows affected</returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# Example one" lang="C#">
        /// SqlQuery.ExecuteNonQuery("[Extranet].[ArchiveEvents]");
        /// </code>
        /// <code title="C# Example two" lang="C#">                  
        /// SqlQuery.ExecuteNonQuery(
        ///                   "[Extranet].[EventsInsertUpdateByKey]"
        ///                 , new SqlParameter("@EventKey", calendarEvent.EventKey)
        ///                 , new SqlParameter("@Title", calendarEvent.Title)
        ///                 , new SqlParameter("@DateFrom", calendarEvent.DateFrom)
        ///                 , new SqlParameter("@DateTo", calendarEvent.DateTo)
        ///                 , new SqlParameter("@Summary", calendarEvent.Summary)
        ///                 , new SqlParameter("@Story", calendarEvent.Story)
        ///                 , new SqlParameter("@EventCategoryKey", calendarEvent.EventCategoryKey));
        /// </code>
        /// </example>
        public static int ExecuteNonQuery([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Executes a Transact-SQL statement against the connection and returns the number of rows affected. 
        /// Use to change the data in a database by executing UPDATE, INSERT, or DELETE statements.
        /// </summary>
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Represents a parameter array to a SqlCommand</param>
        /// <returns>The number of rows affected</returns>
        /// <example>View code: <br />
        /// <code title="C# Example one" lang="C#">
        /// SqlQuery.ExecuteNonQuery(CommandType.StoredProcedure , "[Extranet].[ArchiveEvents]");
        /// </code>
        /// <code lang="C#" title="C# Example two">
        /// SqlQuery.ExecuteNonQuery(
        ///                 CommandType.StoredProcedure
        ///                 , "[Extranet].[EventsInsertUpdateByKey]"
        ///                 , new SqlParameter("@EventKey", calendarEvent.EventKey)
        ///                 , new SqlParameter("@Title", calendarEvent.Title)
        ///                 , new SqlParameter("@DateFrom", calendarEvent.DateFrom)
        ///                 , new SqlParameter("@DateTo", calendarEvent.DateTo)
        ///                 , new SqlParameter("@Summary", calendarEvent.Summary)
        ///                 , new SqlParameter("@Story", calendarEvent.Story)
        ///                 , new SqlParameter("@EventCategoryKey", calendarEvent.EventCategoryKey));
        /// </code>
        /// </example>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static int ExecuteNonQuery(CommandType type, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteNonQuery(SqlQueryConnection.ConnectionString, type, sql, parameters);
        }

        /// <summary>
        /// Executes a Transact-SQL statement against the connection and returns the number of rows affected.
        /// Use to change the data in a database by executing UPDATE, INSERT, or DELETE statements.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Represents a parameter array to a SqlCommand</param>
        /// <returns>The number of rows affected</returns>
        /// <example>View code: <br />
        /// <code title="C# Example one" lang="C#">
        /// SqlQuery.ExecuteNonQuery(connectionStringName, CommandType.StoredProcedure, "[Extranet].[ArchiveEvents]");
        /// </code>
        /// <code lang="C#" title="Example two">
        /// SqlQuery.ExecuteNonQuery(
        ///   connectionStringName
        /// , CommandType.StoredProcedure
        /// , "[Extranet].[EventsInsertUpdateByKey]"
        /// , new SqlParameter("@EventKey", calendarEvent.EventKey)
        /// , new SqlParameter("@Title", calendarEvent.Title)
        /// , new SqlParameter("@DateFrom", calendarEvent.DateFrom)
        /// , new SqlParameter("@DateTo", calendarEvent.DateTo)
        /// , new SqlParameter("@Summary", calendarEvent.Summary)
        /// , new SqlParameter("@Story", calendarEvent.Story)
        /// , new SqlParameter("@EventCategoryKey", calendarEvent.EventCategoryKey));
        /// </code>
        /// </example>
        [SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static int ExecuteNonQuery([NotNull] string connectionString, CommandType type, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            // Initialize SQL connection
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = type;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();

                    return sqlCommand.ExecuteNonQuery();
                }
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Dispose();
                }
            }
        }
    }
}
