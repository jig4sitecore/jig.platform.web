﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data.SqlClient;

    /// <summary>
    /// Web Site Default Sql Server Connections wrapper<br/>
    ///     <b>IMPORTANT:</b>
    ///     <br/>
    /// Add connection string named <b>"SqlQueryConnection"</b> to the web.config file in order use Data Access Library.
    /// </summary>
    /// <remarks>
    /// The library uses Reflection.Emit and required ReflectionPermissions enabled. Some host disabled that functionality like GoDaddy for some hosting plans
    /// </remarks>
    /// <example>View code: <br />
    /// <code title="Web.config" lang="xml">
    /// <![CDATA[
    /// <connectionStrings>
    /// <remove name="LocalSqlServer"/>
    /// <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
    /// </connectionStrings>
    /// ]]>
    /// </code>
    /// </example> 
    public static partial class SqlQueryConnection
    {
        /// <summary>
        /// The settings
        /// </summary>
        public static readonly Settings Settings = new Settings();

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>
        /// The connection string.
        /// </value>
        [NotNull]
        public static string ConnectionString
        {
            get
            {
                return Settings.ConnectionString;
            }
        }

        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>
        /// The name of the connection string.
        /// </value>
        [NotNull]
        public static string ConnectionStringName
        {
            get
            {
                return Settings.ConnectionStringName;
            }
        }

        /// <summary>
        /// Gets the default SQL connection.
        /// </summary>
        /// <returns>Default sql connection</returns>
        [NotNull]
        public static SqlConnection GetDefaultSqlConnection()
        {
            return new SqlConnection(Settings.ConnectionString);
        }

        /// <summary>
        /// Sets the SQL connection.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="connectionString">The connection string.</param>
        public static void SetSqlConnection([NotNull] string name, [NotNull] string connectionString)
        {
            SqlQueryConnection.Settings.SetSqlConnection(name, connectionString);
        }
    }
}
