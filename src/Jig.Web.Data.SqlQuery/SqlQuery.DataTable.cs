﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Select all records from the Sql table. NO caching involved. Command Type equals CommandType.StoredProcedure.
        /// </summary>  
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        ///     The returns SQL DataTable.
        /// </returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# File With SqlParameters" lang="C#">
        ///    /* Stored Procedure = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                         WHERE
        ///                             ProductID <![CDATA[<=]]> @ProductID
        ///                     ";
        ///     */
        ///     // Get DataTable where ID less then 11                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable("Sql-StoredProcedure-Name", new SqlParameter("@ProductID", 10));
        ///     // Do something 
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     /* Stored Procedure = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                         WHERE
        ///                             ProductID <![CDATA[<=]]> @ProductID
        ///                     ";
        ///     */
        ///     // Get DataTable where ID less then 11                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable("Sql-StoredProcedure-Name", new SqlParameter("@ProductID", 10));
        ///     // In the real life always check if dataTable is not null 
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        ///     /* Stored Procedure = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     */
        ///     // Get DataTable                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable("Sql-StoredProcedure-Name");
        ///     // Do something 
        /// }
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     /* Stored Procedure = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     */
        ///     // Get DataTable                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, Sql);
        ///     // In the real life always check if dataTable is not null 
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static DataTable ExecuteReaderAsDataTable([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReaderAsDataTable(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Select all records from the Sql table. NO caching involved.
        /// </summary>  
        /// <param name="commandType">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        ///     The returns SQL DataTable.
        /// </returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# File With SqlParameters" lang="C#">
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                         WHERE
        ///                             ProductID <![CDATA[<=]]> @ProductID
        ///                     ";
        ///     // Get DataTable where ID less then 11                
        ///     using(DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, SqlSql, new SqlParameter("@ProductID", 10)))
        ///     {
        ///         // Do something
        ///     }
        /// }
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                         WHERE
        ///                             ProductID <![CDATA[<=]]> @ProductID
        ///                     ";
        ///     // Get DataTable where ID less then 11                
        ///     using(DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, SqlSql, new SqlParameter("@ProductID", 10)))
        ///     {
        ///         // In the real life always check if dataTable is not null 
        ///         return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        ///     }
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     // Get DataTable                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, Sql);
        ///     // Do something 
        /// }
        /// </code>
        /// <code title="C# source code in System.Web.Services.WebService file" lang="C#">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     // Get DataTable                
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(CommandType.Text, Sql);
        ///     // In the real life always check if dataTable is not null 
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "This is impossible in this scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static DataTable ExecuteReaderAsDataTable(CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReaderAsDataTable(SqlQueryConnection.ConnectionString, commandType, sql, parameters);
        }

        /// <summary>
        /// Select all records from the Sql table. NO caching involved.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns SQL DataTable.</returns>
        /// <example>View code: <br />
        /// <code title="C# file With SqlParameters" lang="C#">
        /// const string Sql = @"
        ///     SELECT
        ///         [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        ///          WHERE
        ///          ProductID <![CDATA[<=]]> @ProductID
        /// ";
        /// // Get DataTable where ID less then 11
        /// using(DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionStringName, CommandType.Text, SqlSql, new SqlParameter("@ProductID", 10)))
        /// {
        ///     // Do something
        /// }
        /// </code>
        /// <code title="C# source code in System.Web.Services.WebService file" lang="C#">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///         SELECT
        ///             [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///             [Current Product List]
        ///         WHERE
        ///             ProductID <![CDATA[<=]]> @ProductID
        /// ";
        /// // Get DataTable where ID less then 11
        /// using(DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionStringName, CommandType.Text, SqlSql, new SqlParameter("@ProductID", 10)))
        /// {
        ///     // In the real life always check if dataTable is not null
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        /// const string Sql = @"
        /// SELECT
        /// [ProductID]
        /// ,[ProductName]
        /// FROM
        /// [Current Product List]
        /// ";
        /// // Get DataTable
        /// DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionStringName, CommandType.Text, Sql);
        /// // Do something
        /// }
        /// -------------------------------------------------------------------
        /// C# source code in System.Web.Services.WebService file
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataTableDemo()
        /// {
        ///     const string Sql = @"
        ///         SELECT
        ///             [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///             [Current Product List]
        ///     ";
        ///     // Get DataTable
        ///     DataTable dataTable = SqlQuery.ExecuteReaderAsDataTable(connectionStringName, CommandType.Text, Sql);
        ///     // In the real life always check if dataTable is not null
        ///     return dataTable == null ? string.Empty : dataTable.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "This is impossible in this scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static DataTable ExecuteReaderAsDataTable([NotNull] string connectionString, CommandType type, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            // Initialize SQL connection
            SqlConnection sqlConnection = null;
            try
            {
                sqlConnection = new SqlConnection(connectionString);
                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = type;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();
                    using (SqlDataReader reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var dataTable = new DataTable("SqlDataTable");
                        dataTable.Load(reader);

                        return dataTable;
                    }
                }
            }
            finally
            {
                if (sqlConnection != null)
                {
                    sqlConnection.Dispose();
                }
            }
        }
    }
}
