﻿namespace Jig.Web.Data.SqlClient
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /// <summary>
        ///     Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="sqlProcedureName">The sql procedure name.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>
        ///     The returns SQL  DataView or throws Sql Exception in case of SQL failure.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static DataView ExecuteReader([NotNull] string sqlProcedureName, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReader(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedureName, cacheTime, parameters);
        }

        /// <summary>
        /// Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="commandType">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cacheTime">The caching time</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>
        /// The returns SQL  DataView or throws Sql Exception in case of SQL failure.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="C# File With SqlParameters" lang="C#">
        ///     const string Sql = @"
        ///         SELECT
        ///              [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///             [Current Product List]
        ///         WHERE
        ///             ProductID <![CDATA[<=]]> @ProductID
        ///     ";
        ///     // Get DataView
        ///     DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql, CacheTime.Normal, new SqlParameter("@ProductID" , 1));
        ///     // Do something
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataViewDemo()
        /// {
        ///     const string Sql = @"
        ///         SELECT
        ///             [ProductID]
        ///             ,[ProductName]
        ///         FROM
        ///             [Current Product List]
        ///         WHERE
        ///             ProductID <![CDATA[<=]]> @ProductID
        ///     ";
        ///     // Get DataView
        ///     DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql, CacheTime.Normal, new SqlParameter("@ProductID" , 1));
        ///     // In the real life always check if dataView is not null
        ///     return dataView == null ? string.Empty : dataView.SerializeToJson();
        /// }
        /// </code>
        /// <code lang="C#" title="Without SqlParameters">
        /// const string Sql = @"
        ///     SELECT
        ///         [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///     [Current Product List]
        ///                    ";
        /// // Get DataView
        /// DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql, CacheTime.Normal, new SqlParameter("@ProductID", 10));
        /// // Do something
        /// </code>
        /// <code lang="C#" title="C# source code in System.Web.Services.WebService file">
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string AjaxDataViewDemo()
        /// {
        /// const string Sql = @"
        ///     SELECT
        ///          [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        /// ";
        ///     // Get DataView where ID less then 11
        ///     DataView dataView = SqlQuery.ExecuteReader(CommandType.Text, Sql, CacheTime.Normal, new SqlParameter("@ProductID", 10));
        ///     // In the real life always check if dataView is not null
        ///     return dataView == null ? string.Empty : dataView.SerializeToJson();
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static DataView ExecuteReader(CommandType commandType, [NotNull] string sql, SqlCachingTime cacheTime, [NotNull] params SqlParameter[] parameters)
        {
            return ExecuteReader(SqlQueryConnection.ConnectionString, commandType, sql, cacheTime, parameters);
        }

        /// <summary>
        /// Select all records from the SQL table. CACHING INVOLVED.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="type">Specifies how a command string is interpreted. Command Type (CommandType.StoredProcedure OR CommandType.Text )</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="cachingTime">The caching time</param>
        /// <param name="parameters">Represents parameters to a SqlCommand</param>
        /// <returns>The returns SQL  DataView or throws Sql Exception in case of SQL failure.</returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static DataView ExecuteReader([NotNull] string connectionString, CommandType type, [NotNull] string sql, SqlCachingTime cachingTime, [NotNull] params SqlParameter[] parameters)
        {
            string cacheKey = SqlQueryCache.GenerateCacheKey<DataView>("SqlQuery.ExecuteReader", sql, parameters);

            var dataTable = Cache.Get<DataView>(cacheKey);

            if (dataTable == null)
            {
                dataTable = ExecuteReader(connectionString, type, sql, parameters);

                Cache.Insert(cacheKey, dataTable, cachingTime);
            }

            return dataTable;
        }
    }
}
