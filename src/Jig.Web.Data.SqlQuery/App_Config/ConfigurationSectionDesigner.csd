﻿<?xml version="1.0" encoding="utf-8"?>
<configurationSectionModel xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="816fceed-1511-43e0-8755-0e2f01612036" namespace="Jig.Web.Data.SqlClient.Configuration" xmlSchemaNamespace="urn:Jig.Web.Data.SqlClient" xmlns="http://schemas.microsoft.com/dsltools/ConfigurationSectionDesigner">
  <typeDefinitions>
    <externalType name="String" namespace="System" />
    <externalType name="Boolean" namespace="System" />
    <externalType name="Int32" namespace="System" />
    <externalType name="Int64" namespace="System" />
    <externalType name="Single" namespace="System" />
    <externalType name="Double" namespace="System" />
    <externalType name="DateTime" namespace="System" />
    <externalType name="TimeSpan" namespace="System" />
  </typeDefinitions>
  <configurationElements>
    <configurationElement name="SqlQueryConnectionSettings">
      <attributeProperties>
        <attributeProperty name="ConnectionStringName" isRequired="true" isKey="false" isDefaultCollection="false" xmlName="connectionStringName" isReadOnly="false">
          <type>
            <externalTypeMoniker name="/816fceed-1511-43e0-8755-0e2f01612036/String" />
          </type>
        </attributeProperty>
      </attributeProperties>
    </configurationElement>
    <configurationSection name="SqlQueryConnectionSection" codeGenOptions="Singleton, XmlnsProperty" xmlSectionName="sqlQueryConnectionSection">
      <elementProperties>
        <elementProperty name="SqlQueryConnectionSettings" isRequired="true" isKey="true" isDefaultCollection="false" xmlName="sqlQueryConnectionSettings" isReadOnly="false">
          <type>
            <configurationElementMoniker name="/816fceed-1511-43e0-8755-0e2f01612036/SqlQueryConnectionSettings" />
          </type>
        </elementProperty>
      </elementProperties>
    </configurationSection>
  </configurationElements>
  <propertyValidators>
    <validators />
  </propertyValidators>
</configurationSectionModel>