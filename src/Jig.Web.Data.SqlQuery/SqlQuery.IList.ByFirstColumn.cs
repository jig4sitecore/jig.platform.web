﻿namespace Jig.Web.Data.SqlClient
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        ///     Find All records from the selected Sql table and add all first column  NOT NULL values to the list. 
        /// </summary>
        /// <typeparam name="TObject">The generic and primitive object types like int, string and etc</typeparam>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns list of TObject's from selected SQL table.</returns>
        /// <example>View code: <br />
        /// </example>
        [NotNull]
        public static IEnumerable<TObject> IListByFirstColumn<TObject>([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters)
        {
            return IListByFirstColumn<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Find All records from the selected Sql table and add all first column  NOT NULL values to the list. 
        /// </summary>
        /// <typeparam name="TObject">The generic and primitive object types like int, string and etc</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns list of TObject's from selected SQL table.</returns>
        /// <example>View code: <br />
        /// </example> 
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static IEnumerable<TObject> IListByFirstColumn<TObject>(CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            return IListByFirstColumn<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, parameters);
        }

        /// <summary>
        /// Find All records from the selected Sql table and add all first column  NOT NULL values to the list.
        /// </summary>
        /// <typeparam name="TObject">The generic and primitive object types like int, string and etc</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns list of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="C# With SqlParameters" lang="C#">
        /// const string Sql = @"
        ///     SELECT
        ///         [EmployeeID]
        ///     FROM
        ///         [Employees]
        ///     WHERE
        ///         EmployeeID <![CDATA[<=]]> @EmployeeID
        ///         ORDER BY EmployeeID ASC
        /// ";
        /// // Get list of employees with id less then 6
        /// var employeeIdList = SqlQuery.IListByFirstColumn<![CDATA[<int>]]>(connectionStringName, CommandType.Text, Sql, new SqlParameter("@EmployeeID", 5));
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        /// const string Sql = @"
        ///     SELECT
        ///         [EmployeeID]
        ///     FROM
        ///         [Employees]
        ///         ORDER BY EmployeeID ASC
        /// ";
        /// var employeeIdList = SqlQuery.IListByFirstColumn<![CDATA[<int>]]>(connectionStringName, CommandType.Text, Sql);
        /// </code>
        /// </example>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static IEnumerable<TObject> IListByFirstColumn<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters)
        {
            // Initialize SQL connection
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = commandType;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();

                    using (var reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                // Add only first column
                                if (reader[0] != DBNull.Value)
                                {
                                    yield return (TObject)reader[0];
                                }
                            }
                        }
                    }
                }
            }
        }

        /* ReSharper restore InconsistentNaming */
    }
}
