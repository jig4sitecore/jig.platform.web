﻿namespace Jig.Web.Data.SqlClient
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class SqlQuery
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        /// Find All records from the selected table. Command Type equals CommandType.StoredProcedure.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="sqlProcedure">The name of a stored procedure</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# Class" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// </code>
        /// <code lang="C#" title="ASPX Page C# with sqlParameters">
        /// // Retrieve all collection where ProductID less then 10
        /// var collection = SqlQuery.IEnumerable<![CDATA[<Product>]]>("Sql-StoredProcedure-Name", new SqlParameter("@ProductID", 10));
        /// // Do some binding with Controls having DataSource
        /// Repeater control = new Repeater();
        /// control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        /// control.DataSource = collection;
        /// control.DataBind();
        /// this.Page.Form.Controls.Add(control);
        /// ........
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br/>", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///         break;
        ///     }
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// .....
        /// // Retrieve all collection
        /// var collection = SqlQuery.IList<![CDATA[<Product>]]>("Sql-StoredProcedure-Name");
        /// // Do some binding with Controls having DataSource
        /// Repeater control = new Repeater();
        /// control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        /// control.DataSource = collection;
        /// control.DataBind();
        /// this.Page.Form.Controls.Add(control);
        /// .....
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br/>", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///             break;
        ///     }
        /// }
        /// </code>
        /// </example>
        [NotNull]
        public static IList<TObject> IList<TObject>([NotNull] string sqlProcedure, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return IList<TObject>(SqlQueryConnection.ConnectionString, CommandType.StoredProcedure, sqlProcedure, parameters);
        }

        /// <summary>
        ///     Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>The returns array of TObject's from selected SQL table.</returns>
        /// <example>View code: <br />
        /// <code lang="xml" title="web.config">
        /// <![CDATA[
        /// /* Add connection string named "SqlQueryConnection" to the web.config file in order use Data Access Library! */
        /// <connectionStrings>
        ///     <remove name="LocalSqlServer"/>
        ///     <add name="SqlQueryConnection" connectionString="Data Source=SERVER_NAME;Initial Catalog=DATABASE_NAME;Persist Security Info=True;User ID=USER_NAME;Password=USER_PASSWORD" providerName="System.Data.SqlClient"/>
        /// </connectionStrings> 
        /// ]]>
        /// </code>
        /// <code title="C# File With SqlParameters" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// .........
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                         WHERE 
        ///                             ProductID <![CDATA[<]]> @ProductID
        ///                     ";
        ///     // Retrieve all collection where ProductID less then 10
        ///     var collection = SqlQuery.IEnumerable<![CDATA[<Product>]]>(CommandType.Text, Sql, new SqlParameter("@ProductID", 10));
        ///     // Do some binding with Controls having DataSource
        ///     Repeater control = new Repeater();
        ///     control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        ///     control.DataSource = collection;
        ///     control.DataBind();
        ///     this.Page.Form.Controls.Add(control);
        /// .........
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br />", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///             break;
        ///     }
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// .........................
        ///     const string Sql = @"
        ///                         SELECT 
        ///                              [ProductID]
        ///                             ,[ProductName]
        ///                         FROM 
        ///                             [Current Product List]
        ///                     ";
        ///     // Retrieve all collection
        ///     var collection = SqlQuery.IList<![CDATA[<Product>]]>(CommandType.Text, Sql);
        ///     // Do some binding with Controls having DataSource
        ///     Repeater control = new Repeater();
        ///     control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        ///     control.DataSource = collection;
        ///     control.DataBind();
        ///     this.Page.Form.Controls.Add(control);
        /// .........................
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br />", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///             break;
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// <pre>
        /// <b>Important:</b>
        ///     For ListControls only use specially designed XListItem<![CDATA[<TObject>]]>
        ///     // OnLoad method
        ///     ListControl control = (ListControl)sender;
        ///     // Id is type of int in SqlTable and Country is typeof( varchar)
        ///     var items = SqlQuery.IEnumerable<![CDATA[<XListItem<int>>]]>(CommandType.Text, "SELECT Id AS Value, Country as Text FROM Countries");
        ///     control.Items.AddRange(items);
        /// </pre>
        /// </remarks>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static IList<TObject> IList<TObject>(CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            return IList<TObject>(SqlQueryConnection.ConnectionString, commandType, sql, parameters);
        }

        /// <summary>
        /// Find All records from the selected table.
        /// </summary>
        /// <typeparam name="TObject">The generic object</typeparam>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="commandType">Specifies how a command string is interpreted.</param>
        /// <param name="sql">The name of a stored procedure or an SQL text command</param>
        /// <param name="parameters">Sql Parameter array</param>
        /// <returns>
        /// The returns array of TObject's from selected SQL table.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="C# File With SqlParameters" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// ...................................
        /// const string Sql = @"
        ///     SELECT
        ///         [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        ///     WHERE
        ///         ProductID <![CDATA[<]]> @ProductID
        /// ";
        /// // Retrieve all collection where ProductID less then 10
        /// var collection = SqlQuery.IEnumerable<![CDATA[<Product>]]>(connectionStringName, CommandType.Text, Sql, new SqlParameter("@ProductID", 10));
        /// // Do some binding with Controls having DataSource
        /// Repeater control = new Repeater();
        /// control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        /// control.DataSource = collection;
        /// control.DataBind();
        /// this.Page.Form.Controls.Add(control);
        /// ..................................
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br/>", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///             break;
        ///     }
        /// }
        /// </code>
        /// <code title="C# File Without SqlParameters" lang="C#">
        /// public class Product
        /// {
        ///     public int ProductID { get; set; }
        ///     public string ProductName { get; set; }
        /// }
        /// .................................
        /// const string Sql = @"
        ///     SELECT
        ///         [ProductID]
        ///         ,[ProductName]
        ///     FROM
        ///         [Current Product List]
        /// ";
        /// // Retrieve all collection
        /// var collection = SqlQuery.IList<![CDATA[<Product>]]>(connectionStringName, CommandType.Text, Sql);
        /// // Do some binding with Controls having DataSource
        /// Repeater control = new Repeater();
        /// control.ItemDataBound += new RepeaterItemEventHandler(DemoRepeaterControl_ItemDataBound);
        /// control.DataSource = collection;
        /// control.DataBind();
        /// this.Page.Form.Controls.Add(control);
        /// .........................................
        /// // Repeater Item Data Bind demo
        /// void DemoRepeaterControl_ItemDataBound(object sender, RepeaterItemEventArgs e)
        /// {
        ///     switch (e.Item.ItemType)
        ///     {
        ///         case ListItemType.AlternatingItem:
        ///         case ListItemType.Item:
        ///             TObjectProductList item = (Product)e.Item.DataItem;
        ///             Literal literal = new Literal();
        ///             literal.Text = string.Format("ProductID - {0}, ProductName - {1}<br/>", item.ProductID, item.ProductName);
        ///             e.Item.Controls.Add(literal);
        ///             break;
        ///         default:
        ///             break;
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <remarks>
        /// <pre>
        /// <b>Important:</b>
        ///     For ListControls only use specially designed XListItem<![CDATA[<TObject>]]>
        ///     // OnLoad method
        ///     ListControl control = (ListControl)sender;
        ///     // Id is type of int in SqlTable and Country is typeof( varchar)
        ///     var items = SqlQuery.IEnumerable<![CDATA[<XListItem<int>>]]>(CommandType.Text, "SELECT Id AS Value, Country as Text FROM Countries");
        ///     control.Items.AddRange(items);
        /// </pre>
        /// </remarks>
        [NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities", Justification = "User must use Sql Stored procedure or sql parameterized command")]
        public static IList<TObject> IList<TObject>([NotNull] string connectionString, CommandType commandType, [NotNull] string sql, [NotNull] params SqlParameter[] parameters) where TObject : class
        {
            // Initialize SQL connection
            using (var sqlConnection = new SqlConnection(connectionString))
            {
                using (var sqlCommand = new SqlCommand(sql, sqlConnection))
                {
                    sqlCommand.CommandType = commandType;

                    if (parameters != null && parameters.Length > 0)
                    {
                        sqlCommand.Parameters.AddRange(parameters);
                    }

                    // Execute Sql statement
                    sqlConnection.Open();

                    using (var reader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        var builder = DynamicSqlDataReader<TObject>.CreateDynamicMethod(reader);

                        var list = new List<TObject>();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                TObject entity = builder.Build(reader);
                                list.Add(entity);
                            }
                        }

                        return list;
                    }
                }
            }
        }

        /* ReSharper restore InconsistentNaming */
    }
}
