// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImplicitUseTargetFlags.cs" company="JetBrains">
//   Copyright (c) JetBrains. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Annotations
{
    using System;

    /// <summary>
    /// Specify what is considered used implicitly when marked
    /// with <see cref="MeansImplicitUseAttribute"/> or <see cref="UsedImplicitlyAttribute"/>
    /// </summary>
    [Flags]
    internal enum ImplicitUseTargetFlags
    {
        /// <summary>
        /// The default
        /// </summary>
        Default = Itself,

        /// <summary>
        /// The itself
        /// </summary>
        Itself = 1,

        /// <summary>Members of entity marked with attribute are considered used</summary>
        Members = 2,

        /// <summary>Entity marked with attribute and all its members considered used</summary>
        WithMembers = Itself | Members
    }
}