﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Option tag
    /// </summary>
    /// <remarks><![CDATA[Represents an option in a <select> element, or a suggestion of a <data list> element.]]></remarks>
    public sealed class Option : XhtmlNonVoidTagBase, IDisabledAttribute, ISelectedAttribute, IValueAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Option"/> class.
        /// </summary>
        /// <param name="values">The values.</param>
        public Option([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Option, values)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the Disabled attribute.
        /// </summary>
        public string Disabled
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Disabled);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Disabled, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected.
        /// </summary>
        /// <value>
        /// The selected.
        /// </value>
        public string Selected
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Selected);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Selected, value);
            }
        }

        /// <summary>
        /// Gets or sets the Value attribute.
        /// </summary>
        public string Value
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Value);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Value, value);
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [Jig.Annotations.NotNull]
        public string Text
        {
            get
            {
                return this.Inner.Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Inner.Value = value;
                }
            }
        }
    }
}
