﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Base tag
    /// </summary>
    /// <remarks>Defines the base URL for relative URLs in the page.</remarks>
    public sealed class Base : XhtmlTagBase, IHrefAttribute, ITargetAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Base class.
        /// </summary>
        public Base()
            : base(XhtmlTagAndAttributeNames.Base, isVoidTag: true)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the target attribute.
        /// </summary>
        public string Target
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Target);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Target, value);
            }
        }

        /// <summary>
        /// Gets or sets the href attribute.
        /// </summary>
        public string Href
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Href);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Href, value);
            }
        }
    }
}
