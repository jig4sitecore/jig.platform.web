﻿namespace Jig.Web.Html
{
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The a tag
    /// </summary>
    public sealed class A :
        XhtmlNonVoidTagBase,
        IHrefAttribute,
        IRelAttribute,
        ITitleAttribute,
        ITypeAttribute,
        ITargetAttribute,
        IAltAttribute,
        IMediaAttribute, IDownloadAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the A class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public A([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.A, values)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the alt.
        /// </summary>
        public string Alt
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Alt);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Alt, value);
            }
        }

        /// <summary>
        /// Gets or sets the download.
        /// </summary>
        public string Download
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Download);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Download, value);
            }
        }

        /// <summary>
        /// Gets or sets the href.
        /// </summary>
        public string Href
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Href);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Href, value);
            }
        }

        /// <summary>
        /// Gets or sets the media.
        /// </summary>
        public string Media
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Media);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Media, value);
            }
        }

        /// <summary>
        /// Gets or sets the target.
        /// </summary>
        public string Target
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Target);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Target, value);
            }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Title);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Title, value);
            }
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        public string Type
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Type);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Type, value);
            }
        }

        /// <summary>
        /// Gets or sets the relative attribute.
        /// </summary>
        /// <value>
        /// The relative attribute.
        /// </value>
        public string Rel
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Rel);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Rel, value);
            }
        }

        #endregion

        /// <summary>
        ///     Strings the specified tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public static implicit operator string(A tag)
        {
            if (tag != null)
            {
                return tag.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts tag to the element.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns>The XElement</returns>
        public static implicit operator XElement(A tag)
        {
            if (tag != null)
            {
                return tag.Inner;
            }

            return null;
        }

        /// <summary>
        /// Controls the specified tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns>The control</returns>
        public static implicit operator Control(A tag)
        {
            if (tag != null)
            {
                return new Literal
                           {
                               Mode = LiteralMode.PassThrough,
                               Text = tag
                           };
            }

            return null;
        }
    }
}
