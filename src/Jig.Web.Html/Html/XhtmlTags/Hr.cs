﻿namespace Jig.Web.Html
{
    /// <summary>
    /// Represents a thematic break between paragraphs of a section or article or any longer content tag
    /// </summary>
    public sealed class Hr : XhtmlVoidTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Hr class.
        /// </summary>
        public Hr()
            : base(XhtmlTagAndAttributeNames.Hr)
        {
        }

        #endregion
    }
}
