﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The img tag
    /// </summary>
    public sealed class Img : XhtmlVoidTagBase, IIdAttribute, IClassAttribute, IAltAttribute, IHeightAttribute, ISrcAttribute, IWidthAttribute, IStyleAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Img class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Img([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Img, values)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the alt attribute.
        /// </summary>
        public string Alt
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Alt);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Alt, value);
            }
        }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public string Height
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Height);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Height, value);
            }
        }

        /// <summary>
        /// Gets or sets the src.
        /// </summary>
        public string Src
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Src);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Src, value);
            }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public string Width
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Width);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Width, value);
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Id);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Id, value);
            }
        }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        /// <value>
        /// The class.
        /// </value>
        public string Class
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Class);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Class, value);
            }
        }

        /// <summary>
        /// Gets or sets the style attribute.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public string Style
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Style);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Style, value);
            }
        }
    }
}
