﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Link tag
    /// </summary>
    /// <remarks>Used to link JavaScript and external CSS with the current HTML document.</remarks>
    public sealed class Link : XhtmlVoidTagBase, IHrefAttribute, IRelAttribute, ITypeAttribute, IMediaAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Link class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Link([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Link, values)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the href attribute.
        /// </summary>
        public string Href
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Href);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Href, value);
            }
        }

        /// <summary>
        /// Gets or sets the relative attribute.
        /// </summary>
        /// <value>
        /// The relative attribute.
        /// </value>
        public string Rel
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Rel);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Rel, value);
            }
        }

        /// <summary>
        /// Gets or sets the type attribute.
        /// </summary>
        public string Type
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Type);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Type, value);
            }
        }

        /// <summary>
        /// Gets or sets the Media attribute.
        /// </summary>
        public string Media
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Media);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Media, value);
            }
        }
    }
}
