﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The NoScript tag
    /// </summary>
    /// <remarks>Defines either an internal NoScript or a link to an external NoScript. The NoScript language is JavaNoScript.</remarks>
    public sealed class NoScript : XhtmlNonVoidTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the NoScript class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public NoScript([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.NoScript, values)
        {
        }

        #endregion
    }
}
