﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Style tag
    /// </summary>
    /// <remarks>Used to write inline CSS.</remarks>
    public sealed class Style : XhtmlVoidTagBase, ITypeAttribute, IMediaAttribute, ITitleAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Style class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Style([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Style, values)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the type attribute.
        /// </summary>
        public string Type
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Type);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Type, value);
            }
        }

        /// <summary>
        /// Gets or sets the Media attribute.
        /// </summary>
        public string Media
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Media);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Media, value);
            }
        }

        /// <summary>
        /// Gets or sets the title attribute.
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Title);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Title, value);
            }
        }
    }
}
