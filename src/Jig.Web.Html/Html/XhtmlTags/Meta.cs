﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Meta tag
    /// </summary>
    /// <remarks>Defines meta data that can't be defined using another HTML element.</remarks>
    public sealed class Meta : XhtmlTagBase, IContentAttribute, ICharset, INameAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Meta class.
        /// </summary>
        public Meta()
            : base(XhtmlTagAndAttributeNames.Meta, isVoidTag: true)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public string Content
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Content);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Content, value);
            }
        }

        /// <summary>
        /// Gets or sets the charset.
        /// </summary>
        /// <value>
        /// The charset.
        /// </value>
        public string Charset
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Charset);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Charset, value);
            }
        }

        /// <summary>
        /// Gets or sets the name attribute.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Name);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Name, value);
            }
        }

        /// <summary>
        /// Gets or sets the HTTP equivalent attribute.
        /// </summary>
        /// <value>
        /// The HTTP equivalent attribute.
        /// </value>
        [Jig.Annotations.NotNull]
        public string HttpEquiv
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.HttpEquiv);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.HttpEquiv, value);
            }
        }
    }
}
