﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Strong tag
    /// </summary>
    public sealed class Strong : XhtmlNonVoidTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Strong class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Strong([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Strong, values)
        {
        }

        #endregion
    }
}
