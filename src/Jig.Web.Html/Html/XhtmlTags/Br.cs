﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Break tag
    /// </summary>
    public sealed class Br : XhtmlVoidTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Br class.
        /// </summary>
        public Br()
            : base(XhtmlTagAndAttributeNames.Br)
        {
        }

        #endregion
    }
}
