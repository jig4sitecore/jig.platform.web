﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Track tag
    /// </summary>
    public sealed class Track : XhtmlVoidTagBase, ISrcAttribute
    {
        /// <summary>
        /// Initializes a new instance of the Track class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Track([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Track, values)
        {
        }

        /// <summary>
        /// Gets or sets the kind.
        /// </summary>
        /// <value>
        /// The kind.
        /// </value>
        [Jig.Annotations.NotNull]
        public string Kind
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Kind);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Kind, value);
            }
        }

        /// <summary>
        /// Gets or sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Src
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Src);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Src, value);
            }
        }

        /// <summary>
        /// Gets or sets the source language.
        /// </summary>
        /// <value>
        /// The source language.
        /// </value>
        [Jig.Annotations.NotNull]
        public string SrcLang
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.SrcLang);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.SrcLang, value);
            }
        }

        /// <summary>
        /// Gets or sets the source language.
        /// </summary>
        /// <value>
        /// The source language.
        /// </value>
        [Jig.Annotations.NotNull]
        public string Label
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Label);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Label, value);
            }
        }
    }
}
