﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The Script tag
    /// </summary>
    /// <remarks>Defines either an internal script or a link to an external script. The script language is JavaScript.</remarks>
    public sealed class Script : XhtmlNonVoidTagBase, ITypeAttribute, ISrcAttribute
    {
        /// <summary>
        /// The asynchronous attribute
        /// </summary>
        private static readonly XName AsyncAttribute = XName.Get("async");

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Script class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Script([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Script, values)
        {
        }

        #endregion

        /// <summary>
        /// Gets or sets the type attribute.
        /// </summary>
        public string Type
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Type);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Type, value);
            }
        }

        /// <summary>
        /// Gets or sets the src attribute.
        /// </summary>
        public string Src
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Src);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Src, value);
            }
        }

        /// <summary>
        /// Gets or sets the asynchronous attribute.
        /// </summary>
        /// <value>The asynchronous.</value>
        [Jig.Annotations.NotNull]
        public string Async
        {
            get
            {
                return this.GetAttributeValue(AsyncAttribute);
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.SetAttributeValue(AsyncAttribute, value);
                }
            }
        }
    }
}
