﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Span tag
    /// </summary>
    public sealed class Span : XhtmlNonVoidTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Span class.
        /// </summary>
        /// <param name="values">
        /// The values.
        /// </param>
        public Span([Jig.Annotations.NotNull] params object[] values)
            : base(XhtmlTagAndAttributeNames.Span, values)
        {
        }

        #endregion
    }
}
