﻿namespace Jig.Web.Html
{
    using System;
    using System.Diagnostics;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The xhtml tag base.
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{ToString()}")]
    public abstract class XhtmlTagBase : IXhtmlTag, IHtmlString
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the XhtmlTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="isVoidTag">if set to <c>true</c> is self closing tag (void).</param>
        protected XhtmlTagBase([Jig.Annotations.NotNull] XName name, bool isVoidTag)
        {
            this.Inner = isVoidTag ? new XElement(name) : new XElement(name, string.Empty);
        }

        /// <summary>
        /// Initializes a new instance of the XhtmlTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="isVoidTag">if set to <c>true</c> [is void tag].</param>
        /// <param name="values">The values.</param>
        protected XhtmlTagBase([Jig.Annotations.NotNull] XName name, bool isVoidTag, [Jig.Annotations.NotNull] params object[] values)
            : this(name, isVoidTag)
        {
            this.Add(values);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the inner.
        /// </summary>
        public XElement Inner { get; private set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        [Jig.Annotations.NotNull]
        public string InnerText
        {
            get
            {
                return this.Inner.Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Inner.Value = value;
                }
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     Strings the specified tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public static implicit operator string(XhtmlTagBase tag)
        {
            if (tag != null)
            {
                return tag.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts tag to the element.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns>The XElement</returns>
        public static implicit operator XElement(XhtmlTagBase tag)
        {
            if (tag != null)
            {
                return tag.Inner;
            }

            return null;
        }

        /// <summary>
        /// Controls the specified tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns>The control</returns>
        public static implicit operator Control(XhtmlTagBase tag)
        {
            if (tag != null)
            {
                return new Literal
                {
                    Mode = LiteralMode.PassThrough,
                    Text = tag
                };
            }

            return null;
        }

        /// <summary>
        ///     Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        ///     A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (this.Inner != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return this.Inner.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Adds the specified content.
        /// </summary>
        /// <param name="content">The content.</param>
        public void Add([Jig.Annotations.NotNull] params object[] content)
        {
            foreach (var item in content)
            {
                var tag = item as IXhtmlTag;
                this.Inner.Add(tag != null ? tag.Inner : item);
            }
        }

        /// <summary>
        /// Adds the specified content.
        /// </summary>
        /// <param name="content">The content.</param>
        public void Add([Jig.Annotations.NotNull] params XAttribute[] content)
        {
            foreach (var item in content)
            {
                this.Inner.Add(item);
            }
        }

        #endregion

        /// <summary>
        /// Returns an HTML-encoded string.
        /// </summary>
        /// <returns>
        /// An HTML-encoded string.
        /// </returns>
        [Jig.Annotations.NotNull]
        public string ToHtmlString()
        {
            return this.ToString();
        }

        #region Methods

        /// <summary>
        /// Gets the attribute value.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns>The attribute value</returns>
        [Jig.Annotations.NotNull]
        public string GetAttributeValue([Jig.Annotations.NotNull] string attributeName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attributeName != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                XName name = XName.Get(attributeName);
                return this.GetAttributeValue(name);
            }

            return string.Empty;
        }

        /// <summary>
        /// The get attribute value.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <returns>
        /// The attribute value
        /// </returns>
        [Jig.Annotations.NotNull]
        public string GetAttributeValue([Jig.Annotations.NotNull] XName attributeName)
        {
            var attribute = this.Inner.Attribute(attributeName);
            if (attribute != null)
            {
                return attribute.Value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the attribute value.
        /// </summary>
        /// <param name="attributeName">Name of the attribute.</param>
        /// <param name="attributeValue">The attribute value.</param>
        public void SetAttributeValue([Jig.Annotations.NotNull] string attributeName, [Jig.Annotations.NotNull] string attributeValue)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attributeName != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                XName name = XName.Get(attributeName);
                this.SetAttributeValue(name, attributeValue);
            }
        }

        /// <summary>
        /// The set attribute value.
        /// </summary>
        /// <param name="attributeName">The name.</param>
        /// <param name="attributeValue">The value.</param>
        public void SetAttributeValue([Jig.Annotations.NotNull] XName attributeName, [Jig.Annotations.NotNull] string attributeValue)
        {
            if (!string.IsNullOrWhiteSpace(attributeValue))
            {
                var attribute = this.Inner.Attribute(attributeName);
                if (attribute != null)
                {
                    attribute.Value = attributeValue;
                }
                else
                {
                    attribute = new XAttribute(attributeName, attributeValue);
                    this.Inner.Add(attribute);
                }
            }
        }

        /// <summary>
        /// Removes the attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        public void RemoveAttribute([Jig.Annotations.NotNull] XName name)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (name != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                XAttribute attribute = this.Inner.Attribute(name);
                if (attribute != null)
                {
                    attribute.Remove();
                }
            }
        }

        /// <summary>
        /// Removes the attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        public void RemoveAttribute([Jig.Annotations.NotNull] string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                XName elementName = XName.Get(name);
                this.RemoveAttribute(elementName);
            }
        }

        #endregion
    }
}