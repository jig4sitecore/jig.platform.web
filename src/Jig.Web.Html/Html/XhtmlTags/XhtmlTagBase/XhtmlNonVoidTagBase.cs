﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The xhtml non-void visual tag base. The visual html elements has CssClass / Style / attributes
    /// </summary>
    public abstract class XhtmlNonVoidTagBase : XhtmlTagBase, IIdAttribute, IClassAttribute, IStyleAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the XhtmlNonVoidTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected XhtmlNonVoidTagBase([Jig.Annotations.NotNull] XName name)
            : base(name, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the XhtmlNonVoidTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        protected XhtmlNonVoidTagBase([Jig.Annotations.NotNull] XName name, [Jig.Annotations.NotNull] params object[] values)
            : base(name, false, values)
        {
        }

        /// <summary>
        /// Sets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public object[] InnerHtml
        {
            set
            {
                this.Add(value);
            }
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Id);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Id, value);
            }
        }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        /// <value>
        /// The class.
        /// </value>
        public string Class
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Class);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Class, value);
            }
        }

        /// <summary>
        /// Gets or sets the style.
        /// </summary>
        /// <value>
        /// The style.
        /// </value>
        public string Style
        {
            get
            {
                return this.GetAttributeValue(XhtmlTagAndAttributeNames.Style);
            }

            set
            {
                this.SetAttributeValue(XhtmlTagAndAttributeNames.Style, value);
            }
        }

        #endregion
    }
}