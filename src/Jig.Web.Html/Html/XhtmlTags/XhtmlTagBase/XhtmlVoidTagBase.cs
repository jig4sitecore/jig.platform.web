﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The xhtml void tag base.
    /// </summary>
    public abstract class XhtmlVoidTagBase : XhtmlTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the XhtmlVoidTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected XhtmlVoidTagBase([Jig.Annotations.NotNull] XName name)
            : base(name, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the XhtmlVoidTagBase class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="values">The values.</param>
        protected XhtmlVoidTagBase([Jig.Annotations.NotNull] XName name, [Jig.Annotations.NotNull] params object[] values)
            : base(name, true, values)
        {
        }

        #endregion
    }
}