﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Title tag
    /// </summary>
    /// <remarks>Defines the title of the document, shown in a browser's title bar or on the page's tab. It can only contain text and any contained tags are not interpreted.</remarks>
    public sealed class Title : XhtmlTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the Title class.
        /// </summary>
        public Title()
            : base(XhtmlTagAndAttributeNames.Title, isVoidTag: false)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Title class.
        /// </summary>
        /// <param name="text">The text.</param>
        public Title([Jig.Annotations.NotNull] string text)
            : base(XhtmlTagAndAttributeNames.Title, isVoidTag: false)
        {
            this.InnerText = text;
        }

        #endregion
    }
}
