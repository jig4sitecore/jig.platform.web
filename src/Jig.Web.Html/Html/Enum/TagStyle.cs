﻿namespace Jig.Web.Html
{
    /// <summary>
    /// Determines how the tag and surrounding whitespace is rendered.
    /// </summary>
    public enum TagStyle
    {
        /// <summary>
        /// Tag is rendered without any preceding space and without any line break between the tag and inner text.
        /// </summary>
        Inline, 

        /// <summary>
        /// Tag is rendered on a new line with indenting but without any line break between the tag and inner text.
        /// </summary>
        TagOnOneLine, 

        /// <summary>
        /// Tag is rendered on a new line with indenting, a line break separates the tag from the inner text on both ends of the tag.
        /// </summary>
        TagOnSeparateLine
    }
}