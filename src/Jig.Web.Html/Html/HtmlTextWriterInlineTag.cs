﻿namespace Jig.Web.Html
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// Creates opening and closing HTML tags.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No unmanaged resources were allocated")]
    public class HtmlTextWriterInlineTag : HtmlWriterBaseTag, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlTextWriterInlineTag"/> class.
        /// </summary>
        /// <param name="htmlTextWriter">The writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.NotNull] params XAttribute[] attributes)
            : base(htmlTextWriter, tag, attributes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the HtmlTextWriterInlineTag class. Renders the opening tag of an HTML node, including any attributes.
        /// </summary>
        /// <param name="htmlTextWriter">The HTMLTextWriter.</param>
        /// <param name="tag">The type of HTML tag.</param>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">HTML attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.CanBeNull] string innerHtml, [Jig.Annotations.NotNull] params XAttribute[] attributes)
            : base(htmlTextWriter, tag, attributes)
        {
            this.InnerHtml = innerHtml;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlTextWriterInlineTag"/> class.
        /// </summary>
        /// <param name="htmlTextWriter">The writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, System.Web.UI.HtmlTextWriterTag tag, [Jig.Annotations.NotNull] params XAttribute[] attributes)
            : base(htmlTextWriter, tag.ToString("G").ToLowerInvariant(), attributes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the HtmlTextWriterInlineTag class. Renders the opening tag of an HTML node, including any attributes.
        /// </summary>
        /// <param name="htmlTextWriter">The HTMLTextWriter.</param>
        /// <param name="tag">The type of HTML tag.</param>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">HTML attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, System.Web.UI.HtmlTextWriterTag tag, [Jig.Annotations.CanBeNull] string innerHtml, [Jig.Annotations.NotNull] params XAttribute[] attributes)
            : base(htmlTextWriter, tag.ToString("G").ToLowerInvariant(), attributes)
        {
            this.InnerHtml = innerHtml;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlTextWriterInlineTag"/> class.
        /// </summary>
        /// <param name="htmlTextWriter">The writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
            : base(htmlTextWriter, tag, attributes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the HtmlTextWriterInlineTag class. Renders the opening tag of an HTML node, including any attributes.
        /// </summary>
        /// <param name="htmlTextWriter">The HTMLTextWriter.</param>
        /// <param name="tag">The type of HTML tag.</param>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">HTML attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.CanBeNull] string innerHtml, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
            : base(htmlTextWriter, tag, attributes)
        {
            this.InnerHtml = innerHtml;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlTextWriterInlineTag"/> class.
        /// </summary>
        /// <param name="htmlTextWriter">The writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, System.Web.UI.HtmlTextWriterTag tag, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
            : base(htmlTextWriter, tag.ToString("G").ToLowerInvariant(), attributes)
        {
        }

        /// <summary>
        /// Initializes a new instance of the HtmlTextWriterInlineTag class. Renders the opening tag of an HTML node, including any attributes.
        /// </summary>
        /// <param name="htmlTextWriter">The HTMLTextWriter.</param>
        /// <param name="tag">The type of HTML tag.</param>
        /// <param name="innerHtml">The inner HTML.</param>
        /// <param name="attributes">HTML attributes.</param>
        public HtmlTextWriterInlineTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, System.Web.UI.HtmlTextWriterTag tag, [Jig.Annotations.CanBeNull] string innerHtml, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
            : base(htmlTextWriter, tag.ToString("G").ToLowerInvariant(), attributes)
        {
            this.InnerHtml = innerHtml;
        }

        /// <summary>
        /// Closes the HTML tag.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly", Justification = "No unmanaged resources were allocated")]
        public void Dispose()
        {
            if (this.Writer != null)
            {
                // TODO: Use Tag Style to format the tag
                foreach (var attr in this.Attributes.Where(x => x != null && !string.IsNullOrWhiteSpace(x.Value)))
                {
                    this.Writer.AddAttribute(attr.Name.LocalName, attr.Value, false);
                }

                this.Writer.RenderBeginTag(this.TagName);
                this.Writer.Write(this.InnerHtml);
                this.Writer.RenderEndTag();
            }
        }
    }
}
