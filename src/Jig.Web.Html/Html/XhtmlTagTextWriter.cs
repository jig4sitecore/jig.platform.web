﻿namespace Jig.Web.Html
{
    using System.IO;
    using System.Text;
    using System.Web.UI;

    /// <summary>
    /// The xhtml tag text writer.
    /// </summary>
    public class XhtmlTagTextWriter : HtmlTextWriter
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the XhtmlTagTextWriter class. 
        /// </summary>
        public XhtmlTagTextWriter()
            : base(new StringWriter(new StringBuilder(512)))
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the html.
        /// </summary>
        [Jig.Annotations.NotNull]
        public string Html
        {
            get
            {
                return this.InnerWriter.ToString();
            }
        }

        /// <summary>
        /// Gets the encoding that the System.Web.UI.HtmlTextWriter object uses to write content to the page.
        /// </summary>
        /// <returns>The System.Text.Encoding in which the markup is written to the page.</returns>
        public override Encoding Encoding
        {
            get
            {
                return Encoding.UTF8;
            }
        }

        #endregion
    }
}