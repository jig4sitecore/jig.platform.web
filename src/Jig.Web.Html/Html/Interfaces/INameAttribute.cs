namespace Jig.Web.Html
{
    /// <summary>
    /// The Name Attribute interface.
    /// </summary>
    public interface INameAttribute
    {
        /// <summary>
        ///     Gets or sets the name attribute.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        [Jig.Annotations.NotNull]
        string Name { get; set; }
    }
}