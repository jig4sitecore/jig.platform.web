namespace Jig.Web.Html
{
    /// <summary>
    /// The Style Attribute interface.
    /// </summary>
    public interface IStyleAttribute
    {
        /// <summary>
        ///     Gets or sets the style attribute.
        /// </summary>
        /// <value>
        ///     The style.
        /// </value>
        [Jig.Annotations.NotNull]
        string Style { get; set; }
    }
}