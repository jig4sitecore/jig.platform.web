﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Width Attribute interface.
    /// </summary>
    public interface IWidthAttribute
    {
        /// <summary>
        ///     Gets or sets the width.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Width { get; set; }
    }
}