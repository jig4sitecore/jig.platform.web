﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The RelAttribute interface.
    /// </summary>
    public interface IRelAttribute
    {
        /// <summary>
        ///     Gets or sets the relative attribute.
        /// </summary>
        /// <value>
        ///     The relative attribute.
        /// </value>
        [Jig.Annotations.NotNull]
        string Rel { get; set; }
    }
}