﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The ID Attribute interface.
    /// </summary>
    public interface IIdAttribute
    {
        /// <summary>
        ///     Gets or sets the id attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Id { get; set; }
    }
}