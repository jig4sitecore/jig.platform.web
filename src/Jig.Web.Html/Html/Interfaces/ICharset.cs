namespace Jig.Web.Html
{
    /// <summary>
    /// The Charset interface.
    /// </summary>
    public interface ICharset
    {
        /// <summary>
        ///     Gets or sets the charset attribute.
        /// </summary>
        /// <value>
        ///     The charset.
        /// </value>
        [Jig.Annotations.NotNull]
        string Charset { get; set; }
    }
}