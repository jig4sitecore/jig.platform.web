﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The HeightAttribute interface.
    /// </summary>
    public interface IHeightAttribute
    {
        /// <summary>
        ///     Gets or sets the height.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Height { get; set; }
    }
}