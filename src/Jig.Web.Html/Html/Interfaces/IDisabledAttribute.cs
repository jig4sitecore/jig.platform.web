﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Disable Attribute interface.
    /// </summary>
    public interface IDisabledAttribute
    {
        /// <summary>
        ///     Gets or sets the Disabled attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Disabled { get; set; }
    }
}