namespace Jig.Web.Html
{
    /// <summary>
    /// The MediaAttribute interface.
    /// </summary>
    public interface IMediaAttribute
    {
        /// <summary>
        ///     Gets or sets the Media attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Media { get; set; }
    }
}