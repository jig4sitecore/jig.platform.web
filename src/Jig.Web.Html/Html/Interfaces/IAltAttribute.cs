﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The AltAttribute interface.
    /// </summary>
    public interface IAltAttribute
    {
        /// <summary>
        ///     Gets or sets the alt attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Alt { get; set; }
    }
}