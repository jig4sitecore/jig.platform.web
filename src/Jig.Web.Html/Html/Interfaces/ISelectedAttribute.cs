﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Selected Attribute interface.
    /// </summary>
    public interface ISelectedAttribute
    {
        /// <summary>
        ///     Gets or sets the Selected attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Selected { get; set; }
    }
}