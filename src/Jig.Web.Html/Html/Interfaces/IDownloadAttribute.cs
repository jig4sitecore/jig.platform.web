﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The DownloadAttribute interface.
    /// </summary>
    public interface IDownloadAttribute
    {
        /// <summary>
        ///     Gets or sets the download.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Download { get; set; }
    }
}