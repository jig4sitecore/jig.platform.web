﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Title Attribute interface.
    /// </summary>
    public interface ITitleAttribute
    {
        /// <summary>
        ///     Gets or sets the title attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Title { get; set; }
    }
}