﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The ClassAttribute interface.
    /// </summary>
    public interface IClassAttribute
    {
        /// <summary>
        /// Gets or sets the class attribute.
        /// </summary>
        /// <value>
        /// The class.
        /// </value>
        [Jig.Annotations.NotNull]
        string Class { get; set; }
    }
}