﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Target Attribute interface.
    /// </summary>
    public interface ITargetAttribute
    {
        /// <summary>
        ///     Gets or sets the target attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Target { get; set; }
    }
}