namespace Jig.Web.Html
{
    /// <summary>
    /// The ContentAttribute interface.
    /// </summary>
    public interface IContentAttribute
    {
        /// <summary>
        ///     Gets or sets the content attribute.
        /// </summary>
        /// <value>
        ///     The content.
        /// </value>
        [Jig.Annotations.NotNull]
        string Content { get; set; }
    }
}