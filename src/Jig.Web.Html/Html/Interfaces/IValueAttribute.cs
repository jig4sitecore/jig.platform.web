﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Value Attribute interface.
    /// </summary>
    public interface IValueAttribute
    {
        /// <summary>
        ///     Gets or sets the Value attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Value { get; set; }
    }
}