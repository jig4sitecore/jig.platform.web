﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XhtmlTag interface.
    /// </summary>
    public interface IXhtmlTag
    {
        /// <summary>
        ///     Gets the inner.
        /// </summary>
        [Jig.Annotations.NotNull]
        XElement Inner { get; }
    }
}