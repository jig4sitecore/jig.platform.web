﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Href attribute interface.
    /// </summary>
    public interface IHrefAttribute
    {
        /// <summary>
        ///     Gets or sets the href attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Href { get; set; }
    }
}