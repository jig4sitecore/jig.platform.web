﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Src Attribute interface.
    /// </summary>
    public interface ISrcAttribute
    {
        /// <summary>
        ///     Gets or sets the src attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Src { get; set; }
    }
}