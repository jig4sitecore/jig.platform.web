﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Type Attribute interface.
    /// </summary>
    public interface ITypeAttribute
    {
        /// <summary>
        ///     Gets or sets the type attribute attribute.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Type { get; set; }
    }
}