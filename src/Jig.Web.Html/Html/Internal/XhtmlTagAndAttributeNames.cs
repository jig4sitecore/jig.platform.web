﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The xhtml tag and attribute names.
    /// </summary>
    internal static partial class XhtmlTagAndAttributeNames
    {
        #region Static Fields

        /// <summary>
        ///     Strongly typed Html tag a
        /// </summary>
        public static readonly XName A = XName.Get("a");

        /// <summary>
        ///     Strongly typed Html tag abbr
        /// </summary>
        public static readonly XName Abbr = XName.Get("abbr");

        /// <summary>
        ///     Strong typed Html global attribute accessKey
        /// </summary>
        public static readonly XName AccessKey = XName.Get("accessKey");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Action = XName.Get("action");

        /// <summary>
        ///     Strongly typed Html tag address
        /// </summary>
        public static readonly XName Address = XName.Get("address");

        /// <summary>
        ///     Strongly Typed XName
        /// </summary>
        public static readonly XName Alt = XName.Get("alt");

        /// <summary>
        ///     Strongly typed Html tag area
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Area = XName.Get("area");

        /// <summary>
        ///     Strongly typed Html tag article
        /// </summary>
        public static readonly XName Article = XName.Get("article");

        /// <summary>
        ///     Strongly typed Html tag aside
        /// </summary>
        public static readonly XName Aside = XName.Get("aside");

        /// <summary>
        ///     Strongly typed Html tag audio
        /// </summary>
        public static readonly XName Audio = XName.Get("audio");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName AutoPlay = XName.Get("autoplay");

        /// <summary>
        ///     Strongly typed Html tag b
        /// </summary>
        public static readonly XName B = XName.Get("b");

        /// <summary>
        ///     Strongly typed Html tag base
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Base = XName.Get("base");

        /// <summary>
        ///     Strongly typed Html tag Bi-Directional Override
        /// </summary>
        // ReSharper disable IdentifierTypo
        public static readonly XName Bdo = XName.Get("bdo");

        // ReSharper restore IdentifierTypo

        /// <summary>
        ///     Strongly typed Html tag block-quote
        /// </summary>
        public static readonly XName BlockQuote = XName.Get("blockquote");

        /// <summary>
        ///     Strongly typed Html tag body
        /// </summary>
        public static readonly XName Body = XName.Get("body");

        /// <summary>
        ///     Strongly typed Html tag br
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Br = XName.Get("br");

        /// <summary>
        ///     Strongly typed Html tag button
        /// </summary>
        public static readonly XName Button = XName.Get("button");

        /// <summary>
        ///     Strongly typed Html tag canvas
        /// </summary>
        public static readonly XName Canvas = XName.Get("canvas");

        /// <summary>
        ///     Strongly typed Html tag caption
        /// </summary>
        public static readonly XName Caption = XName.Get("caption");

        /// <summary>
        ///     Strongly typed Html tag charset
        /// </summary>
        public static readonly XName Charset = XName.Get("charset");

        /// <summary>
        ///     Strongly typed Html tag cite
        /// </summary>
        public static readonly XName Cite = XName.Get("cite");

        /// <summary>
        ///     Strong typed Html global attribute class
        /// </summary>
        public static readonly XName Class = XName.Get("class");

        /// <summary>
        ///     Strongly typed Html tag code
        /// </summary>
        public static readonly XName Code = XName.Get("code");

        /// <summary>
        ///     Strongly typed Html tag col
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Col = XName.Get("col");

        /// <summary>
        /// Strongly typed Html tag
        /// </summary>
        public static readonly XName Cols = XName.Get("cols");

        /// <summary>
        ///     Strongly typed Html tag column group
        /// </summary>
        public static readonly XName ColGroup = XName.Get("colgroup");

        /// <summary>
        ///     Strongly typed Html tag content
        /// </summary>
        public static readonly XName Content = XName.Get("content");

        /// <summary>
        ///     Strong typed Html global attribute contentEditable
        /// </summary>
        public static readonly XName ContentEditable = XName.Get("contenteditable");

        /// <summary>
        ///     Strong typed Html global attribute context menu
        /// </summary>
        public static readonly XName ContextMenu = XName.Get("contextmenu");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Controls = XName.Get("controls");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName CrossOrigin = XName.Get("crossorigin");

        /// <summary>
        ///     Strongly typed attribute data-ga used for Google Analytics
        /// </summary>
        public static readonly XName DataGaAction = XName.Get("data-ga");

        /// <summary>
        ///     Strongly typed attribute data-gaeventaction used for Google Analytics
        /// </summary>
        public static readonly XName DataGaEventAction = XName.Get("data-gaeventaction");

        /// <summary>
        ///     Strongly typed attribute data-gaeventcategory used for Google Analytics
        /// </summary>
        public static readonly XName DataGaEventCategory = XName.Get("data-gaeventcategory");

        /// <summary>
        ///     Strongly typed attribute data-gaeventlabel used for Google Analytics
        /// </summary>
        public static readonly XName DataGaEventLabel = XName.Get("data-gaeventlabel");

        /// <summary>
        ///     Strongly typed attribute data-gaeventvalue used for Google Analytics
        /// </summary>
        public static readonly XName DataGaEventValue = XName.Get("data-gaeventvalue");

        /// <summary>
        ///     Strongly typed attribute data-gapageview used for Google Analytics
        /// </summary>
        public static readonly XName DataGaPageView = XName.Get("data-gapageview");

        /// <summary>
        ///     Strongly typed attribute data-id
        /// </summary>
        public static readonly XName DataId = XName.Get("data-id");

        /// <summary>
        ///     Strongly typed Html tag dd
        /// </summary>
        public static readonly XName Dd = XName.Get("dd");

        /// <summary>
        ///     Strongly typed Html tag del
        /// </summary>
        public static readonly XName Del = XName.Get("del");

        /// <summary>
        ///     Strongly typed Html tag dfn
        /// </summary>
        public static readonly XName Dfn = XName.Get("dfn");

        /// <summary>
        ///     Strong typed Html global attribute dir
        /// </summary>
        public static readonly XName Dir = XName.Get("dir");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Disabled = XName.Get("disabled");

        /// <summary>
        ///     Strongly typed Html tag div
        /// </summary>
        public static readonly XName Div = XName.Get("div");

        /// <summary>
        ///     Strongly typed Html tag dl
        /// </summary>
        public static readonly XName Dl = XName.Get("dl");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Download = XName.Get("download");

        /// <summary>
        ///     Strong typed Html global attribute draggable
        /// </summary>
        public static readonly XName Draggable = XName.Get("draggable");

        /// <summary>
        ///     Strong typed Html global attribute dropzone
        /// </summary>
        public static readonly XName DropZone = XName.Get("dropzone");

        /// <summary>
        ///     Strongly typed Html tag dt
        /// </summary>
        public static readonly XName Dt = XName.Get("dt");

        /// <summary>
        ///     Strongly typed Html tag em
        /// </summary>
        public static readonly XName Em = XName.Get("em");

        /// <summary>
        ///     Strongly typed Html tag embed
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Embed = XName.Get("embed");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Enctype = XName.Get("enctype");

        /// <summary>
        ///     Strongly typed Html tag fieldset
        /// </summary>
        public static readonly XName FieldSet = XName.Get("fieldset");

        /// <summary>
        ///     Strongly typed Html tag figcaption
        /// </summary>
        public static readonly XName FigCaption = XName.Get("figcaption");

        /// <summary>
        ///     Strongly typed Html tag figure
        /// </summary>
        public static readonly XName Figure = XName.Get("figure");

        /// <summary>
        ///     Strongly typed Html tag footer
        /// </summary>
        public static readonly XName Footer = XName.Get("footer");

        /// <summary>
        ///     Strongly typed Html tag form
        /// </summary>
        public static readonly XName Form = XName.Get("form");

        /// <summary>
        ///     Strongly typed Html tag form
        /// </summary>
        public static readonly XName For = XName.Get("for");

        /// <summary>
        ///     Strongly typed Html tag h1
        /// </summary>
        public static readonly XName H1 = XName.Get("h1");

        /// <summary>
        ///     Strongly typed Html tag h2
        /// </summary>
        public static readonly XName H2 = XName.Get("h2");

        /// <summary>
        ///     Strongly typed Html tag h3
        /// </summary>
        public static readonly XName H3 = XName.Get("h3");

        /// <summary>
        ///     Strongly typed Html tag h4
        /// </summary>
        public static readonly XName H4 = XName.Get("h4");

        /// <summary>
        ///     Strongly typed Html tag h5
        /// </summary>
        public static readonly XName H5 = XName.Get("h5");

        /// <summary>
        ///     Strongly typed Html tag h6
        /// </summary>
        public static readonly XName H6 = XName.Get("h6");

        /// <summary>
        ///     Strongly typed Html tag header group
        /// </summary>
        public static readonly XName HGroup = XName.Get("hgroup");

        /// <summary>
        ///     Strongly typed Html tag head
        /// </summary>
        public static readonly XName Head = XName.Get("head");

        /// <summary>
        ///     Strongly typed Html tag header
        /// </summary>
        public static readonly XName Header = XName.Get("header");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Height = XName.Get("height");

        /// <summary>
        ///     Strong typed Html global attribute hidden
        /// </summary>
        public static readonly XName Hidden = XName.Get("hidden");

        /// <summary>
        ///     Strongly typed Html tag hr
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Hr = XName.Get("hr");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Href = XName.Get("href");

        /// <summary>
        ///     Strongly typed Html tag html
        /// </summary>
        public static readonly XName Html = XName.Get("html");

        /// <summary>
        ///     Strongly typed Html tag attribute
        /// </summary>
        public static readonly XName HttpEquiv = XName.Get("http-equiv");

        /// <summary>
        ///     Strongly typed Html tag i
        /// </summary>
        public static readonly XName I = XName.Get("i");

        /// <summary>
        ///     Strongly typed Html tag i-frame
        /// </summary>
        // ReSharper disable InconsistentNaming
        public static readonly XName IFrame = XName.Get("iframe");

        /// <summary>
        ///     Strongly typed Html global attribute id
        /// </summary>
        public static readonly XName Id = XName.Get("id");

        /// <summary>
        ///     Strongly typed Html tag img
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Img = XName.Get("img");

        /// <summary>
        ///     Strongly typed Html tag input
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Input = XName.Get("input");

        // ReSharper restore InconsistentNaming

        /// <summary>
        ///     Strongly typed Html tag ins
        /// </summary>
        public static readonly XName Ins = XName.Get("ins");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Key = XName.Get("key");

        /// <summary>
        ///     Strongly typed Html tag label
        /// </summary>
        public static readonly XName Label = XName.Get("label");

        /// <summary>
        ///     Strongly typed Html global attribute lang
        /// </summary>
        public static readonly XName Lang = XName.Get("lang");

        /// <summary>
        ///     Strongly typed Html tag legend
        /// </summary>
        public static readonly XName Legend = XName.Get("legend");

        /// <summary>
        ///     Strongly typed Html tag li
        /// </summary>
        public static readonly XName Li = XName.Get("li");

        /// <summary>
        ///     Strongly typed Html tag link
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Link = XName.Get("link");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Loop = XName.Get("loop");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Kind = XName.Get("kind");

        /// <summary>
        ///     Strongly typed Html tag main
        /// </summary>
        public static readonly XName Main = XName.Get("main");

        /// <summary>
        ///     Strongly typed Html tag map
        /// </summary>
        public static readonly XName Map = XName.Get("map");

        /// <summary>
        ///     Strongly typed Html tag mark
        /// </summary>
        public static readonly XName Mark = XName.Get("mark");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Media = XName.Get("media");

        /// <summary>
        ///     Strongly typed Html tag meta
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Meta = XName.Get("meta");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Method = XName.Get("method");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Muted = XName.Get("muted");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Name = XName.Get("name");

        /// <summary>
        ///     Strongly typed Html tag navigation links
        /// </summary>
        // ReSharper disable IdentifierTypo
        public static readonly XName Nav = XName.Get("nav");

        // ReSharper restore IdentifierTypo

        /// <summary>
        ///     Strongly typed Html tag no-script
        /// </summary>
        public static readonly XName NoScript = XName.Get("noscript");

        /// <summary>
        ///     Strongly typed Html tag object
        /// </summary>
        public static readonly XName Object = XName.Get("object");

        /// <summary>
        ///     Strongly typed Html tag ordered list
        /// </summary>
        public static readonly XName Ol = XName.Get("ol");

        /// <summary>
        ///     Strongly typed html event onclick
        /// </summary>
        public static readonly XName OnClick = XName.Get("onclick");

        /// <summary>
        ///     Strongly typed Html tag optional group
        /// </summary>
        public static readonly XName OptGroup = XName.Get("optgroup");

        /// <summary>
        ///     Strongly typed Html tag option
        /// </summary>
        public static readonly XName Option = XName.Get("option");

        /// <summary>
        ///     Strongly typed Html tag
        /// </summary>
        public static readonly XName Rows = XName.Get("rows");

        /// <summary>
        ///     Strongly typed Html tag p
        /// </summary>
        public static readonly XName P = XName.Get("p");

        /// <summary>
        ///     Strongly typed Html tag param
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Param = XName.Get("param");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Poster = XName.Get("poster");

        /// <summary>
        ///     Strongly typed Html tag pre
        /// </summary>
        public static readonly XName Pre = XName.Get("pre");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName PreLoad = XName.Get("preload");

        /// <summary>
        ///     Strongly typed Html tag meta
        /// </summary>
        public static readonly XName Property = XName.Get("property");

        /// <summary>
        ///     Strongly typed Html tag q
        /// </summary>
        public static readonly XName Q = XName.Get("q");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Rel = XName.Get("rel");

        /// <summary>
        ///     Strongly typed Html global attribute spell-check
        /// </summary>
        public static readonly XName Role = XName.Get("role");

        /// <summary>
        ///     Strongly typed Html tag s
        /// </summary>
        public static readonly XName S = XName.Get("s");

        /// <summary>
        ///     Strongly typed Html tag format text
        /// </summary>
        // ReSharper disable IdentifierTypo
        public static readonly XName Samp = XName.Get("samp");

        // ReSharper restore IdentifierTypo

        /// <summary>
        ///     Strongly typed Html tag script
        /// </summary>
        public static readonly XName Script = XName.Get("script");

        /// <summary>
        ///     Strongly typed Html tag section
        /// </summary>
        public static readonly XName Section = XName.Get("section");

        /// <summary>
        ///     Strongly typed Html tag select
        /// </summary>
        public static readonly XName Select = XName.Get("select");

        /// <summary>
        ///     Strongly typed html attribute selected
        /// </summary>
        public static readonly XName Selected = XName.Get("selected");

        /// <summary>
        ///     Strongly typed Html tag small
        /// </summary>
        public static readonly XName Small = XName.Get("small");

        /// <summary>
        ///     Strongly typed Html tag source
        ///     <para>This tag cannot contain children</para>
        /// </summary>
        public static readonly XName Source = XName.Get("source");

        /// <summary>
        ///     Strongly typed Html tag span
        /// </summary>
        public static readonly XName Span = XName.Get("span");

        /// <summary>
        ///     Strongly typed Html global attribute spell-check
        /// </summary>
        public static readonly XName SpellCheck = XName.Get("spellcheck");

        /// <summary>
        ///     Strongly typed html attribute src
        /// </summary>
        public static readonly XName Src = XName.Get("src");

        /// <summary>
        ///     Strongly typed Html tag strong
        /// </summary>
        public static readonly XName Strong = XName.Get("strong");

        /// <summary>
        ///     Strongly typed Html global attribute style
        /// </summary>
        public static readonly XName Style = XName.Get("style");

        /// <summary>
        ///     Strongly typed Html tag style
        /// </summary>
        public static readonly XName StyleTag = XName.Get("style");

        /// <summary>
        ///     Strongly typed Html tag sub
        /// </summary>
        public static readonly XName Sub = XName.Get("sub");

        /// <summary>
        ///     Strongly typed Html tag sup
        /// </summary>
        public static readonly XName Sup = XName.Get("sup");

        /// <summary>
        ///     Strongly typed Html tag sup
        /// </summary>
        public static readonly XName Sizes = XName.Get("sizes");

        /// <summary>
        ///     Strongly typed Html tag sup
        /// </summary>
        public static readonly XName SrcLang = XName.Get("srclang");

        /// <summary>
        ///     Strongly typed Html tag table body
        /// </summary>
        // ReSharper disable InconsistentNaming
        public static readonly XName TBody = XName.Get("tbody");
        // ReSharper restore InconsistentNaming

        /// <summary>
        ///     Strongly typed Html tag table foot
        /// </summary>
        // ReSharper disable InconsistentNaming
        public static readonly XName TFoot = XName.Get("tfoot");

        /// <summary>
        ///     Strongly typed Html tag table head
        /// </summary>
        // ReSharper disable InconsistentNaming
        public static readonly XName THead = XName.Get("thead");

        /// <summary>
        ///     Strongly typed Html global attribute tab-index
        /// </summary>
        public static readonly XName TabIndex = XName.Get("tabindex");

        /// <summary>
        ///     Strongly typed Html tag table
        /// </summary>
        public static readonly XName Table = XName.Get("table");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Target = XName.Get("target");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Track = XName.Get("track");

        /// <summary>
        ///     Strongly typed Html tag td
        /// </summary>
        public static readonly XName Td = XName.Get("td");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Text = XName.Get("text");

        /// <summary>
        ///     Strongly typed Html tag text-area
        /// </summary>
        public static readonly XName TextArea = XName.Get("textarea");

        // ReSharper restore InconsistentNaming

        /// <summary>
        ///     Strongly typed Html tag th
        /// </summary>
        public static readonly XName Th = XName.Get("th");

        /// <summary>
        ///     Strongly typed Html global attribute title
        /// </summary>
        public static readonly XName Title = XName.Get("title");

        /// <summary>
        ///     Strongly typed Html tag title
        /// </summary>
        public static readonly XName TitleTag = XName.Get("title");

        /// <summary>
        ///     Strongly typed Html tag tr
        /// </summary>
        public static readonly XName Tr = XName.Get("tr");

        /// <summary>
        ///     Strongly typed html attribute type
        /// </summary>
        public static readonly XName Type = XName.Get("type");

        /// <summary>
        ///     Strongly typed Html tag ul
        /// </summary>
        public static readonly XName Ul = XName.Get("ul");

        /// <summary>
        ///     Strongly typed html attribute value
        /// </summary>
        public static readonly XName Value = XName.Get("value");

        /// <summary>
        ///     Strongly typed Html tag var
        /// </summary>
        public static readonly XName Var = XName.Get("var");

        /// <summary>
        ///     Strongly typed Html tag video
        /// </summary>
        public static readonly XName Video = XName.Get("video");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName Width = XName.Get("width");

        /// <summary>
        ///     Strongly Typed Attribute Name
        /// </summary>
        public static readonly XName ItemProp = XName.Get("itemprop");

        #endregion
    }
}