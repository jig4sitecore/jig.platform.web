namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// Tha base class 
    /// </summary>
    public abstract class HtmlWriterBaseTag
    {
        /// <summary>
        /// The HTMLTextWriter.
        /// </summary>
        protected readonly HtmlTextWriter Writer;

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlWriterBaseTag" /> class.
        /// </summary>
        /// <param name="htmlTextWriter">The HTML text writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        protected HtmlWriterBaseTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            this.Writer = htmlTextWriter;
            this.Attributes = new HashSet<XAttribute>(attributes);
            this.TagName = tag.ToLowerInvariant();
            this.TagStyle = TagStyle.Inline;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HtmlWriterBaseTag" /> class.
        /// </summary>
        /// <param name="htmlTextWriter">The HTML text writer.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="attributes">The attributes.</param>
        protected HtmlWriterBaseTag([Jig.Annotations.NotNull] HtmlTextWriter htmlTextWriter, [Jig.Annotations.NotNull] string tag, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            this.Writer = htmlTextWriter;
            this.Attributes = new HashSet<XAttribute>(attributes);
            this.TagName = tag.ToLowerInvariant();
            this.TagStyle = TagStyle.Inline;
        }

        /// <summary>
        /// Gets or sets the tag style.
        /// </summary>
        /// <value>
        /// The tag style.
        /// </value>
        [Jig.Annotations.UsedImplicitly]
        public TagStyle TagStyle { get; set; }

        /// <summary>
        /// Gets or sets the inner HTML.
        /// </summary>
        /// <value>
        /// The inner HTML.
        /// </value>
        [Jig.Annotations.CanBeNull]
        public string InnerHtml { get; set; }

        /// <summary>
        /// Gets or sets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        [Jig.Annotations.NotNull]
        public string TagName { get; protected set; }

        /// <summary>
        /// Gets the attributes.
        /// </summary>
        /// <value>
        /// The attributes.
        /// </value>
        [Jig.Annotations.NotNull]
        public ICollection<XAttribute> Attributes { get; private set; }

        /// <summary>
        /// Sets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public string Id
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new IdAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new NameAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the CSS class.
        /// </summary>
        /// <value>
        /// The CSS class.
        /// </value>
        public string CssClass
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new CssClassAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the href.
        /// </summary>
        /// <value>
        /// The href.
        /// </value>
        public string Href
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new HrefAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the target.
        /// </summary>
        /// <value>
        /// The target.
        /// </value>
        public string Target
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new TargetAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new TitleAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the relative.
        /// </summary>
        /// <value>
        /// The relative.
        /// </value>
        public string Rel
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new RelAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new TypeAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the source.
        /// </summary>
        /// <value>
        /// The source.
        /// </value>
        public string Src
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new SrcAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public string Width
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new WidthAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public string Height
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new HeightAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public string Value
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new ValueAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets the selected.
        /// </summary>
        /// <value>
        /// The selected.
        /// </value>
        public string Selected
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new SelectedAttribute(value));
                }
            }
        }

        /// <summary>
        /// Sets for attribute.
        /// </summary>
        /// <value>
        /// For attribute.
        /// </value>
        public string ForAttribute
        {
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.Attributes.Add(new ForAttribute(value));
                }
            }
        }
    }
}