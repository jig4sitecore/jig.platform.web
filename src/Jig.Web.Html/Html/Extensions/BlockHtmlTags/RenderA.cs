﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The render link tags extensions.
    /// </summary>
    public static class RenderAExtensions
    {
        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
                          {
                              Href = href
                          };
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
                          {
                              Href = href,
                              CssClass = cssClass
                          };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="target">The target.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] string target,
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href,
                CssClass = cssClass,
                Target = target
            };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="target">The target.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] string target,
            [Jig.Annotations.NotNull] string title,
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href,
                CssClass = cssClass,
                Target = target,
                Title = title
            };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href
            };
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href,
                CssClass = cssClass
            };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="target">The target.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] string target,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href,
                CssClass = cssClass,
                Target = target
            };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="target">The target.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderA(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] string target,
            [Jig.Annotations.NotNull] string title,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, System.Web.UI.HtmlTextWriterTag.A, attributes)
            {
                Href = href,
                CssClass = cssClass,
                Target = target,
                Title = title
            };

            tag.StartRender();
            return tag;
        }
    }
}