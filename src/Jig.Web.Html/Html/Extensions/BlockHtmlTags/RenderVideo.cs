﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The render Video tag extensions.
    /// </summary>
    public static class RenderVideoExtensions
    {
        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderVideo(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "video", attributes);
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="src">The source.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>A HTML tag with attributes.</returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderVideo(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] string src, 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "video", attributes)
                          {
                              Src = src
                          };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderVideo(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "video", attributes);
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="src">The source.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>A HTML tag with attributes.</returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderVideo(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string src,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "video", attributes)
            {
                Src = src
            };

            tag.StartRender();
            return tag;
        }
    }
}