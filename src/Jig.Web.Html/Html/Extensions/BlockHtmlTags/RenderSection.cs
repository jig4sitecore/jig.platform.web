﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The render div extensions.
    /// </summary>
    public static class RenderSectionExtensions
    {
        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderSection(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "section", attributes);
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderSection(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] string cssClass, 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "section", attributes)
                          {
                              CssClass = cssClass
                          };

            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderSection(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "section", attributes);
            tag.StartRender();
            return tag;
        }

        /// <summary>
        /// Creates a A HTML tag and adds any provided attributes to the tag.
        /// </summary>
        /// <param name="writer">The HTMLTextWriter.</param>
        /// <param name="cssClass">The css Class.</param>
        /// <param name="attributes">The tag attributes.</param>
        /// <returns>
        /// A HTML tag with attributes.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HtmlTextWriterBlockTag RenderSection(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer,
            [Jig.Annotations.NotNull] string cssClass,
            [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            var tag = new HtmlTextWriterBlockTag(writer, "section", attributes)
            {
                CssClass = cssClass
            };

            tag.StartRender();
            return tag;
        }
    }
}