﻿namespace Jig.Web.Html
{
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineOptionExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="value">The value.</param>
        /// <param name="selected">The selected.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Option([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.CanBeNull] string value, [Jig.Annotations.CanBeNull] string selected = null, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Option, innerHtml, attributes))
            {
                tag.Value = value;
                tag.Selected = selected;
            }
        }
    }
}