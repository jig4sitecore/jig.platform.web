﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineAExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="target">The target.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.CanBeNull] string target, [Jig.Annotations.CanBeNull] string title, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
                tag.Target = target;
                tag.Title = title;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="target">The target.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.CanBeNull] string target, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
                tag.Target = target;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="target">The target.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.CanBeNull] string target, [Jig.Annotations.CanBeNull] string title, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
                tag.Target = target;
                tag.Title = title;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="target">The target.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.CanBeNull] string target, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
                tag.Target = target;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
                tag.CssClass = cssClass;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="href">The href.</param>
        /// <param name="attributes">The attributes.</param>
        public static void A([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] string href, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.A, innerHtml, attributes))
            {
                tag.Href = href;
            }
        }
    }
}