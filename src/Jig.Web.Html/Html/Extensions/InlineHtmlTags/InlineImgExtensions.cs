﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineImgExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="src">The source.</param>
        /// <param name="alt">The alt.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Img([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string src, [Jig.Annotations.NotNull] string alt, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            var img = new XElement(XhtmlTagAndAttributeNames.Img, attributes);
            img.Add(new SrcAttribute(src));
            img.Add(new AltAttribute(alt));

            string html = img.ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="src">The source.</param>
        /// <param name="alt">The alt.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Img([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string src, [Jig.Annotations.NotNull] string alt, [Jig.Annotations.NotNull] string title, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            var img = new XElement(XhtmlTagAndAttributeNames.Img, attributes);
            img.Add(new SrcAttribute(src));
            img.Add(new AltAttribute(alt));
            img.Add(new TitleAttribute(title));

            string html = img.ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="src">The source.</param>
        /// <param name="alt">The alt.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Img([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string src, [Jig.Annotations.NotNull] string alt, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            var img = new XElement(XhtmlTagAndAttributeNames.Img, attributes);
            img.Add(new SrcAttribute(src));
            img.Add(new AltAttribute(alt));

            string html = img.ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="src">The source.</param>
        /// <param name="alt">The alt.</param>
        /// <param name="title">The title.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Img([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string src, [Jig.Annotations.NotNull] string alt, [Jig.Annotations.NotNull] string title, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            var img = new XElement(XhtmlTagAndAttributeNames.Img, attributes);
            img.Add(new SrcAttribute(src));
            img.Add(new AltAttribute(alt));
            img.Add(new TitleAttribute(title));

            string html = img.ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }
    }
}
