﻿namespace Jig.Web.Html
{
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineIFrameExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="src">The source.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="attributes">The attributes.</param>
        // ReSharper disable InconsistentNaming
        public static void IFrame([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string src, [Jig.Annotations.CanBeNull] string cssClass = null, [Jig.Annotations.CanBeNull] string width = null, [Jig.Annotations.CanBeNull] string height = null, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        // ReSharper restore InconsistentNaming
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Iframe, attributes))
            {
                tag.Src = src;
                tag.CssClass = cssClass;
                tag.Width = width;
                tag.Height = height;
            }
        }
    }
}