﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineStyleExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Style([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Style, innerHtml, attributes))
            {
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Style([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Style, innerHtml, attributes))
            {
            }
        }
    }
}