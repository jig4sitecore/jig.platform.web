﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineHrExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Hr([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            string html = new XElement(XhtmlTagAndAttributeNames.Hr, attributes).ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Hr([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable CoVariantArrayConversion
            string html = new XElement(XhtmlTagAndAttributeNames.Hr, attributes.ToArray()).ToString();
            writer.WriteLineNoTabs(html);
            // ReSharper restore CoVariantArrayConversion
        }
    }
}
