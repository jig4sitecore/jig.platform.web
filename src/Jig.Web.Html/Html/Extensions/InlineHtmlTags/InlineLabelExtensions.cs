﻿namespace Jig.Web.Html
{
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineLabelExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="forAttribute">For attribute.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Label([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.CanBeNull] string forAttribute = null, [Jig.Annotations.CanBeNull] string cssClass = null, [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Label, innerHtml, attributes))
            {
                tag.ForAttribute = forAttribute;
                tag.CssClass = cssClass;
            }
        }
    }
}