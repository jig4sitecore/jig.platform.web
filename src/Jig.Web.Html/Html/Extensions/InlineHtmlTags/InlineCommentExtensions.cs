﻿namespace Jig.Web.Html
{
    using System.Web.UI;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineCommentExtensions
    {
        /// <summary>
        /// Comments the specified writer.
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="values">The values.</param>
        public static void Comment([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] params string[] values)
        {
            writer.WriteLineNoTabs("<!--" + string.Join(" ", values) + "-->");
        }
    }
}
