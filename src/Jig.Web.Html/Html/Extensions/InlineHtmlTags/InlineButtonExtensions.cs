﻿namespace Jig.Web.Html
{
    using System.Collections.Generic;
    using System.Web.UI;
    using System.Xml.Linq;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineButtonExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="name">The name.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="type">The type.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Button([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.CanBeNull] string name, [Jig.Annotations.CanBeNull] string cssClass = null, [Jig.Annotations.CanBeNull] string type = "button", [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Button, innerHtml, attributes))
            {
                tag.Name = name;
                tag.CssClass = cssClass;
                tag.Type = type;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="name">The name.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Button([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.CanBeNull] string name, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Button, innerHtml, attributes))
            {
                tag.Name = name;
                tag.Type = "button";
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="innerHtml">The inner text.</param>
        /// <param name="name">The name.</param>
        /// <param name="cssClass">The CSS class.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Button([Jig.Annotations.NotNull] this HtmlTextWriter writer, [Jig.Annotations.NotNull] string innerHtml, [Jig.Annotations.CanBeNull] string name, [Jig.Annotations.CanBeNull] string cssClass, [Jig.Annotations.NotNull] IEnumerable<XAttribute> attributes)
        {
            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Button, innerHtml, attributes))
            {
                tag.Name = name;
                tag.CssClass = cssClass;
                tag.Type = "button";
            }
        }
    }
}