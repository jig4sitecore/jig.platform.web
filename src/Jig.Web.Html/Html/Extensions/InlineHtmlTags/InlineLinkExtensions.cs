﻿namespace Jig.Web.Html
{
    using System.Web.UI;
    using System.Xml.Linq;

    using Jig.Web.Collections;

    /// <summary>
    /// The inline tag extensions.
    /// </summary>
    public static class InlineLinkExtensions
    {
        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="href">The href.</param>
        /// <param name="type">The type.</param>
        /// <param name="rel">The relative.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Link(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] string href, 
            [Jig.Annotations.NotNull] string type = "text/css", 
            [Jig.Annotations.NotNull] string rel = "stylesheet", 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var attr = new XAttributeDictionary(attributes)
                           {
                               new TypeAttribute(type),
                               new RelAttribute(rel)
                           };

            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Link, attr.ToArray()))
            {
                tag.Href = href;
            }
        }

        /// <summary>
        /// Writes the specified inline tag with attributes
        /// </summary>
        /// <param name="writer">The writer.</param>
        /// <param name="href">The href.</param>
        /// <param name="type">The type.</param>
        /// <param name="rel">The relative.</param>
        /// <param name="media">The media.</param>
        /// <param name="attributes">The attributes.</param>
        public static void Link(
            [Jig.Annotations.NotNull] this HtmlTextWriter writer, 
            [Jig.Annotations.NotNull] string href,
            [Jig.Annotations.NotNull] string type = "text/css",
            [Jig.Annotations.NotNull] string rel = "stylesheet",
            [Jig.Annotations.NotNull] string media = "all", 
            [Jig.Annotations.NotNull] params XAttribute[] attributes)
        {
            var attr = new XAttributeDictionary(attributes)
                           {
                               new TypeAttribute(type),
                               new RelAttribute(rel),
                               new MediaAttribute(media)
                           };

            using (var tag = new HtmlTextWriterInlineTag(writer, System.Web.UI.HtmlTextWriterTag.Link, attr.ToArray()))
            {
                tag.Href = href;
            }
        }
    }
}