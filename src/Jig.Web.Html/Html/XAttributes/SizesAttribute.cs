﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class SizesAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the SizesAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public SizesAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the SizesAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public SizesAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Sizes, value)
        {
        }
    }
}