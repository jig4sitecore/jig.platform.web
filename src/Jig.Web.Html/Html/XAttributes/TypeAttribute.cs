﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class TypeAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the TypeAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public TypeAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the TypeAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public TypeAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Type, value)
        {
        }

        /// <summary>
        /// Gets the JavaScript.
        /// </summary>
        /// <value>
        /// The JavaScript mime.
        /// </value>
        [Jig.Annotations.NotNull]
        public static TypeAttribute JavaScript
        {
            get
            {
                return new TypeAttribute("text/javascript");
            }
        }

        /// <summary>
        /// Gets the stylesheet.
        /// </summary>
        /// <value>
        /// The stylesheet.
        /// </value>
        [Jig.Annotations.NotNull]
        public static TypeAttribute Stylesheet
        {
            get
            {
                return new TypeAttribute("text/css");
            }
        }
    }
}