﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The data module attribute.
    /// </summary>
    public sealed class DataModuleAttribute : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataModuleAttribute" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public DataModuleAttribute([Jig.Annotations.NotNull] object value)
            : base(XName.Get("data-module"), value)
        {
        }

        #endregion
    }
}