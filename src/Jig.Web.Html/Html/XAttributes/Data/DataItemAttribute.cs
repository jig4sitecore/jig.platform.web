﻿namespace Jig.Web.Html
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// The data item attribute.
    /// </summary>
    public sealed class DataItemAttribute : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataItemAttribute" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public DataItemAttribute([Jig.Annotations.NotNull] object value)
            : base(XName.Get("data-item"), value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataItemAttribute" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public DataItemAttribute(Guid value)
            : base(XName.Get("data-item"), value.ToString("D"))
        {
        }

        #endregion
    }
}