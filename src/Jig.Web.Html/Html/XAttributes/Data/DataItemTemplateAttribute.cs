﻿namespace Jig.Web.Html
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// The data item template attribute.
    /// </summary>
    public sealed class DataItemTemplateAttribute : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataItemTemplateAttribute"/> class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public DataItemTemplateAttribute([Jig.Annotations.NotNull]object value)
            : base(XName.Get("data-item-template"), value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataItemTemplateAttribute"/> class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public DataItemTemplateAttribute(Guid value)
            : base(XName.Get("data-item-template"), value.ToString("D"))
        {
        }

        #endregion
    }
}