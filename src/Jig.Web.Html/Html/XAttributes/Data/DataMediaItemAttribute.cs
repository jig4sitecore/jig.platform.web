﻿namespace Jig.Web.Html
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// The data media item attribute.
    /// </summary>
    public sealed class DataMediaItemAttribute : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMediaItemAttribute" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public DataMediaItemAttribute([Jig.Annotations.NotNull]object value)
            : base(XName.Get("data-mediaitem"), value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMediaItemAttribute" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public DataMediaItemAttribute(Guid value)
            : base(XName.Get("data-mediaitem"), value.ToString("D"))
        {
        }

        #endregion
    }
}