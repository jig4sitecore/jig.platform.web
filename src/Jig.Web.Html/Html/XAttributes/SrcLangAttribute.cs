﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class SrcLangAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the SrcLangAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public SrcLangAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the SrcLangAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public SrcLangAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.SrcLang, value)
        {
        }
    }
}