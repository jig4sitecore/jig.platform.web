﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class DisabledAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the DisabledAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public DisabledAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the DisabledAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public DisabledAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Disabled, value)
        {
        }
    }
}