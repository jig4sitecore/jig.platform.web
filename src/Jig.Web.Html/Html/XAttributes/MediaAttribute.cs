﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class MediaAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the MediaAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public MediaAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the MediaAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public MediaAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Media, value)
        {
        }
    }
}