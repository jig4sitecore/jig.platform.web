﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class HrefAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the HrefAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public HrefAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the HrefAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public HrefAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Href, value)
        {
        }
    }
}