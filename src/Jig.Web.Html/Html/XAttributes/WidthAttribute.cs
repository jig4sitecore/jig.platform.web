﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class WidthAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the WidthAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public WidthAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the WidthAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public WidthAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Width, value)
        {
        }
    }
}