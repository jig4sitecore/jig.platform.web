﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics track attribute.
    /// </summary>
    public sealed class GoogleAnalyticsTrack : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsTrack class.
        /// </summary>
        public GoogleAnalyticsTrack()
            : base(XName.Get("data-ga"), "track")
        {
        }

        #endregion
    }
}