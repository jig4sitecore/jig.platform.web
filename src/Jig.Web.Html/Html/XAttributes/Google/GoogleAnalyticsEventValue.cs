﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics event value attribute.
    /// </summary>
    public sealed class GoogleAnalyticsEventValue : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsEventValue class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public GoogleAnalyticsEventValue([Jig.Annotations.NotNull] string value)
            : base(XName.Get("data-ga-event-value"), value)
        {
        }

        #endregion
    }
}