﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics event bounce attribute.
    /// </summary>
    public sealed class GoogleAnalyticsEventBounce : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsEventBounce class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public GoogleAnalyticsEventBounce(bool value = false)
            : base(XName.Get("data-ga-event-bounce"), value)
        {
        }

        #endregion
    }
}