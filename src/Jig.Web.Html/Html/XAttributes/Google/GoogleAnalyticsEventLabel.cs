﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics event label attribute.
    /// </summary>
    public sealed class GoogleAnalyticsEventLabel : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsEventLabel class.
        /// </summary>
        /// <param name="label">
        /// The category.
        /// </param>
        public GoogleAnalyticsEventLabel([Jig.Annotations.NotNull] string label)
            : base(XName.Get("data-ga-event-label"), label)
        {
        }

        #endregion
    }
}