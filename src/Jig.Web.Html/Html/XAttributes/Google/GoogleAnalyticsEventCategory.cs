﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics event category.
    /// </summary>
    public sealed class GoogleAnalyticsEventCategory : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsEventCategory class.
        /// </summary>
        /// <param name="category">
        /// The category.
        /// </param>
        public GoogleAnalyticsEventCategory([Jig.Annotations.NotNull] string category)
            : base(XName.Get("data-ga-event-category"), category)
        {
        }

        #endregion
    }
}