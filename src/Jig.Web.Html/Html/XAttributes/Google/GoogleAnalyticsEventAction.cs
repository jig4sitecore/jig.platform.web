﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The google analytics event action attribute.
    /// </summary>
    public sealed class GoogleAnalyticsEventAction : XAttribute
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the GoogleAnalyticsEventAction class.
        /// </summary>
        /// <param name="action">
        /// The action.
        /// </param>
        public GoogleAnalyticsEventAction([Jig.Annotations.NotNull] string action)
            : base(XName.Get("data-ga-event-action"), action)
        {
        }

        #endregion
    }
}