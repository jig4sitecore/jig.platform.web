﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class AltAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the AltAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public AltAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the AltAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public AltAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Alt, value)
        {
        }
    }
}