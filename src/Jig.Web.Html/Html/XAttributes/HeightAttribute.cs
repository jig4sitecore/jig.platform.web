﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class HeightAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the HeightAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public HeightAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the HeightAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public HeightAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Height, value)
        {
        }
    }
}