﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class SrcAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the SrcAttribute class.
        /// </summary>
        /// <param name="value">
        /// An System.Object containing the value of the attribute.
        /// </param>
        public SrcAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the SrcAttribute class.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        public SrcAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Src, value)
        {
        }
    }
}