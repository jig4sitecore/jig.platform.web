﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class DataAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the DataAttribute class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public DataAttribute([Jig.Annotations.NotNull] string name, [Jig.Annotations.NotNull] string value)
            : base("data-" + name, value)
        {
        }

        /// <summary>
        /// Initializes a new instance of the DataAttribute class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">An System.Object" /> containing the value of the attribute.</param>
        public DataAttribute([Jig.Annotations.NotNull] string name, [Jig.Annotations.NotNull] object value)
            : base("data-" + name, string.Concat(value))
        {
        }
    }
}