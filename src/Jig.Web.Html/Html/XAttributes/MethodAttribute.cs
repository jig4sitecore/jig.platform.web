﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class MethodAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MethodAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public MethodAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MethodAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public MethodAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Method, value)
        {
        }
    }
}
