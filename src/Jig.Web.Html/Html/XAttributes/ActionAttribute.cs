﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The XHtml attribute class.
    /// </summary>
    public sealed class ActionAttribute : XAttribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ActionAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public ActionAttribute([Jig.Annotations.NotNull] object value)
            : this(string.Concat(value))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ActionAttribute"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public ActionAttribute([Jig.Annotations.NotNull] string value)
            : base(XhtmlTagAndAttributeNames.Action, value)
        {
        }
    }
}
