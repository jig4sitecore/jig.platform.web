﻿namespace Jig.Web.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// The XAttribute dictionary.
    /// </summary>
    public class XAttributeDictionary : Dictionary<string, XAttribute>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XAttributeDictionary"/> class.
        /// </summary>
        public XAttributeDictionary()
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XAttributeDictionary" /> class.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="replaceUnderscoreWithDash">if set to <c>true</c> [replace underscore with dash].</param>
        public XAttributeDictionary([Jig.Annotations.NotNull]NameValueCollection parameters, bool replaceUnderscoreWithDash = true)
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
            this.Add(parameters, replaceUnderscoreWithDash);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XAttributeDictionary" /> class.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        public XAttributeDictionary([Jig.Annotations.NotNull]params XAttribute[] attributes)
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
            this.Add(attributes);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XAttributeDictionary"/> class.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        public XAttributeDictionary([Jig.Annotations.NotNull]IEnumerable<XAttribute> attributes)
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
            this.Add(attributes);
        }

        /// <summary>
        /// Gets or sets the value associated with the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// The Attribute by key
        /// </returns>
        public new XAttribute this[string key]
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(key))
                {
                    return base[key];
                }

                return null;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(key) && value != null)
                {
                    base[key] = value;
                }
            }
        }

        /// <summary>
        /// Adds the specified attribute.
        /// </summary>
        /// <param name="attribute">The attribute.</param>
        /// <returns>The current instance of XAttributeDictionary</returns>
        public XAttributeDictionary Add([Jig.Annotations.NotNull]XAttribute attribute)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attribute != null && !string.IsNullOrWhiteSpace(attribute.Value))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                this[attribute.Name.LocalName] = attribute;
            }

            return this;
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <returns>The current instance of XAttributeDictionary</returns> 
        public XAttributeDictionary Add([Jig.Annotations.NotNull]string key, [Jig.Annotations.NotNull]string value)
        {
            if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(value))
            {
                return this;
            }

            var builder = new StringBuilder(key.Length);
            foreach (char character in key)
            {
                if (char.IsLetterOrDigit(character) || character == '-')
                {
                    builder.Append(character);
                }
                else
                {
                    builder.Append('-');
                }
            }

            var name = builder.ToString();
            var attr = new XAttribute(XName.Get(name), value);
            this[name] = attr;

            return this;
        }

        /// <summary>
        /// Adds the specified parameters.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="replaceUnderscoreWithDash">if set to <c>true</c> [replace underscore with dash].</param>
        /// <remarks>This methods is handy for Sitecore Rendering definition parameters. They store data-widget=callout as data_widget=callout</remarks>
        /// <returns>The current instance of XAttributeDictionary</returns>
        public XAttributeDictionary Add([Jig.Annotations.NotNull]NameValueCollection parameters, bool replaceUnderscoreWithDash = true)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (parameters != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (string key in parameters.AllKeys)
                {
                    if (!string.IsNullOrWhiteSpace(key))
                    {
                        var nameBuilder = new StringBuilder(key.Length);
                        foreach (char c in key)
                        {
                            if (char.IsLetterOrDigit(c) || '-'.Equals(c))
                            {
                                nameBuilder.Append(c);
                            }
                            else if (!replaceUnderscoreWithDash && '_'.Equals(c))
                            {
                                nameBuilder.Append('_');
                            }
                            else
                            {
                                nameBuilder.Append('-');
                            }
                        }

                        var name = nameBuilder.ToString().Trim('-');
                        this[name] = new XAttribute(XName.Get(name), parameters[key]);
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Adds the specified attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The current instance of XAttributeDictionary</returns>
        public XAttributeDictionary Add([Jig.Annotations.NotNull]params XAttribute[] attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attributes != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var attribute in attributes)
                {
                    if (attribute != null && !string.IsNullOrWhiteSpace(attribute.Value))
                    {
                        this[attribute.Name.LocalName] = attribute;
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Adds the specified attributes.
        /// </summary>
        /// <param name="attributes">The attributes.</param>
        /// <returns>The current instance of XAttributeDictionary</returns> 
        public XAttributeDictionary Add([Jig.Annotations.NotNull]IEnumerable<XAttribute> attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attributes != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var attribute in attributes)
                {
                    if (attribute != null && !string.IsNullOrWhiteSpace(attribute.Value))
                    {
                        this[attribute.Name.LocalName] = attribute;
                    }
                }
            }

            return this;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The Attribute from collection by key</returns>
        [Jig.Annotations.CanBeNull]
        public XAttribute GetValue([Jig.Annotations.NotNull]string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                var attr = this[key];
                return attr;
            }

            return null;
        }

        /// <summary>
        /// To the array.
        /// </summary>
        /// <returns>The array of value</returns>
        [Jig.Annotations.NotNull]
        public IList<XAttribute> ToArray()
        {
            var values = this.Values.ToArray();
            return values;
        }

        #endregion
    }
}