﻿namespace Jig.Web.Html
{
    using System;
    using System.ComponentModel;
    using System.Web.UI;
    using System.Web.UI.Adapters;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The control base
    /// </summary>
    [ParseChildren(false)]
    [PersistChildren(false)]
    [ControlBuilder(typeof(LiteralControlBuilder))]
    [ToolboxItem(typeof(HtmlHeadTagControl))]
    [ToolboxData("<{0}:HtmlHeadTagControl runat='server' />")]
    public sealed class HtmlHeadTagControl : Control
    {
        /// <summary>
        ///     Gets a value indicating whether View state is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableViewState
        {
            get
            {
                return false;
            }

            private set
            {
                base.EnableViewState = value;
            }
        }

        /// <summary>
        ///     Gets a value indicating whether EnableTheming is enabled in control.
        /// </summary>
        /// <remarks>
        ///     This property is unsupported in this control.
        /// </remarks>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool EnableTheming
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Outputs server control content to a provided System.Web.UI.HtmlTextWriter object and stores tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        public override void RenderControl(HtmlTextWriter writer)
        {
            var type = this.GetType();

            writer.WriteLineNoTabs(Environment.NewLine + "<!-- Begin: " + type.Name + "-->");

            if (this.Visible && !this.DesignMode)
            {
                try
                {
                    var tags = this.Context.GetHtmlHeadTags();
                    tags.RenderControl(writer);
                }
                catch (Exception exception)
                {
                    string message = this.GetExceptionMessage(exception).ToString();
                    writer.WriteLineNoTabs(message);

                    System.Diagnostics.Trace.WriteLine(exception.ToString());
                }
            }

            writer.WriteLineNoTabs("<!-- END: " + type.Name + "-->");
        }

        /// <summary>
        /// Applies the style properties defined in the page style sheet to the control.
        /// </summary>
        /// <param name="page">
        /// The System.Web.UI.Page containing the control. 
        /// </param>
        public override void ApplyStyleSheetSkin([Jig.Annotations.NotNull] Page page)
        {
            // Don't do anything
        }

        /// <summary>
        ///   Binds a data source to the invoked server control and all its child controls.
        /// </summary>
        public override void DataBind()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        /// Searches the current naming container for a server control with the specified id parameter.
        /// </summary>
        /// <param name="id">
        /// The identifier for the control to be found. 
        /// </param>
        /// <returns>
        /// The specified control, or null if the specified control does not exist. 
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public override Control FindControl(string id)
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        ///   Sets input focus to a control.
        /// </summary>
        public override void Focus()
        {
            throw new InvalidOperationException("The Method is not supported!");
        }

        /// <summary>
        ///   Determines if the server control contains any child controls.
        /// </summary>
        /// <returns> true if the control contains other controls; otherwise, false. </returns>
        public override bool HasControls()
        {
            return false;
        }

        /// <summary>
        /// Good programming practice
        /// </summary>
        /// <param name="obj">
        /// An Object that represents the parsed element. 
        /// </param>
        protected override void AddParsedSubObject(object obj)
        {
            // Don't do anything
        }

        /// <summary>
        ///   Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
        /// </summary>
        protected override void CreateChildControls()
        {
            // Don't do anything
        }

        /// <summary>
        ///   Good programming practice
        /// </summary>
        /// <returns> Empty Control collection. No children. </returns>
        protected override ControlCollection CreateControlCollection()
        {
            return new EmptyControlCollection(this);
        }

        /// <summary>
        /// Binds a data source to the invoked server control and all its child controls with an option to raise the System.Web.UI.Control.DataBinding event.
        /// </summary>
        /// <param name="raiseOnDataBinding">
        /// true if the System.Web.UI.Control.DataBinding event is raised; otherwise, false. 
        /// </param>
        protected override void DataBind(bool raiseOnDataBinding)
        {
            // Don't do anything
        }

        /// <summary>
        ///   Binds a data source to the server control's child controls.
        /// </summary>
        protected override void DataBindChildren()
        {
            // Don't do anything
        }

        /// <summary>
        ///   Determines whether the server control contains child controls. If it does not, it creates child controls.
        /// </summary>
        protected override void EnsureChildControls()
        {
            // Don't do anything
        }

        /// <summary>
        /// Restores control-state information from a previous page request that was saved by the M:System.Web.UI.Control.SaveControlState method.
        /// </summary>
        /// <param name="savedState">
        /// An System.Object that represents the control state to be restored. 
        /// </param>
        protected override void LoadControlState([Jig.Annotations.NotNull] object savedState)
        {
            // Don't do anything  
        }

        /// <summary>
        ///   Gets the control adapter responsible for rendering the specified control.
        /// </summary>
        /// <returns> A System.Web.UI.Adapters.ControlAdapter" /> that will render the control. </returns>
        [Jig.Annotations.CanBeNull]
        protected override ControlAdapter ResolveAdapter()
        {
            return null;
        }

        /// <summary>
        ///   Saves any server control view-state changes that have occurred since the time the page was posted back to the server.
        /// </summary>
        /// <returns> Returns the server control's current view state. If there is no view state associated with the control, this method returns null. </returns>
        protected override object SaveViewState()
        {
            return null;
        }

        /// <summary>
        ///   Causes tracking of view-state changes to the server control so they can be stored in the server control's System.Web.UI.StateBag object. 
        /// This object is accessible through the System.Web.UI.Control.ViewState property.
        /// </summary>
        protected override void TrackViewState()
        {
            // Don't do anything
        }

        /// <summary>
        /// Gets the exception message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>
        /// The exception message
        /// </returns>
        [Jig.Annotations.NotNull]
        private XElement GetExceptionMessage([Jig.Annotations.NotNull] Exception exception)
        {
            var attributes = new XElement("code", string.Empty);

            attributes.Add(new XAttribute("class", "exception"));
            attributes.Add(new XAttribute("data-exception-message", exception.Message));
            attributes.Add(new XAttribute("data-request-url", this.Context.Request.Url));
            attributes.Add(new XAttribute("data-type-fullname", this.GetType().FullName));

            return attributes;
        }

        /// <summary>
        ///   Determines if the server control holds only literal content.
        /// </summary>
        /// <returns> true if the server control contains solely literal content; otherwise false. </returns>
        // ReSharper disable UnusedMember.Local
        private new bool IsLiteralContent()
        // ReSharper restore UnusedMember.Local
        {
            return true;
        }
    }
}