﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <example>
    /// <![CDATA[<meta name="twitter:card" content="summary">]]>
    /// </example>
    [DisplayName("twitter:card")]
    public sealed class TwitterCardTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterCardTag"/> class.
        /// </summary>
        public TwitterCardTag()
            : base("twitter:card", "summary")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterCardTag"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public TwitterCardTag([Jig.Annotations.NotNull]string content)
            : base("twitter:card", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:card";
            }
        }
    }
}