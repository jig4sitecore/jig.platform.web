﻿namespace Jig.Web.Html
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <remarks>twitter:image - URL to a unique image representing the content of the page. Do not use a generic image such as your website logo, author photo, or other image that spans multiple pages. The image must be a minimum size of 120x120px. Images larger than 120x120px will be resized and cropped square based on its longest dimension. Images must be less than 1MB in size.</remarks>
    /// <example>
    /// <![CDATA[<meta name="twitter:image" content="http://graphics8.nytimes.com/images/2012/02/19/us/19whitney-span/19whitney-span-article.jpg">]]>
    /// </example>
    [DisplayName("twitter:image")]
    public sealed class TwitterImageTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageTag" /> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        public TwitterImageTag([Jig.Annotations.NotNull]string uri)
            : base("twitter:image", uri)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageTag"/> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        public TwitterImageTag([Jig.Annotations.NotNull] Uri uri)
            : base("twitter:image", string.Concat(uri))
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:image";
            }
        }
    }
}