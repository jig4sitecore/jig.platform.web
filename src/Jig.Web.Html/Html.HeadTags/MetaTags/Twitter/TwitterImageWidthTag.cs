﻿namespace Jig.Web.Html
{
    using System.ComponentModel;
    using System.Globalization;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <remarks>twitter:image:width- Providing width in px helps us more accurately preserve the aspect ratio of the image when resizing.</remarks>
    /// <example>
    /// <![CDATA[<meta name="twitter:image:width" content="610">]]>
    /// </example>
    [DisplayName("twitter:image:width")]
    public sealed class TwitterImageWidthTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageWidthTag" /> class.
        /// </summary>
        /// <param name="height">The height.</param>
        public TwitterImageWidthTag([Jig.Annotations.NotNull]string height)
            : base("twitter:image:width", height)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageWidthTag" /> class.
        /// </summary>
        /// <param name="height">The height.</param>
        public TwitterImageWidthTag(int height)
            : base("twitter:image:width", height.ToString(CultureInfo.InvariantCulture))
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:image:width";
            }
        }
    }
}