﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <remarks>twitter:description - A description that concisely summarizes the content of the page, as appropriate for presentation within a Tweet. Do not re-use the title text as the description, or use this field to describe the general services provided by the website. Description text will be truncated at the word to 200 characters.</remarks>
    /// <example>
    /// <![CDATA[<meta name="twitter:description" content="NEWARK - The guest list and parade of limousines with celebrities emerging from them seemed more suited to a red carpet event in Hollywood or New York than than a gritty stretch of Sussex Avenue near the former site of the James M. Baxter Terrace public housing project here.">]]>
    /// </example>
    [DisplayName("twitter:description")]
    public sealed class TwitterDescriptionTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterDescriptionTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public TwitterDescriptionTag([Jig.Annotations.NotNull]string content)
            : base("twitter:description", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:description";
            }
        }
    }
}