﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <example>
    /// <![CDATA[
    /// <meta name="twitter:data1" content="$3">
    /// <meta name="twitter:label1" content="Price">
    /// <meta name="twitter:data2" content="Black">
    /// <meta name="twitter:label2" content="Color">]]>
    /// </example>
    [DisplayName("twitter:{partialName}")]
    public sealed class TwitterTag : HeadTagBase
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterTag"/> class.
        /// </summary>
        /// <param name="partialName">The partial name.</param>
        /// <param name="content">The content.</param>
        public TwitterTag([Jig.Annotations.NotNull]string partialName, [Jig.Annotations.NotNull]string content)
            : base("twitter:" + partialName, content)
        {
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:" + this.Name;
            }
        }

        #endregion
    }
}