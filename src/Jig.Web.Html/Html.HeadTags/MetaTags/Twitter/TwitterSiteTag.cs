﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <example>
    /// <![CDATA[<meta name="twitter:site" content="@nytimes">]]>
    /// </example>
    [DisplayName("twitter:site")]
    public sealed class TwitterSiteTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterSiteTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public TwitterSiteTag([Jig.Annotations.NotNull]string content)
            : base("twitter:site", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:site";
            }
        }
    }
}