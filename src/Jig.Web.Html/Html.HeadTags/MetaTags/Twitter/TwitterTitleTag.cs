﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <remarks>twitter:title - Title should be concise and will be truncated at 70 characters.</remarks>
    /// <example>
    /// <![CDATA[<meta name="twitter:title" content="Parade of Fans for Houston’s Funeral">]]>
    /// </example>
    [DisplayName("twitter:title")]
    public sealed class TwitterTitleTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterTitleTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public TwitterTitleTag([Jig.Annotations.NotNull]string content)
            : base("twitter:title", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:title";
            }
        }
    }
}