﻿namespace Jig.Web.Html
{
    using System.ComponentModel;
    using System.Globalization;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <remarks>twitter:image:height- Providing width in px helps us more accurately preserve the aspect ratio of the image when resizing.</remarks>
    /// <example>
    /// <![CDATA[<meta name="twitter:image:height" content="610">]]>
    /// </example>
    [DisplayName("twitter:image:height")]
    public sealed class TwitterImageHeightTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageHeightTag" /> class.
        /// </summary>
        /// <param name="height">The height.</param>
        public TwitterImageHeightTag([Jig.Annotations.NotNull]string height)
            : base("twitter:image:height", height)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterImageHeightTag" /> class.
        /// </summary>
        /// <param name="height">The height.</param>
        public TwitterImageHeightTag(int height)
            : base("twitter:image:height", height.ToString(CultureInfo.InvariantCulture))
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:image:height";
            }
        }
    }
}