﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Twitter tag.
    /// </summary>
    /// <example>
    /// <![CDATA[<meta name="twitter:creator" content="@Sarah">]]>
    /// </example>
    [DisplayName("twitter:creator")]
    public sealed class TwitterCreatorTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TwitterCreatorTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public TwitterCreatorTag([Jig.Annotations.NotNull]string content)
            : base("twitter:creator", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "twitter:creator";
            }
        }
    }
}