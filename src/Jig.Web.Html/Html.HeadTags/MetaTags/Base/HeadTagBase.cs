﻿namespace Jig.Web.Html
{
    using System;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The Meta Tag Base.
    /// </summary>
    public abstract class HeadTagBase : IHeadTagBase
    {
        /// <summary>
        /// The content attribute name.
        /// </summary>
        private static readonly XName ContentAttribute = XName.Get("content");

        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName NameAttribute = XName.Get("name");

        /// <summary>
        /// Initializes a new instance of the <see cref="HeadTagBase"/> class.
        /// </summary>
        /// <param name="nameAttributeValue">The name attribute value.</param>
        /// <param name="content">The content.</param>
        protected HeadTagBase([Jig.Annotations.NotNull] string nameAttributeValue, [Jig.Annotations.NotNull] string content)
            : this()
        {
            this.Name = nameAttributeValue;
            this.Content = content;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="HeadTagBase"/> class from being created.
        /// </summary>
        private HeadTagBase()
        {
            this.MetaTag = new XElement("meta", new XAttribute(NameAttribute, string.Empty), new XAttribute(ContentAttribute, string.Empty));
        }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content
        {
            get
            {
                return this.MetaTag.Attribute(ContentAttribute).Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.MetaTag.Attribute(ContentAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the property.
        /// </summary>
        public string Name
        {
            get
            {
                return this.MetaTag.Attribute(NameAttribute).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.MetaTag.Attribute(NameAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public abstract string Id { get; }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        {
            get
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.MetaTag != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return this.MetaTag.Name.LocalName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the meta tag.
        /// </summary>
        /// <value>
        /// The meta.
        /// </value>
        [Jig.Annotations.NotNull]
        protected XElement MetaTag { get; private set; }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Control([Jig.Annotations.NotNull] HeadTagBase headTag)
        {
            var control = new Literal
                              {
                                  Mode = LiteralMode.PassThrough,
                                  Text = headTag
                              };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Literal([Jig.Annotations.NotNull] HeadTagBase headTag)
        {
            var control = new Literal
            {
                Mode = LiteralMode.PassThrough,
                Text = headTag
            };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator LiteralControl([Jig.Annotations.NotNull] HeadTagBase headTag)
        {
            var control = new LiteralControl
            {
                Text = headTag
            };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator string([Jig.Annotations.NotNull] HeadTagBase headTag)
        {
            return string.Concat((object)headTag);
        }

        /// <summary>
        /// HTMLs the string.
        /// </summary>
        /// <param name="headTag">The meta tag.</param>
        /// <returns>The html encoded string</returns>
        public static implicit operator HtmlString([Jig.Annotations.NotNull] HeadTagBase headTag)
        {
            return new HtmlString(headTag);
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(this.MetaTag, Environment.NewLine);
        }

        /// <summary>
        /// Serves as a hash function for a particular type. 
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public void RenderControl(HtmlTextWriter writer)
        {
            if (string.IsNullOrWhiteSpace(this.Content))
            {
                writer.WriteLineNoTabs("<!--" + this + "-->");
            }
            else
            {
                string html = this.MetaTag.ToString();
                writer.WriteLineNoTabs(html);
            }
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(this.Content);
        }
    }
}