﻿namespace Jig.Web.Html
{
    using System;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The Google Plus Meta Tag Base
    /// </summary>
    public abstract class GooglePlusHeadTagBase : IGoogleHeadTagBase
    {
        /// <summary>
        /// The content attribute name.
        /// </summary>
        private static readonly XName ContentAttributeName = XName.Get("content");

        /// <summary>
        /// The property attribute name.
        /// </summary>
        private static readonly XName PropertyAttributeName = XName.Get("itemprop");

        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusHeadTagBase" /> class.
        /// </summary>
        /// <param name="itemPropertyAttributeValue">The property attribute value.</param>
        /// <param name="content">The content.</param>
        protected GooglePlusHeadTagBase([Jig.Annotations.NotNull] string itemPropertyAttributeValue, [Jig.Annotations.NotNull]string content)
            : this()
        {
            this.ItemProperty = itemPropertyAttributeValue;
            this.Content = content;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="GooglePlusHeadTagBase"/> class from being created.
        /// </summary>
        private GooglePlusHeadTagBase()
        {
            this.MetaTag = new XElement("meta", new XAttribute("itemprop", string.Empty), new XAttribute(ContentAttributeName, string.Empty));
        }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content
        {
            get
            {
                return this.MetaTag.Attribute(ContentAttributeName).Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.MetaTag.Attribute(ContentAttributeName).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the property.
        /// </summary>
        public string ItemProperty
        {
            get
            {
                return this.MetaTag.Attribute(PropertyAttributeName).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.MetaTag.Attribute(PropertyAttributeName).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public abstract string Id { get; }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        {
            get
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.MetaTag != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return this.MetaTag.Name.LocalName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the meta tag.
        /// </summary>
        /// <value>
        /// The meta.
        /// </value>
        [Jig.Annotations.NotNull]
        protected XElement MetaTag { get; private set; }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Control([Jig.Annotations.NotNull] GooglePlusHeadTagBase headTag)
        {
            var control = new Literal
                              {
                                  Mode = LiteralMode.PassThrough,
                                  Text = string.Concat(headTag, string.Empty)
                              };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator string([Jig.Annotations.NotNull] GooglePlusHeadTagBase headTag)
        {
            return string.Concat((object)headTag);
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="headTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator HtmlString([Jig.Annotations.NotNull] GooglePlusHeadTagBase headTag)
        {
            return new HtmlString(headTag);
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(this.MetaTag, Environment.NewLine);
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public void RenderControl(HtmlTextWriter writer)
        {
            if (string.IsNullOrWhiteSpace(this.Content))
            {
                writer.WriteLineNoTabs("<!--" + this + "-->");
            }
            else
            {
                string html = this.MetaTag.ToString();
                writer.WriteLineNoTabs(html);
            }
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(this.Content);
        }
    }
}