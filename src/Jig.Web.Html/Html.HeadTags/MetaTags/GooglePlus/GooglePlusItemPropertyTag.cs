﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The GooglePlus Name Tag
    /// </summary>
    /// <example>
    /// <![CDATA[<meta itemprop="name" content="The Name or Title Here">]]>
    /// </example>
    [DisplayName("name")]
    public sealed class GooglePlusItemPropertyTag : GooglePlusHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusItemPropertyTag" /> class.
        /// </summary>
        /// <param name="itemprop">The itemprop.</param>
        /// <param name="content">The content.</param>
        public GooglePlusItemPropertyTag([Jig.Annotations.NotNull]string itemprop, [Jig.Annotations.NotNull]string content)
            : base(itemprop, content)
        {
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public override string Id
        {
            get
            {
                return this.ItemProperty;
            }
        }
    }
}