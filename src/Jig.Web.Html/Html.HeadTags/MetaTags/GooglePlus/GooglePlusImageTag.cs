﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The GooglePlus Image Tag
    /// </summary>
    /// <example>
    /// <![CDATA[<meta itemprop="image" content="http://www.example.com/image.jpg">]]>
    /// </example>
    [DisplayName("image")]
    public sealed class GooglePlusImageTag : GooglePlusHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusImageTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public GooglePlusImageTag([Jig.Annotations.NotNull]string content)
            : base("image", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "image";
            }
        }
    }
}