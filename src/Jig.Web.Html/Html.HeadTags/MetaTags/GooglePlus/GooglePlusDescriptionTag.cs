﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The GooglePlus Description Tag
    /// </summary>
    /// <example>
    /// <![CDATA[<meta itemprop="description" content="This is the page description">]]>
    /// </example>
    [DisplayName("description")]
    public sealed class GooglePlusDescriptionTag : GooglePlusHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusDescriptionTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public GooglePlusDescriptionTag([Jig.Annotations.NotNull]string content)
            : base("description", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "description";
            }
        }
    }
}