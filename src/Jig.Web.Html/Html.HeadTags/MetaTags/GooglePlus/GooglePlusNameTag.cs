﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The GooglePlus Name Tag
    /// </summary>
    /// <example>
    /// <![CDATA[<meta itemprop="name" content="This is the page description">]]>
    /// </example>
    [DisplayName("description")]
    public sealed class GooglePlusNameTag : GooglePlusHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusNameTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public GooglePlusNameTag([Jig.Annotations.NotNull]string content)
            : base("name", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "name";
            }
        }
    }
}