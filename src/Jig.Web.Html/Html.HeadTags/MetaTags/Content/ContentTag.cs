﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The Content tag.
    /// </summary>
    /// <example>
    /// <![CDATA[<meta name="{name}" content="{content}">]]>
    /// </example>
    [DisplayName("meta: name={name} conten={content}")]
    public sealed class ContentTag : HeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTag" /> class.
        /// </summary>
        /// <param name="tagName">Name of the tag.</param>
        /// <param name="content">The content.</param>
        public ContentTag([Jig.Annotations.NotNull]string tagName, [Jig.Annotations.NotNull]string content)
            : base(tagName, content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "content:" + this.Name;
            }
        }
    }
}