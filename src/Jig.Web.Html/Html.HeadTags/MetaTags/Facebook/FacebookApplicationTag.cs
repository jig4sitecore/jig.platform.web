﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>The facebook application tag</remarks>
    [DisplayName("fb:app_id")]
    public sealed class FacebookApplicationTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacebookApplicationTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public FacebookApplicationTag([Jig.Annotations.NotNull]string content)
            : base("fb:app_id", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "fb:app_id";
            }
        }
    }
}