﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>profile:first_name - string - A name normally given to an individual by a parent or self-chosen.</remarks>
    [DisplayName("profile:first_name")]
    public sealed class ProfileFirstNameTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileFirstNameTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ProfileFirstNameTag([Jig.Annotations.NotNull]string content)
            : base("profile:first_name", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "profile:first_name";
            }
        }
    }
}