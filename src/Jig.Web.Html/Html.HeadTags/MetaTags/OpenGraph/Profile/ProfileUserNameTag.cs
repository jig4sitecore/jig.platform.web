﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>profile:username - string - A short unique string to identify them.</remarks>
    [DisplayName("profile:username")]
    public sealed class ProfileUserNameTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileUserNameTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ProfileUserNameTag([Jig.Annotations.NotNull]string content)
            : base("profile:username", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "profile:username";
            }
        }
    }
}