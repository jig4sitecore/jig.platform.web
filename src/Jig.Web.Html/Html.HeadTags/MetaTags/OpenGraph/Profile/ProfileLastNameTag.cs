﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>profile:last_name - string - A name inherited from a family or marriage and by which the individual is commonly known.</remarks>
    [DisplayName("profile:last_name")]
    public sealed class ProfileLastNameTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileLastNameTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ProfileLastNameTag([Jig.Annotations.NotNull]string content)
            : base("profile:last_name", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "profile:last_name";
            }
        }
    }
}