﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>article:tag - string array - Tag words associated with this article.</remarks>
    [DisplayName("article:tag")]
    public sealed class ArticleTags : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleTags" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticleTags([Jig.Annotations.NotNull]string content)
            : base("article:tag", content)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleTags"/> class.
        /// </summary>
        /// <param name="tags">The tags.</param>
        public ArticleTags([Jig.Annotations.NotNull]params string[] tags)
            : base("article:tag", string.Join(" ,", tags))
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:tag";
            }
        }
    }
}