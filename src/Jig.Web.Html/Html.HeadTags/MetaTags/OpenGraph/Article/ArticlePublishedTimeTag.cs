﻿namespace Jig.Web.Html
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>article:published_time - datetime - When the article was first published</remarks>
    [DisplayName("article:published_time")]
    public sealed class ArticlePublishedTimeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticlePublishedTimeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticlePublishedTimeTag([Jig.Annotations.NotNull]string content)
            : base("article:published_time", content)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArticlePublishedTimeTag"/> class.
        /// </summary>
        /// <param name="time">The time.</param>
        public ArticlePublishedTimeTag(DateTime time)
            : base("article:published_time", time.ToString("u"))
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:published_time";
            }
        }
    }
}