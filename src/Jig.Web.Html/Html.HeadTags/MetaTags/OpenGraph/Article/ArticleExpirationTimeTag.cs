﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>article:expiration_time - datetime - When the article is out of date after.</remarks>
    [DisplayName("article:expiration_time")]
    public sealed class ArticleExpirationTimeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleExpirationTimeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticleExpirationTimeTag([Jig.Annotations.NotNull]string content)
            : base("article:expiration_time", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:expiration_time";
            }
        }
    }
}