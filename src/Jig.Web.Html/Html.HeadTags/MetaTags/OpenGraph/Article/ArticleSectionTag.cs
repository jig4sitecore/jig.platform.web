﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>article:section - string - A high-level section name. E.g. Technology</remarks>
    [DisplayName("article:section")]
    public sealed class ArticleSectionTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleSectionTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticleSectionTag([Jig.Annotations.NotNull]string content)
            : base("article:section", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:section";
            }
        }
    }
}