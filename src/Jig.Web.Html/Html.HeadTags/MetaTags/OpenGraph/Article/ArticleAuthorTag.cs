﻿namespace Jig.Web.Html
{
    using System.ComponentModel;
    using System.Runtime.InteropServices;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>article:author - profile array - Writers of the article.</remarks>
    [DisplayName("article:author"), Guid("929AF72F-E4F5-4819-9680-D1CD0445A6EF")]
    public sealed class ArticleAuthorTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleAuthorTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticleAuthorTag([Jig.Annotations.NotNull]string content)
            : base("article:author", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:author";
            }
        }
    }
}