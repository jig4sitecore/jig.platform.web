﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>
    /// article:Modified_time - datetime - When the article was last modified
    /// </remarks>
    [DisplayName("article:modified_time")]
    public sealed class ArticleModifiedTimeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ArticleModifiedTimeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public ArticleModifiedTimeTag([Jig.Annotations.NotNull]string content)
            : base("article:modified_time", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "article:modified_time";
            }
        }
    }
}