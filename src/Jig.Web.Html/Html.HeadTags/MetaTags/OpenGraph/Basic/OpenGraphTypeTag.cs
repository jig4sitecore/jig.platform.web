﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:type - The type of your object, e.g., "video.movie". Depending on the type you specify, other properties may</remarks>
    [DisplayName("og:type")]
    public sealed class OpenGraphTypeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphTypeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphTypeTag([Jig.Annotations.NotNull]string content)
            : base("og:type", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:type";
            }
        }
    }
}