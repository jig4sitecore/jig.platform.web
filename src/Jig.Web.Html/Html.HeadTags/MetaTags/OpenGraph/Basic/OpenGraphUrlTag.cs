﻿namespace Jig.Web.Html
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:url - The canonical URL of your object that will be used as its permanent ID in the graph, e.g., "http://www.imdb.com/title/tt0117500/".</remarks>
    [DisplayName("og:url")]
    public sealed class OpenGraphUrlTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphUrlTag"/> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        public OpenGraphUrlTag([Jig.Annotations.NotNull]Uri uri)
            : base("og:url", string.Concat(uri))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphUrlTag"/> class.
        /// </summary>
        /// <param name="uri">The URI.</param>
        public OpenGraphUrlTag([Jig.Annotations.NotNull]string uri)
            : base("og:url", uri)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:url";
            }
        }
    }
}