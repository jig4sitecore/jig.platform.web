﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:title - The title of your object as it should appear within the graph, e.g., "The Rock".</remarks>
    [DisplayName("og:title")]
    public sealed class OpenGraphTitleTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphTitleTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphTitleTag([Jig.Annotations.NotNull]string content)
            : base("og:title", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:title";
            }
        }
    }
}