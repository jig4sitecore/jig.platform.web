﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:image - An image URL which should represent your object within the graph.</remarks>
    [DisplayName("og:image")]
    public sealed class OpenGraphImageTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphImageTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphImageTag([Jig.Annotations.NotNull]string content)
            : base("og:image", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:image";
            }
        }
    }
}