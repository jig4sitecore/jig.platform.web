﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:image:secure_url - An alternate url to use if the web page requires HTTPS.</remarks>
    [DisplayName("og:image:secure_url")]
    public sealed class OpenGraphImageSecureUrlTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphImageSecureUrlTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphImageSecureUrlTag([Jig.Annotations.NotNull]string content)
            : base("og:image:secure_url", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:image:secure_url";
            }
        }
    }
}