﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:video:secure_url - An alternate url to use if the web page requires HTTPS.</remarks>
    [DisplayName("og:video:secure_url")]
    public sealed class OpenGraphVideoSecureUrlTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphVideoSecureUrlTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphVideoSecureUrlTag([Jig.Annotations.NotNull]string content)
            : base("og:video:secure_url", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:video:secure_url";
            }
        }
    }
}