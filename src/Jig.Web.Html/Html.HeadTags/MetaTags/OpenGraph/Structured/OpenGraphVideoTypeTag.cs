﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:video:type - A MIME type for this video.</remarks>
    [DisplayName("og:video:type")]
    public sealed class OpenGraphVideoTypeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphVideoTypeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphVideoTypeTag([Jig.Annotations.NotNull]string content)
            : base("og:video:type", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:video:type";
            }
        }
    }
}