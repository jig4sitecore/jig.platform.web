﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:video:height - The number of pixels high.</remarks>
    [DisplayName("og:video:height"), System.Runtime.InteropServices.GuidAttribute("1A8C4615-36CC-45A9-BD5A-02AF50B3E630")]
    public sealed class OpenGraphVideoHeightTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphVideoHeightTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphVideoHeightTag([Jig.Annotations.NotNull]string content)
            : base("og:video:height", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:video:height";
            }
        }
    }
}