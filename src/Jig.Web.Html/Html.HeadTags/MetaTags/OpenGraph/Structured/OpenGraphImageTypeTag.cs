﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:image:type - A MIME type for this image.</remarks>
    [DisplayName("og:image:type")]
    public sealed class OpenGraphImageTypeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphImageTypeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphImageTypeTag([Jig.Annotations.NotNull]string content)
            : base("og:image:type", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:image:type";
            }
        }
    }
}