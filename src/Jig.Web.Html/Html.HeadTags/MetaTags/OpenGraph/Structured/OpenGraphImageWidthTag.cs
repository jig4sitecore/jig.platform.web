﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:image:width - The number of pixels wide.</remarks>
    [DisplayName("og:image:width")]
    public sealed class OpenGraphImageWidthTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphImageWidthTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphImageWidthTag([Jig.Annotations.NotNull]string content)
            : base("og:image:width", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:image:width";
            }
        }
    }
}