﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:image:height - The number of pixels high.</remarks>
    [DisplayName("og:image:height")]
    public sealed class OpenGraphImageHeightTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphImageHeightTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphImageHeightTag([Jig.Annotations.NotNull]string content)
            : base("og:image:height", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:image:height";
            }
        }
    }
}