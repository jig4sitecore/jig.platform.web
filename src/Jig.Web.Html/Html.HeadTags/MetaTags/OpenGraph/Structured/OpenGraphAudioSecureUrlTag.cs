﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:audio:secure_url - An alternate url to use if the web page requires HTTPS.</remarks>
    [DisplayName("og:audio:secure_url")]
    public sealed class OpenGraphAudioSecureUrlTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphAudioSecureUrlTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphAudioSecureUrlTag([Jig.Annotations.NotNull]string content)
            : base("og:audio:secure_url", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:audio:secure_url";
            }
        }
    }
}