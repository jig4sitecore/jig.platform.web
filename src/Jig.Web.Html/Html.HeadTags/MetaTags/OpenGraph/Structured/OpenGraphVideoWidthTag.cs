﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:video:width - The number of pixels wide.</remarks>
    [DisplayName("og:video:width")]
    public sealed class OpenGraphVideoWidthTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphVideoWidthTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphVideoWidthTag([Jig.Annotations.NotNull]string content)
            : base("og:video:width", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:video:width";
            }
        }
    }
}