﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>
    /// og:audio:type - A MIME type for this audio.
    /// </remarks>
    [DisplayName("og:audio:type")]
    public sealed class OpenGraphAudioTypeTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphAudioTypeTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphAudioTypeTag([Jig.Annotations.NotNull]string content)
            : base("og:audio:type", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:audio:type";
            }
        }
    }
}