﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:video - A URL to a video file that complements this object.</remarks>
    [DisplayName("og:video")]
    public sealed class OpenGraphVideoTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphVideoTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphVideoTag([Jig.Annotations.NotNull]string content)
            : base("og:video", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:video";
            }
        }
    }
}