﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:audio - A URL to an audio file to accompany this object.</remarks>
    [DisplayName("og:audio")]
    public sealed class OpenGraphAudioTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphAudioTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphAudioTag([Jig.Annotations.NotNull]string content)
            : base("og:audio", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:audio";
            }
        }
    }
}