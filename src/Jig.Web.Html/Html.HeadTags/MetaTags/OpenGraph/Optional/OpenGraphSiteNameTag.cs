﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:site_name - If your object is part of a larger web site, the name which should be displayed for the overall site. e.g., "IMDb".</remarks>
    [DisplayName("og:site_name")]
    public sealed class OpenGraphSiteNameTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphSiteNameTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphSiteNameTag([Jig.Annotations.NotNull]string content)
            : base("og:site_name", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:site_name";
            }
        }
    }
}