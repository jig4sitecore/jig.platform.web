﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:locale - The locale these tags are marked up in. Of the format language_TERRITORY. Default is en_US.</remarks>
    [DisplayName("og:locale")]
    public sealed class OpenGraphLocaleTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphLocaleTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphLocaleTag([Jig.Annotations.NotNull]string content)
            : base("og:locale", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:locale";
            }
        }
    }
}