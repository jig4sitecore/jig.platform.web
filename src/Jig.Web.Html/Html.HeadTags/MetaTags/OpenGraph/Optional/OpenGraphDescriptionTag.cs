﻿namespace Jig.Web.Html
{
    using System.ComponentModel;

    /// <summary>
    /// The open graph tag.
    /// </summary>
    /// <remarks>og:description - A one to two sentence description of your object.</remarks>
    [DisplayName("og:description")]
    public sealed class OpenGraphDescriptionTag : OpenGraphHeadTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenGraphDescriptionTag" /> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenGraphDescriptionTag([Jig.Annotations.NotNull]string content)
            : base("og:description", content)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public override string Id
        {
            get
            {
                return "og:description";
            }
        }
    }
}