﻿namespace Jig.Web.Html
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The Link Tag Base.
    /// </summary>
    public abstract class LinkTagBase : ILinkTagBase, IComparer<LinkTagBase>
    {
        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName HrefAttribute = XName.Get("href");

        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName RelAttribute = XName.Get("rel");

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkTagBase"/> class.
        /// </summary>
        /// <param name="relAttributeValue">
        /// The name attribute value.
        /// </param>
        protected LinkTagBase([Jig.Annotations.NotNull] string relAttributeValue)
            : this()
        {
            this.Rel = relAttributeValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkTagBase"/> class.
        /// </summary>
        /// <param name="relAttributeValue">The relative attribute value.</param>
        /// <param name="hrefAttributeValue">The href attribute value.</param>
        protected LinkTagBase([Jig.Annotations.NotNull] string relAttributeValue, [Jig.Annotations.NotNull] string hrefAttributeValue)
            : this()
        {
            this.Rel = relAttributeValue;
            this.Href = hrefAttributeValue;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="LinkTagBase"/> class from being created.
        /// </summary>
        private LinkTagBase()
        {
            this.LinkTag = new XElement("link", new XAttribute(RelAttribute, string.Empty), new XAttribute(HrefAttribute, string.Empty));
        }

        /// <summary>
        /// Gets or sets the Href attribute value.
        /// </summary>
        public string Href
        {
            get
            {
                return this.LinkTag.Attribute(HrefAttribute).Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.LinkTag.Attribute(HrefAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the rel attribute value.
        /// </summary>
        public string Rel
        {
            get
            {
                return this.LinkTag.Attribute(RelAttribute).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.LinkTag.Attribute(RelAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public string Id
        {
            get
            {
                return this.Rel;
            }
        }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        {
            get
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.LinkTag != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return this.LinkTag.Name.LocalName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the meta tag.
        /// </summary>
        /// <value>
        /// The meta.
        /// </value>
        [Jig.Annotations.NotNull]
        // ReSharper disable MemberCanBePrivate.Global
        protected XElement LinkTag { get; private set; }
        // ReSharper restore MemberCanBePrivate.Global

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Control([Jig.Annotations.NotNull] LinkTagBase metaTag)
        {
            var control = new Literal
                              {
                                  Mode = LiteralMode.PassThrough,
                                  Text = string.Concat(metaTag, string.Empty)
                              };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator string([Jig.Annotations.NotNull] LinkTagBase metaTag)
        {
            return string.Concat((object)metaTag);
        }

        /// <summary>
        /// HTMLs the string.
        /// </summary>
        /// <param name="metaTag">The meta tag.</param>
        /// <returns>The html encoded string</returns>
        public static implicit operator HtmlString([Jig.Annotations.NotNull] LinkTagBase metaTag)
        {
            return new HtmlString(metaTag);
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(this.LinkTag, Environment.NewLine);
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrWhiteSpace(this.Href))
            {
                writer.WriteLineNoTabs("<!--" + this + "-->");
            }
            else
            {
                string html = this.LinkTag.ToString();
                writer.WriteLineNoTabs(html);
            }
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.
        /// </returns>
        public int Compare([Jig.Annotations.NotNull] LinkTagBase x, [Jig.Annotations.NotNull] LinkTagBase y)
        {
            return string.Compare(x.Id, y.Id, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(this.Href);
        }
    }
}