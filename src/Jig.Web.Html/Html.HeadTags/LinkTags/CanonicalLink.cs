﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The canonical link tag.
    /// </summary>
    public sealed class CanonicalLinkTag : LinkTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CanonicalLinkTag" /> class.
        /// </summary>
        /// <param name="href">The href.</param>
        public CanonicalLinkTag(string href)
            : base(TagId, href)
        {
            /* <link href="{url}" rel="canonical" /> */
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public static string TagId
        {
            get
            {
                return "canonical";
            }
        }
    }
}