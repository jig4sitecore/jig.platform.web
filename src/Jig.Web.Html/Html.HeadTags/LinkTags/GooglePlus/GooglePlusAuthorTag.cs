﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Google tag.
    /// </summary>
    public sealed class GooglePlusAuthorTag : LinkTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusAuthorTag"/> class.
        /// </summary>
        /// <param name="googlePlusProfile">The google plus profile.</param>
        public GooglePlusAuthorTag([Jig.Annotations.NotNull] string googlePlusProfile)
            : base(TagId, "https://plus.google.com/" + googlePlusProfile + "/posts")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusAuthorTag"/> class.
        /// </summary>
        public GooglePlusAuthorTag()
            : base(TagId)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public static string TagId
        {
            get
            {
                return "author";
            }
        }
    }
}