﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The Google tag.
    /// </summary>
    public sealed class GooglePlusPublisherTag : LinkTagBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusPublisherTag"/> class.
        /// </summary>
        public GooglePlusPublisherTag()
            : base(TagId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GooglePlusPublisherTag"/> class.
        /// </summary>
        /// <param name="googlePlusPageProfile">The google plus page profile.</param>
        public GooglePlusPublisherTag([Jig.Annotations.NotNull] string googlePlusPageProfile)
            : base(TagId, "https://plus.google.com/" + googlePlusPageProfile)
        {
        }

        /// <summary>
        /// Gets the tag unique identifier.
        /// </summary>
        /// <value>
        /// The tag unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        public static string TagId
        {
            get
            {
                return "publisher";
            }
        }
    }
}