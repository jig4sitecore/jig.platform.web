﻿namespace Jig.Web.Html
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    /// The head tags.
    /// </summary>
    public sealed partial class HeadTagCollection : IRenderControlView, IDisposable
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HeadTagCollection"/> class.
        /// </summary>
        /// <param name="httpContext">
        /// The http context.
        /// </param>
        public HeadTagCollection([Jig.Annotations.NotNull] HttpContext httpContext)
            : this()
        {
            if (httpContext == null)
            {
                throw new HttpException("The httpContext cannot be null!");
            }

            this.Context = new HttpContextWrapper(httpContext);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeadTagCollection"/> class.
        /// </summary>
        /// <param name="httpContext">
        /// The http context.
        /// </param>
        public HeadTagCollection([Jig.Annotations.NotNull] HttpContextBase httpContext)
            : this()
        {
            if (httpContext == null)
            {
                throw new HttpException("The httpContext cannot be null!");
            }

            this.Context = httpContext;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="HeadTagCollection"/> class from being created.
        /// </summary>
        private HeadTagCollection()
        {
            this.Tags = new Dictionary<string, IHeadTag>(StringComparer.InvariantCultureIgnoreCase);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the keys.
        /// </summary>
        /// <value>
        /// The keys.
        /// </value>
        [Jig.Annotations.NotNull]
        public ICollection<string> Keys
        {
            get
            {
                return this.Tags.Keys.ToArray();
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the context.
        /// </summary>
        [Jig.Annotations.NotNull]
        private HttpContextBase Context { get; set; }

        /// <summary>
        /// Gets or sets the tags.
        /// </summary>
        [Jig.Annotations.NotNull]
        private IDictionary<string, IHeadTag> Tags { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Control([Jig.Annotations.NotNull] HeadTagCollection metaTag)
        {
            var control = new Literal { Mode = LiteralMode.PassThrough, Text = metaTag };

            return control;
        }

        /// <summary>
        ///     The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator LiteralControl([Jig.Annotations.NotNull] HeadTagCollection metaTag)
        {
            var control = new LiteralControl { Text = metaTag };

            return control;
        }

        /// <summary>
        ///     The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator string([Jig.Annotations.NotNull] HeadTagCollection metaTag)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return metaTag == null ? string.Empty : metaTag.RenderControl();

            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        ///     The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator HtmlString([Jig.Annotations.NotNull] HeadTagCollection metaTag)
        {
            return new HtmlString(metaTag);
        }

        /// <summary>
        /// The add.
        /// </summary>
        /// <param name="tag">The tag control.</param>
        public void Add([Jig.Annotations.NotNull] IHeadTag tag)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (tag != null && tag.Validate())
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                this.Tags[tag.Id] = tag;
            }
        }

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="tags">The tags.</param>
        public void AddRange([Jig.Annotations.NotNull]IEnumerable<IHeadTag> tags)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (tags != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var tag in tags)
                {
                    this.Add(tag);
                }
            }
        }

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="tags">The tags.</param>
        public void AddRange([Jig.Annotations.NotNull]params IHeadTag[] tags)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (tags != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var tag in tags)
                {
                    this.Add(tag);
                }
            }
        }

        /// <summary>
        /// Determines whether [contains meta tag] [the specified type].
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// true if the HeadTagCollection contains an element with the key; otherwise, false.
        /// </returns>
        public bool ContainsMetaTag([Jig.Annotations.NotNull] Type type)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type != null)
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                return this.Tags.ContainsKey(type.Name);
            }

            return false;
        }

        /// <summary>
        /// Determines whether [contains meta tag] [the specified identifier].
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// true if the HeadTagCollection contains an element with the key; otherwise, false.
        /// </returns>
        public bool ContainsMetaTag([Jig.Annotations.NotNull] string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                return this.Tags.ContainsKey(id);
            }

            return false;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Context.Items[HeadTagExtensions.ContextItemName] = this;
        }

        /// <summary>
        /// The find tag.
        /// </summary>
        /// <typeparam name="TMetaTagControl">The type of the meta tag control.</typeparam>
        /// <param name="id">The id.</param>
        /// <returns>
        /// The <see cref="TMetaTagControl" />.
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public TMetaTagControl FindTag<TMetaTagControl>([Jig.Annotations.NotNull] string id) where TMetaTagControl : class, IHeadTag
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var tag = this.Tags[id] as TMetaTagControl;
                return tag;
            }

            return default(TMetaTagControl);
        }

        /// <summary>
        /// The render control.
        /// </summary>
        /// <param name="writer">
        /// The writer.
        /// </param>
        public void RenderControl(HtmlTextWriter writer)
        {
            foreach (IHeadTag metaTagControl in this.Tags.Values.OrderBy(x => x.TagName).ThenBy(x => x.Id).ToArray())
            {
                if (metaTagControl != null)
                {
                    metaTagControl.RenderControl(writer);
                }
            }
        }

        /// <summary>
        /// Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            using (var writer = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
            {
                foreach (IHeadTag metaTagControl in this.Tags.Values)
                {
                    if (metaTagControl != null)
                    {
                        metaTagControl.RenderControl(writer);
                    }
                }

                return writer.InnerWriter.ToString();
            }
        }

        /// <summary>
        /// Tries the get htm head tag by id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="control">The control.</param>
        /// <returns>
        /// True if tag is found
        /// </returns>
        public bool TryGetValue([Jig.Annotations.NotNull] string id, out IHeadTag control)
        {
            return this.Tags.TryGetValue(id, out control);
        }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        /// <returns>The list of the heat tags</returns>
        public IList<KeyValuePair<string, IHeadTag>> GetTags()
        {
            var value = this.Tags.ToArray();
            return value;
        }

        /// <summary>
        /// Gets the tags as HTML list.
        /// </summary>
        /// <returns>The list of rendered tags</returns>
        public IList<string> GetTagsAsHtmlList()
        {
            /* Fail safe and partially only. Not critical page error */
            var list = new List<string>();
            foreach (var tag in this.Tags)
            {
                HtmlTextWriter writer = null;
                try
                {
                    writer = new HtmlTextWriter(new StringWriter(new StringBuilder(128)));

                    tag.Value.RenderControl(writer);

                    var html = writer.InnerWriter.ToString();
                    list.Add(html);
                }
                catch (Exception exception)
                {
                    var meta = new XElement("meta", new XAttribute("name", "server-side-error"), new XAttribute("content", exception.Message));
                    list.Add(meta.ToString());
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Dispose();
                    }
                }
            }

            return list;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Renders the control.
        /// </summary>
        /// <returns>The Html  head tags</returns>
        [Jig.Annotations.NotNull]
        private string RenderControl()
        {
            using (var writer = new HtmlTextWriter(new StringWriter(new StringBuilder(512))))
            {
                this.RenderControl(writer);

                return writer.InnerWriter.ToString();
            }
        }

        #endregion
    }
}