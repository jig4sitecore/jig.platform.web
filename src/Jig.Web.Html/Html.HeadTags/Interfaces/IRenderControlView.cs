﻿namespace Jig.Web.Html
{
    using System.Web.UI;

    /// <summary>
    /// The RenderControl interface.
    /// </summary>
    public interface IRenderControlView
    {
        #region Public Methods and Operators

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object and stores
        ///     tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">
        /// The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.
        /// </param>
        void RenderControl([Jig.Annotations.NotNull] HtmlTextWriter writer);

        #endregion
    }
}