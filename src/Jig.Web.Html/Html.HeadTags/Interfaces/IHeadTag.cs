﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The MetaTag Control interface.
    /// </summary>
    public interface IHeadTag : IRenderControlView
    {
        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        [Jig.Annotations.NotNull]
        string Id { get; }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        [Jig.Annotations.NotNull]
        string TagName { get; }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        bool Validate();
    }
}