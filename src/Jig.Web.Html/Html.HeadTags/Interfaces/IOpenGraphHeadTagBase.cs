﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The OpenGraphHeadTagBase interface.
    /// </summary>
    public interface IOpenGraphHeadTagBase : IHeadTag
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the content.
        /// </summary>
        [Jig.Annotations.CanBeNull]
        string Content { get; set; }

        /// <summary>
        ///     Gets the property.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Property { get; }

        #endregion
    }
}