namespace Jig.Web.Html
{
    /// <summary>
    /// Link Tag Base
    /// </summary>
    public interface ILinkTagBase : IHeadTag
    {
        /// <summary>
        /// Gets or sets the Href attribute value.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Href { get; set; }

        /// <summary>
        /// Gets the rel attribute value.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Rel { get; }
    }
}