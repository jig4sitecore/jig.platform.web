﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The MetaTag Base interface.
    /// </summary>
    public interface IHeadTagBase : IHeadTag
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the content.
        /// </summary>
        [Jig.Annotations.CanBeNull]
        string Content { get; set; }

        /// <summary>
        ///     Gets the property.
        /// </summary>
        [Jig.Annotations.NotNull]
        string Name { get; }

        #endregion
    }
}