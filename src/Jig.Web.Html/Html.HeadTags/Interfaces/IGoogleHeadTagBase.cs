﻿namespace Jig.Web.Html
{
    /// <summary>
    /// The GooglePlusHeadTagBase interface.
    /// </summary>
    public interface IGoogleHeadTagBase : IHeadTag
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the content.
        /// </summary>
        [Jig.Annotations.CanBeNull]
        string Content { get; set; }

        /// <summary>
        ///     Gets the property.
        /// </summary>
        [Jig.Annotations.NotNull]
        string ItemProperty { get; }

        #endregion
    }
}