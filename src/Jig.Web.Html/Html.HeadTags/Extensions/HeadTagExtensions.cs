﻿namespace Jig.Web.Html
{
    using System.Web;

    /// <summary>
    /// The Head Tag extensions.
    /// </summary>
    public static partial class HeadTagExtensions
    {
        #region Constants

        /// <summary>
        /// The context item name.
        /// </summary>
        public static readonly string ContextItemName = typeof(HeadTagCollection).FullName;

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The get html head tags.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>
        /// The HeadTagCollection.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static HeadTagCollection GetHtmlHeadTags([Jig.Annotations.NotNull] this HttpContext context)
        {
            var tags = context.Items[ContextItemName] as HeadTagCollection;
            if (tags == null)
            {
                tags = new HeadTagCollection(context);
                context.Items[ContextItemName] = tags;
            }

            return tags;
        }

        /// <summary>
        /// Gets the html head tags.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The HeadTagCollection.</returns>
        [Jig.Annotations.NotNull]
        public static HeadTagCollection GetHtmlHeadTags([Jig.Annotations.NotNull] this HttpContextBase context)
        {
            var tags = context.Items[ContextItemName] as HeadTagCollection;
            if (tags == null)
            {
                tags = new HeadTagCollection(context);
                context.Items[ContextItemName] = tags;
            }

            return tags;
        }

        #endregion
    }
}