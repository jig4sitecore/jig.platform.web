﻿namespace Jig.Web.Html
{
    using System.Xml.Linq;

    /// <summary>
    /// The Link Head Tag.
    /// </summary>
    public sealed class LinkHeadTag : LinkTagBase
    {
        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName TypeAttribute = XName.Get("type");

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkHeadTag" /> class.
        /// </summary>
        public LinkHeadTag()
            : base("stylesheet")
        {
            this.Type = "text/css";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkHeadTag" /> class.
        /// </summary>
        /// <param name="hrefAttributeValue">The href attribute value.</param>
        /// <param name="relAttributeValue">The relative attribute value.</param>
        public LinkHeadTag([Jig.Annotations.NotNull]string hrefAttributeValue, [Jig.Annotations.NotNull] string relAttributeValue = "stylesheet")
            : base(relAttributeValue, hrefAttributeValue)
        {
            this.Type = "text/css";
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>The type.</value>
        [Jig.Annotations.NotNull]
        public string Type
        {
            get
            {
                return this.LinkTag.Attribute(TypeAttribute).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.LinkTag.Attribute(TypeAttribute).Value = value;
                }
            }
        }
    }
}