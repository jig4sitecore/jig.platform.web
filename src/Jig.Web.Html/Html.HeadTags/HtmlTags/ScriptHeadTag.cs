﻿namespace Jig.Web.Html
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Xml.Linq;

    /// <summary>
    ///  The JS Script tag
    /// </summary>
    public sealed class ScriptHeadTag : IHeadTag, IComparer<ScriptHeadTag>
    {
        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName SrcAttribute = XName.Get("src");

        /// <summary>
        /// The attribute name.
        /// </summary>
        private static readonly XName TypeAttribute = XName.Get("type");

        /// <summary>
        /// The asynchronous attribute
        /// </summary>
        private static readonly XName AsyncAttribute = XName.Get("async");

        /// <summary>
        /// Initializes a new instance of the <see cref="ScriptHeadTag" /> class.
        /// </summary>
        /// <param name="srcAttributeValue">The src attribute value.</param>
        /// <param name="typeAttributeValue">The type attribute value.</param>
        public ScriptHeadTag([Jig.Annotations.NotNull] string srcAttributeValue, [Jig.Annotations.NotNull] string typeAttributeValue = "text/javascript")
            : this()
        {
            this.Src = srcAttributeValue;
            this.Type = typeAttributeValue;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ScriptHeadTag"/> class from being created.
        /// </summary>
        private ScriptHeadTag()
        {
            this.ScriptTag = new XElement("script", new XAttribute(SrcAttribute, string.Empty), new XAttribute(TypeAttribute, string.Empty));
        }

        /// <summary>
        /// Gets or sets the Src attribute value.
        /// </summary>
        [Jig.Annotations.NotNull]
        public string Src
        {
            get
            {
                return this.ScriptTag.Attribute(SrcAttribute).Value;
            }

            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.ScriptTag.Attribute(SrcAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <value>The asynchronous.</value>
        [Jig.Annotations.NotNull]
        public string Async
        {
            get
            {
                return this.ScriptTag.Attribute(AsyncAttribute).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.ScriptTag.Attribute(AsyncAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the type attribute.
        /// </summary>
        /// <value>The type attribute.</value>
        [Jig.Annotations.NotNull]
        public string Type
        {
            get
            {
                return this.ScriptTag.Attribute(TypeAttribute).Value;
            }

            private set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.ScriptTag.Attribute(TypeAttribute).Value = value;
                }
            }
        }

        /// <summary>
        /// Gets the unique identifier.
        /// </summary>
        /// <value>
        /// The unique identifier.
        /// </value>
        public string Id
        {
            get
            {
                return this.Type;
            }
        }

        /// <summary>
        /// Gets the name of the tag.
        /// </summary>
        /// <value>
        /// The name of the tag.
        /// </value>
        public string TagName
        {
            get
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (this.ScriptTag != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return this.ScriptTag.Name.LocalName;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the script tag.
        /// </summary>
        /// <value>The meta.</value>
        [Jig.Annotations.NotNull]
        private XElement ScriptTag { get; set; }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The control instance from tag</returns>
        public static implicit operator Control([Jig.Annotations.NotNull] ScriptHeadTag metaTag)
        {
            var control = new Literal
            {
                Mode = LiteralMode.PassThrough,
                Text = string.Concat(metaTag, string.Empty)
            };

            return control;
        }

        /// <summary>
        /// The implicit operator.
        /// </summary>
        /// <param name="metaTag">The tag.</param>
        /// <returns>The string value of tag</returns>
        public static implicit operator string([Jig.Annotations.NotNull] ScriptHeadTag metaTag)
        {
            return string.Concat((object)metaTag);
        }

        /// <summary>
        /// HTMLs the string.
        /// </summary>
        /// <param name="metaTag">The meta tag.</param>
        /// <returns>The html encoded string</returns>
        public static implicit operator HtmlString([Jig.Annotations.NotNull] ScriptHeadTag metaTag)
        {
            return new HtmlString(metaTag);
        }

        /// <summary>
        ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </summary>
        /// <returns>
        ///     A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.
        /// </returns>
        public override string ToString()
        {
            return string.Concat(this.ScriptTag, Environment.NewLine);
        }

        /// <summary>
        /// Outputs server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter" /> object and stores
        /// tracing information about the control if tracing is enabled.
        /// </summary>
        /// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter" /> object that receives the control content.</param>
        public void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            if (string.IsNullOrWhiteSpace(this.Src))
            {
                writer.WriteLineNoTabs("<!--" + this + "-->");
            }
            else
            {
                string html = this.ScriptTag.ToString();
                writer.WriteLineNoTabs(html);
            }
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.Value Meaning Less than zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than <paramref name="y" />.
        /// </returns>
        public int Compare([Jig.Annotations.NotNull] ScriptHeadTag x, [Jig.Annotations.NotNull] ScriptHeadTag y)
        {
            return string.Compare(x.Id, y.Id, StringComparison.InvariantCultureIgnoreCase);
        }

        /// <summary>
        /// Validates this instance.
        /// </summary>
        /// <returns>True if valid otherwise false</returns>
        public bool Validate()
        {
            return !string.IsNullOrWhiteSpace(this.Src);
        }
    }
}