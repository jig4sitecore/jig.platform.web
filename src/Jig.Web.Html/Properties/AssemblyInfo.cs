﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("06e62a17-f909-4a02-bf87-c1cd0bc39229")]
[assembly: AssemblyTitle("Jig Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig Team")]
[assembly: AssemblyProduct("Jig Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig Web Team")]
[assembly: AssemblyTrademark("Jig Team")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
