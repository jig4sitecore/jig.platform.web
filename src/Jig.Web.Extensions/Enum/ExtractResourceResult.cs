﻿namespace Jig.Web
{
    /// <summary>
    /// The extract resource result.
    /// </summary>
    [System.Serializable]
    public enum ExtractResourceResult
    {
        /// <summary>
        /// The created.
        /// </summary>
        Created, 

        /// <summary>
        /// The failed.
        /// </summary>
        Failed, 

        /// <summary>
        /// The exists.
        /// </summary>
        Exists
    }
}