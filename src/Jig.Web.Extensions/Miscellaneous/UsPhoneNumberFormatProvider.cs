﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UsPhoneNumberFormatProvider.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     M.Gramolini
//  Date:       04/27/2012
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// Us Phone Number Formatter
    /// </summary>
    public sealed partial class UsPhoneNumberFormatProvider : IFormatProvider, ICustomFormatter
    {
        /// <summary>
        /// Returns an object that provides formatting services for the specified type.
        /// </summary>
        /// <param name="formatType">An object that specifies the type of format object to return.</param>
        /// <returns>
        /// An instance of the object specified by <paramref name="formatType"/>, if the T:System.IFormatProvider"/> implementation can supply that type of object; otherwise, null.
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public object GetFormat([Jig.Annotations.NotNull] Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }

            return null;
        }

        /// <summary>
        /// Converts the value of a specified object to an equivalent string representation using specified format and culture-specific formatting information.
        /// </summary>
        /// <param name="format">A format string containing formatting specifications.</param>
        /// <param name="args">An object to format.</param>
        /// <param name="formatProvider">An object that supplies format information about the current instance.</param>
        /// <returns>
        /// The string representation of the value of <paramref name="args"/>, formatted as specified by <paramref name="format"/> and <paramref name="formatProvider"/>.
        /// </returns>
        [Jig.Annotations.NotNull]
        public string Format([Jig.Annotations.NotNull] string format, [Jig.Annotations.NotNull] object args, [Jig.Annotations.NotNull] IFormatProvider formatProvider)
        {
            /* d: 555.555.5555 x0000
             * D: 555-555-5555 x0000
             * P: (555) 555-5555 x0000
             * */

            string result = string.Concat(args).RemoveNonDigitCharacters();
            if (!string.IsNullOrWhiteSpace(result) && result.Length >= 10 && !result.StartsWith("1"))
            {
                var sb = new StringBuilder(21);

                switch (format)
                {
                    case "d":
                        sb.Append(result, 0, 3).Append('.').Append(result, 3, 3).Append('.').Append(result, 6, 4);
                        break;
                    case "D":
                        sb.Append(result, 0, 3).Append('-').Append(result, 3, 3).Append('-').Append(result, 6, 4);
                        break;
                    case "P":
                        sb.Append('(').Append(result, 0, 3).Append(") ").Append(result, 3, 3).Append('-').Append(result, 6, 4);
                        break;
                    default:
                        return this.HandleOtherFormats(format, args);
                }

                if (result.Length > 10)
                {
                    sb.Append(" x").Append(result.Substring(10));
                }

                return sb.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Handles the other formats.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The result string</returns>
        [Jig.Annotations.NotNull]
        private string HandleOtherFormats([Jig.Annotations.NotNull] string format, [Jig.Annotations.NotNull] object args)
        {
            // ReSharper disable CanBeReplacedWithTryCastAndCheckForNull
            if (args is IFormattable) // ReSharper restore CanBeReplacedWithTryCastAndCheckForNull
            {
                return ((IFormattable)args).ToString(format, CultureInfo.CurrentCulture);
            }

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (args != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return args.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// US Phone Number Formats
        /// </summary>
        public static partial class Formats
        {
            /// <summary>
            /// Dotted Format 555.555.5555 x0000
            /// </summary>
            public const string Dotted = "{0:d}";

            /// <summary>
            /// Dashed Format 555-555-5555 x0000
            /// </summary>
            public const string Dashed = "{0:D}";

            /// <summary>
            /// Parenthesis and Dashed Format (555) 555-5555 x0000
            /// </summary>
            public const string ParenthesisDash = "{0:P}";
        }
    }
}
