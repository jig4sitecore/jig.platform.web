﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: Guid("C6703F4A-D853-4bb5-BE23-BC72B9210E33")]
[assembly: AssemblyTitle("Jig Sitecore Framework")]
[assembly: AssemblyDescription("The library extending the Sitecore CMS.")]

[assembly: AssemblyCompany("Jig Team")]
[assembly: AssemblyProduct("Jig Sitecore Framework")]
[assembly: AssemblyCopyright("Copyright © Jig Web Team 2014")]
[assembly: AssemblyTrademark("Jig Team")]

[assembly: ComVisible(false)]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif

[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
