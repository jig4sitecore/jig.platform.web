﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DictionaryExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       06/24/2012
// --------------------------------------------------------------------------------------------------------------------

namespace Jig.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Configuration;
    using System.Web.SessionState;

    using Jig.Web;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    [DebuggerStepThrough]
    public static partial class DictionaryExtensions
    {
        /// <summary>
        /// Converts the IDictionary to a query string
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="prependQuestionMark">if set to <c>true</c> prepends question mark '?'.</param>
        /// <returns>
        /// The IDictionary expressed as a string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsQueryString([Jig.Annotations.NotNull] this IDictionary<string, string> collection, bool prependQuestionMark = false)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Count > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var queryBuilder = new StringBuilder(128);

                if (prependQuestionMark)
                {
                    queryBuilder.Append('?');
                }

                string[] keys = collection.Keys.ToArray();

                for (int i = 0; i < keys.Length; i++)
                {
                    if (i > 0)
                    {
                        queryBuilder.Append('&');
                    }

                    var key = keys[i];
                    queryBuilder.UrlEncode(key).Append('=').UrlEncode(collection[key]);
                }

                return queryBuilder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts the IDictionary to a query string
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="prependQuestionMark">if set to <c>true</c> prepends question mark '?'.</param>
        /// <returns>
        /// The IDictionary expressed as a string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsQueryString([Jig.Annotations.NotNull] this IDictionary<string, object> collection, bool prependQuestionMark = false)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Count > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var queryBuilder = new StringBuilder(128);

                if (prependQuestionMark)
                {
                    queryBuilder.Append('?');
                }

                string[] keys = collection.Keys.ToArray();

                for (int i = 0; i < keys.Length; i++)
                {
                    if (i > 0)
                    {
                        queryBuilder.Append('&');
                    }

                    var key = keys[i];
                    queryBuilder.UrlEncode(key).Append('=').UrlEncode(string.Concat(collection[key]));
                }

                return queryBuilder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts the IDictionary to a query string
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="prependQuestionMark">if set to <c>true</c> prepends question mark '?'.</param>
        /// <returns>
        /// The IDictionary expressed as a string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsQueryString([Jig.Annotations.NotNull] this IDictionary collection, bool prependQuestionMark = false)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && collection.Count > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var queryBuilder = new StringBuilder(128);

                if (prependQuestionMark)
                {
                    queryBuilder.Append('?');
                }

                var keys = collection.Keys.Cast<string>().ToArray();

                for (int i = 0; i < keys.Length; i++)
                {
                    if (i > 0)
                    {
                        queryBuilder.Append('&');
                    }

                    var key = keys[i];
                    queryBuilder.UrlEncode(key).Append('=').UrlEncode(string.Concat(collection[key]));
                }

                return queryBuilder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Copy HttpCookieCollection to Xml/JSON Serializable KeyValue array.
        /// </summary>
        /// <param name="cookies">HTTP cookies.</param>
        /// <returns>The new dictionary</returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> ConvertAsDictionary([Jig.Annotations.NotNull] this HttpCookieCollection cookies)
        {
            var collection = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (cookies != null && cookies.Count > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                for (int i = 0; i < cookies.Count; i++)
                {
                    HttpCookie cookie = cookies[i];
                    if (cookie != null)
                    {
                        collection[cookie.Name] = cookie.Value;
                    }
                }
            }

            return collection;
        }

        /// <summary>
        ///     Gets Browser Cap Info
        /// </summary>
        /// <param name="capabilities">Browser capabilities</param>
        /// <returns>The new VNameValueCollection</returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> ConvertAsDictionary([Jig.Annotations.NotNull] this HttpCapabilitiesBase capabilities)
        {
            var browserCollection = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (capabilities != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                browserCollection["Browser"] = capabilities.Browser;
                browserCollection["EcmaScriptVersion"] = capabilities.EcmaScriptVersion.ToString();
                browserCollection["MajorVersion"] = capabilities.MajorVersion.ToString(CultureInfo.InvariantCulture);
                browserCollection["MinorVersion"] = capabilities.MinorVersion.ToString(CultureInfo.InvariantCulture);
                browserCollection["MSDomVersion"] = capabilities.MSDomVersion.ToString();
                browserCollection["Platform"] = capabilities.Platform;
                browserCollection["Type"] = capabilities.Type;
                browserCollection["Version"] = capabilities.Version;
                browserCollection["W3CDomVersion"] = capabilities.W3CDomVersion.ToString();
                browserCollection["ActiveXControls"] = capabilities.ActiveXControls.ToString(CultureInfo.InvariantCulture);
                browserCollection["AOL"] = capabilities.AOL.ToString(CultureInfo.InvariantCulture);
                browserCollection["Beta"] = capabilities.Beta.ToString(CultureInfo.InvariantCulture);
                browserCollection["Cookies"] = capabilities.Cookies.ToString(CultureInfo.InvariantCulture);
                browserCollection["Crawler"] = capabilities.Crawler.ToString(CultureInfo.InvariantCulture);
                browserCollection["Frames"] = capabilities.Frames.ToString(CultureInfo.InvariantCulture);
            }

            return browserCollection;
        }

        /// <summary>
        /// Gets a Application session state
        /// </summary>
        /// <param name="session">The session.</param>
        /// <returns>The new dictionary</returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> ConvertAsDictionary([Jig.Annotations.NotNull] this HttpSessionState session)
        {
            var results = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (session != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                NameObjectCollectionBase.KeysCollection keys = session.Keys;
                if (keys.Count > 0)
                {
                    for (int i = 0; i < keys.Count; i++)
                    {
                        results[keys[i]] = string.Concat(session[i]);
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Gets a Application state
        /// </summary>
        /// <param name="applicationState">The application state.</param>
        /// <returns>The new VNameValueCollection</returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> ConvertAsDictionary([Jig.Annotations.NotNull] this HttpApplicationState applicationState)
        {
            var results = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (applicationState != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                for (int i = 0; i < applicationState.Count; i++)
                {
                    results[applicationState.GetKey(i)] = string.Concat(applicationState.Get(i));
                }
            }

            return results;
        }

        /// <summary>
        /// Converts NameValueCollection to IDictionary
        /// </summary>
        /// <param name="collection">The NameValueCollection instance</param>
        /// <param name="excludeEmptyKeys">if set to <c>true</c> [exclude empty keys].</param>
        /// <returns>
        /// IDictionary of key value pairs both type string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> ConvertAsDictionary([Jig.Annotations.NotNull] this NameValueCollection collection, bool excludeEmptyKeys = true)
        {
            var dictionary = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (string key in collection.Keys)
                {
                    if (!string.IsNullOrWhiteSpace(key) && !dictionary.ContainsKey(key))
                    {
                        if (!excludeEmptyKeys)
                        {
                            dictionary[key] = collection[key];
                        }
                        else
                        {
                            var value = collection[key];
                            if (!string.IsNullOrWhiteSpace(value))
                            {
                                dictionary[key] = collection[key];
                            }
                        }
                    }
                }
            }

            return dictionary;
        }

        /// <summary>
        /// Removes the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="keyCollection">The key collection.</param>
        /// <returns>The collection without specific keys</returns>
        [Jig.Annotations.CanBeNull]
        public static IDictionary<string, string> Remove([Jig.Annotations.NotNull] this IDictionary<string, string> collection, [Jig.Annotations.NotNull] IEnumerable<string> keyCollection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && keyCollection != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var key in keyCollection)
                {
                    if (!string.IsNullOrWhiteSpace(key))
                    {
                        collection.Remove(key);
                    }
                }
            }

            return collection;
        }

        /// <summary>
        /// Removes the specified collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="keyCollection">The key collection.</param>
        /// <returns>The collection without specific keys</returns>
        [Jig.Annotations.CanBeNull]
        public static IDictionary<string, string> Remove([Jig.Annotations.NotNull] this IDictionary<string, string> collection, [Jig.Annotations.NotNull] params string[] keyCollection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && keyCollection != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var key in keyCollection)
                {
                    if (!string.IsNullOrWhiteSpace(key))
                    {
                        collection.Remove(key);
                    }
                }
            }

            return collection;
        }

        /// <summary>
        /// Gets the specified object by key from collection.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="key">The cache key.</param>
        /// <returns>
        /// The object instance or null
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static TObject Get<TObject>([Jig.Annotations.NotNull] this IDictionary collection, [Jig.Annotations.NotNull] object key) where TObject : class
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (key != null && collection.Contains(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key] as TObject;
            }

            return null;
        }

        /// <summary>
        /// Gets the specified object by key from collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="key">The key.</param>
        /// <returns>The object instance/value or null</returns>
        [Jig.Annotations.CanBeNull]
        public static object Get([Jig.Annotations.NotNull] this IDictionary collection, [Jig.Annotations.NotNull] object key)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && key != null && collection.Contains(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key];
            }

            return null;
        }

        /// <summary>
        /// Gets the value as string.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <returns>
        /// The object instance/value or null
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static string GetValueAsString([Jig.Annotations.NotNull] this IDictionary<string, object> dictionary, [Jig.Annotations.NotNull] string key)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (dictionary != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                object value;
                if (dictionary.TryGetValue(key, out value))
                {
                    return value as string;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the value as nullable int.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The object instance/value or null
        /// </returns>
        public static int? GetValueAsInt([Jig.Annotations.NotNull] this IDictionary<string, object> dictionary, [Jig.Annotations.NotNull] string key, int? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (dictionary != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                object value;
                if (dictionary.TryGetValue(key, out value))
                {
                    return value as int?;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value as nullable float.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The object instance/value or null
        /// </returns>
        public static float? GetValueAsFloat([Jig.Annotations.NotNull] this IDictionary<string, object> dictionary, [Jig.Annotations.NotNull] string key, float? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (dictionary != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                object value;
                if (dictionary.TryGetValue(key, out value))
                {
                    return value as float?;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value as nullable double.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The object instance/value or null
        /// </returns>
        public static double? GetValueAsDouble([Jig.Annotations.NotNull] this IDictionary<string, object> dictionary, [Jig.Annotations.NotNull] string key, double? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (dictionary != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                object value;
                if (dictionary.TryGetValue(key, out value))
                {
                    return value as double?;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the value as nullable Boolean.
        /// </summary>
        /// <param name="dictionary">The dictionary.</param>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The object instance/value or null
        /// </returns>
        public static bool? GetValueAsBoolean([Jig.Annotations.NotNull] this IDictionary<string, object> dictionary, [Jig.Annotations.NotNull] string key, bool? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (dictionary != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                object value;
                if (dictionary.TryGetValue(key, out value))
                {
                    return value as bool?;
                }
            }

            return defaultValue;
        }
    }
}
