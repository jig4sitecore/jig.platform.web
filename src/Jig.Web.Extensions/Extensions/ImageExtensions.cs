﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Date:       06/24/2010
// --------------------------------------------------------------------------------------------------------------------
//  Based on: http://aspnet.codeplex.com/
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class ImageExtensions
    {
        /// <summary>
        ///     Gets the content type by image format.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>The content type by image format. Default 'image/x-unknown'</returns>
        [Jig.Annotations.NotNull]
        public static string GetContentTypeByImageFormat([Jig.Annotations.NotNull] this ImageFormat format)
        {
            string type = "image/x-unknown";

            if (format.Equals(ImageFormat.Gif))
            {
                type = "image/gif";
            }
            else if (format.Equals(ImageFormat.Jpeg))
            {
                type = "image/jpeg";
            }
            else if (format.Equals(ImageFormat.Png))
            {
                type = "image/png";
            }
            else if (format.Equals(ImageFormat.Bmp) || format.Equals(ImageFormat.MemoryBmp))
            {
                type = "image/bmp";
            }
            else if (format.Equals(ImageFormat.Icon))
            {
                type = "image/x-icon";
            }
            else if (format.Equals(ImageFormat.Tiff))
            {
                type = "image/tiff";
            }

            return type;
        }

        /// <summary>
        ///     Gets the bytes from image. The image format is Jpeg!
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>The byte array from image</returns>
        [Jig.Annotations.CanBeNull]
        public static byte[] GetBytesFromImage([Jig.Annotations.NotNull] this Image image)
        {
            return image.GetBytesFromImage(image.RawFormat);
        }

        /// <summary>
        ///     Gets the bytes from image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <returns>The byte array from image</returns>
        [Jig.Annotations.CanBeNull]
        public static byte[] GetBytesFromImage([Jig.Annotations.NotNull] this Image image, [Jig.Annotations.NotNull] ImageFormat imageFormat)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (image != null && imageFormat != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var ms = new MemoryStream())
                {
                    image.Save(ms, imageFormat);
                    ms.Seek(0, SeekOrigin.Begin);
                    var imageBytes = new byte[ms.Length];
                    ms.Read(imageBytes, 0, (int)ms.Length);

                    return imageBytes;
                }
            }

            return null;
        }

        /// <summary>
        ///     Gets the image from bytes.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>The image from byte array</returns>
        [Jig.Annotations.CanBeNull]
        public static Image GetImageFromBytes([Jig.Annotations.NotNull] this byte[] bytes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bytes == null || bytes.Length == 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return null;
            }

            using (var ms = new MemoryStream(bytes))
            {
                return Image.FromStream(ms);
            }
        }

        /// <summary>
        ///     Scales the image.
        /// </summary>
        /// <param name="bytes">The image bytes.</param>
        /// <param name="maxWidth">Max width of the image.</param>
        /// <param name="maxHeight">Max weight of the image.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <returns>The scaled image</returns>
        [Jig.Annotations.CanBeNull]
        public static byte[] ScaleImage([Jig.Annotations.NotNull] this byte[] bytes, int maxWidth, int maxHeight, [Jig.Annotations.NotNull] ImageFormat imageFormat)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bytes != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                MemoryStream memory = null;
                byte[] result;
                try
                {
                    memory = new MemoryStream(bytes);
                    using (Image image = Image.FromStream(memory))
                    {
                        result = image.ScaleImage(maxWidth, maxHeight, imageFormat);
                    }
                }
                finally
                {
                    if (memory != null)
                    {
                        memory.Dispose();
                    }
                }

                return result;
            }

            return null;
        }

        /// <summary>
        ///     Scales the image.
        /// </summary>
        /// <param name="image">The image instance.</param>
        /// <param name="maxWidth">Max width of the image.</param>
        /// <param name="maxHeight">Max weight of the image.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <returns>The scaled image</returns>
        [Jig.Annotations.NotNull]
        public static byte[] ScaleImage([Jig.Annotations.NotNull] this Image image, int maxWidth, int maxHeight, [Jig.Annotations.CanBeNull] ImageFormat imageFormat)
        {
            if (image == null)
            {
                throw new HttpException(550, "The Image cannot be null!");
            }

            if (maxWidth < 1)
            {
                throw new HttpException(550, "The Max Width of the image must be greater then 0!");
            }

            if (maxHeight < 1)
            {
                throw new HttpException(550, "The Max height of the image must be greater then 0!");
            }

            if (imageFormat == null)
            {
                imageFormat = image.RawFormat;
            }

            if (image.Size.Width > maxWidth || image.Size.Height > maxHeight)
            {
                // resize the image to fit our website required size
                int newWidth = image.Size.Width;
                int newHeight = image.Size.Height;

                if (newWidth > maxWidth)
                {
                    newWidth = maxWidth;
                    newHeight = (int)(newHeight * ((float)newWidth / image.Size.Width));
                }

                if (newHeight > maxHeight)
                {
                    newHeight = maxHeight;
                    newWidth = image.Size.Width;
                    newWidth = (int)(newWidth * ((float)newHeight / image.Size.Height));
                }

                /* Resize the image to fit in the allowed image size */
                bool indexed = image.PixelFormat == PixelFormat.Format1bppIndexed || image.PixelFormat == PixelFormat.Format4bppIndexed || image.PixelFormat == PixelFormat.Format8bppIndexed
                               || image.PixelFormat == PixelFormat.Indexed;

                using (Bitmap newImage = indexed ? new Bitmap(newWidth, newHeight) : new Bitmap(newWidth, newHeight, image.PixelFormat))
                {
                    using (var graphics = Graphics.FromImage(newImage))
                    {
                        if (indexed)
                        {
                            graphics.FillRectangle(Brushes.White, 0, 0, newWidth, newHeight);
                        }

                        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphics.DrawImage(image, new Rectangle(0, 0, newWidth, newHeight));
                    }

                    using (var memoryStream = new MemoryStream())
                    {
                        newImage.Save(memoryStream, imageFormat);
                        return memoryStream.ToArray();
                    }
                }
            }

            using (var memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, imageFormat);
                return memoryStream.ToArray();
            }
        }

        /// <summary>
        /// To the base64 string.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns>The Base 64 string representation</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsBase64String([Jig.Annotations.NotNull] this Image image)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (image != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var format = image.RawFormat.GetContentTypeByImageFormat();
                var bytes = image.GetBytesFromImage();

                return string.Concat("data:", format, ";base64,", Convert.ToBase64String(bytes));
            }

            return string.Empty;
        }
    }
}
