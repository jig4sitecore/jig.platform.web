﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ControlExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here."), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "This is library of extensions methods!")]
    [DebuggerStepThrough]
    public static partial class ControlExtensions
    {
        /// <summary>
        ///     Loads a System.Web.UI.Control object from a file based on a specified virtual path.
        /// </summary>
        /// <typeparam name="TObject">The object inherited from UserControl</typeparam>
        /// <param name="virtualPath">The virtual Path to the control</param>
        /// <returns>The loaded control instance</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// <![CDATA[
        /// [WebMethod]
        /// [ScriptMethod(UseHttpGet = true)]
        /// public string LoadHelloWorldControlWithParams()
        /// {
        ///     HelloWorld world = Extensions.LoadControl<HelloWorld>("/Examples/AjaxWebServices/HelloWorld.ascx");
        ///     world.UserName = "Jim"; // Set the property
        ///     return Extensions.RenderControlAsHtml(world);
        /// }   
        /// ]]>
        /// </code>
        /// </example>
        [Jig.Annotations.CanBeNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "It's load the user control from provided path")]
        public static TObject LoadControl<TObject>([Jig.Annotations.NotNull] this string virtualPath) where TObject : UserControl
        {
            if (virtualPath.FileExists() && virtualPath.EndsWith(".ascx"))
            {
                // Create a new Page and add the control to it.
                using (var page = new Page())
                {
                    page.EnableViewState = false;
                    page.Controls.Clear();

                    return page.LoadControl(virtualPath) as TObject;
                }
            }

            return null;
        }

        /// <summary>
        /// Renders the control as HTML.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns>The control html</returns>
        [Jig.Annotations.NotNull]
        public static string RenderControlAsHtml([Jig.Annotations.NotNull] this Control control)
        {
            return RenderControlAsHtml(new[] { control });
        }

        /// <summary>
        ///     Renders a control to a string - useful for AJAX partials.
        /// Works only with non-post back controls
        /// </summary>
        /// <param name="controls">Instance of the any ASP.NET control</param>
        /// <returns>Return rendered Control HTML, as a string.</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// /* Declaration: */
        ///     LiteralControl one = new LiteralControl("Hello world");
        ///     UserControl userControl = this.Page.LoadControl("/App_UserControls/WebUserControl1.ascx");
        ///     .....
        ///     LiteralControl last = new LiteralControl("Last Control");
        ///  /* Call:  */ 
        ///     string html = Extensions.RenderControlAsHtml(one);
        ///         OR    
        ///     string html = Extensions.RenderControlAsHtml(one, userControl, ..., last);
        /// </code>
        /// </example>
        [Jig.Annotations.NotNull]
        public static string RenderControlAsHtml([Jig.Annotations.NotNull] this IEnumerable<Control> controls)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (controls != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var html = new StringBuilder(1024);
                StringWriter stringWriter = null;
                try
                {
                    stringWriter = new StringWriter(html);

                    var xhtmlTextWriter = new XhtmlTextWriter(stringWriter);

                    // Create a new Page and add the control to it.
                    using (var page = new Page())
                    {
                        page.EnableViewState = false;
                        page.Controls.Clear();
                        foreach (Control item in controls)
                        {
                            page.Controls.Add(item);
                        }

                        HttpContext.Current.Server.Execute(page, xhtmlTextWriter, false);
                    }
                }
                finally
                {
                    if (stringWriter != null)
                    {
                        stringWriter.Dispose();
                    }
                }

                return html.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Renders a control to a string - useful for AJAX partials.
        /// Works only with non-post back controls
        /// </summary>
        /// <param name="virtualPath">The virtual Path to the control</param>
        /// <returns> Return rendered Control HTML, as a string.</returns>
        /// <example>View code: <br />
        ///      string html = Extensions.RenderControlAsHtml("/App_UserControls/WebUserControl1.ascx");
        /// </example>
        [Jig.Annotations.NotNull]
        public static string RenderControlAsHtml([Jig.Annotations.NotNull] this string virtualPath)
        {
            if (!string.IsNullOrWhiteSpace(virtualPath) && virtualPath.FileExists() && virtualPath.EndsWith(".ascx"))
            {
                var html = new StringBuilder(1024);
                StringWriter stringWriter = null;
                try
                {
                    stringWriter = new StringWriter(html);

                    var xhtmlTextWriter = new XhtmlTextWriter(stringWriter);

                    // Create a new Page and add the control to it.
                    using (var page = new Page())
                    {
                        page.EnableViewState = false;
                        page.Controls.Clear();
                        page.Controls.Add(page.LoadControl(virtualPath));

                        HttpContext.Current.Server.Execute(page, xhtmlTextWriter, false);
                    }
                }
                finally
                {
                    if (stringWriter != null)
                    {
                        stringWriter.Dispose();
                    }
                }

                return html.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Renders a control to a string - useful for AJAX partials.
        /// Works only with non-post back controls
        /// </summary>
        /// <param name="userControlType">The type of the control</param>
        /// <param name="parameters"> 
        ///     An array of arguments that match in number, order, and type the parameters
        /// of the constructor to invoke. If parameters is an empty array or null, the
        /// constructor that takes no parameters (the default constructor) is invoked.
        /// </param>
        /// <returns> Return rendered Control HTML, as a string.</returns>
        /// <example>View code: <br />
        ///      string html = Extensions.RenderControlAsHtml(typeof(WebUserControl1), new object[]{ some_id , someday }");
        /// </example>
        [Jig.Annotations.NotNull]
        public static string RenderControlAsHtml([Jig.Annotations.NotNull] Type userControlType, [Jig.Annotations.NotNull] object[] parameters)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (userControlType != null && parameters != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var html = new StringBuilder(1024);

                StringWriter stringWriter = null;
                try
                {
                    stringWriter = new StringWriter(html);

                    var xhtmlTextWriter = new XhtmlTextWriter(stringWriter);

                    // Create a new Page and add the control to it.
                    using (var page = new Page())
                    {
                        page.EnableViewState = false;
                        page.Controls.Clear();
                        page.Controls.Add(page.LoadControl(userControlType, parameters));

                        HttpContext.Current.Server.Execute(page, xhtmlTextWriter, false);
                    }
                }
                finally
                {
                    if (stringWriter != null)
                    {
                        stringWriter.Dispose();
                    }
                }

                return html.ToString();
            }

            return string.Empty;
        }
    }
}
