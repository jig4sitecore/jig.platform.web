﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlDocumentExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Xml;
    using System.Xml.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class XmlDocumentExtensions
    {
        /// <summary>
        /// Converts an XDocument to an XmlDocument.
        /// </summary>
        /// <param name="document">The XDocument to convert.</param>
        /// <returns>The equivalent XmlDocument.</returns>
        [Jig.Annotations.CanBeNull]
        public static XmlDocument ToXmlDocument([Jig.Annotations.CanBeNull] this XDocument document)
        {
            if (document != null)
            {
                var xmlDocument = new XmlDocument();
                xmlDocument.Load(document.CreateReader());

                return xmlDocument;
            }

            return null;
        }

        /// <summary>
        /// Converts an XmlDocument to an XDocument.
        /// </summary>
        /// <param name="xmlDocument">The XmlDocument to convert.</param>
        /// <returns>The equivalent XDocument.</returns>
        [Jig.Annotations.CanBeNull]
        public static XDocument ToXDocument([Jig.Annotations.CanBeNull] this XmlDocument xmlDocument)
        {
            if (xmlDocument != null)
            {
                return XDocument.Load(xmlDocument.CreateNavigator().ReadSubtree());
            }

            return null;
        }
    }
}
