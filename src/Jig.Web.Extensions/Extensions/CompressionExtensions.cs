﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CompressionExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class CompressionExtensions
    {
        /// <summary>
        ///     The GZIP compress.
        /// </summary>
        /// <param name="data">The data to compress.</param>
        /// <returns>The compressed data</returns>
        [Jig.Annotations.NotNull]
        public static byte[] GZipCompress([Jig.Annotations.NotNull] this byte[] data)
        {
            byte[] bytes = null;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (data != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var output = new MemoryStream(data.Length))
                {
                    var gzip = new GZipStream(output, CompressionMode.Compress);
                    gzip.Write(data, 0, data.Length);

                    bytes = output.ToArray();
                }
            }

            return bytes;
        }

        /// <summary>
        ///     The GZIP decompress.
        /// </summary>
        /// <param name="data">The data to decompress.</param>
        /// <returns>The decompressed data</returns>
        [Jig.Annotations.NotNull]
        public static byte[] GZipDecompress([Jig.Annotations.NotNull] this byte[] data)
        {
            byte[] bytes = null;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (data != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var input = new MemoryStream(data.Length))
                {
                    input.Write(data, 0, data.Length);
                    input.Position = 0;

                    var gzip = new GZipStream(input, CompressionMode.Decompress);

                    using (var output = new MemoryStream(data.Length))
                    {
                        var buff = new byte[64];
                        int read = gzip.Read(buff, 0, buff.Length);

                        while (read > 0)
                        {
                            output.Write(buff, 0, buff.Length);
                            read = gzip.Read(buff, 0, buff.Length);
                        }

                        bytes = output.ToArray();
                    }
                }
            }

            return bytes;
        }

        /// <summary>
        ///     The deflate compress.
        /// </summary>
        /// <param name="data">The data to compress.</param>
        /// <returns>The compressed data</returns>
        [Jig.Annotations.NotNull]
        public static byte[] DeflateCompress([Jig.Annotations.NotNull] this byte[] data)
        {
            byte[] bytes = null;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (data != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var output = new MemoryStream(data.Length))
                {
                    var gzip = new DeflateStream(output, CompressionMode.Compress);

                    gzip.Write(data, 0, data.Length);
                    bytes = output.ToArray();
                }
            }

            return bytes;
        }

        /// <summary>
        ///     The Deflate decompress.
        /// </summary>
        /// <param name="data">The data to decompress.</param>
        /// <returns>The decompressed data</returns>
        [Jig.Annotations.NotNull]
        public static byte[] DeflateDecompress([Jig.Annotations.NotNull] this byte[] data)
        {
            byte[] bytes = null;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (data != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var input = new MemoryStream(data.Length))
                {
                    input.Write(data, 0, data.Length);
                    input.Position = 0;

                    var gzip = new DeflateStream(input, CompressionMode.Decompress);

                    using (var output = new MemoryStream(data.Length))
                    {
                        var buff = new byte[64];
                        int read = gzip.Read(buff, 0, buff.Length);

                        while (read > 0)
                        {
                            output.Write(buff, 0, buff.Length);
                            read = gzip.Read(buff, 0, buff.Length);
                        }

                        bytes = output.ToArray();
                    }
                }
            }

            return bytes;
        }
    }
}
