﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VirtualPathUtilityExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.IO;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class VirtualPathUtilityExtensions
    {
        /// <summary>
        /// Gets the directory portion of a virtual path.
        /// </summary>
        /// <param name="path">The virtual/absolute path.</param>
        /// <param name="isVirtualPath">if set to <c>true</c> isVirtualPath.</param>
        /// <returns>
        /// Returns the directory portion of a virtual/absolute path.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string GetDirectory([Jig.Annotations.NotNull] this string path, bool isVirtualPath = true)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                if (isVirtualPath)
                {
                    return VirtualPathUtility.GetDirectory(path);
                }

                return Path.GetDirectoryName(path).EnsureEndsWith(@"\");
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the extension of the file that is referenced in the virtual path.
        /// </summary>
        /// <param name="path">The virtual path.</param>
        /// <param name="isVirtualPath">if set to <c>true</c> [isVirtualPath].</param>
        /// <returns>
        /// The file name extension string literal, including the period (.), null, or an empty string ("").
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string GetExtension([Jig.Annotations.NotNull] this string path, bool isVirtualPath = true)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                if (isVirtualPath)
                {
                    return VirtualPathUtility.GetExtension(path);
                }

                return Path.GetExtension(path);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the file name of the file that is referenced in the virtual path.
        /// </summary>
        /// <param name="path">The virtual path.</param>
        /// <param name="isVirtualPath">if set to <c>true</c> [isVirtualPath].</param>
        /// <returns>
        /// The file name literal after the last directory character in virtualPath; otherwise, an empty string (""), if the last character of virtualPath is a directory or volume separator character.
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string GetFileName([Jig.Annotations.NotNull] this string path, bool isVirtualPath = true)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                if (isVirtualPath)
                {
                    return VirtualPathUtility.GetFileName(path);
                }

                return Path.GetFileName(path);
            }

            return string.Empty;
        }
    }
}
