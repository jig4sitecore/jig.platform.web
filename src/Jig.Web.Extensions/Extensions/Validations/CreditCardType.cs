﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreditCardType.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///    Acceptable Credit Card Types 
    /// </summary>
    [SuppressMessage("Microsoft.Design", "CA1008:EnumsShouldHaveZeroValue", Justification = "None target isn't necessary")]
    [Serializable, Flags]
    public enum CreditCardType : byte
    {
        /// <summary>
        /// The function known
        /// </summary>
        Unknown = 0,

        /// <summary>
        ///     Visa card. Default
        /// </summary>
        Visa = 1,

        /// <summary>
        ///    Master card 
        /// </summary>
        Master = 2,

        /// <summary>
        ///     American Express Card
        /// </summary>
        AmericanExpress = 4,

        /// <summary>
        ///     Discovery Card
        /// </summary>
        Discover = 8,

        /// <summary>
        ///     Diners Club Card
        /// </summary>
        DinersClub = 16
    }
}
