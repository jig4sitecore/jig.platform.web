// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.Validation.HttpPostedFile.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/15/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Drawing;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class Extensions
    {
        /// <summary>
        ///     Rule determines whether the uploaded file is Web format Image (JPEG or Gif  or Png).
        /// </summary>
        /// <remarks>
        ///     The progressive JPEG are valid. <br />
        ///  A simple or "baseline" JPEG file is stored as one top-to-bottom scan of the
        /// image.  Progressive JPEG divides the file into a series of scans and not supported by many browsers still.
        /// </remarks>
        /// <param name="postedFile">The HttpPosted File instance</param>
        /// <returns>
        ///     <c>true</c> if is; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        public static bool IsUploadedFileWebFormatImage([Jig.Annotations.NotNull] this HttpPostedFile postedFile)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (postedFile != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return postedFile.ContentType == ContentTypes.Jpeg || postedFile.ContentType == ContentTypes.Jpg || postedFile.ContentType == ContentTypes.Gif || postedFile.ContentType == ContentTypes.Png;
            }

            return false;
        }

        /// <summary>
        /// Determines whether [is uploaded file JPEG] [the specified posted file].
        /// </summary>
        /// <param name="postedFile">The posted file.</param>
        /// <returns>
        ///   <c>true</c> if [is uploaded file JPEG] [the specified posted file]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsUploadedFileJpeg([Jig.Annotations.NotNull] this HttpPostedFile postedFile)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (postedFile != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return postedFile.ContentType == ContentTypes.Jpeg || postedFile.ContentType == ContentTypes.Jpg;
            }

            return false;
        }

        /// <summary>
        ///     Rule determines whether the uploaded file is Web format Image (Jpeg or Gif  or Png).
        /// </summary>
        /// <remarks>
        ///     The progressive Jpeg are valid. <br />
        ///  A simple or "baseline" JPEG file is stored as one top-to-bottom scan of the
        /// image.  Progressive JPEG divides the file into a series of scans and not supported by many browsers still.
        /// </remarks>
        /// <param name="postedFile">The HttpPosted File instance</param>
        /// <param name="maxWidth">The max width of the image</param>
        /// <param name="maxHeight">The max height of the image</param>
        /// <returns>
        ///     <c>true</c> if is; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        public static bool IsUploadedFileWebFormatImage([Jig.Annotations.NotNull] this HttpPostedFile postedFile, int maxWidth, int maxHeight)
        {
            if (postedFile == null)
            {
                throw new HttpException(550, "The HttpPosted file is NULL!");
            }

            if (maxWidth < 1)
            {
                throw new HttpException(550, "The Max Width of the image must be greater then 0!");
            }

            if (maxHeight < 1)
            {
                throw new HttpException(550, "The Max height of the image must be greater then 0!");
            }

            if (postedFile.IsUploadedFileWebFormatImage())
            {
                using (Image image = Image.FromStream(postedFile.InputStream))
                {
                    return image.Width <= maxWidth && image.Height <= maxHeight;
                }
            }

            return false;
        }

        /// <summary>
        ///     Rule determines whether the uploaded file is Web format Image (Jpeg or Gif  or Png).
        /// </summary>
        /// <remarks>
        ///     The progressive Jpeg are valid. <br />
        ///  A simple or "baseline" JPEG file is stored as one top-to-bottom scan of the
        /// image.  Progressive JPEG divides the file into a series of scans and not supported by many browsers still.
        /// </remarks>
        /// <param name="postedFile">The HttpPosted File instance</param>
        /// <param name="maxWidth">The max width of the image</param>
        /// <param name="maxHeight">The max height of the image</param>
        /// <param name="maxSize">The max size of the image in bytes  like '100000' which is around 100kb</param>
        /// <returns>
        ///     <c>true</c> if is; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// </example>
        public static bool IsUploadedFileWebFormatImage([Jig.Annotations.NotNull] this HttpPostedFile postedFile, int maxWidth, int maxHeight, int maxSize)
        {
            if (postedFile == null)
            {
                throw new HttpException(550, "The HttpPosted file is NULL!");
            }

            if (maxWidth < 1)
            {
                throw new HttpException(550, "The Max Width of the image must be greater then 0!");
            }

            if (maxHeight < 1)
            {
                throw new HttpException(550, "The Max height of the image must be greater then 0!");
            }

            if (maxSize < 1)
            {
                throw new HttpException(550, "The Max size of the image must be greater then 0!");
            }

            if (postedFile.IsUploadedFileWebFormatImage())
            {
                using (Image image = Image.FromStream(postedFile.InputStream))
                {
                    return image.Width <= maxWidth && image.Height <= maxHeight && postedFile.ContentLength <= maxSize;
                }
            }

            return false;
        }
    }
}