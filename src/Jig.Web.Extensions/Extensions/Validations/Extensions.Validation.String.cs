// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.Validation.String.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class Extensions
    {
        /// <summary>
        ///     Rule determines whether the specified string Is NOT Null Or Empty AND doesn't exceed a specified length.
        /// </summary>
        /// <param name="input">String containing the data to validate.</param>
        /// <param name="maxLength">The Max Length of string</param>
        /// <returns>
        ///     <c>true</c> if valid; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// <![CDATA[
        /// An extension method attached to any typeof(string) object instance 
        ///     bool isValid = "some text".IsNotNullOrEmptyAndMaxLength(50);
        /// OR
        ///     if(this.Page.TextNotes.Text.IsNotNullOrEmptyAndMaxLength(50))
        ///     {
        ///         // Do something
        ///     }
        ///  OR
        ///     bool isValid = Extensions.IsNotNullOrEmptyAndMaxLength("some text", 50 );
        /// ]]>
        /// </code>
        /// </example>
        public static bool IsNotNullOrEmptyAndMaxLength([Jig.Annotations.NotNull] this string input, int maxLength)
        {
            return !string.IsNullOrEmpty(input) && input.Length <= maxLength;
        }

        /// <summary>
        ///     Rule determines whether the specified string is IsNullOrEmpty and doesn't exceed a specified length.
        /// </summary>
        /// <param name="input">String containing the data to validate.</param>
        /// <param name="maxLength">The Max Length of string</param>
        /// <returns>
        ///     <c>true</c> if valid; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// <![CDATA[
        /// /* An extension method attached to any typeof(string) object instance */ 
        ///     bool isValid = "some text".IsNullOrEmptyAndMaxLength(50);
        /// OR
        ///     if(this.Page.TextNotes.Text.IsNullOrEmptyAndMaxLength(50))
        ///     {
        ///         // Do something
        ///     }
        ///  OR
        ///     bool isValid = Extensions.IsNullOrEmptyAndMaxLength("some text", 50 );
        /// ]]>
        /// </code>
        /// </example>
        public static bool IsNullOrEmptyAndMaxLength([Jig.Annotations.NotNull] this string input, int maxLength)
        {
            return string.IsNullOrEmpty(input) || (!string.IsNullOrEmpty(input) && input.Length <= maxLength);
        }
    }
}