// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.Validation.States.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class Extensions
    {
        /// <summary>
        ///     Rule determines whether the specified string is a valid US state either by full name.
        /// </summary>
        /// <param name="stateName">The state name.</param>
        /// <returns>
        ///     <c>true</c> if valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUsStateName([Jig.Annotations.NotNull] this string stateName)
        {
            if (string.IsNullOrEmpty(stateName))
            {
                return false;
            }

            return USStateHelper.GetStateCollection().Any(x => string.Equals(x.Value, stateName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        ///     Rule ensuring a String target is matching a specified two letter State abbreviation regular expression.
        /// </summary>
        /// <param name="stateNameAbbreviation">String containing the data to validate.</param>
        /// <returns>
        ///     <c>true</c> if valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidUsStateNameAbbreviation([Jig.Annotations.CanBeNull] this string stateNameAbbreviation)
        {
            if (string.IsNullOrWhiteSpace(stateNameAbbreviation))
            {
                return false;
            }

            // ReSharper disable PossibleNullReferenceException
            if (stateNameAbbreviation.Length != 2)
            // ReSharper restore PossibleNullReferenceException
            {
                return false;
            }

            return USStateHelper.GetStateCollection().Any(x => string.Equals(x.Key, stateNameAbbreviation, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}