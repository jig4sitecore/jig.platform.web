﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="NameValueCollectionExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       09/29/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Collections
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;

    using Jig.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class NameValueCollectionExtensions
    {
        /// <summary>
        /// Gets a double from the specified key
        /// </summary>
        /// <param name="collection">The NameValueCollection collection instance</param>
        /// <param name="key">The NameValueCollection collection key</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The converted value or NULL
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static double? GetAsDoubleNullable([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key, double? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key].ConvertAsDoubleNullable(defaultValue);
            }

            return null;
        }

        /// <summary>
        /// Gets a float from the specified key
        /// </summary>
        /// <param name="collection">The NameValueCollection collection instance</param>
        /// <param name="key">The NameValueCollection collection key</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The converted value or NULL
        /// </returns>
        public static float? GetAsFloatNullable([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key, float? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key].ConvertAsFloatNullable(defaultValue);
            }

            return null;
        }

        /// <summary>
        /// Gets an int from the specified key
        /// </summary>
        /// <param name="collection">The NameValueCollection collection instance</param>
        /// <param name="key">The NameValueCollection collection key</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The converted value or NULL
        /// </returns>
        public static int? GetAsIntNullable([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key, int? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key].ConvertAsIntNullable(defaultValue);
            }

            return null;
        }

        /// <summary>
        /// Gets an long from the specified key
        /// </summary>
        /// <param name="collection">The NameValueCollection collection instance</param>
        /// <param name="key">The NameValueCollection collection key</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The converted value or NULL
        /// </returns>
        public static long? GetAsLongNullable([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key, long? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key].ConvertAsLongNullable(defaultValue);
            }

            return null;
        }

        /// <summary>
        /// Gets a Boolean from the specified key
        /// </summary>
        /// <param name="collection">The NameValueCollection collection instance</param>
        /// <param name="key">The NameValueCollection collection key</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>
        /// The converted value or NULL
        /// </returns>
        public static bool? GetBooleanNullable([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key, bool? defaultValue = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrWhiteSpace(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return collection[key].ConvertAsBooleanNullable(defaultValue);
            }

            return null;
        }

        /// <summary>
        ///     Gets the keys containing partial key.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="key">The key to match.</param>
        /// <returns>The IEnumerable of strings matching key</returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<string> GetKeysContainingPartialKey([Jig.Annotations.NotNull] this NameValueCollection collection, [Jig.Annotations.NotNull] string key)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && !string.IsNullOrEmpty(key))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                key = key.ToLowerInvariant();
                var keys = collection.AllKeys.ToArray();

                return from value in keys where !string.IsNullOrEmpty(value) && value.ToLowerInvariant().Contains(key) select value;
            }

            return new string[] { };
        }

        /// <summary>
        /// The current NameValueCollection to the query string.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="prependQuestionMark">if set to <c>true</c> prepend question marks.</param>
        /// <returns>
        /// The current collection to the query string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsQueryString([Jig.Annotations.NotNull] this NameValueCollection collection, bool prependQuestionMark = false)
        {
            var queryString = new StringBuilder(64);
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                int count = collection.Count;
                for (int i = 0; i < count; i++)
                {
                    if (prependQuestionMark && i == 0)
                    {
                        queryString.Append('?');
                    }

                    string key = collection.GetKey(i);
                    string value = collection[key];

                    if (!string.IsNullOrEmpty(value))
                    {
                        queryString.UrlEncode(key).Append('=').UrlEncode(value);
                    }

                    if (i != count - 1)
                    {
                        queryString.Append('&');
                    }
                }
            }

            return queryString.ToString();
        }
    }
}
