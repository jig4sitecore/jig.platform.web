﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CacheExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/18/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Caching;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class CacheExtensions
    {
        /// <summary>
        /// All cached items keys
        /// </summary>
        /// <param name="cache">The cache.</param>
        /// <returns>All cache keys</returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<string> AllKeys([Jig.Annotations.NotNull] this Cache cache)
        {
            var keys = new List<string>();

            IDictionaryEnumerator cacheEnum = cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                keys.Add(cacheEnum.Key.ToString());
            }

            return keys.ToArray();
        }

        /// <summary>
        /// Clear All cached all items
        /// </summary>
        /// <param name="cache">The cache.</param>
        public static void Clear([Jig.Annotations.NotNull] this Cache cache)
        {
            var keys = new List<string>();
            IDictionaryEnumerator cacheEnum = cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                keys.Add(cacheEnum.Key.ToString());
            }

            foreach (string key in keys)
            {
                cache.Remove(key);
            }
        }

        /// <summary>
        /// Clear All cached all items with specific partial key within cache keys
        /// </summary>
        /// <param name="cache">The cache for a Web application</param>
        /// <param name="key">The partial Cache key</param>
        public static void RemoveKeysContaining([Jig.Annotations.NotNull] this Cache cache, [Jig.Annotations.NotNull] string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                var keys = new List<string>();
                IDictionaryEnumerator cacheEnum = cache.GetEnumerator();
                while (cacheEnum.MoveNext())
                {
                    keys.Add(cacheEnum.Key.ToString());
                }

                foreach (string collectionKey in keys)
                {
                    if (collectionKey.IndexOf(key, StringComparison.OrdinalIgnoreCase) != -1)
                    {
                        cache.Remove(collectionKey);
                    }
                }
            }
        }

        /// <summary>
        /// Inserts the specified  object to the cache.
        /// </summary>
        /// <param name="cache">The cache.</param>
        /// <param name="key">The cache key.</param>
        /// <param name="cacheObject">The object to cache.</param>
        /// <param name="time">The cache time.</param>
        /// <param name="cacheDependency">The cache dependency.</param>
        public static void Insert([Jig.Annotations.NotNull] this Cache cache, [Jig.Annotations.NotNull] string key, [Jig.Annotations.NotNull] object cacheObject, CachingTime time = CachingTime.Normal, [Jig.Annotations.CanBeNull] CacheDependency cacheDependency = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (!string.IsNullOrWhiteSpace(key) && null != cacheObject)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                cache.Insert(key, cacheObject, cacheDependency, DateTime.Now.AddMinutes((double)time), Cache.NoSlidingExpiration);
            }
        }

        /// <summary>
        /// Caches the insert.
        /// </summary>
        /// <param name="cacheObject">The cache object.</param>
        /// <param name="key">The cache key.</param>
        /// <param name="time">The cache  time.</param>
        /// <param name="cacheDependency">The cache dependency.</param>
        public static void CacheInsert([Jig.Annotations.NotNull] this object cacheObject, [Jig.Annotations.NotNull] string key, CachingTime time = CachingTime.Normal, [Jig.Annotations.CanBeNull] CacheDependency cacheDependency = null)
        {
            Insert(HttpRuntime.Cache, key, cacheObject, time, cacheDependency);
        }

        /// <summary>
        /// Caches the insert.
        /// </summary>
        /// <param name="cacheObject">The cache object.</param>
        /// <param name="key">The key.</param>
        /// <param name="time">The time.</param>
        /// <param name="cacheDependency">The cache dependency.</param>
        public static void CacheInsert([Jig.Annotations.NotNull] this ICollection<object> cacheObject, [Jig.Annotations.NotNull] string key, CachingTime time = CachingTime.Normal, [Jig.Annotations.NotNull] CacheDependency cacheDependency = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (cacheObject != null && cacheObject.Count != 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                Insert(HttpRuntime.Cache, key, cacheObject, time, cacheDependency);
            }
        }

        /// <summary>
        /// Caches the insert.
        /// </summary>
        /// <param name="cacheObject">The cache object.</param>
        /// <param name="key">The key.</param>
        /// <param name="time">The time.</param>
        /// <param name="cacheDependency">The cache dependency.</param>
        public static void CacheInsert([Jig.Annotations.NotNull] this IList<object> cacheObject, [Jig.Annotations.NotNull] string key, CachingTime time = CachingTime.Normal, [Jig.Annotations.NotNull] CacheDependency cacheDependency = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (cacheObject != null && cacheObject.Count != 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                Insert(HttpRuntime.Cache, key, cacheObject, time, cacheDependency);
            }
        }

        /// <summary>
        /// Gets the specified object by key from cache.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="cache">The cache instance .</param>
        /// <param name="key">The cache key.</param>
        /// <returns>The object instance or null</returns>
        [Jig.Annotations.CanBeNull]
        public static TObject Get<TObject>([Jig.Annotations.NotNull] this Cache cache, [Jig.Annotations.NotNull] string key) where TObject : class
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                return cache[key] as TObject;
            }

            return null;
        }
    }
}
