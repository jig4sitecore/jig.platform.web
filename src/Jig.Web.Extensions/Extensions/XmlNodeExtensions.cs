﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlNodeExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       09/15/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Xml;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class XmlNodeExtensions
    {
        /// <summary>
        /// Selects the single node inner text as string.
        /// </summary>
        /// <param name="node">The xml node.</param>
        /// <param name="xpath">The XPath expression.</param>
        /// <returns>The value of the inner text</returns>
        [Jig.Annotations.NotNull]
        public static string SelectSingleNodeInnerText([Jig.Annotations.NotNull] this XmlNode node, [Jig.Annotations.NotNull] string xpath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (node != null && !string.IsNullOrWhiteSpace(xpath))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var childNode = node.SelectSingleNode(xpath);
                if (childNode != null)
                {
                    return childNode.InnerText;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get specific attribute the from specified node.
        /// </summary>
        /// <param name="node">The xml node.</param>
        /// <param name="attributeName">The attribute name.</param>
        /// <returns>The attribute value or empty string</returns>
        [Jig.Annotations.NotNull]
        public static string GetAttributeValue([Jig.Annotations.NotNull] this XmlNode node, [Jig.Annotations.NotNull] string attributeName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (node != null && node.Attributes != null && !string.IsNullOrWhiteSpace(attributeName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var attribute = node.Attributes[attributeName];
                if (attribute != null)
                {
                    return attribute.Value;
                }
            }

            return string.Empty;
        }
    }
}
