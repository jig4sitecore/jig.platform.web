﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CollectionExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class CollectionExtensions
    {
        /// <summary>
        ///     Indicates whether the specified System.Collections.ICollection"/> is null or empty.
        /// </summary>
        /// <param name="collection">System.Collections.ICollection"/> to check for.</param>
        /// <returns>Return true if Count is 0 or it is null</returns>
        public static bool IsNullOrEmpty([Jig.Annotations.NotNull] this ICollection collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return collection == null || collection.Count == 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        ///     Indicates whether the specified ICollection is null or empty.
        /// </summary>
        /// <typeparam name="T">The type of the collection.</typeparam>
        /// <param name="collection">The collection instance</param>
        /// <returns>
        ///     <c>true</c> if the collection is null or empty, otherwise <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<T>([Jig.Annotations.NotNull] this ICollection<T> collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return collection == null || collection.Count == 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Determines whether [is not null or empty] [the specified collection].
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>
        ///   <c>true</c> if [is not null or empty] [the specified collection]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNullOrEmpty([Jig.Annotations.NotNull] this ICollection collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return !(collection == null || collection.Count == 0);
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Determines whether [is not null or empty] [the specified collection].
        /// </summary>
        /// <typeparam name="T">The entity</typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>
        ///   <c>true</c> if [is not null or empty] [the specified collection]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNullOrEmpty<T>([Jig.Annotations.NotNull] this ICollection<T> collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return !(collection == null || collection.Count == 0);
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Adds the specified collection.
        /// </summary>
        /// <typeparam name="T">The entity</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="values">The values.</param>
        /// <returns>The collection of entities</returns>
        [Jig.Annotations.CanBeNull]
        public static ICollection<T> Add<T>([Jig.Annotations.NotNull] this ICollection<T> collection, [Jig.Annotations.NotNull] IEnumerable<T> values)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null && values != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var value in values)
                {
                    collection.Add(value);
                }
            }

            return collection;
        }

        /// <summary>
        ///     Adds a value uniquely to to a collection and returns a value whether the value was added or not.
        /// </summary>
        /// <typeparam name="T">The generic collection value type</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="value">The value to be added.</param>
        /// <returns>Indicates whether the value was added or not</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// list.AddUnique(1); // returns true;
        /// list.AddUnique(1); // returns false the second time;
        /// </code></example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        public static ICollection<T> AddUnique<T>([Jig.Annotations.NotNull] this ICollection<T> collection, T value)
        {
            if (!collection.Contains(value))
            {
                collection.Add(value);
            }

            return collection;
        }
    }
}
