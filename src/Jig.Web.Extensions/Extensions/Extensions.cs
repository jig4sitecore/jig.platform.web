﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Extensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    public static partial class Extensions
    {
        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The Exception Message text</returns>
        [Jig.Annotations.NotNull]
        public static string GetExceptionMessage([Jig.Annotations.NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (exception.InnerException != null)
                {
                    return exception.InnerException.Message;
                }

                return exception.Message;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the error source.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception source text</returns>
        [Jig.Annotations.NotNull]
        public static string GeExceptionSource([Jig.Annotations.NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (exception.InnerException != null)
                {
                    return exception.InnerException.Source;
                }

                return exception.Source;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the name of the error type.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception type name text</returns>
        [Jig.Annotations.NotNull]
        public static string GetExceptionTypeName([Jig.Annotations.NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (exception.InnerException != null)
                {
                    return exception.InnerException.GetType().FullName;
                }

                return exception.GetType().FullName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the error error details.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>the exception detail text</returns>
        [Jig.Annotations.NotNull]
        public static string GetExceptionDetails([Jig.Annotations.NotNull] this Exception exception)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (exception != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (exception.InnerException != null)
                {
                    return exception.InnerException.ToString();
                }

                return exception.ToString();
            }

            return string.Empty;
        }
    }
}