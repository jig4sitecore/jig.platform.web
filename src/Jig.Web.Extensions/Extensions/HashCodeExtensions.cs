﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HashCodeExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Globalization;
    using System.Xml.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class HashCodeExtensions
    {
        /// <summary>
        ///     Case insensitive hash for small strings
        /// </summary>
        /// <param name="input">string to get hash code</param>
        /// <returns>The hash value of the string</returns>
        /// <remarks>This method were modified for .NET</remarks>
        [DebuggerHidden]
        public static int ConvertAsHashCode([Jig.Annotations.NotNull] this string input)
        {
            unchecked
            {
                if (!string.IsNullOrEmpty(input))
                {
                    return input.ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                return string.Empty.GetHashCode();
            }
        }

        /// <summary>
        ///     Case insensitive hash for small size XElement
        /// </summary>
        /// <param name="element">XElement to get hash code</param>
        /// <returns>The hash value of the XElement</returns>
        /// <remarks>This method were modified for .NET</remarks>
        [DebuggerHidden]
        public static int ConvertAsHashCode([Jig.Annotations.NotNull] this XElement element)
        {
            unchecked
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (element != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return element.ToString().ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                // ReSharper disable HeuristicUnreachableCode
                return string.Empty.GetHashCode();
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        /// <summary>
        ///     Case insensitive hash for small size XContainer
        /// </summary>
        /// <param name="element">XContainer to get hash code</param>
        /// <returns>The hash value of the XContainer</returns>
        /// <remarks>This method were modified for .NET</remarks>
        [DebuggerHidden]
        public static int ConvertAsHashCode([Jig.Annotations.NotNull] this XContainer element)
        {
            unchecked
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (element != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return element.ToString(SaveOptions.None).ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                // ReSharper disable HeuristicUnreachableCode
                return "XContainer".GetHashCode();
                // ReSharper restore HeuristicUnreachableCode
            }
        }

        /// <summary>
        ///     Case insensitive hash for small size XNode
        /// </summary>
        /// <param name="element">XNode to get hash code</param>
        /// <returns>The hash value of the XNode</returns>
        /// <remarks>This method were modified for .NET</remarks>
        [DebuggerHidden]
        public static int ConvertAsHashCode([Jig.Annotations.NotNull] this XNode element)
        {
            unchecked
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (element != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return element.ToString(SaveOptions.None).ToUpper(CultureInfo.InvariantCulture).GetHashCode();
                }

                // ReSharper disable HeuristicUnreachableCode
                return "XNode".GetHashCode();
                // ReSharper restore HeuristicUnreachableCode
            }
        }
    }
}
