﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.Replace.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Collections.Generic;
    using System.Text;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        /// Replace All non-digit chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string.</param>
        /// <returns>Input string without additional chars</returns>
        [Jig.Annotations.NotNull]
        public static string ReplaceNonDigitsChars([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] string replacement, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsDigit(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsDigit(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Replace All non-digit chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string.</param>
        /// <returns>User input with digits only left</returns>
        [Jig.Annotations.NotNull]
        public static string ReplaceNonNumberChars([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] string replacement, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                int firstIndex = input.IndexOf('.');
                if (firstIndex > -1)
                {
                    int lastIndex = input.LastIndexOf('.');
                    if (firstIndex != lastIndex)
                    {
                        throw new HttpException("The string can't contain more then one '.' char!");
                    }
                }

                var builder = new StringBuilder(input.Length);
                if (exceptCharacters.Length == 0)
                {
                    for (int i = 0; i < input.Length; i++)
                    {
                        char current = input[i];
                        if (i == 0 && '.'.Equals(current))
                        {
                            /* Add 0 prefix for cases like '.05' */
                            builder.Append(0);
                            builder.Append(current);
                            continue;
                        }

                        if (char.IsDigit(current) || '.'.Equals(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    for (int i = 0; i < input.Length; i++)
                    {
                        char current = input[i];
                        if (i == 0 && '.'.Equals(current))
                        {
                            /* Add 0 prefix for cases like '.05' */
                            builder.Append(0);
                            builder.Append(current);
                            continue;
                        }

                        if (char.IsDigit(current) || '.'.Equals(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Replace All non-letter chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string</param>
        /// <returns>User input with letters only left</returns>
        [Jig.Annotations.NotNull]
        public static string ReplaceNonLettersChars([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] string replacement, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsLetter(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsLetter(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Replace All non-alphanumeric chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="replacement">The replacement.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string.</param>
        /// <returns>User input with Alphanumeric only left</returns>
        [Jig.Annotations.NotNull]
        public static string ReplaceNonLetterOrDigitChars([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] string replacement, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsLetterOrDigit(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsLetterOrDigit(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            builder.Append(replacement);
                        }
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Deletes/removes a specified string from the current string
        /// </summary>
        /// <param name="value">Value to modify</param>
        /// <param name="stringToReplace">Substring, which is deleted from value</param>
        /// <param name="replacement">The replacement.</param>
        /// <returns>Modified string</returns>
        [Jig.Annotations.NotNull]
        public static string Replace([Jig.Annotations.NotNull] this string value, [Jig.Annotations.NotNull] string stringToReplace, [Jig.Annotations.NotNull] string replacement)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return value.Replace(stringToReplace, replacement);
            }

            return value;
        }
    }
}
