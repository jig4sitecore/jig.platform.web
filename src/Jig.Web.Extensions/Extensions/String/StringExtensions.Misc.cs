﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.Misc.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Text;
    using System.Text.RegularExpressions;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        ///     Determines whether the comparison value string is contained within the input value string
        /// </summary>
        /// <param name="inputValue">The input value.</param>
        /// <param name="comparisonValue">The comparison value.</param>
        /// <param name="comparisonType">Type of the comparison to allow case sensitive or insensitive comparison.</param>
        /// <returns>
        ///     <c>true</c> if input value contains the specified value, otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains([Jig.Annotations.NotNull] this string inputValue, [Jig.Annotations.NotNull] string comparisonValue, StringComparison comparisonType)
        {
            if (!string.IsNullOrEmpty(inputValue) && !string.IsNullOrEmpty(comparisonValue))
            {
                return inputValue.IndexOf(comparisonValue, comparisonType) != -1;
            }

            return false;
        }

        /// <summary>
        /// Trims the text to a provided maximum length.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="lastIndexOfAny">The last index of any character.</param>
        /// <returns>The string with length equal or less max length</returns>
        [Jig.Annotations.NotNull]
        public static string Truncate([Jig.Annotations.NotNull] this string value, int maxLength, [Jig.Annotations.NotNull] params char[] lastIndexOfAny)
        {
            /* Accounted for the case: the case then last char is equal one of last index of any and max length are equal too */
            if (!string.IsNullOrEmpty(value) && value.Length >= maxLength - 1)
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (lastIndexOfAny == null || lastIndexOfAny.Length == 0)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return value.Substring(0, maxLength);
                }

                string truncated = value.Substring(0, maxLength);
                int index = truncated.LastIndexOfAny(lastIndexOfAny);
                if (index > 1)
                {
                    return truncated.Substring(0, index - 1);
                }

                return truncated;
            }

            return value;
        }

        /// <summary>
        /// Ensures that a string starts with a given prefix.
        /// </summary>
        /// <param name="value">The string value to check.</param>
        /// <param name="prefix">The prefix value to check for.</param>
        /// <param name="comparison">The comparison.</param>
        /// <returns>
        /// The string value including the prefix
        /// </returns>
        /// <example>View code: <br/>
        ///   <code title="C# File" lang="C#">
        /// var extension = "txt";
        /// var fileName = string.Concat(file.Name, extension.EnsureStartsWith("."));
        ///   </code>
        /// </example>
        [Jig.Annotations.NotNull]
        public static string EnsureStartsWith([Jig.Annotations.NotNull] this string value, [Jig.Annotations.NotNull] string prefix, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (value.StartsWith(prefix, comparison))
                {
                    return value;
                }
            }

            return string.Concat(prefix, value);
        }

        /// <summary>
        /// Ensures that a string ends with a given suffix.
        /// </summary>
        /// <param name="value">The string value to check.</param>
        /// <param name="suffix">The suffix value to check for.</param>
        /// <param name="comparison">The comparison.</param>
        /// <returns>
        /// The string value including the suffix
        /// </returns>
        /// <example>View code: <br/>
        ///   <code title="C# File" lang="C#">
        /// var dir = Environment.CurrentDirectory;
        /// dir = dir.EnsureEndsWith("/"));
        ///   </code>
        /// </example>
        [Jig.Annotations.NotNull]
        public static string EnsureEndsWith([Jig.Annotations.NotNull] this string value, [Jig.Annotations.NotNull] string suffix, StringComparison comparison = StringComparison.InvariantCultureIgnoreCase)
        {
            if (!string.IsNullOrEmpty(value) && value.EndsWith(suffix, comparison))
            {
                return value;
            }

            return string.Concat(value, suffix);
        }

        /// <summary>
        /// Splits the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="regex">The regex.</param>
        /// <returns>The collection of strings</returns>
        [Jig.Annotations.NotNull]
        public static IList<string> Split([Jig.Annotations.NotNull] this string value, [Jig.Annotations.NotNull] Regex regex)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (!string.IsNullOrWhiteSpace(value) || regex != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // ReSharper disable AssignNullToNotNullAttribute
                return regex.Split(value);
                // ReSharper restore AssignNullToNotNullAttribute
            }

            return new Collection<string>();
        }

        /// <summary>
        /// Splits the given string into words and returns a string array.
        /// </summary>
        /// <param name="value">The input string.</param>
        /// <returns>The spitted string array</returns>
        [Jig.Annotations.NotNull]
        public static IList<string> GetWords([Jig.Annotations.NotNull] this string value)
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                var regex = new Regex(@"\W", RegexOptions.Compiled | RegexOptions.CultureInvariant);
                return value.Split(regex);
            }

            return new Collection<string>();
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <param name="bytes">The bytes.</param>
        /// <returns>The converted bytes to string</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsString([Jig.Annotations.NotNull] this byte[] bytes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bytes != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return Encoding.UTF8.GetString(bytes);
            }

            return string.Empty;
        }
    }
}
