﻿// -----------------------------------------------------------------------------
// <copyright file="StringExtensions.XmlSymbols.cs" company="genuine">
//      Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------------
//   Author:     J.Baltika
//   Date:       10/29/2013
// -----------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics.CodeAnalysis;
    using System.Text;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        /// Encodes input strings for use in Xml.
        /// </summary>
        /// <param name="input">String input to HtmlEncode.</param>
        /// <returns>
        /// Encoded  string.
        /// </returns>
        /// <remarks>
        /// Intended use for Control method 'public override void RenderControl(HtmlTextWriter writer)'.
        /// </remarks>
        [Jig.Annotations.NotNull]
        public static string ConvertAsXmlExtendedSymbols([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var symbols = XmlSymbolHelper.XmlSymbols;
                var builder = new StringBuilder(string.Empty, input.Length + 128);

                foreach (char ch in input)
                {
                    if (symbols.ContainsKey(ch))
                    {
                        builder.Append(symbols[ch]);
                    }
                    else
                    {
                        builder.Append(ch);
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Encodes input strings for use in Xml.
        /// </summary>
        /// <param name="input">String input to HtmlEncode.</param>
        /// <returns>
        /// Encoded  string.
        /// </returns>
        /// <remarks>
        /// Intended use for Control method 'public override void RenderControl(HtmlTextWriter writer)'.
        /// </remarks>
        [Jig.Annotations.NotNull]
        public static string ConvertAsXmlExtendedSymbols([Jig.Annotations.NotNull] this StringBuilder input)
        {
            var symbols = XmlSymbolHelper.XmlSymbols;
            var builder = new StringBuilder(string.Empty, input.Length + 128);

            for (int i = 0; i < input.Length; i++)
            {
                char ch = input[i];
                if (symbols.ContainsKey(ch))
                {
                    builder.Append(symbols[ch]);
                }
                else
                {
                    builder.Append(ch);
                }
            }

            return builder.ToString();
        }

        #endregion
    }
}