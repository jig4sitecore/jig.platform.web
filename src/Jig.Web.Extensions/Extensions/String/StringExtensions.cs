﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Text;
    using System.Text.RegularExpressions;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class StringExtensions
    {
        /// <summary>
        ///     Converts First char lower case except first letter will be capital.
        /// </summary>
        /// <param name="input">The input value</param>
        /// <param name="separators">The char array to split string in tokens and capitalize first letters</param>
        /// <returns>Formatted first or last name to first capital letter and rest lower case like 'Jim' or 'Beam'</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertToUpperCaseFirstLetters([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] params char[] separators)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                if (separators.Length == 0)
                {
                    return char.ToUpperInvariant(input[0]) + input.Substring(1);
                }

                char[] inputCharacters = input.ToCharArray();

                // Case then has additional chars
                var list = new List<char>(separators.Length);
                list.AddRange(separators);

                for (int i = 0; i < inputCharacters.Length; i++)
                {
                    if (i == 0)
                    {
                        inputCharacters[0] = char.ToUpperInvariant(inputCharacters[0]);
                    }
                    else
                    {
                        if (list.Contains(inputCharacters[i - 1]))
                        {
                            inputCharacters[i] = char.ToUpperInvariant(inputCharacters[i]);
                        }
                    }
                }

                return new string(inputCharacters);
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts the string to pascal case.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The converted string</returns>
        /// <remarks>Its the same method as 'ToLowerCaseFirstLetter'. The different developers prefer different names. Make it ease for for all.</remarks>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ConvertToPascalCase([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                return char.ToUpperInvariant(input[0]) + input.Substring(1);
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts string first letter to the lower case.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The modified string</returns>
        /// <remarks>Its the same method as 'ToPascalCase'. The different developers prefer different names. Make it ease for for all.</remarks>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ConvertToLowerCaseFirstLetter([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                return char.ToUpperInvariant(input[0]) + input.Substring(1);
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts the string to camel case.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The modified string</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ConvertToCamelCase([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                char[] inputCharacters = input.ToCharArray();
                inputCharacters[0] = char.ToLowerInvariant(inputCharacters[0]);

                return new string(inputCharacters);
            }

            return string.Empty;
        }

        /// <summary>
        /// Splits pascal case, so "FooBar" would become "Foo Bar"
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Split words</returns>
        [Jig.Annotations.NotNull]
        public static string SplitPascalCase([Jig.Annotations.NotNull] this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            return Regex.Replace(input, "([A-Z])", " $1").Trim();
        }

        /// <summary>
        ///     Compares a string to a given string. The comparison is case insensitive.
        /// </summary>      
        /// <param name="source">The string to compare</param>
        /// <param name="target">The string to compare against</param>
        /// <returns>True if the strings are the same, false otherwise.</returns>
        [DebuggerHidden]
        public static bool IsEquals([Jig.Annotations.NotNull] this string source, [Jig.Annotations.NotNull] string target)
        {
            return string.Compare(source, target, StringComparison.OrdinalIgnoreCase) == 0;
        }

        /// <summary>
        /// Safe removal all leading and trailing white-space characters from the current string.
        /// </summary>
        /// <param name="value">The string value.</param>
        /// <returns> A string without leading and trailing white-space characters.</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string TrimSpaces([Jig.Annotations.NotNull] this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }

            return value.Trim();
        }

        /// <summary>
        /// Normalizes string to ISO-8859-1.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The normalized string</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string NormalizeToIso88591([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                /* Normalize */
                byte[] tempBytes = Encoding.GetEncoding("ISO-8859-1").GetBytes(input);
                return Encoding.UTF8.GetString(tempBytes);
            }

            return string.Empty;
        }

        /// <summary>
        /// Normalizes string to Utf8.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The normalized string</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string NormalizeToUtf8([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                /* Normalize */
                byte[] tempBytes = Encoding.GetEncoding("Utf-8").GetBytes(input);
                return Encoding.UTF8.GetString(tempBytes);
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the value or default.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The value or default (string.Empty)</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string GetValueOrDefault([Jig.Annotations.NotNull] this string input)
        {
            return string.IsNullOrWhiteSpace(input) ? string.Empty : input;
        }

        /// <summary>
        /// Gets the value or default.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="value">The value.</param>
        /// <returns>The value or default (string.Empty)</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string GetValueOrDefault([Jig.Annotations.NotNull] this string input, string value)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return string.IsNullOrWhiteSpace(value) ? string.Empty : value;
            }

            return input;
        }
    }
}