﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.AsciiAndUnicode.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/14/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Globalization;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        ///     Encodes/Converts the extended ASCII chars from string to Xml And Html Entities.
        ///     http://en.wikipedia.org/wiki/List_of_XML_and_HTML_character_entity_references
        /// </summary>
        /// <param name="text">The text input.</param>
        /// <returns>The text without Extended ASCII codes</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsXmlExtendedAsciiChars([Jig.Annotations.NotNull] this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var charArray = text.ToCharArray();

                var textBuilder = new StringBuilder(charArray.Length);
                foreach (char character in charArray)
                {
                    if (character < 128)
                    {
                        textBuilder.Append(character);
                    }
                    else
                    {
                        if ((character >= '\x00a0') && (character < 'Ā'))
                        {
                            textBuilder.Append("&#");
                            textBuilder.Append(((int)character).ToString(NumberFormatInfo.InvariantInfo));
                            textBuilder.Append(';');
                        }
                        else
                        {
                            textBuilder.Append(character);
                        }
                    }
                }

                return textBuilder.ToString().Trim();
            }

            return string.Empty;
        }
    }
}
