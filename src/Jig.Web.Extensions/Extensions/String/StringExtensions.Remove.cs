﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.Remove.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Collections.Generic;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        /// Removes the non digits chars.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Input string without additional chars</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonDigitCharacters([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);
                foreach (char current in input)
                {
                    if (char.IsDigit(current))
                    {
                        builder.Append(current);
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Remove All non-digit chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="appendWhiteSpaceInstead">if set to <c>true</c> append white space instead non-valid character.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string.</param>
        /// <returns>User input with digits only left</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonDigitCharacters([Jig.Annotations.NotNull] this string input, bool appendWhiteSpaceInstead = false, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsDigit(current) || '.'.Equals(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsDigit(current) || '.'.Equals(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }

                return builder.ToString().Trim();
            }

            return string.Empty;
        }

        /// <summary>
        /// Remove All non-letter chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <returns>
        /// User input with letters only left
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonLettersCharacters([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                foreach (char current in input)
                {
                    if (char.IsLetter(current))
                    {
                        builder.Append(current);
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Remove All non-letter chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="appendWhiteSpaceInstead">if set to <c>true</c> append white space instead non-valid character.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string</param>
        /// <returns>
        /// User input with letters only left
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonLettersCharacters([Jig.Annotations.NotNull] this string input, bool appendWhiteSpaceInstead = false, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsLetter(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsLetter(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }

                return builder.ToString().Trim();
            }

            return string.Empty;
        }

        /// <summary>
        /// Remove All non-alphanumeric chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <returns>
        /// User input with Alphanumeric only left
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonLetterOrDigitCharacters([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);

                foreach (char current in input)
                {
                    if (char.IsLetterOrDigit(current))
                    {
                        builder.Append(current);
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Remove All non-alphanumeric chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="appendWhiteSpaceInstead">if set to <c>true</c> append white space instead non-valid character.</param>
        /// <param name="exceptCharacters">Leave (except) chars in the string.</param>
        /// <returns>User input with Alphanumeric only left</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonLetterOrDigitCharacters([Jig.Annotations.NotNull] this string input, bool appendWhiteSpaceInstead = false, [Jig.Annotations.NotNull] params char[] exceptCharacters)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);
                if (exceptCharacters.Length == 0)
                {
                    foreach (char current in input)
                    {
                        if (char.IsLetterOrDigit(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }
                else
                {
                    // Case then has additional chars
                    var list = new List<char>(exceptCharacters.Length);
                    list.AddRange(exceptCharacters);

                    foreach (char current in input)
                    {
                        if (char.IsLetterOrDigit(current) || list.Contains(current))
                        {
                            builder.Append(current);
                        }
                        else
                        {
                            if (appendWhiteSpaceInstead)
                            {
                                builder.Append(' ');
                            }
                        }
                    }
                }

                return builder.ToString().Trim();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Remove All illegal non file name chars from string
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="appendWhiteSpaceInstead">if set to <c>true</c> append white space instead non-valid character.</param>
        /// <returns>User input with correct chars only</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNonFileNameCharacters([Jig.Annotations.NotNull] this string input, bool appendWhiteSpaceInstead = false)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(input.Length);
                foreach (char current in input)
                {
                    if (char.IsLetterOrDigit(current) || char.IsWhiteSpace(current) || '-'.Equals(current) || '.'.Equals(current) || '_'.Equals(current))
                    {
                        builder.Append(current);
                    }
                    else
                    {
                        if (appendWhiteSpaceInstead)
                        {
                            builder.Append(' ');
                        }
                    }
                }

                return builder.ToString().Trim();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Deletes/removes a specified string from the current string
        /// </summary>
        /// <param name="value">Value to modify</param>
        /// <param name="stringToRemove">Substring, which is deleted from value</param>
        /// <returns>Modified string</returns>
        [Jig.Annotations.NotNull]
        public static string Remove([Jig.Annotations.NotNull] this string value, [Jig.Annotations.NotNull] string stringToRemove)
        {
            // From Standard Extensions Library idea
            if (!string.IsNullOrEmpty(value))
            {
                return value.Replace(stringToRemove, string.Empty);
            }

            return value;
        }

        /// <summary>
        /// HTMLs the decode.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>The collection with decoded inputs</returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<string> HtmlDecode([Jig.Annotations.NotNull] this IEnumerable<string> collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var input in collection)
                {
                    yield return input.HtmlDecode();
                }
            }
        }

        /// <summary>
        /// Removes the new lines and tabs.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>The cleaned string</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNewLinesAndTabs([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                var builder = new StringBuilder(input, input.Length + 16);
                foreach (char character in input)
                {
                    if ('\r'.Equals(character) || '\n'.Equals(character))
                    {
                        builder.Append(' ');
                        continue;
                    }

                    if ('\t'.Equals(character))
                    {
                        builder.Append("    ");
                        continue;
                    }

                    if ('\xA0'.Equals(character))
                    {
                        builder.Append('\x20');
                        continue;
                    }

                    builder.Append(character);
                }

                return builder.ToString();
            }

            return string.Empty;
        }
    }
}
