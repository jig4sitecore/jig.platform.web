﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.Base64AndBytes.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        /// Converts the string to a byte-array using the supplied encoding
        /// </summary>
        /// <param name="value">The input string.</param>
        /// <param name="encoding">The encoding to be used.</param>
        /// <returns>The created byte array</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// var value = "Hello World";
        /// var ansiBytes = value.ConvertAsBytes(Encoding.GetEncoding(1252)); // 1252 = ANSI
        /// var utf8Bytes = value.ConvertAsBytes(Encoding.UTF8);
        /// </code></example>
        [Jig.Annotations.CanBeNull]
        public static byte[] ConvertAsBytes([Jig.Annotations.NotNull] this string value, [Jig.Annotations.CanBeNull] Encoding encoding = null)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (value != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                encoding = encoding ?? Encoding.UTF8;
                return encoding.GetBytes(value);
            }

            return null;
        }
    }
}
