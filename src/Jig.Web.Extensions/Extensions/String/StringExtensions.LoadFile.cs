﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.LoadFile.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       08/01/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.IO;
    using System.Net;
    using System.Web;
    using System.Web.Caching;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        /// Load Xml file from Cache if exists, if not will load from file system
        /// </summary>
        /// <param name="virtualPath">The XML virtual path to convert to an application-relative path</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>Loaded XmlTextReader</returns>
        /// <remarks>
        ///     The CacheTime parameter works together with CacheDependency (when this resource changes, the cached object becomes obsolete and is removed from the cache.)
        /// </remarks>
        /// <example>View code: <br />
        /// <code title="Usage" lang="C#">
        /// var xml = "/App_Xml/States.xml".LoadXmlTextReader(CacheTime.Normal);
        /// /* Do something */
        /// </code> 
        /// </example>
        [Jig.Annotations.NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The System.Web.Caching.CacheDependency class monitors the dependency relationships so that when any of them changes, the cached item will be automatically removed.")]
        public static XmlTextReader LoadXmlTextReader([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            XmlTextReader xmlTextReader;

            if (virtualPath.FileExists())
            {
                string mapPath = virtualPath.MapPath();
                var cache = HttpRuntime.Cache;

                // Could be the case then context is null like call from Unit Test DLL.
                if (cache != null)
                {
                    string cacheKey = string.Concat("LoadXmlTextReader-", mapPath);

                    xmlTextReader = cache[cacheKey] as XmlTextReader;
                    if (xmlTextReader == null)
                    {
                        xmlTextReader = new XmlTextReader(cacheKey);

                        if (cacheTime > CachingTime.None)
                        {
                            cache.Insert(cacheKey, xmlTextReader, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                        }
                    }
                }
                else
                {
                    xmlTextReader = new XmlTextReader(mapPath);
                }
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format("The XML file {0} doesn't exist on file system: ", virtualPath.MapPath()));
            }

            return xmlTextReader;
        }

        /// <summary>
        /// Load Xml file from Cache if exists, if not will load from file system
        /// </summary>
        /// <param name="virtualPath">The XML virtual path to convert to an application-relative path</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>Loaded Xml document</returns>
        /// <remarks>
        ///     The CacheTime parameter works together with CacheDependency (when this resource changes, the cached object becomes obsolete and is removed from the cache.)
        /// </remarks>
        /// <example>View code: <br />
        /// <code title="Usage" lang="C#">
        /// /* CacheTime.Normal equals 20 min */
        /// var xml = "/App_Xml/States.xml".LoadXml(CacheTime.Normal);
        /// /* Do something */
        /// </code> 
        /// </example> 
        [Jig.Annotations.NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The System.Web.Caching.CacheDependency class monitors the dependency relationships so that when any of them changes, the cached item will be automatically removed.")]
        public static XmlDocument LoadXml([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            XmlDocument xmlDocument;

            if (virtualPath.FileExists())
            {
                // Converts a virtual path to an application absolute path.
                string mapPath = virtualPath.MapPath();

                var cache = HttpRuntime.Cache;

                // Could be the case then context is null like call from Unit Test DLL.
                if (cache != null)
                {
                    string cacheKey = string.Concat("LoadXml-", mapPath);

                    xmlDocument = cache[cacheKey] as XmlDocument;

                    if (xmlDocument == null)
                    {
                        xmlDocument = new XmlDocument();
                        xmlDocument.Load(mapPath);

                        if (cacheTime > CachingTime.None)
                        {
                            cache.Insert(cacheKey, xmlDocument, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                        }
                    }
                }
                else
                {
                    xmlDocument = new XmlDocument();
                    xmlDocument.Load(mapPath);
                }
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format("The XML file {0} doesn't exist on file system: ", virtualPath.MapPath()));
            }

            return xmlDocument;
        }

        /// <summary>
        /// Load Xml file from Cache if exists, if not will load from file system
        /// </summary>
        /// <param name="virtualPath">The XML virtual path to convert to an application-relative path</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>Loaded XPathDocument</returns>
        /// <remarks>
        ///     The CacheTime parameter works together with CacheDependency (when this resource changes, the cached object becomes obsolete and is removed from the cache.)
        /// </remarks>
        /// <example>View code: <br />
        /// <code title="Usage" lang="C#">
        /// /* CacheTime.Normal equals 20 min */
        /// var xml = "/App_Xml/States.xml".LoadXPathDocument(CacheTime.Normal);
        /// /* Do something */
        /// </code> 
        /// </example>
        [Jig.Annotations.NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The System.Web.Caching.CacheDependency class monitors the dependency relationships so that when any of them changes, the cached item will be automatically removed.")]
        public static XPathDocument LoadXPathDocument([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            XPathDocument document;
            if (virtualPath.FileExists())
            {
                // Converts a virtual path to an application absolute path.
                string mapPath = virtualPath.MapPath();

                var cache = HttpRuntime.Cache;

                // Could be the case then context is null like call from Unit Test DLL.
                if (cache != null)
                {
                    string cacheKey = string.Concat("LoadXPathDocument-", mapPath);

                    document = cache[cacheKey] as XPathDocument;
                    if (document == null)
                    {
                        document = new XPathDocument(cacheKey);

                        if (cacheTime > CachingTime.None)
                        {
                            cache.Insert(cacheKey, document, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                        }
                    }
                }
                else
                {
                    document = new XPathDocument(mapPath);
                }
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format("The XML file {0} doesn't exist on file system: ", virtualPath.MapPath()));
            }

            return document;
        }

        /// <summary>
        /// Loads the file.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>The file content</returns>
        [Jig.Annotations.NotNull]
        public static string LoadTextFile([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            if (!string.IsNullOrWhiteSpace(virtualPath) && virtualPath.FileExists())
            {
                string mapPath = virtualPath.MapPath();

                var cache = HttpRuntime.Cache;

                string cacheKey = string.Concat("LoadFile-", mapPath);

                var file = cache[cacheKey] as string;
                if (string.IsNullOrWhiteSpace(file))
                {
                    file = File.ReadAllText(mapPath);
                    if (cacheTime > CachingTime.None)
                    {
                        cache.Insert(cacheKey, file, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                    }
                }

                // ReSharper disable AssignNullToNotNullAttribute
                return file;
                // ReSharper restore AssignNullToNotNullAttribute
            }

            return string.Empty;
        }

        /// <summary>
        /// Load XElement from Cache if exists, if not will load from file system.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>Loaded XElement</returns>
        /// <remarks>
        ///     The CacheTime parameter works together with CacheDependency (when this resource changes, the cached object becomes obsolete and is removed from the cache.)
        /// </remarks>
        /// <example>View code: <br />
        /// <code title="Usage" lang="C#">
        /// /* CacheTime.Normal equals 20 min */
        /// var xml = "/App_Xml/States.xml".LoadXElement(CacheTime.Normal);
        /// /* Do something */
        /// </code> 
        /// </example>
        [Jig.Annotations.NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The System.Web.Caching.CacheDependency class monitors the dependency relationships so that when any of them changes, the cached item will be automatically removed.")]
        public static XElement LoadXElement([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            XElement root;

            if (virtualPath.FileExists())
            {
                string mapPath = virtualPath.MapPath();

                var cache = HttpRuntime.Cache;

                // Could be the case then context is null like call from Unit Test DLL.
                if (cache != null)
                {
                    string cacheKey = string.Concat("LoadXElement-", mapPath);

                    root = cache[cacheKey] as XElement;
                    if (root == null)
                    {
                        root = XElement.Load(mapPath);

                        if (cacheTime > CachingTime.None)
                        {
                            cache.Insert(cacheKey, root, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                        }
                    }
                }
                else
                {
                    root = XElement.Load(mapPath);
                }
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format("The XML file {0} doesn't exist on file system: ", virtualPath.MapPath()));
            }

            return root;
        }

        /// <summary>
        /// Load XDocument from Cache if exists, if not will load from file system.
        /// </summary>
        /// <param name="virtualPath">The virtual path.</param>
        /// <param name="cacheTime">The cache time.</param>
        /// <returns>Loaded XDocument</returns>
        /// <remarks>
        ///     The CacheTime parameter works together with CacheDependency (when this resource changes, the cached object becomes obsolete and is removed from the cache.)
        /// </remarks>
        /// <example>View code: <br />
        /// <code title="Usage" lang="C#">
        /// /* CacheTime.Normal equals 20 min */
        /// var xml = "/App_Xml/States.xml".LoadXDocument(CacheTime.Normal);
        /// /* Do something */
        /// </code> 
        /// </example>
        [Jig.Annotations.NotNull]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "The System.Web.Caching.CacheDependency class monitors the dependency relationships so that when any of them changes, the cached item will be automatically removed.")]
        public static XDocument LoadXDocument([Jig.Annotations.NotNull] this string virtualPath, CachingTime cacheTime = CachingTime.AboveHigh)
        {
            XDocument root;

            if (virtualPath.FileExists())
            {
                string mapPath = virtualPath.MapPath();

                var cache = HttpRuntime.Cache;

                // Could be the case then context is null like call from Unit Test DLL.
                if (cache != null)
                {
                    string cacheKey = string.Concat("LoadXElement-", mapPath);

                    root = cache[cacheKey] as XDocument;
                    if (root == null)
                    {
                        root = XDocument.Load(mapPath);

                        if (cacheTime > CachingTime.None)
                        {
                            cache.Insert(cacheKey, root, new CacheDependency(mapPath), DateTime.Now.AddMinutes((double)cacheTime), Cache.NoSlidingExpiration);
                        }
                    }
                }
                else
                {
                    root = XDocument.Load(mapPath);
                }
            }
            else
            {
                throw new HttpException((int)HttpStatusCode.NotFound, string.Format("The XML file {0} doesn't exist on file system: ", virtualPath.MapPath()));
            }

            return root;
        }
    }
}
