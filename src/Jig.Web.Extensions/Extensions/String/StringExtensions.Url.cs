﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringExtensions.Url.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    public static partial class StringExtensions
    {
        /// <summary>
        /// Removes the not allowed URL segment characters.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="allowed">The allowed.</param>
        /// <returns>The url segment</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNotAllowedUrlSegmentCharacters([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] params char[] allowed)
        {
            var value = RemoveNotAllowedUrlSegmentCharacters(input, new CultureInfo("en-US"), allowed);

            return value;
        }

        /// <summary>
        /// Removes the not allowed URL segment characters.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="cultureInfo">The culture information.</param>
        /// <param name="allowed">The allowed.</param>
        /// <returns>The url segment</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNotAllowedUrlSegmentCharacters([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] CultureInfo cultureInfo, [Jig.Annotations.NotNull] params char[] allowed)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                input = input.Trim(' ', '~', '.', '?', '|', '!', ',').ToLower(cultureInfo);
                if (input.Length > 0)
                {
                    /* Normalize */
                    byte[] tempBytes = Encoding.GetEncoding("ISO-8859-1").GetBytes(input);
                    input = Encoding.UTF8.GetString(tempBytes);

                    var builder = new StringBuilder(input.Length);
                    for (int i = 0; i < input.Length; i++)
                    {
                        char current = input[i];

                        if (char.IsLetterOrDigit(current))
                        {
                            builder.Append(current);
                        }
                        else if (char.IsWhiteSpace(current))
                        {
                            if (!'-'.Equals(input[i - 1]))
                            {
                                builder.Append('-');
                            }
                        }
                        else if ('-'.Equals(current))
                        {
                            if (i != 0 && i + 1 != input.Length && !'-'.Equals(input[i - 1]))
                            {
                                builder.Append('-');
                            }
                        }
                        else if (allowed.Contains(current))
                        {
                            builder.Append(current);
                        }
                    }

                    builder.Replace("--", "-");

                    return builder.ToString().Trim(' ', '-', '.');
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Removes the non URL chars.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <param name="allowed">The allowed.</param>
        /// <returns>The extension-less url</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveNotAllowedUrlCharacters([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] CultureInfo cultureInfo, [Jig.Annotations.NotNull] params char[] allowed)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                input = input.Trim(' ', '~', '.').ToLower(cultureInfo);
                if (input.Length > 0)
                {
                    /* Normalize */
                    byte[] tempBytes = Encoding.GetEncoding("ISO-8859-1").GetBytes(input);
                    input = Encoding.UTF8.GetString(tempBytes);

                    var builder = new StringBuilder(input.Length);
                    for (int i = 0; i < input.Length; i++)
                    {
                        char current = input[i];

                        if (char.IsLetterOrDigit(current) || '/'.Equals(current))
                        {
                            builder.Append(current);
                        }
                        else if (char.IsWhiteSpace(current))
                        {
                            if (!'-'.Equals(input[i - 1]))
                            {
                                builder.Append('-');
                            }
                        }
                        else if ('-'.Equals(current))
                        {
                            if (i != 0 && i + 1 != input.Length && !'-'.Equals(input[i - 1]))
                            {
                                builder.Append('-');
                            }
                        }
                        else if (allowed.Contains(current))
                        {
                            builder.Append(current);
                        }
                    }

                    builder.Replace("//", "/");

                    builder.Replace("..", ".");
                    builder.Replace("--", "-");

                    builder.Replace("/.", "/");
                    builder.Replace("./", "/");

                    builder.Replace("-/", "/");
                    builder.Replace("/-", "/");

                    builder.Replace(".-", ".");
                    builder.Replace("-.", ".");

                    builder.Replace("//", "/");

                    return builder.ToString();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Leaves all alphanumeric, '/' and '-' chars in string. The '.' are removed by default.
        /// </summary>
        /// <param name="input">User input</param>
        /// <param name="allowed">The allowed.</param>
        /// <returns>
        /// The extension-less url
        /// </returns>
        /// <seealso href="http://blogs.msdn.com/b/michkap/archive/2007/05/14/2629747.aspx"/>
        /// <remarks>
        /// Not 100% prof the normalization of the string. The normalization is complex thing and the method normalization will take care of most cases.
        /// </remarks>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Justification = "This case must be Url as string")]
        public static string RemoveNotAllowedUrlCharacters([Jig.Annotations.NotNull] this string input, [Jig.Annotations.NotNull] params char[] allowed)
        {
            var value = RemoveNotAllowedUrlCharacters(input, new CultureInfo("en-US"), allowed);

            return value;
        }

        /// <summary>
        /// Gets the file name from URL, stripping off any Query String.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns>The File Name</returns>
        [Jig.Annotations.NotNull]
        public static string GetFileNameFromUrl([Jig.Annotations.NotNull] this string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                var urlArray = url.Split('/');

                var lastPiece = urlArray[urlArray.Length - 1];
                if (lastPiece.Contains("?"))
                {
                    var queryStringArray = lastPiece.Split('?');
                    return queryStringArray[0];
                }

                return lastPiece;
            }

            return url;
        }
    }
}
