﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BitmapExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Linq;
    using System.Web;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class BitmapExtensions
    {
        /// <summary>
        /// Gets the image codec info.
        /// </summary>
        /// <param name="format">The format.</param>
        /// <returns>The Image codec info</returns>
        [Jig.Annotations.CanBeNull]
        public static ImageCodecInfo GetImageCodecInfo([Jig.Annotations.NotNull] this ImageFormat format)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (format != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                ImageCodecInfo[] codecInfo = ImageCodecInfo.GetImageDecoders();

                return codecInfo.FirstOrDefault(codec => codec.FormatID == format.Guid);
            }

            return null;
        }

        /// <summary>
        /// Saves as Specified format.
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="isVirtualPath">if set to <c>true</c> [is virtual path].</param>
        public static void SaveWithFormat([Jig.Annotations.NotNull] this Image bmp, [Jig.Annotations.NotNull] ImageFormat imageFormat, [Jig.Annotations.NotNull] string filePath, long quality = 85L, bool isVirtualPath = true)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bmp != null && imageFormat != null && !string.IsNullOrWhiteSpace(filePath))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (quality > 100 || quality < 30)
                {
                    quality = 85L;
                }

                using (var encoderParameters = new EncoderParameters(1))
                {
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);

                    if (isVirtualPath)
                    {
                        filePath = filePath.MapPath();
                    }

                    bmp.Save(filePath, imageFormat.GetImageCodecInfo(), encoderParameters);
                }
            }
        }

        /// <summary>
        /// Saves as Specified format.
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="imageFormat">The image format.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="quality">The quality.</param>
        public static void SaveWithFormat([Jig.Annotations.NotNull] this Image bmp, [Jig.Annotations.NotNull] ImageFormat imageFormat, [Jig.Annotations.NotNull] Stream stream, long quality = 85L)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bmp != null && imageFormat != null && stream != null && stream.CanWrite)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (quality > 100 || quality < 30)
                {
                    quality = 85L;
                }

                using (var encoderParameters = new EncoderParameters(1))
                {
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
                    bmp.Save(stream, imageFormat.GetImageCodecInfo(), encoderParameters);
                }
            }
        }

        /// <summary>
        /// Saves Bitmap as as JPEG.
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="quality">The quality.</param>
        /// <param name="isVirtualPath">if set to <c>true</c> [is virtual path].</param>
        public static void SaveAsJpeg([Jig.Annotations.NotNull] this Image bmp, [Jig.Annotations.NotNull] string filePath, long quality = 80L, bool isVirtualPath = true)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bmp != null && !string.IsNullOrWhiteSpace(filePath))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (quality > 100 || quality < 30)
                {
                    quality = 85L;
                }

                using (var encoderParameters = new EncoderParameters(1))
                {
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);

                    if (isVirtualPath)
                    {
                        filePath = filePath.MapPath();
                    }

                    bmp.Save(filePath, ImageFormat.Jpeg.GetImageCodecInfo(), encoderParameters);
                }
            }
        }

        /// <summary>
        /// Saves as JPEG.
        /// </summary>
        /// <param name="bmp">The BMP.</param>
        /// <param name="stream">The stream.</param>
        /// <param name="quality">The quality.</param>
        public static void SaveAsJpeg([Jig.Annotations.NotNull] this Image bmp, [Jig.Annotations.NotNull] Stream stream, long quality = 85L)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bmp != null && stream != null && stream.CanWrite)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (quality > 100 || quality < 30)
                {
                    quality = 85L;
                }

                using (var encoderParameters = new EncoderParameters(1))
                {
                    encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, quality);
                    bmp.Save(stream, ImageFormat.Jpeg.GetImageCodecInfo(), encoderParameters);
                }
            }
        }

        /// <summary>
        /// Crops the center.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="targetWidth">Width of the target.</param>
        /// <param name="targetHeight">Height of the target.</param>
        /// <returns>The cropped image</returns>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        public static Bitmap CropCenter([Jig.Annotations.NotNull] this Bitmap bitmap, int targetWidth, int targetHeight)
        {
            if (targetHeight <= bitmap.Height && targetWidth <= bitmap.Width)
            {
                var x = (bitmap.Width / 2) - (targetWidth / 2);
                var y = (bitmap.Height / 2) - (targetHeight / 2);

                var cropArea = new Rectangle(x, y, targetWidth, targetHeight);

                return bitmap.Clone(cropArea, bitmap.PixelFormat);
            }

            return bitmap;
        }

        /// <summary>
        /// Scales the proportional min.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <param name="minWidth">Width of the min.</param>
        /// <param name="minHeight">Height of the min.</param>
        /// <returns>The new image</returns>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        public static Bitmap ScaleProportionalMin([Jig.Annotations.NotNull] this Bitmap bitmap, int minWidth, int minHeight)
        {
            if (bitmap == null)
            {
                throw new HttpException(550, "The bitmap cannot be null");
            }

            var ratioX = (double)minWidth / bitmap.Width;
            var ratioY = (double)minHeight / bitmap.Height;
            var ratio = Math.Max(ratioX, ratioY);

            var newWidth = (int)(bitmap.Width * ratio);
            var newHeight = (int)(bitmap.Height * ratio);

            return bitmap.ScaleToSize(newWidth, newHeight);
        }

        /// <summary>
        ///     Scales the bitmap to the passed target size without respecting the aspect.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="size">The target size.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSize(100, 100))
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        public static Bitmap ScaleToSize([Jig.Annotations.NotNull] this Bitmap bitmap, Size size)
        {
            return bitmap.ScaleToSize(size.Width, size.Height);
        }

        /// <summary>
        ///     Scales the bitmap to the passed target size without respecting the aspect.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="width">The target width.</param>
        /// <param name="height">The target height.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSize(100, 100))
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleToSize([Jig.Annotations.NotNull] this Bitmap bitmap, int width, int height)
        {
            if (bitmap == null)
            {
                throw new HttpException(550, "The bitmap cannot be null");
            }

            var scaledBitmap = new Bitmap(width, height);

            using (var g = Graphics.FromImage(scaledBitmap))
            {
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(bitmap, 0, 0, width, height);
            }

            return scaledBitmap;
        }

        /// <summary>
        ///     Scales the bitmap to the passed target size by respecting the aspect.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="size">The target size.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleProportional(100, 100)
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <remarks>Please keep in mind that the returned bitmaps size might not match the desired size due to original bitmaps aspect.</remarks>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleProportional([Jig.Annotations.NotNull] this Bitmap bitmap, Size size)
        {
            return bitmap.ScaleProportional(size.Width, size.Height);
        }

        /// <summary>
        /// Scales the bitmap to the passed target size by respecting the aspect.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="width">The target width.</param>
        /// <param name="height">The target height.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleProportional(100, 100))
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <remarks>Please keep in mind that the returned bitmaps size might not match the desired size due to original bitmaps aspect.</remarks>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleProportional([Jig.Annotations.NotNull] this Bitmap bitmap, int width, int height)
        {
            if (bitmap == null)
            {
                throw new HttpException(550, "The bitmap cannot be null");
            }

            float proportionalWidth, proportionalHeight;

            if (width.Equals(0))
            {
                proportionalWidth = ((float)height) / bitmap.Size.Height * bitmap.Width;
                proportionalHeight = height;
            }
            else if (height.Equals(0))
            {
                proportionalWidth = width;
                proportionalHeight = ((float)width) / bitmap.Size.Width * bitmap.Height;
            }
            else if (((float)width) / bitmap.Size.Width * bitmap.Size.Height <= height)
            {
                proportionalWidth = width;
                proportionalHeight = ((float)width) / bitmap.Size.Width * bitmap.Height;
            }
            else
            {
                proportionalWidth = ((float)height) / bitmap.Size.Height * bitmap.Width;
                proportionalHeight = height;
            }

            return bitmap.ScaleToSize((int)proportionalWidth, (int)proportionalHeight);
        }

        /// <summary>
        /// Scales the bitmap to the passed target size by respecting the aspect. The overlapping background is filled with the given background color.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="size">The target size.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSizeProportional(100, 100))
        ///     {
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleToSizeProportional([Jig.Annotations.NotNull] this Bitmap bitmap, Size size)
        {
            return bitmap.ScaleToSizeProportional(Color.White, size);
        }

        /// <summary>
        /// Scales the bitmap to the passed target size by respecting the aspect. The overlapping background is filled with the given background color.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="backgroundColor">The color of the background.</param>
        /// <param name="size">The target size.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSizeProportional(100, 100))
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleToSizeProportional([Jig.Annotations.NotNull] this Bitmap bitmap, Color backgroundColor, Size size)
        {
            return bitmap.ScaleToSizeProportional(backgroundColor, size.Width, size.Height);
        }

        /// <summary>
        /// Scales the bitmap to the passed target size by respecting the aspect. The overlapping background is filled with the given background color.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="width">The target width.</param>
        /// <param name="height">The target height.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSizeProportional(100, 100))
        ///     {
        ///     }
        /// }
        /// </code></example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleToSizeProportional([Jig.Annotations.NotNull] this Bitmap bitmap, int width, int height)
        {
            return bitmap.ScaleToSizeProportional(Color.White, width, height);
        }

        /// <summary>
        ///     Scales the bitmap to the passed target size by respecting the aspect. The overlapping background is filled with the given background color.
        /// </summary>
        /// <param name="bitmap">The source bitmap.</param>
        /// <param name="backgroundColor">The color of the background.</param>
        /// <param name="width">The target width.</param>
        /// <param name="height">The target height.</param>
        /// <returns>The scaled bitmap</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// using(var bitmap = new Bitmap("/app_images/image.png".MapPath())){
        ///     using(var thumbnail = bitmap.ScaleToSizeProportional(100, 100))
        ///     {
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Must be dispose in the users code.")]
        public static Bitmap ScaleToSizeProportional([Jig.Annotations.NotNull] this Bitmap bitmap, Color backgroundColor, int width, int height)
        {
            if (bitmap == null)
            {
                throw new HttpException(550, "The bitmap cannot be null");
            }

            var scaledBitmap = new Bitmap(width, height);

            using (var g = Graphics.FromImage(scaledBitmap))
            {
                g.Clear(backgroundColor);

                var proportionalBitmap = bitmap.ScaleProportional(width, height);

                var imagePosition = new Point((int)((width - proportionalBitmap.Width) / 2m), (int)((height - proportionalBitmap.Height) / 2m));

                g.DrawImage(proportionalBitmap, imagePosition);
            }

            return scaledBitmap;
        }

        /// <summary>
        /// Image to stream.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="format">The format.</param>
        /// <returns>Memory Stream of image</returns>
        [Jig.Annotations.NotNull]
        public static Stream ConvertAsStream([Jig.Annotations.NotNull] this Bitmap image, [Jig.Annotations.NotNull] ImageFormat format)
        {
            var stream = new MemoryStream();
            image.Save(stream, format);
            stream.Position = 0;
            return stream;
        }
    }
}
