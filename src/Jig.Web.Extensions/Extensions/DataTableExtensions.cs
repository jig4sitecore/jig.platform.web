﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataTableExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       06/16/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Data;
    using System.Diagnostics;
    using System.IO;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class DataTableExtensions
    {
        /// <summary>
        ///     Export Data View to the Xml Table which can be passed to Excel
        /// </summary>
        /// <param name="view">The data in the memory table</param>
        /// <returns>The data formatted as Xml</returns>
        [Jig.Annotations.NotNull]
        public static string ExportToXmlTable([Jig.Annotations.NotNull] this DataView view)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (view != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return view.Table.ExportToXmlTable();
            }

            return string.Empty;
        }

        /// <summary>
        ///     Export Data Table to the Xml Table which can be passed to Excel
        /// </summary>
        /// <param name="table">The data in the memory table</param>
        /// <returns>The data formatted as Xml</returns>
        [Jig.Annotations.NotNull]
        public static string ExportToXmlTable([Jig.Annotations.NotNull] this DataTable table)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (table != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // Allocated 8kb initial size
                var builder = new StringBuilder(8096);
                builder.AppendLine(@"<?xml version=""1.0"" encoding=""UTF-8"" standalone=""yes""?>");

                using (var writer = new StringWriter(builder))
                {
                    table.WriteXml(writer, XmlWriteMode.IgnoreSchema);
                }

                return builder.ToString();
            }

            return string.Empty;
        }
    }
}
