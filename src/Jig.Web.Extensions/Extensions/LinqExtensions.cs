﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LinqExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       11/02/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class LinqExtensions
    {
        /// <summary>
        ///     Paginates the specified <![CDATA[IQueryable<T>]]> collection  using LINQ query.
        /// </summary>
        /// <typeparam name="T">The type of the collection item</typeparam>
        /// <param name="source">The query source.</param>
        /// <param name="skip">The skip items number.</param>
        /// <param name="take">The take items number.</param>
        /// <returns>The paged collection items</returns>
        [Jig.Annotations.NotNull]
        public static IQueryable<T> Paginate<T>([Jig.Annotations.NotNull] this IQueryable<T> source, byte skip, byte take = 10)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null && skip > 0 && take > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.Skip(skip).Take(take);
            }

            return source;
        }

        /// <summary>
        /// Pages the specified source.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="pageIndex">The page.</param>
        /// <param name="pageSize">The page size.</param>
        /// <returns>The Page collection records</returns>
        /// <remarks>This extension method is design for relatively small collections ~2550 size with safety in mind. What's why page index and page size are bytes[0-255]</remarks>
        [Jig.Annotations.NotNull]
        public static IQueryable<TSource> Page<TSource>([Jig.Annotations.NotNull] this IQueryable<TSource> source, byte pageIndex, byte pageSize = 10)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null && pageIndex > 0 && pageSize > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }

            return source;
        }

        /// <summary>
        /// Pages the specified source.
        /// </summary>
        /// <typeparam name="TSource">The type of the source.</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="pageIndex">The page  index.</param>
        /// <param name="pageSize">The page size.</param>
        /// <returns>The Page collection records</returns>
        /// <remarks>This extension method is design for relatively small collections ~2550 size with safety in mind. What's why page index and page size are bytes[0-255]</remarks>
        [Jig.Annotations.NotNull]
        public static IEnumerable<TSource> Page<TSource>([Jig.Annotations.NotNull] this IEnumerable<TSource> source, byte pageIndex, byte pageSize = 10)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null && pageIndex > 0 && pageSize > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.Skip((pageIndex - 1) * pageSize).Take(pageSize);
            }

            return source;
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified collection].
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>
        ///   <c>true</c> if [is null or empty] [the specified collection]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty([Jig.Annotations.NotNull] this IEnumerable collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return collection == null || !collection.GetEnumerator().MoveNext();
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Determines whether [is null or empty] [the specified collection].
        /// </summary>
        /// <typeparam name="T">The Generic Type</typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns>
        ///   <c>true</c> if [is null or empty] [the specified collection]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<T>([Jig.Annotations.NotNull] this IEnumerable<T> collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (collection == null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            // ReSharper disable HeuristicUnreachableCode
            {
                return true;
            }

            /* ReSharper restore HeuristicUnreachableCode */

            using (var enumerator = collection.GetEnumerator())
            {
                return !enumerator.MoveNext();
            }
        }

        /// <summary>
        /// Randomizes the specified source.
        /// </summary>
        /// <typeparam name="T">The generic list object types</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>The randomized list</returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<T> Randomize<T>([Jig.Annotations.NotNull] this IEnumerable<T> source)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.ToArray().OrderBy(item => Guid.NewGuid());
            }

            return Enumerable.Empty<T>();
        }

        /// <summary>
        /// Randomizes the specified source.
        /// </summary>
        /// <typeparam name="T">The generic list object types</typeparam>
        /// <param name="source">The source.</param>
        /// <param name="take">The take.</param>
        /// <returns>
        /// The randomized list
        /// </returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<T> Randomize<T>([Jig.Annotations.NotNull] this IEnumerable<T> source, byte take)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.Randomize().Take(take);
            }

            return Enumerable.Empty<T>();
        }

        /// <summary>
        /// Random item from the specified source.
        /// </summary>
        /// <typeparam name="T">The object instance </typeparam>
        /// <param name="source">The source.</param>
        /// <returns>The random item from source</returns>
        public static T Random<T>([Jig.Annotations.NotNull] this IEnumerable<T> source)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return source.Randomize().FirstOrDefault();
            }

            return default(T);
        }

        /// <summary>
        /// Compacts the specified source by removing any null items.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="source">The source.</param>
        /// <returns>The source enumerable without null items</returns>
        [Jig.Annotations.NotNull]
        public static IEnumerable<T> Compact<T>([Jig.Annotations.NotNull] this IEnumerable<T> source) where T : class
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var item in source)
                {
                    if (item != null)
                    {
                        yield return item;
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the specified value is between the the defined minimum and maximum range (including those values).
        /// </summary>
        /// <typeparam name="T">The generic comparable object instance</typeparam>
        /// <param name="value">The value to compare.</param>
        /// <param name="minValue">The minimum value.</param>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns>
        ///     <c>true</c> if the specified value is between min and max; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// var value = 5;
        /// if(value.IsBetween(1, 10)) { 
        ///     // ... 
        /// }
        /// </example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        public static bool IsBetween<T>(this T value, T minValue, T maxValue) where T : IComparable<T>
        {
            return value.IsBetween(minValue, maxValue, null);
        }

        /// <summary>
        ///     Determines whether the specified value is between the the defined minimum and maximum range (including those values).
        /// </summary>
        /// <typeparam name="T">The generic comparable object instance</typeparam>
        /// <param name="value">The value to compare.</param>
        /// <param name="minValue">The minimum value.</param>
        /// <param name="maxValue">The maximum value.</param>
        /// <param name="comparer">An optional comparer to be used instead of the types default comparer.</param>
        /// <returns>
        ///     <c>true</c> if the specified value is between min and max; otherwise, <c>false</c>.
        /// </returns>
        /// <example>View code: <br />
        /// var value = 5;
        /// if(value.IsBetween(1, 10)) {
        /// // ...
        /// }
        /// </example>
        /// <author>http://dnpextensions.codeplex.com/</author>
        public static bool IsBetween<T>(this T value, T minValue, T maxValue, [Jig.Annotations.CanBeNull] IComparer<T> comparer) where T : IComparable<T>
        {
            comparer = comparer ?? Comparer<T>.Default;

            var minMaxCompare = comparer.Compare(minValue, maxValue);
            if (minMaxCompare < 0)
            {
                return (comparer.Compare(value, minValue) >= 0) && (comparer.Compare(value, maxValue) <= 0);
            }

            if (minMaxCompare == 0)
            {
                return comparer.Compare(value, minValue) == 0;
            }

            return (comparer.Compare(value, maxValue) >= 0) && (comparer.Compare(value, minValue) <= 0);
        }
    }
}
