﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ListExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class ListExtensions
    {
        /// <summary>
        /// Indicates whether the specified System.Collections.IList"/>
        /// is null or empty.
        /// </summary>
        /// <param name="list">System.Collections.IList"/>
        ///     to check for.</param>
        /// <returns>Return true if Count is 0 or it is null</returns>
        public static bool IsNullOrEmpty([Jig.Annotations.CanBeNull] this IList list)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return list == null || list.Count == 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Indicates whether the specified ICollectionT
        /// is null or empty.
        /// </summary>
        /// <typeparam name="TObject">The type of the collection.</typeparam>
        /// <param name="list">The list instance</param>
        /// <returns>
        ///     <c>true</c> if the collection is null or empty,
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool IsNullOrEmpty<TObject>([Jig.Annotations.CanBeNull] this IList<TObject> list)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return list == null || list.Count == 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Determines whether [is not null or empty] [the specified list].
        /// </summary>
        /// <param name="list">The list.</param>
        /// <returns>
        ///   <c>true</c> if [is not null or empty] [the specified list]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNullOrEmpty([Jig.Annotations.CanBeNull] this IList list)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return list != null && list.Count > 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Determines whether [is not null or empty] [the specified list].
        /// </summary>
        /// <typeparam name="TObject">The objects type of</typeparam>
        /// <param name="list">The list.</param>
        /// <returns>
        ///   <c>true</c> if [is not null or empty] [the specified list]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNotNullOrEmpty<TObject>([Jig.Annotations.CanBeNull] this IList<TObject> list)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return list != null && list.Count > 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }

        /// <summary>
        /// Inserts an item uniquely to to a list and returns a value whether the item was inserted or not.
        /// </summary>
        /// <typeparam name="T">The generic list item type.</typeparam>
        /// <param name="list">The list to be inserted into.</param>
        /// <param name="index">The index to insert the item at.</param>
        /// <param name="item">The item to be added.</param>
        /// <returns>
        /// Indicates whether the item was inserted or not
        /// </returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static bool InsertUnique<T>([Jig.Annotations.NotNull] this IList<T> list, int index, T item) where T : struct
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (list != null && !list.Contains(item))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                list.Insert(index, item);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Inserts an item uniquely to to a list and returns a value whether the item was inserted or not.
        /// </summary>
        /// <typeparam name="TObject">The generic list item type.</typeparam>
        /// <param name="list">The list to be inserted into.</param>
        /// <param name="item">The item to be added.</param>
        /// <param name="index">The index to insert the item at.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns>
        /// Indicates whether the item was inserted or not
        /// </returns>
        public static bool InsertUnique<TObject>([Jig.Annotations.NotNull] this IList<TObject> list, TObject item, int index = 0, [Jig.Annotations.CanBeNull] IEqualityComparer<TObject> comparer = null)
        {
            comparer = comparer ?? EqualityComparer<TObject>.Default;
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (list != null && !list.Contains(item, comparer))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                list.Insert(index, item);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Inserts a range of items uniquely to a list starting at a given index and returns the amount of items inserted.
        /// </summary>
        /// <typeparam name="TObject">The generic list item type.</typeparam>
        /// <param name="list">The list to be inserted into.</param>
        /// <param name="items">The items to be inserted.</param>
        /// <param name="startIndex">The start index.</param>
        /// <param name="comparer">The comparer.</param>
        /// <returns>
        /// The amount if items that were inserted.
        /// </returns>
        public static int InsertUnique<TObject>([Jig.Annotations.NotNull] this IList<TObject> list, [Jig.Annotations.NotNull] IEnumerable<TObject> items, int startIndex = 0, [Jig.Annotations.CanBeNull] IEqualityComparer<TObject> comparer = null)
        {
            var index = startIndex + items.Count(item => list.InsertUnique(item, startIndex, comparer));

            return index - startIndex;
        }
    }
}
