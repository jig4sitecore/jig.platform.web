﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StringBuilderExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     M.Gramolini
//  Date:       09/21/2012
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class StringBuilderExtensions
    {
        /// <summary>
        /// Does string.Concat operation on args and appends to string builder.
        /// </summary>
        /// <param name="stringBuilder">The string builder.</param>
        /// <param name="args">The args.</param>
        /// <returns>The string builder with args appended</returns>
        [Jig.Annotations.CanBeNull]
        public static StringBuilder Append([Jig.Annotations.NotNull] this StringBuilder stringBuilder, [Jig.Annotations.NotNull] params object[] args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (stringBuilder != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var value in args)
                {
                    stringBuilder.Append(value);
                }
            }

            return stringBuilder;
        }

        /// <summary>
        /// Does string.Concat operation on args and appends with a newline to string builder.
        /// </summary>
        /// <param name="stringBuilder">The string builder.</param>
        /// <param name="args">The args.</param>
        /// <returns>The string builder with args appended</returns>
        [Jig.Annotations.NotNull]
        public static StringBuilder AppendLine([Jig.Annotations.NotNull] this StringBuilder stringBuilder, [Jig.Annotations.NotNull] params object[] args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (stringBuilder != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var value in args)
                {
                    stringBuilder.Append(value);
                    stringBuilder.AppendLine();
                }
            }

            return stringBuilder;
        }
    }
}