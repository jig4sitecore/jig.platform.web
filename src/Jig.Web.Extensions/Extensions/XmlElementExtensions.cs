﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlElementExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Xml;
    using System.Xml.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class XmlElementExtensions
    {
        /// <summary>
        /// Converts an XElement to an XmlElement.
        /// </summary>
        /// <param name="element">The XElement to convert.</param>
        /// <returns>The equivalent XmlElement.</returns>
        [Jig.Annotations.CanBeNull]
        public static XmlElement ToXmlElement([Jig.Annotations.CanBeNull] this XElement element)
        {
            if (element != null)
            {
                return new XmlDocument().ReadNode(element.CreateReader()) as XmlElement;
            }

            return null;
        }

        /// <summary>
        /// Converts an XmlElement to an XElement.
        /// </summary>
        /// <param name="xmlElement">The XmlElement to convert.</param>
        /// <returns>The equivalent XElement.</returns>
        [Jig.Annotations.CanBeNull]
        public static XElement ToXElement([Jig.Annotations.CanBeNull] this XmlElement xmlElement)
        {
            if (xmlElement != null)
            {
                return XElement.Load(xmlElement.CreateNavigator().ReadSubtree());
            }

            return null;
        }
    }
}
