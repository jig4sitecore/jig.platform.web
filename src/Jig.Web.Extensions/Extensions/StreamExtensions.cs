﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StreamExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       10/23/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.IO;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class StreamExtensions
    {
        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The converted stream to string.</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsString([Jig.Annotations.NotNull] this Stream stream)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (stream != null && stream.Length > 0 && stream.CanRead)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Copies the stream.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="output">The output.</param>
        public static void CopyStream([Jig.Annotations.NotNull] this Stream input, [Jig.Annotations.NotNull] Stream output)
        {
            var buffer = new byte[8192];

            bool hasMore;
            do
            {
                int read = input.Read(buffer, 0, buffer.Length);
                hasMore = read > 0;

                if (hasMore)
                {
                    output.Write(buffer, 0, read);
                }
            }
            while (hasMore);
        }

        /// <summary>
        /// Converts stream to the byte array.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns>The byte array from stream.</returns>
        /// <remarks>This method designed for the Web uploaded files up to 5MB.</remarks>
        [Jig.Annotations.CanBeNull]
        public static byte[] ConvertAsByteArray([Jig.Annotations.NotNull] this Stream stream)
        {
            /* check readable before reading streams */
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (stream != null && stream.CanRead)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var binaryReader = new BinaryReader(stream))
                {
                    return binaryReader.ReadBytes((int)stream.Length);
                }
            }

            return null;
        }
    }
}
