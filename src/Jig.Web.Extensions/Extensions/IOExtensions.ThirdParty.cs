﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOExtensions.ThirdParty.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Date:       07/24/2010
//  Source:     .NET Extensions - Extensions Methods Library
//  Url:        http://dnpextensions.codeplex.com/
// Modified:    J.Baltika
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    // ReSharper disable InconsistentNaming
    public static partial class IOExtensions
    // ReSharper restore InconsistentNaming
    {
        #region Thirh party extensions

        /// <summary>
        ///     Gets all files in the directory matching one of the several (!) supplied patterns (instead of just one in the regular implementation).
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="patterns">The patterns.</param>
        /// <returns>The matching files.</returns>
        /// <remarks>This methods is quite perfect to be used in conjunction with the newly created FileInfo-Array extension methods.</remarks>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// </code>
        /// </example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> GetFiles([Jig.Annotations.NotNull] this DirectoryInfo directory, [Jig.Annotations.NotNull] params string[] patterns)
        {
            var files = new List<FileInfo>();
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (directory != null && patterns != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var pattern in patterns)
                {
                    files.AddRange(directory.GetFiles(pattern));
                }
            }

            return files;
        }

        /// <summary>
        ///     Gets all files in the directory except matching one of the supplied patterns.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="excludePatterns">The patterns.</param>
        /// <returns>The matching files.</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFilesExcept("*.txt", "*.xml");
        /// </code>
        /// </example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> GetFilesExcept([Jig.Annotations.NotNull] this DirectoryInfo directory, [Jig.Annotations.NotNull] params string[] excludePatterns)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (directory != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var allFiles = directory.EnumerateFiles().ToList();
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (excludePatterns != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    var excludedFiles = excludePatterns.SelectMany(directory.GetFiles).ToArray();

                    return allFiles.Except(excludedFiles, new FileSystemInfoEqualityComparer<FileInfo>()).ToList();
                }

                // ReSharper disable HeuristicUnreachableCode
                return allFiles;
                // ReSharper restore HeuristicUnreachableCode
            }

            return new FileInfo[] { };
        }

        /// <summary>
        ///     Renames a file.
        /// </summary>
        /// <param name="file">The file info.</param>
        /// <param name="newName">The new name.</param>
        /// <returns>The renamed file</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var file = new FileInfo(@"c:\test.txt");
        /// file.Rename("test2.txt");
        /// </code></example>
        [Jig.Annotations.CanBeNull]
        public static FileInfo Rename([Jig.Annotations.NotNull] this FileInfo file, [Jig.Annotations.NotNull] string newName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (file != null && !string.IsNullOrWhiteSpace(newName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var path = Path.GetDirectoryName(file.FullName);
                // ReSharper disable AssignNullToNotNullAttribute
                if (string.IsNullOrWhiteSpace(path))
                {
                    throw new HttpException(550, "The path can't be null or empty!");
                }

                var filePath = Path.Combine(path, newName);
                // ReSharper restore AssignNullToNotNullAttribute
                file.MoveTo(filePath);
            }

            return file;
        }

        /// <summary>
        /// Renames a without changing its extension.
        /// </summary>
        /// <param name="file">The file path.</param>
        /// <param name="newName">The new name.</param>
        /// <returns>The renamed file</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var file = new FileInfo(@"c:\test.txt");
        /// file.RenameFileWithoutExtension("test3");
        /// </code></example>
        [Jig.Annotations.CanBeNull]
        public static FileInfo RenameFileWithoutExtension([Jig.Annotations.NotNull] this FileInfo file, [Jig.Annotations.NotNull] string newName)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (file != null && !string.IsNullOrWhiteSpace(newName))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var fileName = string.Concat(newName, file.Extension);
                file.Rename(fileName);
            }

            return file;
        }

        /// <summary>
        /// Changes the files extension.
        /// </summary>
        /// <param name="file">The file path.</param>
        /// <param name="newExtension">The new extension.</param>
        /// <returns>The renamed file</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var file = new FileInfo(@"c:\test.txt");
        /// file.ChangeExtension("xml");
        /// </code></example>
        [Jig.Annotations.CanBeNull]
        public static FileInfo ChangeExtension([Jig.Annotations.NotNull] this FileInfo file, [Jig.Annotations.NotNull] string newExtension)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (file != null && !string.IsNullOrWhiteSpace(newExtension))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                newExtension = newExtension.EnsureStartsWith(".");

                var fileName = string.Concat(Path.GetFileNameWithoutExtension(file.FullName), newExtension);
                file.Rename(fileName);
            }

            return file;
        }

        /// <summary>
        ///     Deletes several files at once.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// files.Delete()
        /// </code></example>
        public static void Delete([Jig.Annotations.NotNull] this IEnumerable<FileInfo> files)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (files != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var file in files)
                {
                    file.Delete();
                }
            }
        }

        /// <summary>
        ///     Copies several files to a new folder at once.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="targetPath">The target path.</param>
        /// <returns>The newly created file copies</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// var copiedFiles = files.CopyTo(@"c:\temp\");
        /// </code></example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> CopyTo([Jig.Annotations.NotNull] this IEnumerable<FileInfo> files, [Jig.Annotations.NotNull] string targetPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (files != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var file in files)
                {
                    var fileName = Path.Combine(targetPath, file.Name);
                    yield return file.CopyTo(fileName);
                }
            }
        }

        /// <summary>
        ///     Movies several files to a new folder at once.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="targetPath">The target path.</param>
        /// <returns>The moved files</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// files.MoveTo(@"c:\temp\");
        /// </code></example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> MoveTo([Jig.Annotations.NotNull] this IEnumerable<FileInfo> files, [Jig.Annotations.NotNull] string targetPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (files != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var file in files)
                {
                    var fileName = Path.Combine(targetPath, file.Name);
                    file.MoveTo(fileName);
                    yield return file;
                }
            }
        }

        /// <summary>
        /// Sets file attributes for several files at once
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="attributes">The attributes to be set.</param>
        /// <returns>The changed files</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// files.SetAttributes(FileAttributes.Archive);
        /// </code></example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> SetAttributes([Jig.Annotations.NotNull] this IEnumerable<FileInfo> files, FileAttributes attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (files != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var file in files)
                {
                    file.Attributes = attributes;
                    yield return file;
                }
            }
        }

        /// <summary>
        /// Appends file attributes for several files at once (additive to any existing attributes)
        /// </summary>
        /// <param name="files">The files.</param>
        /// <param name="attributes">The attributes to be set.</param>
        /// <returns>The changed files</returns>
        /// <example>View code: <br />
        /// <code title="C# File" lang="C#">
        /// var files = directory.GetFiles("*.txt", "*.xml");
        /// files.SetAttributesAdditive(FileAttributes.Archive);
        /// </code></example>
        [Jig.Annotations.NotNull]
        public static IEnumerable<FileInfo> SetAttributesAdditive([Jig.Annotations.NotNull] this IEnumerable<FileInfo> files, FileAttributes attributes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (files != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                foreach (var file in files)
                {
                    file.Attributes = file.Attributes | attributes;
                    yield return file;
                }
            }
        }

        #endregion

        /// <summary>
        /// The Equality comparer
        /// </summary>
        /// <typeparam name="TInfo">The type of the info.</typeparam>
        internal class FileSystemInfoEqualityComparer<TInfo> : IEqualityComparer<TInfo> where TInfo : FileSystemInfo
        {
            /// <summary>
            /// Determines whether the specified objects are equal.
            /// </summary>
            /// <param name="x">The first object of type TInfo to compare.</param>
            /// <param name="y">The second object of type TInfo to compare.</param>
            /// <returns>
            /// true if the specified objects are equal; otherwise, false.
            /// </returns>
            public bool Equals([Jig.Annotations.NotNull] TInfo x, [Jig.Annotations.NotNull] TInfo y)
            {
                return x.FullName.Equals(y.FullName, StringComparison.InvariantCultureIgnoreCase);
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <param name="obj">The obj.</param>
            /// <returns>
            /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
            /// </returns>
            public int GetHashCode([Jig.Annotations.NotNull] TInfo obj)
            {
                // ReSharper disable ConditionIsAlwaysTrueOrFalse
                if (obj != null)
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                {
                    return obj.FullName.GetHashCode();
                }

                return 0;
            }
        }
    }
}