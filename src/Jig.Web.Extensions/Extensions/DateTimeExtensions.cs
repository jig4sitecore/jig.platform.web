﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DateTimeExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       11/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Globalization;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class DateTimeExtensions
    {
        /// <summary>
        /// Convert date to the US date.
        /// </summary>
        /// <param name="date">The specified date.</param>
        /// <returns>The specified date formatted as a 'MM/dd/yyyy' date string</returns>
        /* ReSharper disable InconsistentNaming */
        [Jig.Annotations.NotNull]
        public static string ConvertAsUnitedStatesDate(this DateTime date)
        /* ReSharper restore InconsistentNaming */
        {
            return date.ToString("MM/dd/yyyy");
        }

        /// <summary>
        /// Converts the asynchronous int.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>The Date in int format</returns>
        public static int ConvertAsInt(this DateTime date)
        {
            return (date.Year * 10000) + (date.Month * 100) + date.Day;
        }

        /// <summary>
        /// Formats date time to correct ISO date.
        /// </summary>
        /// <param name="datetime">The date time.</param>
        /// <param name="ignoreTime">if set to <c>true</c> ignore time part.</param>
        /// <returns>The date time in ISO format</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsIsoDate(this DateTime datetime, bool ignoreTime = true)
        {
            if (ignoreTime)
            {
                return datetime.Date.ToString("yyyyMMddTHHmmss");
            }

            return datetime.ToString("yyyyMMddTHHmmss");
        }

        /// <summary>
        /// Formats the HTTP cookie date time.
        /// </summary>
        /// <param name="datetime">The datetime.</param>
        /// <param name="isUniversalTime">if set to <c>true</c> [is universal time].</param>
        /// <returns>
        /// The Date as string for cookie time
        /// </returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsHttpCookieDateTime(this DateTime datetime, bool isUniversalTime = false)
        {
            if (!isUniversalTime)
            {
                datetime = datetime.ToUniversalTime();
            }

            return datetime.ToString("ddd, dd-MMM-yyyy HH':'mm':'ss 'GMT'", DateTimeFormatInfo.InvariantInfo);
        }

        /// <summary>
        /// Converts a DateTime value to w3c format
        /// based on http://www.w3.org/TR/NOTE-datetime notes
        /// </summary>
        /// <param name="date">The date.</param>
        /// <returns>
        /// The date in W3C format
        /// </returns>
        /// <see href="http://www.bytechaser.com/en/functions/k4s6frpca5/convert-date-time-to-w3c-datetime-format.aspx"/>
        [Jig.Annotations.NotNull]
        public static string ConvertAsW3CDateTime(this DateTime date)
        {
            // Get the UTC offset from the date value
            var utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset(date);

            string time = date.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss");

            // append the offset e.g. z=0, add 1 hour is +01:00
            time += utcOffset == TimeSpan.Zero ? "Z" : string.Format("{0}{1:00}:{2:00}", utcOffset > TimeSpan.Zero ? "+" : "-", utcOffset.Hours, utcOffset.Minutes);

            return time;
        }

        /// <summary>
        /// Converts the iso date time like '20130904T020426'.
        /// </summary>
        /// <param name="isoDate">The iso date.</param>
        /// <param name="ignoreTime">if set to <c>true</c> [ignore time].</param>
        /// <param name="dateTimeFormat">The date time format.</param>
        /// <param name="dateTimeStyle">The date time style.</param>
        /// <returns>
        /// The datetime from ISO string like '20130904T020426'
        /// </returns>
        public static DateTime? ConvertAsIsoDateTime([Jig.Annotations.NotNull] this string isoDate, bool ignoreTime = true, [Jig.Annotations.NotNull] string dateTimeFormat = "yyyyMMddTHHmmss", DateTimeStyles dateTimeStyle = DateTimeStyles.AssumeLocal)
        {
            DateTime dateTime;
            if (!string.IsNullOrWhiteSpace(isoDate) && DateTime.TryParseExact(isoDate, dateTimeFormat, CultureInfo.InvariantCulture, dateTimeStyle, out dateTime))
            {
                if (ignoreTime)
                {
                    return dateTime.Date;
                }

                return dateTime;
            }

            return null;
        }

        /// <summary>
        /// Calculates the age based on today.
        /// </summary>
        /// <param name="dateOfBirth">The date of birth.</param>
        /// <returns>The calculated age.</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static int CalculateAge(this DateTime dateOfBirth)
        {
            return CalculateAge(dateOfBirth, DateTime.Today);
        }

        /// <summary>
        /// Calculates the age based on a passed reference date.
        /// </summary>
        /// <param name="dateOfBirth">The date of birth.</param>
        /// <param name="referenceDate">The reference date to calculate on.</param>
        /// <returns>The calculated age.</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static int CalculateAge(this DateTime dateOfBirth, DateTime referenceDate)
        {
            int years = referenceDate.Year - dateOfBirth.Year;

            if (referenceDate.Month < dateOfBirth.Month || (referenceDate.Month == dateOfBirth.Month && referenceDate.Day < dateOfBirth.Day))
            {
                --years;
            }

            return years;
        }

        /// <summary>
        /// Returns the number of days in the month of the provided date.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <returns>The number of days.</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static int CountDaysOfMonth(this DateTime date)
        {
            var nextMonth = date.AddMonths(1);
            return new DateTime(nextMonth.Year, nextMonth.Month, 1).AddDays(-1).Day;
        }

        /// <summary>
        /// Returns the first day of the month of the provided date.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <returns>The first day of the month</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertsAsFirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }

        /// <summary>
        /// Returns the first day of the month of the provided date.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <param name="dayOfWeek">The desired day of week.</param>
        /// <returns>The first day of the month</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/> 
        public static DateTime ConvertsAsFirstDayOfMonth(this DateTime date, DayOfWeek dayOfWeek)
        {
            var dt = date.ConvertsAsFirstDayOfMonth();
            while (dt.DayOfWeek != dayOfWeek)
            {
                dt = dt.AddDays(1);
            }

            return dt;
        }

        /// <summary>
        /// Returns the last day of the month of the provided date.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <returns>The last day of the month.</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/> 
        public static DateTime ConvertsAsLastDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, CountDaysOfMonth(date));
        }

        /// <summary>
        /// Returns the last day of the month of the provided date.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <param name="dayOfWeek">The desired day of week.</param>
        /// <returns>The date time</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/> 
        public static DateTime ConvertsAsLastDayOfMonth(this DateTime date, DayOfWeek dayOfWeek)
        {
            var dt = date.ConvertsAsLastDayOfMonth();
            while (dt.DayOfWeek != dayOfWeek)
            {
                dt = dt.AddDays(-1);
            }

            return dt;
        }

        /// <summary>
        /// Indicates whether the date is today.
        /// </summary>
        /// <param name="dateTime">The date instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified date is today; otherwise, <c>false</c>.
        /// </returns>
        /// <see href="http://dnpextensions.codeplex.com/"/> 
        public static bool IsToday(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime() == DateTime.UtcNow;
        }

        /// <summary>
        /// Sets the time on the specified DateTime value.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="hours">The hours to be set.</param>
        /// <param name="minutes">The minutes to be set.</param>
        /// <param name="seconds">The seconds to be set.</param>
        /// <returns>The DateTime including the new time value</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime SetTime(this DateTime date, int hours, int minutes, int seconds)
        {
            return date.SetTime(new TimeSpan(hours, minutes, seconds));
        }

        /// <summary>
        /// Sets the time on the specified DateTime value.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="time">The TimeSpan to be applied.</param>
        /// <returns>
        /// The DateTime including the new time value
        /// </returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime SetTime(this DateTime date, TimeSpan time)
        {
            return date.Date.Add(time);
        }

        /// <summary>
        /// Converts a DateTime into a DateTimeOffset using the local system time zone.
        /// </summary>
        /// <param name="localDateTime">The local DateTime.</param>
        /// <returns>The converted DateTimeOffset</returns>
        public static DateTimeOffset ConvertAsDateTimeOffset(this DateTime localDateTime)
        {
            return localDateTime.ConvertAsDateTimeOffset(null);
        }

        /// <summary>
        /// Converts a DateTime into a DateTimeOffset using the specified time zone.
        /// </summary>
        /// <param name="localDateTime">The local DateTime.</param>
        /// <param name="localTimeZone">The local time zone.</param>
        /// <returns>The converted DateTimeOffset</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTimeOffset ConvertAsDateTimeOffset(this DateTime localDateTime, [Jig.Annotations.CanBeNull] TimeZoneInfo localTimeZone)
        {
            localTimeZone = localTimeZone ?? TimeZoneInfo.Local;

            if (localDateTime.Kind != DateTimeKind.Unspecified)
            {
                localDateTime = new DateTime(localDateTime.Ticks, DateTimeKind.Unspecified);
            }

            return TimeZoneInfo.ConvertTimeToUtc(localDateTime, localTimeZone);
        }

        /// <summary>
        /// Gets the first day of the week using the current culture.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <returns>The first day of the week</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsFirstDayOfWeek(this DateTime date)
        {
            return date.ConvertAsFirstDayOfWeek(null);
        }

        /// <summary>
        /// Gets the first day of the week using the specified culture.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <param name="cultureInfo">The culture to determine the first weekday of a week.</param>
        /// <returns>The first day of the week</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsFirstDayOfWeek(this DateTime date, [Jig.Annotations.CanBeNull] CultureInfo cultureInfo)
        {
            cultureInfo = cultureInfo ?? CultureInfo.CurrentCulture;

            var firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            while (date.DayOfWeek != firstDayOfWeek)
            {
                date = date.AddDays(-1);
            }

            return date;
        }

        /// <summary>
        /// Gets the last day of the week using the current culture.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <returns>The first day of the week</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsLastDayOfWeek(this DateTime date)
        {
            return date.ConvertAsLastDayOfWeek(null);
        }

        /// <summary>
        /// Gets the last day of the week using the specified culture.
        /// </summary>
        /// <param name="date">The date instance.</param>
        /// <param name="cultureInfo">The culture to determine the first weekday of a week.</param>
        /// <returns>The first day of the week</returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsLastDayOfWeek(this DateTime date, [Jig.Annotations.CanBeNull] CultureInfo cultureInfo)
        {
            return date.ConvertAsFirstDayOfWeek(cultureInfo).AddDays(6);
        }

        /// <summary>
        /// Gets the next occurrence of the specified weekday within the current week using the current culture.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="weekday">The desired weekday.</param>
        /// <returns>The calculated date.</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// var thisWeeksMonday = DateTime.Now.GetWeekday(DayOfWeek.Monday);
        /// </code></example>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsWeekday(this DateTime date, DayOfWeek weekday)
        {
            return date.ConvertAsWeekday(weekday, null);
        }

        /// <summary>
        /// Gets the next occurrence of the specified weekday within the current week using the specified culture.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="weekday">The desired weekday.</param>
        /// <param name="cultureInfo">The culture to determine the first weekday of a week.</param>
        /// <returns>The calculated date.</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// var thisWeeksMonday = DateTime.Now.GetWeekday(DayOfWeek.Monday);
        /// </code></example>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertAsWeekday(this DateTime date, DayOfWeek weekday, [Jig.Annotations.CanBeNull] CultureInfo cultureInfo)
        {
            var firstDayOfWeek = date.ConvertAsFirstDayOfWeek(cultureInfo);
            return firstDayOfWeek.ConvertsAsNextWeekday(weekday);
        }

        /// <summary>
        /// Gets the next occurrence of the specified weekday.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="weekday">The desired weekday.</param>
        /// <returns>The calculated date.</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// var lastMonday = DateTime.Now.ToNextWeekday(DayOfWeek.Monday);
        /// </code></example>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertsAsNextWeekday(this DateTime date, DayOfWeek weekday)
        {
            while (date.DayOfWeek != weekday)
            {
                date = date.AddDays(1);
            }

            return date;
        }

        /// <summary>
        /// Gets the previous occurrence of the specified weekday.
        /// </summary>
        /// <param name="date">The base date.</param>
        /// <param name="weekday">The desired weekday.</param>
        /// <returns>The calculated date.</returns>
        /// <example>View code: <br /><code title="C# File" lang="C#">
        /// var lastMonday = DateTime.Now.ToPreviousWeekday(DayOfWeek.Monday);
        /// </code></example>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        public static DateTime ConvertsAsPreviousWeekday(this DateTime date, DayOfWeek weekday)
        {
            while (date.DayOfWeek != weekday)
            {
                date = date.AddDays(-1);
            }

            return date;
        }

        /// <summary>
        /// Converts a regular DateTime to a RFC822 date string.
        /// </summary>
        /// <param name="date">The specific date.</param>
        /// <returns>
        /// The specified date formatted as a RFC822 date string.
        /// </returns>
        /// <see href="http://dnpextensions.codeplex.com/"/>
        [Jig.Annotations.NotNull]
        public static string ConvertAsRfc822Date(this DateTime date)
        {
            int offset = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now).Hours;
            string timeZone = "+" + offset.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0');

            if (offset < 0)
            {
                int i = offset * -1;
                timeZone = "-" + i.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0');
            }

            return date.ToString("ddd, dd MMM yyyy HH:mm:ss " + timeZone.PadRight(5, '0'));
        }
    }
}
