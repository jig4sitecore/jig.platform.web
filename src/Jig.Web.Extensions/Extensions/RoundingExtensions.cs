﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RoundingExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    [DebuggerStepThrough]
    public static partial class RoundingExtensions
    {
        /// <summary>
        /// Rounds to nearest.
        /// </summary>
        /// <example>
        /// 3.6 rounds to 4
        /// 3.5 rounds to 4
        /// 3.4 rounds to 3
        /// </example>
        /// <param name="value">The value.</param>
        /// <param name="digits">The digits.</param>
        /// <returns>The Rounded number</returns>
        public static decimal? RoundToNearest(this decimal? value, int digits = 0)
        {
            if (value.HasValue)
            {
                return value.Value.RoundToNearest(digits);
            }

            return null;
        }

        /// <summary>
        /// Rounds to nearest.
        /// </summary>
        /// <example>
        /// 3.6 rounds to 4
        /// 3.5 rounds to 4
        /// 3.4 rounds to 3
        /// </example>
        /// <param name="value">The value.</param>
        /// <param name="digits">The digits.</param>
        /// <returns>The Rounded number</returns>
        public static decimal RoundToNearest(this decimal value, int digits = 0)
        {
            return Math.Round(value, digits, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Rounds to nearest.
        /// </summary>
        /// <example>
        /// 3.6 rounds to 4
        /// 3.5 rounds to 4
        /// 3.4 rounds to 3
        /// </example>
        /// <param name="value">The value.</param>
        /// <param name="digits">The digits.</param>
        /// <returns>The Rounded number</returns>
        public static double? RoundToNearest(this double? value, int digits = 0)
        {
            if (value.HasValue)
            {
                return value.Value.RoundToNearest(digits);
            }

            return null;
        }

        /// <summary>
        /// Rounds to nearest.
        /// </summary>
        /// <example>
        /// 3.6 rounds to 4
        /// 3.5 rounds to 4
        /// 3.4 rounds to 3
        /// </example>
        /// <param name="value">The value.</param>
        /// <param name="digits">The digits.</param>
        /// <returns>The Rounded number</returns>
        public static double RoundToNearest(this double value, int digits = 0)
        {
            return Math.Round(value, digits, MidpointRounding.AwayFromZero);
        }
    }
}
