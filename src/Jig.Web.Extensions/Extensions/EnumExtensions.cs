﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class EnumExtensions
    {
        /// <summary>
        ///     Converts integer, byte, long and etc to ENUM.
        /// </summary>
        /// <typeparam name="TEnum">The type of the ENUM.</typeparam>
        /// <param name="value">The value.</param>
        /// <returns>The default or converted instance of the ENUM</returns>
        public static TEnum ConvertAsEnum<TEnum>([Jig.Annotations.NotNull] this string value) where TEnum : struct, IComparable, IFormattable
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                TEnum parsedEnum;
                if (Enum.TryParse(value, ignoreCase: true, result: out parsedEnum))
                {
                    return parsedEnum;
                }
            }

            return default(TEnum);
        }

        /// <summary>
        /// Converts to ENUM.
        /// </summary>
        /// <typeparam name="TEnum">The type of the ENUM.</typeparam>
        /// <param name="value">The value.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>The default ENUM or converted instance of the ENUM</returns>
        public static TEnum ConvertAsEnum<TEnum>([Jig.Annotations.NotNull] this string value, TEnum defaultValue) where TEnum : struct, IComparable, IFormattable
        {
            if (!string.IsNullOrWhiteSpace(value))
            {
                TEnum parsedEnum;
                if (Enum.TryParse(value, ignoreCase: true, result: out parsedEnum))
                {
                    return parsedEnum;
                }
            }

            return defaultValue;
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <param name="enumType">Type of the ENUM.</param>
        /// <returns>The ENUM descriptions</returns>
        [Jig.Annotations.NotNull]
        public static string GetDescription([Jig.Annotations.NotNull] this Enum enumType)
        {
            Type type = enumType.GetType();

            /* ReSharper disable SpecifyACultureInStringConversionExplicitly */
            MemberInfo[] memberInfo = type.GetMember(enumType.ToString());
            /* ReSharper restore SpecifyACultureInStringConversionExplicitly */

            if (memberInfo.Length > 0)
            {
                var attributes = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes.Length > 0)
                {
                    return ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            /* ReSharper disable SpecifyACultureInStringConversionExplicitly */
            return enumType.ToString();
            /* ReSharper restore SpecifyACultureInStringConversionExplicitly */
        }
    }
}
