﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       06/23/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class UriExtensions
    {
        /// <summary>
        ///     Get the current Uri to https
        /// </summary>
        /// <param name="uri">Request URI (a uniform resource identifier)</param>
        /// <returns>
        ///     A new instance of Uri with https schema
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static Uri AsHttps([Jig.Annotations.CanBeNull] this Uri uri)
        {
            if (uri != null)
            {
                var newUri = new UriBuilder(uri) { Scheme = Uri.UriSchemeHttps, Port = 443 };

                return newUri.Uri;
            }

            return null;
        }

        /// <summary>
        /// Get current Https URI to HTTP.
        /// </summary>
        /// <param name="uri">The uniform resource identifier (URI).</param>
        /// <returns>A new instance of Uri with http schema</returns>
        [Jig.Annotations.CanBeNull]
        public static Uri AsHttp([Jig.Annotations.CanBeNull] this Uri uri)
        {
            if (uri != null)
            {
                var newUri = new UriBuilder(uri) { Scheme = Uri.UriSchemeHttp, Port = 80 };

                return newUri.Uri;
            }

            return null;
        }

        /// <summary>
        /// Converts The Uri to href attribute value
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns>The href value</returns>
        [Jig.Annotations.NotNull]
        public static string AsHref([Jig.Annotations.CanBeNull] this Uri uri)
        {
            if (uri != null)
            {
                var url = uri.ToString().ToLowerInvariant();
                var index = url.IndexOf('/');
                return url.Substring(index);
            }

            return string.Empty;
        }
    }
}
