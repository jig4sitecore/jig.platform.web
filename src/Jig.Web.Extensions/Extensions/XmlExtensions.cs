﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       06/15/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class XmlExtensions
    {
        /// <summary>
        ///     Holds static instance of REGEX
        /// </summary>
        private static Regex regexForXmlAttribute;

        /// <summary>
        ///     Holds static instance of REGEX
        /// </summary>
        private static Regex regexForXmlIllegalUnicodeChars;

        /// <summary>
        ///     Gets the REGEX for XML attribute encoding.
        /// </summary>
        /// <value>The REGEX for XML attribute.</value>
        [Jig.Annotations.NotNull]
        private static Regex RegexForXmlAttribute
        {
            get
            {
                return XmlExtensions.regexForXmlAttribute ?? (XmlExtensions.regexForXmlAttribute = new Regex(@"[^\u0009\u000A\u000D\u0020-\uD7FF\uE000-\uFFFD]", RegexOptions.Compiled));
            }
        }

        /// <summary>
        ///     Gets the REGEX for text to XML encoding.
        /// </summary>
        /// <value>The REGEX for text to XML encoding.</value>
        [Jig.Annotations.NotNull]
        private static Regex RegexForXmlIllegalUnicodeChars
        {
            get
            {
                return XmlExtensions.regexForXmlIllegalUnicodeChars ?? (XmlExtensions.regexForXmlIllegalUnicodeChars = new Regex(@"(?<![\uD800-\uDBFF])[\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|[\x00-\x08\x0B\x0C\x0E-\x1F\x7F-\x9F\uFEFF\uFFFE\uFFFF]", RegexOptions.Compiled));
            }
        }

        /// <summary>
        ///     Removes the illegal XML chars.
        /// </summary>
        /// <param name="text">The text for xml.</param>
        /// <returns>The string without illegal xml chars</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveIllegalXmlAttributeChars([Jig.Annotations.NotNull] this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = RegexForXmlAttribute.Replace(text, string.Empty);
            }

            return text;
        }

        /// <summary>
        ///     Removes the illegal XML chars.
        /// </summary>
        /// <param name="text">The text for xml.</param>
        /// <returns>The string without illegal xml chars</returns>
        [Jig.Annotations.NotNull]
        public static string RemoveIllegalXmlChars([Jig.Annotations.NotNull] this string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = RegexForXmlIllegalUnicodeChars.Replace(text, string.Empty);
            }

            return text;
        }

        /// <summary>
        ///     Loads the string into a LINQ to XML XDocument
        /// </summary>
        /// <param name="xml">The XML string.</param>
        /// <returns>The XML document object model (XDocument)</returns>
        [Jig.Annotations.CanBeNull]
        public static XDocument ConvertAsXDocument([Jig.Annotations.NotNull] this string xml)
        {
            if (!string.IsNullOrWhiteSpace(xml))
            {
                return XDocument.Parse(xml);
            }

            return null;
        }

        /// <summary>
        ///     Loads the string into a LINQ to XElement
        /// </summary>
        /// <param name="xml">The XML string.</param>
        /// <returns>The XElement instance</returns>
        [Jig.Annotations.CanBeNull]
        public static XElement ConvertAsXElement([Jig.Annotations.NotNull] this string xml)
        {
            if (!string.IsNullOrWhiteSpace(xml))
            {
                return XElement.Parse(xml);
            }

            return null;
        }

        /// <summary>
        ///     Loads the string into a XML DOM object (XmlDocument)
        /// </summary>
        /// <param name="xml">The XML string.</param>
        /// <returns>The XML document object model (XmlDocument)</returns>
        [Jig.Annotations.CanBeNull]
        public static XmlDocument ConvertAsXmlDocument([Jig.Annotations.NotNull] this string xml)
        {
            if (!string.IsNullOrWhiteSpace(xml))
            {
                var document = new XmlDocument();
                document.LoadXml(xml);

                return document;
            }

            return null;
        }

        /// <summary>
        ///     Loads the string into a XML XPath DOM (XPathDocument)
        /// </summary>
        /// <param name="xml">The XML string.</param>
        /// <returns>The XML XPath document object model (XPathNavigator)</returns>
        [Jig.Annotations.CanBeNull]
        public static XPathNavigator ConvertAsXPathNavigator([Jig.Annotations.NotNull] this string xml)
        {
            if (!string.IsNullOrWhiteSpace(xml))
            {
                using (var stream = new StringReader(xml))
                {
                    var document = new XPathDocument(stream);

                    return document.CreateNavigator();
                }
            }

            return null;
        }

        /// <summary>
        /// Gets Xml node the named attribute item value.
        /// </summary>
        /// <param name="attributeCollection">The attribute collection.</param>
        /// <param name="name">The attribute name.</param>
        /// <returns>The value of the attribute</returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string GetNamedItemValue([Jig.Annotations.NotNull] this XmlAttributeCollection attributeCollection, [Jig.Annotations.NotNull] string name)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (attributeCollection != null && !string.IsNullOrWhiteSpace(name))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var attribute = attributeCollection.GetNamedItem(name);
                if (attribute != null)
                {
                    return attribute.Value;
                }
            }

            return string.Empty;
        }
    }
}
