﻿namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// The assembly extensions.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules",
        "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class AssemblyExtensions
    {
        /// <summary>
        /// Gets the XML document from assembly.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <param name="assemblyXmlFile">
        /// The xml file embedded in the assembly.
        /// </param>
        /// <returns>
        /// The Xml document
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static XmlDocument GetXmlDocumentFromAssembly(
            [Jig.Annotations.NotNull] this Assembly assembly,
            [Jig.Annotations.NotNull] string assemblyXmlFile)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (assembly != null && !string.IsNullOrWhiteSpace(assemblyXmlFile))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                using (var stream = assembly.GetManifestResourceStream(assemblyXmlFile))
                {
                    // ReSharper disable PossibleNullReferenceException
                    // ReSharper disable AssignNullToNotNullAttribute
                    if (stream == null)
                    {
                        throw new HttpException(550, "The stream cannot be null!");
                    }

                    // ReSharper restore AssignNullToNotNullAttribute
                    var bytes = new byte[stream.Length];

                    // ReSharper restore PossibleNullReferenceException
                    stream.Read(bytes, 0, (int)stream.Length);

                    var document = new XmlDocument();
                    document.Load(stream);

                    return document;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the XDocument from assembly.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <param name="assemblyXmlFile">
        /// The xml file embedded in the assembly.
        /// </param>
        /// <returns>
        /// The XDocument instance
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times", Justification = "The resources must be dispose if the stream is not xml format")]
        [Jig.Annotations.CanBeNull]
        public static XDocument GetXDocumentFromAssembly(
            [Jig.Annotations.NotNull] this Assembly assembly,
            [Jig.Annotations.NotNull] string assemblyXmlFile)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (assembly != null && !string.IsNullOrWhiteSpace(assemblyXmlFile))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                Stream stream = null;
                try
                {
                    stream = assembly.GetManifestResourceStream(assemblyXmlFile);

                    /* ReSharper disable AssignNullToNotNullAttribute */
                    if (stream == null)
                    {
                        throw new HttpException(550, "The stream cannot be null!");
                    }

                    using (var xmlReader = XmlReader.Create(stream))
                    {
                        /* ReSharper restore AssignNullToNotNullAttribute */
                        return XDocument.Load(xmlReader);
                    }
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Dispose();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the file bytes from assembly.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <param name="assemblyFile">
        /// The file embedded in the assembly.
        /// </param>
        /// <returns>
        /// The file bytes
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static byte[] GetFileBytesFromAssembly(
            [Jig.Annotations.NotNull] this Assembly assembly,
            [Jig.Annotations.NotNull] string assemblyFile)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (assembly != null && !string.IsNullOrWhiteSpace(assemblyFile))
            {
                // ReSharper restore ConditionIsAlwaysTrueOrFalse
                using (var stream = assembly.GetManifestResourceStream(assemblyFile))
                {
                    // ReSharper disable PossibleNullReferenceException
                    var bytes = new byte[stream.Length];

                    // ReSharper restore PossibleNullReferenceException
                    stream.Read(bytes, 0, (int)stream.Length);

                    return bytes;
                }
            }

            return null;
        }

        /// <summary>
        /// The extract resource to file.
        /// </summary>
        /// <param name="assembly">
        /// The assembly.
        /// </param>
        /// <param name="resourceName">
        /// The resource name.
        /// </param>
        /// <param name="outPath">
        /// The out path.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public static ExtractResourceResult ExtractResourceToFile(this Assembly assembly, string resourceName, string outPath)
        {
            assembly = assembly ?? Assembly.GetExecutingAssembly();

            using (var resourceStream = assembly.GetManifestResourceStream(resourceName))
            {
                if (resourceStream == null)
                {
                    return ExtractResourceResult.Failed;
                }

                var fi = new FileInfo(outPath);
                if (fi.Exists)
                {
                    using (var stream = File.OpenRead(outPath))
                    {
                        if (GetHash(stream) == GetHash(resourceStream))
                        {
                            return ExtractResourceResult.Exists;
                        }
                    }
                }

                fi.DirectoryName.EnsureFolderExistsOrCreate(false);

                using (var output = File.Create(outPath))
                {
                    int bytesRead;
                    var buf = new byte[4096];
                    resourceStream.Seek(0, SeekOrigin.Begin);
                    while ((bytesRead = resourceStream.Read(buf, 0, buf.Length)) > 0)
                    {
                        output.Write(buf, 0, bytesRead);
                    }
                }
            }

            return ExtractResourceResult.Created;
        }

        /// <summary>
        /// The get hash.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        internal static string GetHash(Stream stream)
        {
            var sha = new SHA256Managed();
            var hash = sha.ComputeHash(stream);
            return BitConverter.ToString(hash).Replace("-", string.Empty);
        }
    }
}