﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XmlSerializeExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/30/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class XmlSerializeExtensions
    {
        /// <summary>
        ///     Serialize Object to the Xml string
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>The Xml string</returns>
        /// <exception cref="System.InvalidOperationException">An error occurred during serialization.</exception>
        [Jig.Annotations.NotNull]
        public static string SerializeToXml([Jig.Annotations.NotNull] this object obj)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var builder = new StringBuilder(4096);
                using (var writer = new StringWriter(builder))
                {
                    var xmlSerializer = new XmlSerializer(obj.GetType());

                    var xmlSerializerNamespaces = new XmlSerializerNamespaces();
                    xmlSerializerNamespaces.Add(string.Empty, string.Empty);

                    xmlSerializer.Serialize(writer, obj, xmlSerializerNamespaces);
                }

                return builder.ToString();
            }

            return null;
        }

        /// <summary>
        /// Serializes to XML file.
        /// </summary>
        /// <param name="obj">The object instance.</param>
        /// <param name="virtualPath">The virtual path.</param>
        /// <returns>True if success, otherwise false</returns>
        public static bool SerializeToXmlFile([Jig.Annotations.NotNull] this object obj, [Jig.Annotations.NotNull] string virtualPath)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null && !string.IsNullOrWhiteSpace(virtualPath))
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                byte[] data = SerializeToXmlBytes(obj);
                File.WriteAllBytes(virtualPath.MapPath(), data);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Serializes Object to SQL XML (without namespace, NewLines and indent).
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>The Xml string</returns>
        /// <exception cref="System.InvalidOperationException">An error occurred during serialization.</exception>
        [Jig.Annotations.NotNull]
        public static string SerializeToSqlXml([Jig.Annotations.NotNull] this object obj)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var builder = new StringBuilder(4096);

                var settings = new XmlWriterSettings
                                   {
                                       Indent = false,
                                       NewLineHandling = NewLineHandling.None,
                                       OmitXmlDeclaration = true,
                                       CheckCharacters = true,
                                       CloseOutput = true
                                   };

                using (XmlWriter writer = XmlWriter.Create(builder, settings))
                {
                    var xmlSerializer = new XmlSerializer(obj.GetType());

                    var xmlSerializerNamespaces = new XmlSerializerNamespaces();
                    xmlSerializerNamespaces.Add(string.Empty, string.Empty);

                    xmlSerializer.Serialize(writer, obj, xmlSerializerNamespaces);
                }

                return builder.ToString();
            }

            return null;
        }

        /// <summary>
        ///     Serialize Object to the Xml byte array
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>The Xml byte array</returns>
        /// <exception cref="System.InvalidOperationException">An error occurred during serialization.</exception>
        [Jig.Annotations.NotNull]
        public static byte[] SerializeToXmlBytes([Jig.Annotations.NotNull] this object obj)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var stream = new MemoryStream(4096))
                {
                    var settings = new XmlWriterSettings { Indent = false, NewLineHandling = NewLineHandling.None, OmitXmlDeclaration = false, CheckCharacters = true, };

                    XmlWriter writer = XmlWriter.Create(stream, settings);

                    var xmlSerializer = new XmlSerializer(obj.GetType());

                    var xmlSerializerNamespaces = new XmlSerializerNamespaces();
                    xmlSerializerNamespaces.Add(string.Empty, string.Empty);

                    xmlSerializer.Serialize(writer, obj, xmlSerializerNamespaces);

                    return stream.ToArray();
                }
            }

            return null;
        }

        /// <summary>
        ///     Serializes an instance of a type into an XML stream
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <returns>The Xml string</returns>
        [Jig.Annotations.NotNull]
        public static string SerializeToDataContract([Jig.Annotations.NotNull] this object obj)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var builder = new StringBuilder(4096);

                using (XmlWriter writer = XmlWriter.Create(builder))
                {
                    var dataContractSerializer = new DataContractSerializer(obj.GetType());

                    // Serialize from an object to a stream
                    dataContractSerializer.WriteObject(writer, obj);
                }

                return builder.ToString();
            }

            return null;
        }

        /// <summary>
        ///     Deserialize an instance of a type into an object from XML stream
        /// </summary>
        /// <param name="xml">The Xml as string</param>
        /// <typeparam name="TObject">Object type to de-serialize</typeparam>
        /// <returns>The XML string</returns>
        [Jig.Annotations.CanBeNull]
        public static TObject DeserializeFromDataContract<TObject>([Jig.Annotations.NotNull] this string xml) where TObject : class
        {
            if (!string.IsNullOrEmpty(xml))
            {
                using (XmlReader reader = XElement.Parse(xml).CreateReader())
                {
                    var dataContractSerializer = new DataContractSerializer(typeof(TObject));

                    // Serialize from an object to a stream
                    return (TObject)dataContractSerializer.ReadObject(reader);
                }
            }

            return default(TObject);
        }

        /// <summary>
        ///     Deserialize an instance of a type into an object from XML stream
        /// </summary>
        /// <param name="xml">The XML as string</param>
        /// <param name="type">The type of the object.</param>
        /// <returns>The Xml string</returns>
        [Jig.Annotations.CanBeNull]
        public static object DeserializeFromDataContract([Jig.Annotations.NotNull] this string xml, [Jig.Annotations.NotNull] Type type)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                using (XmlReader reader = XElement.Parse(xml).CreateReader())
                {
                    var dataContractSerializer = new DataContractSerializer(type);

                    // Serialize from an object to a stream
                    return dataContractSerializer.ReadObject(reader);
                }
            }

            return null;
        }

        /// <summary>
        ///     Serialize Object to the Xml string
        /// </summary>
        /// <param name="obj">Object to serialize</param>
        /// <param name="type">The object Type</param>
        /// <returns>The Xml string</returns>
        /// <exception cref="System.InvalidOperationException">An error occurred during serialization.</exception>
        [Jig.Annotations.NotNull]
        public static string SerializeToXml([Jig.Annotations.NotNull] this object obj, [Jig.Annotations.NotNull] Type type)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (obj != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var builder = new StringBuilder(4096);

                using (var writer = new StringWriter(builder))
                {
                    var xmlSerializer = new XmlSerializer(type);
                    xmlSerializer.Serialize(writer, obj, new XmlSerializerNamespaces());
                }

                return builder.ToString();
            }

            return null;
        }

        /// <summary>
        ///     Deserialize Object from XML
        /// </summary>
        /// <typeparam name="TObject">Generic type to de-serialize to</typeparam>
        /// <param name="xml">The xml in string format</param>
        /// <returns>The instance of new TObject</returns>
        /// <exception cref="System.InvalidOperationException">
        ///     An error occurred during deserialization. The original exception is available
        /// using the System.Exception.InnerException property.
        /// </exception>
        [Jig.Annotations.CanBeNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No restriction on object type")]
        public static TObject DeserializeFromXml<TObject>([Jig.Annotations.NotNull] this string xml) where TObject : class
        {
            if (!string.IsNullOrEmpty(xml))
            {
                Type type = typeof(TObject);
                var serializer = new XmlSerializer(type);

                using (var reader = new StringReader(xml))
                {
                    return (TObject)serializer.Deserialize(reader);
                }
            }

            return default(TObject);
        }

        /// <summary>
        /// Deserialize from XML file.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="filePath">The virtual path.</param>
        /// <param name="virtualPath">if set to <c>true</c> virtual path.</param>
        /// <returns>
        /// The deserialized object instance or null
        /// </returns>
        [Jig.Annotations.CanBeNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No restriction on object type")]
        public static TObject DeserializeFromXmlFile<TObject>([Jig.Annotations.NotNull] this string filePath, bool virtualPath = true) where TObject : class
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                if (virtualPath)
                {
                    filePath = filePath.MapPath();
                }

                var cache = System.Runtime.Caching.MemoryCache.Default;

                var fileContent = cache[filePath] as TObject;
                if (fileContent != null)
                {
                    return fileContent;
                }

                if (File.Exists(filePath))
                {
                    byte[] data = File.ReadAllBytes(filePath);

                    var obj = data.DeserializeFromXmlBytes<TObject>();

                    var policy = new System.Runtime.Caching.CacheItemPolicy
                        {
                            AbsoluteExpiration = DateTimeOffset.Now.AddDays(1)
                        };

                    policy.ChangeMonitors.Add(new System.Runtime.Caching.HostFileChangeMonitor(new[] { filePath }));
                    cache.Set(filePath, obj, policy);

                    return obj;
                }
            }

            return default(TObject);
        }

        /// <summary>
        ///     De-serialize Object from XML byte array
        /// </summary>
        /// <typeparam name="TObject">Generic type to de-serialize to</typeparam>
        /// <param name="xml">The xml in byte array format</param>
        /// <returns>The instance of new TObject</returns>
        /// <exception cref="System.InvalidOperationException">
        ///     An error occurred during deserialization. The original exception is available
        /// using the System.Exception.InnerException property.
        /// </exception>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "No restriction on object type")]
        public static TObject DeserializeFromXmlBytes<TObject>([Jig.Annotations.NotNull] this byte[] xml) where TObject : class
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (xml != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                Type type = typeof(TObject);
                var serializer = new XmlSerializer(type);

                using (var reader = new MemoryStream(xml))
                {
                    return (TObject)serializer.Deserialize(reader);
                }
            }

            return default(TObject);
        }

        /// <summary>
        ///     Deserialize Object from XML
        /// </summary>
        /// <param name="xml">The xml in string format</param>
        /// <param name="type">The Type of the deserialized object</param>
        /// <returns>The instance of new TObject</returns>
        /// <exception cref="System.InvalidOperationException">
        ///     An error occurred during deserialization. The original exception is available
        /// using the System.Exception.InnerException property.
        /// </exception>
        [Jig.Annotations.CanBeNull]
        public static object DeserializeFromXml([Jig.Annotations.NotNull] this string xml, [Jig.Annotations.NotNull] Type type)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                var serializer = new XmlSerializer(type);
                using (var reader = new StringReader(xml))
                {
                    return serializer.Deserialize(reader);
                }
            }

            return null;
        }

        #region Serialization

        /// <summary>
        ///     Deserialize object from byte array
        /// </summary>
        /// <typeparam name="TObject">The generic object type</typeparam>
        /// <param name="bytes">The object instance as byte array</param>
        /// <returns>The instance of new TObject</returns>
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "It's converts the object stored in byte array to specific user object.")]
        public static TObject DeserializeFromBinary<TObject>([Jig.Annotations.NotNull] this byte[] bytes)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (bytes != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                using (var memoryStream = new MemoryStream(bytes))
                {
                    var formatter = new BinaryFormatter();
                    return (TObject)formatter.Deserialize(memoryStream);
                }
            }

            return default(TObject);
        }

        /// <summary>
        ///     Serialize object to byte array
        /// </summary>
        /// <param name="serializableObject">The serializable object</param>
        /// <returns>The object instance as byte array</returns>
        [Jig.Annotations.NotNull]
        public static byte[] SerializeToBinary([Jig.Annotations.NotNull] this object serializableObject)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (serializableObject != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                // Make initial size 4kb
                using (var memoryStream = new MemoryStream(4096))
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(memoryStream, serializableObject);

                    return memoryStream.ToArray();
                }
            }

            return null;
        }

        #endregion
    }
}
