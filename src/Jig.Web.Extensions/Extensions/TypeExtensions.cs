﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TypeExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       02/07/2012
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Reflection;
    using System.Web;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    [DebuggerStepThrough]
    public static partial class TypeExtensions
    {
        /// <summary>
        /// Fully qualified the type name and assembly.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The fully qualified type name with assembly</returns>
        [Jig.Annotations.NotNull]
        public static string FullNameWithAssembly([Jig.Annotations.NotNull] this Type type)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (type != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var value = string.Concat(type.FullName, ", ", type.Assembly.GetName().Name);
                return value;
            }

            return string.Empty;
        }

        /// <summary>
        /// Resolves the primitive setters.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>The list of properties</returns>
        [Jig.Annotations.NotNull]
        public static IList<PropertyInfo> ResolvePrimitiveSetters([Jig.Annotations.NotNull] this Type type)
        {
            if (type == null)
            {
                throw new HttpException(550, "The type cannot be null");
            }

            var attr = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.SetProperty).ToArray();
            var primitiveSetters = attr.Where(x => x.PropertyType.IsValueType || x.PropertyType == typeof(string)).ToArray();

            return primitiveSetters;
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="args">The args.</param>
        /// <returns>
        /// The new instance of specified type
        /// </returns>
        [Jig.Annotations.NotNull]
        public static object CreateInstance([Jig.Annotations.NotNull] this Type type, [Jig.Annotations.NotNull] params object[] args)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (args != null && args.Length > 0)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return Activator.CreateInstance(type, args);
            }

            return Activator.CreateInstance(type);
        }

        /// <summary>
        /// Creates the instance.
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="type">The type.</param>
        /// <param name="args">The args.</param>
        /// <returns>
        /// The new instance of specified type
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static TObject CreateInstance<TObject>([Jig.Annotations.NotNull] this Type type, [Jig.Annotations.NotNull] params object[] args) where TObject : class
        {
            return type.CreateInstance(args) as TObject;
        }
    }
}