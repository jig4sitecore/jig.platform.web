﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EncodeExtensions.cs" company="genuine">
//     Copyright (c) Microsoft. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Modified by:     J.Baltika
//  Date:            03/18/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Text;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class EncodeExtensions
    {
        /// <summary>
        ///     Encodes input strings for use in HTML
        /// </summary>
        /// <param name="input">String input to HtmlEncode</param>
        /// <returns>Encoded  string</returns>
        [Jig.Annotations.NotNull]
        public static string HtmlEncode([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var builder = new StringBuilder(string.Empty, input.Length * 2);
                foreach (char ch in input)
                {
                    if ((((ch > '`') && (ch < '{')) || ((ch > '@') && (ch < '['))) || (((ch == ' ') || ((ch > '/') && (ch < ':'))) || (((ch == '.') || (ch == ',')) || ((ch == '-') || (ch == '_')))))
                    {
                        builder.Append(ch);
                    }
                    else
                    {
                        builder.Append("&#").Append(((int)ch).ToString(CultureInfo.InvariantCulture)).Append(";");
                    }
                }

                return builder.ToString();
            }

            return input;
        }

        /// <summary>
        /// Encodes input strings for use in HTML
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="input">String input to HtmlEncode</param>
        /// <returns>
        /// Encoded  string
        /// </returns>
        [Jig.Annotations.NotNull]
        public static StringBuilder HtmlEncode([Jig.Annotations.NotNull] this StringBuilder builder, [Jig.Annotations.NotNull] string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                foreach (char ch in input)
                {
                    if ((((ch > '`') && (ch < '{')) || ((ch > '@') && (ch < '['))) || (((ch == ' ') || ((ch > '/') && (ch < ':'))) || (((ch == '.') || (ch == ',')) || ((ch == '-') || (ch == '_')))))
                    {
                        builder.Append(ch);
                    }
                    else
                    {
                        builder.Append("&#").Append(((int)ch).ToString(CultureInfo.InvariantCulture)).Append(";");
                    }
                }
            }

            return builder;
        }

        /// <summary>
        ///     Encodes input strings for use in XML file
        /// </summary>
        /// <param name="input">String input to XmlEncode</param>
        /// <returns>Encoded  string</returns>
        [Jig.Annotations.NotNull]
        public static string XmlEncode([Jig.Annotations.NotNull] this string input)
        {
            return input.HtmlEncode();
        }

        /// <summary>
        ///     Encodes input strings for use in Uniform Resource Identifier (URLs)
        /// </summary>
        /// <param name="input">Url as string</param>
        /// <returns>Encoded Url</returns>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Justification = "Should be similar to Microsoft's AntiXss Library API")]
        public static string UrlEncode([Jig.Annotations.NotNull] this string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                var builder = new StringBuilder(string.Empty, input.Length * 2);
                foreach (char ch in input)
                {
                    if ((((ch > '`') && (ch < '{')) || ((ch > '@') && (ch < '['))) || (((ch > '/') && (ch < ':')) || (((ch == '.') || (ch == '-')) || (ch == '_'))))
                    {
                        builder.Append(ch);
                    }
                    else if (ch > '\x007f')
                    {
                        builder.Append("%u").Append(TwoByteHex(ch));
                    }
                    else
                    {
                        builder.Append("%").Append(SingleByteHex(ch));
                    }
                }

                return builder.ToString();
            }

            return string.Empty;
        }

        /// <summary>
        /// Encodes input strings for use in Uniform Resource Identifier (URLs)
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="input">Url as string</param>
        /// <returns>
        /// Encoded Url
        /// </returns>
        [Jig.Annotations.NotNull]
        [SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Justification = "Should be similar to Microsoft's AntiXss Library API")]
        public static StringBuilder UrlEncode([Jig.Annotations.NotNull] this StringBuilder builder, [Jig.Annotations.NotNull] string input)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                foreach (char ch in input)
                {
                    if ((((ch > '`') && (ch < '{')) || ((ch > '@') && (ch < '['))) || (((ch > '/') && (ch < ':')) || (((ch == '.') || (ch == '-')) || (ch == '_'))))
                    {
                        builder.Append(ch);
                    }
                    else if (ch > '\x007f')
                    {
                        builder.Append("%u").Append(TwoByteHex(ch));
                    }
                    else
                    {
                        builder.Append("%").Append(SingleByteHex(ch));
                    }
                }
            }

            return builder;
        }

        /// <summary>
        ///     Coverts to hex decimal
        /// </summary>
        /// <param name="c">Single Character</param>
        /// <returns>Char as hex target</returns>
        [Jig.Annotations.NotNull]
        internal static string TwoByteHex(char c)
        {
            uint number = c;
            return number.ToString("x").PadLeft(4, '0');
        }

        /// <summary>
        ///     Coverts to hex decimal
        /// </summary>
        /// <param name="c">Single Character</param>
        /// <returns>Char as hex target</returns>
        [Jig.Annotations.NotNull]
        internal static string SingleByteHex(char c)
        {
            uint number = c;
            return number.ToString("x").PadLeft(2, '0');
        }
    }
}
