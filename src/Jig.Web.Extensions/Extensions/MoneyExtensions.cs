﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MoneyExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       08/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class MoneyExtensions
    {
        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The float formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this float? value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }

        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The float formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this float value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }

        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The double formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this double? value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }

        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The double formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this double value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }

        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The decimal formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this decimal? value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }

        /// <summary>
        /// Format value as the money.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The decimal formatted as money
        /// </returns>
        [Jig.Annotations.NotNull]
        [DebuggerHidden]
        public static string ToMoney(this decimal value, [Jig.Annotations.NotNull] string format = "{0:C0}")
        {
            return string.Format(format, value);
        }
    }
}
