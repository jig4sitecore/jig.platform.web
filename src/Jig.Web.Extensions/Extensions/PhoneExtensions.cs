﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PhoneExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/03/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class PhoneExtensions
    {
        /// <summary>
        /// Formats string the phone number format.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="format">The format.</param>
        /// <returns>The formatted phone or fax number</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsPhoneFormat([Jig.Annotations.NotNull] this string phoneNumber, [Jig.Annotations.NotNull] string format = "###-###-####")
        {
            var cleaned = phoneNumber.RemoveNonDigitCharacters();
            if (!string.IsNullOrWhiteSpace(cleaned))
            {
                long phone;
                if (long.TryParse(cleaned, out phone))
                {
                    return phone.ToString(format);
                }

                return phoneNumber;
            }

            return string.Empty;
        }

        /// <summary>
        /// Coverts number the phone format.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="format">The format.</param>
        /// <returns>The formatted phone number</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsPhoneFormat(this long phoneNumber, [Jig.Annotations.NotNull] string format = "###-###-####")
        {
            if (phoneNumber > 1000000000 && !string.IsNullOrWhiteSpace(format))
            {
                if (phoneNumber > 10000000000000)
                {
                    return phoneNumber.ToString(format + " ext ####");
                }

                if (phoneNumber > 1000000000000)
                {
                    return phoneNumber.ToString(format + " ext ###");
                }

                return phoneNumber.ToString(format);
            }

            return phoneNumber.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts the phone format.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="format">The format.</param>
        /// <returns>The formatted phone number</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsPhoneFormat(this long? phoneNumber, [Jig.Annotations.NotNull] string format = "###-###-####")
        {
            if (phoneNumber.HasValue)
            {
                return ConvertAsPhoneFormat(phoneNumber.Value, format);
            }

            return string.Empty;
        }

        /// <summary>
        /// Converts to Us phone format. d: 555.555.5555 x0000 Or D: 555-555-5555 x0000 OR P: (555) 555-5555 x0000
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="format">The format.</param>
        /// <returns>
        /// The formatted phone string
        /// </returns>
        [Jig.Annotations.NotNull]
        [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1631:DocumentationMustMeetCharacterPercentage", Justification = "Reviewed. Suppression is OK here.")]
        public static string ConvertAsUsPhoneFormat([Jig.Annotations.NotNull] this object phoneNumber, [Jig.Annotations.NotNull] string format = "{0:D}")
        {
            /* d: 555.555.5555 x0000
             * D: 555-555-5555 x0000
             * P: (555) 555-5555 x0000
             * */

            return string.Format(new UsPhoneNumberFormatProvider(), format, phoneNumber);
        }
    }
}