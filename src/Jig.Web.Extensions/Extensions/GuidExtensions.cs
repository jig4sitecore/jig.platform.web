﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GuidExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       11/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.Security.Cryptography;
    using System.Text;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class GuidExtensions
    {
        /// <summary>
        /// The MD5 cryptography service provider
        /// </summary>
        private static readonly MD5CryptoServiceProvider Md5CryptographyServiceProvider = new MD5CryptoServiceProvider();

        /// <summary>
        ///     Removes all special chars like {-} from Guid string
        /// </summary>
        /// <param name="id">The Guid instance</param>
        /// <returns>Returns string version of GUID without special chars. Which can be used for some cases like search engines</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsShortId(this Guid id)
        {
            return id.ToString("N").ToLowerInvariant();
        }

        /// <summary>
        ///     Removes all special chars like {-} from Guid string
        /// </summary>
        /// <param name="id">The Guid instance</param>
        /// <returns>Returns string version of GUID without special chars. Which can be used for some cases like search engines</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsShortId(this Guid? id)
        {
            if (id.HasValue)
            {
                return id.Value.ToString("N").ToLowerInvariant();
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets a generates the deterministic GUID from string.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="caseInsensitive">if set to <c>true</c> case insensitive.</param>
        /// <returns>
        /// The Unique ID
        /// </returns>
        /// <see ref="http://geekswithblogs.net/EltonStoneman/Default.aspx" />
        public static Guid ConvertAsETag([Jig.Annotations.NotNull] this string input, bool caseInsensitive = true)
        {
            if (!string.IsNullOrWhiteSpace(input))
            {
                if (caseInsensitive)
                {
                    input = input.ToLowerInvariant();
                }

                // use MD5 hash to get a 16-byte hash of the string:
                byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = Md5CryptographyServiceProvider.ComputeHash(inputBytes);

                // Generate a GUID from the hash:
                var hashGuid = new Guid(hashBytes);
                return hashGuid;
            }

            return Guid.Empty;
        }
    }
}
