﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IOExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       11/02/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Web.Hosting;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    // ReSharper disable InconsistentNaming
    [DebuggerStepThrough]
    public static partial class IOExtensions
    // ReSharper restore InconsistentNaming
    {
        /// <summary>
        ///     Maps a virtual path to a physical path on the server.
        /// </summary>
        /// <param name="virtualPath">The virtual path (absolute or relative).</param>
        /// <returns>The physical path on the server specified by virtualPath.</returns>
        [Jig.Annotations.NotNull]
        public static string MapPath([Jig.Annotations.NotNull] this string virtualPath)
        {
            if (!string.IsNullOrWhiteSpace(virtualPath))
            {
                return HostingEnvironment.MapPath(virtualPath);
            }

            return string.Empty;
        }

        /// <summary>
        ///     Determines whether the specified file exists.
        /// </summary>
        /// <param name="virtualPath">The virtual path (absolute or relative).</param>
        /// <returns>True if the path contains the name of an existing file; otherwise, false.</returns>
        public static bool FileExists([Jig.Annotations.NotNull] this string virtualPath)
        {
            if (!string.IsNullOrWhiteSpace(virtualPath))
            {
                return HostingEnvironment.VirtualPathProvider.FileExists(virtualPath);
            }

            return false;
        }

        /// <summary>
        ///     Determines whether the specified folder exists.
        /// </summary>
        /// <param name="virtualPath">The virtual path (absolute or relative).</param>
        /// <returns>True if the path contains the name of an existing folder; otherwise, false.</returns>
        public static bool DirectoryExists([Jig.Annotations.NotNull] this string virtualPath)
        {
            if (!string.IsNullOrWhiteSpace(virtualPath))
            {
                return HostingEnvironment.VirtualPathProvider.DirectoryExists(virtualPath);
            }

            return false;
        }

        /// <summary>
        /// Ensures the settings folder exists.
        /// </summary>
        /// <param name="folderPath">The folder path to check if exists or create</param>
        /// <param name="isVirtualPath">if set to <c>true</c> is virtual path.</param>
        public static void EnsureFolderExistsOrCreate([Jig.Annotations.NotNull] this string folderPath, bool isVirtualPath = true)
        {
            if (!string.IsNullOrWhiteSpace(folderPath))
            {
                string settingsFolder = null;

                if (isVirtualPath)
                {
                    /* In Unit test HostingEnvironment.MapPath will return null */
                    settingsFolder = HostingEnvironment.MapPath(folderPath);
                }

                if (string.IsNullOrWhiteSpace(settingsFolder))
                {
                    settingsFolder = folderPath;
                }

                // ReSharper disable AssignNullToNotNullAttribute
                Directory.CreateDirectory(settingsFolder);
                // ReSharper restore AssignNullToNotNullAttribute
            }
        }

        /// <summary>
        ///     Modified the directory files.
        /// </summary>
        /// <param name="sourceDirectory">The source directory.</param>
        /// <param name="targetDirectory">The target directory.</param>
        /// <param name="lastModification">The last modification.</param>
        public static void ModifiedDirectoryFiles([Jig.Annotations.NotNull] string sourceDirectory, [Jig.Annotations.NotNull] string targetDirectory, DateTime lastModification)
        {
            if (!string.IsNullOrWhiteSpace(sourceDirectory) && !string.IsNullOrWhiteSpace(targetDirectory))
            {
                if (!Directory.Exists(targetDirectory))
                {
                    Directory.CreateDirectory(targetDirectory);
                }

                string[] files = Directory.GetFiles(sourceDirectory);
                foreach (string file in files)
                {
                    FileAttributes attributes = File.GetAttributes(file);
                    if ((attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        string name = Path.GetFileName(file);

                        /* ReSharper disable AssignNullToNotNullAttribute */
                        string destination = Path.Combine(targetDirectory, name);
                        /* ReSharper restore AssignNullToNotNullAttribute */

                        if (File.GetLastWriteTime(file) >= lastModification)
                        {
                            File.Copy(file, destination, false); // overwrite any existing files.
                        }
                    }
                }

                string[] directories = Directory.GetDirectories(sourceDirectory);
                foreach (string directory in directories)
                {
                    FileAttributes attributes = File.GetAttributes(directory);
                    if ((attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        string name = Path.GetFileName(directory);

                        // ReSharper disable AssignNullToNotNullAttribute
                        string destination = Path.Combine(targetDirectory, name);
                        // ReSharper restore AssignNullToNotNullAttribute

                        // Recursive call
                        ModifiedDirectoryFiles(directory, destination, lastModification);
                    }
                }
            }
        }

        /// <summary>
        ///     Recursively copies a directory to given location. Overwrites any existing files. Skips hidden files and directories.
        /// </summary>
        /// <param name="sourceDirectory">The source directory.</param>
        /// <param name="targetDirectory">The target directory.</param>
        /// <remarks>
        ///     Modified version of the http://github.com/cuyahogaproject/cuyahoga/blob/master/src/Cuyahoga.Core/Util/IOUtil.cs
        /// </remarks>
        public static void CopyDirectory([Jig.Annotations.NotNull] string sourceDirectory, [Jig.Annotations.NotNull] string targetDirectory)
        {
            if (!string.IsNullOrWhiteSpace(sourceDirectory) && !string.IsNullOrWhiteSpace(targetDirectory))
            {
                if (!Directory.Exists(targetDirectory))
                {
                    Directory.CreateDirectory(targetDirectory);
                }

                string[] files = Directory.GetFiles(sourceDirectory);
                foreach (string file in files)
                {
                    FileAttributes attributes = File.GetAttributes(file);
                    if ((attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        string name = Path.GetFileName(file);

                        // ReSharper disable AssignNullToNotNullAttribute
                        string destination = Path.Combine(targetDirectory, name);
                        // ReSharper restore AssignNullToNotNullAttribute
                        File.Copy(file, destination, false); // overwrite any existing files.
                    }
                }

                string[] directories = Directory.GetDirectories(sourceDirectory);
                foreach (string directory in directories)
                {
                    FileAttributes attributes = File.GetAttributes(directory);
                    if ((attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                    {
                        string name = Path.GetFileName(directory);

                        // ReSharper disable AssignNullToNotNullAttribute
                        string destination = Path.Combine(targetDirectory, name);
                        // ReSharper restore AssignNullToNotNullAttribute

                        // Recursive call
                        CopyDirectory(directory, destination);
                    }
                }
            }
        }

        /// <summary>
        ///     Checks if the given directory is writable.
        /// </summary>
        /// <param name="physicalDirectory">The physical directory.</param>
        /// <returns>
        ///     <c>true</c> if is the specified physical directory writable; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsDirectoryWritable([Jig.Annotations.NotNull] string physicalDirectory)
        {
            try
            {
                // Check if the given directory is writable by creating a dummy file.
                string fileName = Path.Combine(physicalDirectory, "empty.txt");
                File.WriteAllText(fileName, "Test");

                File.Delete(fileName);
                return true;
            }
            catch (UnauthorizedAccessException)
            {
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {
            }

            return false;
        }

        /// <summary>
        /// Copies all files from one directory to another.
        /// </summary>
        /// <param name="source">The source directory</param>
        /// <param name="target">The target directory</param>
        /// <param name="recursive">The flag indicating to copy sub-directories</param>
        /// <param name="fileExcludePatterns">The file exclude patterns.</param>
        /// <remarks>
        /// Based on the Christian Liensberger http://www.liensberger.it/ code
        /// </remarks>
        public static void CopyTo([Jig.Annotations.NotNull] this DirectoryInfo source, [Jig.Annotations.NotNull] DirectoryInfo target, bool recursive = false, [Jig.Annotations.NotNull] params string[] fileExcludePatterns)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (source != null && target != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (!target.Exists)
                {
                    target.Create();
                }

                foreach (var file in source.GetFilesExcept(fileExcludePatterns))
                {
                    file.CopyTo(Path.Combine(target.FullName, file.Name), true);
                }

                if (recursive)
                {
                    foreach (var directory in source.GetDirectories())
                    {
                        directory.CopyTo(new DirectoryInfo(Path.Combine(target.FullName, directory.Name)), true, fileExcludePatterns);
                    }
                }
            }
        }

        /// <summary>
        /// Converts a numeric value into a string that represents the number
        /// expressed as a size value in bytes, kilobytes, megabytes, gigabytes,
        /// or tera bytes depending on the size. Output is identical to
        /// StrFormatByteSize() in shlwapi.dll. This is a format similar to
        /// the Windows Explorer file Properties page. For example:
        /// 532 -&gt;  532 bytes
        /// 1240 -&gt; 1.21 KB
        /// 235606 -&gt;  230 KB
        /// 5400016 -&gt; 5.14 MB
        /// </summary>
        /// <param name="fileSize">The file size from FileInfo</param>
        /// <returns>
        /// The format similar to the Windows Explorer file Properties page. For example:
        /// 532 -&gt;  532 bytes
        /// 1240 -&gt; 1.21 KB
        /// 235606 -&gt;  230 KB
        /// 5400016 -&gt; 5.14 MB
        /// </returns>
        /// <remarks>
        /// It was surprisingly difficult to emulate the StrFormatByteSize() function
        /// due to a few quirks. First, the function only displays three digits:
        /// - displays 2 decimal places for values under 10            (e.graphics. 2.12 KB)
        /// - displays 1 decimal place for values under 100            (e.graphics. 88.2 KB)
        /// - displays 0 decimal places for values under 1000         (e.graphics. 532 KB)
        /// - jumps to the next unit of measure for values over 1000    (e.graphics. 0.97 MB)
        /// The second quirk: insignificant digits are truncated rather than
        /// rounded. The original function likely uses integer math.
        /// This implementation was tested to 100 TB.
        /// </remarks>
        /// <author>Unitlities.Net</author>
        [Jig.Annotations.NotNull]
        public static string FileSizeToString(long fileSize)
        {
            if (fileSize < 1024)
            {
                return string.Format("{0} bytes", fileSize);
            }

            double value = fileSize;
            value = value / 1024;
            string unit = "KB";

            if (value >= 1000)
            {
                value = Math.Floor(value);
                value = value / 1024;
                unit = "MB";
            }

            if (value >= 1000)
            {
                value = Math.Floor(value);
                value = value / 1024;
                unit = "GB";
            }

            if (value >= 1000)
            {
                value = Math.Floor(value);
                value = value / 1024;
                unit = "TB";
            }

            if (value < 10)
            {
                value = Math.Floor(value * 100) / 100;
                return string.Format("{0:n2} {1}", value, unit);
            }

            if (value < 100)
            {
                value = Math.Floor(value * 10) / 10;
                return string.Format("{0:n1} {1}", value, unit);
            }

            value = Math.Floor(value * 1) / 1;
            return string.Format("{0:n0} {1}", value, unit);
        }
    }
}