﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       07/24/2010
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Reviewed. Suppression is OK here.")]
    [DebuggerStepThrough]
    public static partial class ObjectExtensions
    {
        /// <summary>
        /// Clones the specified original and keeps the type (must return object of the same type)
        /// </summary>
        /// <typeparam name="TObject">The type of the object.</typeparam>
        /// <param name="original">The original.</param>
        /// <returns>The clone or null if the original object is null or does not return an object of the same type</returns>
        [Jig.Annotations.CanBeNull]
        public static TObject GetClone<TObject>([Jig.Annotations.NotNull] this TObject original) where TObject : class, System.ICloneable
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (original != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return original.Clone() as TObject;
            }

            return default(TObject);
        }

        /// <summary>
        /// Determines whether [is not null original empty] [the specified collection].
        /// </summary>
        /// <typeparam name="TObject">The TObject</typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns><c>true</c> if [is not null or empty] [the specified collection]; otherwise, <c>false</c>.</returns>
        public static bool IsNotNullOrEmpty<TObject>([Jig.Annotations.NotNull] this TObject[] collection)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            return collection != null && collection.Length > 0;
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }
    }
}
