﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="XCDataExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       11/02/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Xml.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    // ReSharper disable InconsistentNaming
    [DebuggerStepThrough]
    public static partial class XCDataExtensions
    // ReSharper restore InconsistentNaming
    {
        /* ReSharper disable InconsistentNaming */

        /// <summary>
        /// Toes the XC data.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The string as XData object instance</returns>
        [Jig.Annotations.CanBeNull]
        public static XCData ConvertAsXCData([Jig.Annotations.NotNull] this string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return new XCData(value);
        }

        /// <summary>
        /// Converts Object the XCData.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>The string as XData object instance</returns>
        [Jig.Annotations.CanBeNull]
        public static XCData ConvertAsXCData([Jig.Annotations.NotNull] this XElement value)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (value != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                return new XCData(string.Concat(value));
            }

            return null;
        }

        /* ReSharper restore InconsistentNaming */
    }
}
