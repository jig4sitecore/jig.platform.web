﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UriBuilderExtensions.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       06/23/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    using Jig.Collections;

    [SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class UriBuilderExtensions
    {
        /// <summary>
        /// Converts The Uri Builder to href attribute value
        /// </summary>
        /// <param name="uriBuilder">The URI builder.</param>
        /// <returns>The HREF attribute source</returns>
        [Jig.Annotations.NotNull]
        public static string ConvertAsHref([Jig.Annotations.NotNull] this UriBuilder uriBuilder)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (uriBuilder != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (string.Equals(uriBuilder.Host, "localhost", StringComparison.InvariantCultureIgnoreCase))
                {
                    return (uriBuilder.Path + uriBuilder.Query + uriBuilder.Fragment).ToLowerInvariant();
                }

                var uri = uriBuilder.ToString().ToLowerInvariant();
                int index = uri.IndexOf('/');
                return uri.Substring(index);
            }

            return string.Empty;
        }

        /// <summary>
        /// Appends the query param.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <returns>The original uri builder</returns>
        [Jig.Annotations.CanBeNull]
        public static UriBuilder AppendQueryParam([Jig.Annotations.NotNull] this UriBuilder builder, [Jig.Annotations.NotNull] string name, [Jig.Annotations.NotNull] object value)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (builder != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var encodedValue = string.Concat(value).UrlEncode();
                builder.Query = string.Concat(string.IsNullOrEmpty(builder.Query) ? string.Empty : builder.Query.Substring(1) + "&", name, '=', encodedValue);
            }

            return builder;
        }

        /// <summary>
        /// Appends the query params.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="values">The values.</param>
        /// <returns>The original uri builder</returns>
        [Jig.Annotations.CanBeNull]
        public static UriBuilder AppendQueryParams([Jig.Annotations.NotNull] this UriBuilder builder, [Jig.Annotations.NotNull] IDictionary<string, string> values)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (builder != null && values != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                builder.Query = string.Concat(string.IsNullOrEmpty(builder.Query) ? string.Empty : builder.Query.Substring(1) + "&", values.ConvertAsQueryString());
            }

            return builder;
        }

        /// <summary>
        /// Appends the query params.
        /// </summary>
        /// <param name="builder">The builder.</param>
        /// <param name="values">The values.</param>
        /// <returns>The original uri builder</returns>
        [Jig.Annotations.CanBeNull]
        public static UriBuilder AppendQueryParams([Jig.Annotations.NotNull] this UriBuilder builder, [Jig.Annotations.NotNull] IDictionary<string, object> values)
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (builder != null && values != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                builder.Query = string.Concat(string.IsNullOrEmpty(builder.Query) ? string.Empty : builder.Query.Substring(1) + "&", values.ConvertAsQueryString());
            }

            return builder;
        }
    }
}
