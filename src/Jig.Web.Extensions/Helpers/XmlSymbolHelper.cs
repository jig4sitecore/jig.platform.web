﻿// -----------------------------------------------------------------------------
// <copyright file="XmlSymbolHelper.cs" company="genuine">
//      Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------------
//   Author:     J.Baltika
//   Date:       10/29/2013
// -----------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Collections.Generic;

    /// <summary>
    /// The xml symbol helper.
    /// </summary>
    public static partial class XmlSymbolHelper
    {
        #region Public Properties

        /// <summary>
        ///     Gets Xml Symbol hash table without Html entities.
        /// </summary>
        /// <value>
        ///     The xml symbols.
        /// </value>
        [Jig.Annotations.NotNull]
        public static IDictionary<char, string> XmlSymbols
        {
            // ReSharper disable once FunctionComplexityOverflow
            get
            {
                var xmlSymbols = new Dictionary<char, string>();

                xmlSymbols['¡'] = "&#161";
                xmlSymbols['¢'] = "&#162";
                xmlSymbols['£'] = "&#163";
                xmlSymbols['¤'] = "&#164";
                xmlSymbols['¥'] = "&#165";
                xmlSymbols['¦'] = "&#166";
                xmlSymbols['§'] = "&#167";
                xmlSymbols['¨'] = "&#168";
                xmlSymbols['©'] = "&#169";
                xmlSymbols['ª'] = "&#170";
                xmlSymbols['«'] = "&#171";
                xmlSymbols['¬'] = "&#172";
                xmlSymbols['®'] = "&#174";
                xmlSymbols['¯'] = "&#175";
                xmlSymbols['°'] = "&#176";
                xmlSymbols['±'] = "&#177";
                xmlSymbols['²'] = "&#178";
                xmlSymbols['³'] = "&#179";
                xmlSymbols['´'] = "&#180";
                xmlSymbols['µ'] = "&#181";
                xmlSymbols['¶'] = "&#182";
                xmlSymbols['•'] = "&#183";
                xmlSymbols['¸'] = "&#184";
                xmlSymbols['¹'] = "&#185";
                xmlSymbols['º'] = "&#186";
                xmlSymbols['»'] = "&#187";
                xmlSymbols['¼'] = "&#188";
                xmlSymbols['½'] = "&#189";
                xmlSymbols['¾'] = "&#190";
                xmlSymbols['¿'] = "&#191";
                xmlSymbols['À'] = "&#192";
                xmlSymbols['Á'] = "&#193";
                xmlSymbols['Â'] = "&#194";
                xmlSymbols['Ã'] = "&#195";
                xmlSymbols['Ä'] = "&#196";
                xmlSymbols['Å'] = "&#197";
                xmlSymbols['Æ'] = "&#198";
                xmlSymbols['Ç'] = "&#199";
                xmlSymbols['È'] = "&#200";
                xmlSymbols['É'] = "&#201";
                xmlSymbols['Ê'] = "&#202";
                xmlSymbols['Ë'] = "&#203";
                xmlSymbols['Ì'] = "&#204";
                xmlSymbols['Í'] = "&#205";
                xmlSymbols['Î'] = "&#206";
                xmlSymbols['Ï'] = "&#207";
                xmlSymbols['Ð'] = "&#208";
                xmlSymbols['Ñ'] = "&#209";
                xmlSymbols['Ò'] = "&#210";
                xmlSymbols['Ó'] = "&#211";
                xmlSymbols['Ô'] = "&#212";
                xmlSymbols['Õ'] = "&#213";
                xmlSymbols['Ö'] = "&#214";
                xmlSymbols['×'] = "&#215";
                xmlSymbols['Ø'] = "&#216";
                xmlSymbols['Ù'] = "&#217";
                xmlSymbols['Ú'] = "&#218";
                xmlSymbols['Û'] = "&#219";
                xmlSymbols['Ü'] = "&#220";
                xmlSymbols['Ý'] = "&#221";
                xmlSymbols['Þ'] = "&#222";
                xmlSymbols['ß'] = "&#223";
                xmlSymbols['à'] = "&#224";
                xmlSymbols['á'] = "&#225";
                xmlSymbols['â'] = "&#226";
                xmlSymbols['ã'] = "&#227";
                xmlSymbols['ä'] = "&#228";
                xmlSymbols['å'] = "&#229";
                xmlSymbols['æ'] = "&#230";
                xmlSymbols['ç'] = "&#231";
                xmlSymbols['è'] = "&#232";
                xmlSymbols['é'] = "&#233";
                xmlSymbols['ê'] = "&#234";
                xmlSymbols['ë'] = "&#235";
                xmlSymbols['ì'] = "&#236";
                xmlSymbols['í'] = "&#237";
                xmlSymbols['î'] = "&#238";
                xmlSymbols['ï'] = "&#239";
                xmlSymbols['ð'] = "&#240";
                xmlSymbols['ñ'] = "&#241";
                xmlSymbols['ò'] = "&#242";
                xmlSymbols['ó'] = "&#243";
                xmlSymbols['ô'] = "&#244";
                xmlSymbols['õ'] = "&#245";
                xmlSymbols['ö'] = "&#246";
                xmlSymbols['÷'] = "&#247";
                xmlSymbols['ø'] = "&#248";
                xmlSymbols['ù'] = "&#249";
                xmlSymbols['ú'] = "&#250";
                xmlSymbols['û'] = "&#251";
                xmlSymbols['ü'] = "&#252";
                xmlSymbols['ý'] = "&#253";
                xmlSymbols['þ'] = "&#254";
                xmlSymbols['ÿ'] = "&#255";
                xmlSymbols['Œ'] = "&#338";
                xmlSymbols['œ'] = "&#339";
                xmlSymbols['Š'] = "&#352";
                xmlSymbols['š'] = "&#353";
                xmlSymbols['Ÿ'] = "&#376";
                xmlSymbols['ƒ'] = "&#402";
                xmlSymbols['ˆ'] = "&#710";
                xmlSymbols['˜'] = "&#732";
                xmlSymbols['Α'] = "&#913";
                xmlSymbols['Β'] = "&#914";
                xmlSymbols['Γ'] = "&#915";
                xmlSymbols['Δ'] = "&#916";
                xmlSymbols['Ε'] = "&#917";
                xmlSymbols['Ζ'] = "&#918";
                xmlSymbols['Η'] = "&#919";
                xmlSymbols['Θ'] = "&#920";
                xmlSymbols['Ι'] = "&#921";
                xmlSymbols['Κ'] = "&#922";
                xmlSymbols['Λ'] = "&#923";
                xmlSymbols['Μ'] = "&#924";
                xmlSymbols['Ν'] = "&#925";
                xmlSymbols['Ξ'] = "&#926";
                xmlSymbols['Ο'] = "&#927";
                xmlSymbols['Π'] = "&#928";
                xmlSymbols['Ρ'] = "&#929";
                xmlSymbols['Σ'] = "&#931";
                xmlSymbols['Τ'] = "&#932";
                xmlSymbols['Υ'] = "&#933";
                xmlSymbols['Φ'] = "&#934";
                xmlSymbols['Χ'] = "&#935";
                xmlSymbols['Ψ'] = "&#936";
                xmlSymbols['Ω'] = "&#937";
                xmlSymbols['α'] = "&#945";
                xmlSymbols['β'] = "&#946";
                xmlSymbols['γ'] = "&#947";
                xmlSymbols['δ'] = "&#948";
                xmlSymbols['ε'] = "&#949";
                xmlSymbols['ζ'] = "&#950";
                xmlSymbols['η'] = "&#951";
                xmlSymbols['θ'] = "&#952";
                xmlSymbols['ι'] = "&#953";
                xmlSymbols['κ'] = "&#954";
                xmlSymbols['λ'] = "&#955";
                xmlSymbols['μ'] = "&#956";
                xmlSymbols['ν'] = "&#957";
                xmlSymbols['ξ'] = "&#958";
                xmlSymbols['ο'] = "&#959";
                xmlSymbols['π'] = "&#960";
                xmlSymbols['ρ'] = "&#961";
                xmlSymbols['ς'] = "&#962";
                xmlSymbols['σ'] = "&#963";
                xmlSymbols['τ'] = "&#964";
                xmlSymbols['υ'] = "&#965";
                xmlSymbols['φ'] = "&#966";
                xmlSymbols['χ'] = "&#967";
                xmlSymbols['ψ'] = "&#968";
                xmlSymbols['ω'] = "&#969";
                xmlSymbols['ϑ'] = "&#977";
                xmlSymbols['ϒ'] = "&#978";
                xmlSymbols['ϖ'] = "&#982";
                xmlSymbols['–'] = "&#8211";
                xmlSymbols['—'] = "&#8212";
                xmlSymbols['‘'] = "&#8216";
                xmlSymbols['’'] = "&#8217";
                xmlSymbols['‚'] = "&#8218";
                xmlSymbols['“'] = "&#8220";
                xmlSymbols['”'] = "&#8221";
                xmlSymbols['„'] = "&#8222";
                xmlSymbols['†'] = "&#8224";
                xmlSymbols['‡'] = "&#8225";
                xmlSymbols['…'] = "&#8230";
                xmlSymbols['‰'] = "&#8240";
                xmlSymbols['′'] = "&#8242";
                xmlSymbols['″'] = "&#8243";
                xmlSymbols['‹'] = "&#8249";
                xmlSymbols['›'] = "&#8250";
                xmlSymbols['‾'] = "&#8254";
                xmlSymbols['⁄'] = "&#8260";
                xmlSymbols['€'] = "&#8364";
                xmlSymbols['™'] = "&#8482";
                xmlSymbols['℠'] = "&#8480";
                xmlSymbols['←'] = "&#8592";
                xmlSymbols['↑'] = "&#8593";
                xmlSymbols['→'] = "&#8594";
                xmlSymbols['↓'] = "&#8595";
                xmlSymbols['↔'] = "&#8596";
                xmlSymbols['∂'] = "&#8706";
                xmlSymbols['∏'] = "&#8719";
                xmlSymbols['∑'] = "&#8721";
                xmlSymbols['−'] = "&#8722";
                xmlSymbols['√'] = "&#8730";
                xmlSymbols['∞'] = "&#8734";
                xmlSymbols['∩'] = "&#8745";
                xmlSymbols['∫'] = "&#8747";
                xmlSymbols['≈'] = "&#8776";
                xmlSymbols['≠'] = "&#8800";
                xmlSymbols['≡'] = "&#8801";
                xmlSymbols['≤'] = "&#8804";
                xmlSymbols['≥'] = "&#8805";
                xmlSymbols['〈'] = "&#9001";
                xmlSymbols['〉'] = "&#9002";
                xmlSymbols['◊'] = "&#9674";
                xmlSymbols['♠'] = "&#9824";
                xmlSymbols['♣'] = "&#9827";
                xmlSymbols['♥'] = "&#9829";

                return xmlSymbols;
            }
        }

        #endregion
    }
}