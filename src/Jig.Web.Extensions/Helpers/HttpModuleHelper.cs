﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HttpModuleHelper.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       05/06/2009
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class.")]
    [DebuggerStepThrough]
    public static partial class HttpModuleHelper
    {
        /// <summary>
        ///     Finds the running instance of IHttpModule
        /// </summary>
        /// <typeparam name="TObject">The type of the IHttpModule instance</typeparam>
        /// <returns>The instance of IHttpModule otherwise Null</returns>
        [Jig.Annotations.CanBeNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "It's tries to find the Http Module in web.config section and return to the user specified instance")]
        public static TObject FirstOrDefault<TObject>() where TObject : class, IHttpModule
        {
            var context = HttpContext.Current;
            if (context != null)
            {
                HttpModuleCollection collection = context.ApplicationInstance.Modules;

                return (from string moduleName in collection select collection[moduleName]).OfType<TObject>().FirstOrDefault();
            }

            return default(TObject);
        }

        /// <summary>
        ///     Finds the running instance of IHttpModule
        /// </summary>
        /// <typeparam name="TObject">The type of the IHttpModule instance</typeparam>
        /// <param name="moduleName">The IHttpModule name in web.config</param>
        /// <returns>The instance of IHttpModule if found otherwise Null </returns>
        [Jig.Annotations.CanBeNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "It's tries to find the Http Module in web.config section and return to the user specified instance")]
        public static TObject FirstOrDefault<TObject>([Jig.Annotations.NotNull] string moduleName) where TObject : class, IHttpModule
        {
            var context = HttpContext.Current;

            if (!string.IsNullOrWhiteSpace(moduleName) && context != null)
            {
                var module = context.ApplicationInstance.Modules[moduleName] as TObject;

                return module;
            }

            return default(TObject);
        }

        /// <summary>
        ///     Finds the running instance of IHttpModule
        /// </summary>
        /// <typeparam name="TObject">The type of the IHttpModule instance</typeparam>
        /// <param name="applicationInstance">The base class for applications that are defined by the user in the Global.asax file</param>
        /// <param name="moduleName">The IHttpModule name in web.config</param>
        /// <returns>The instance of IHttpModule if found otherwise Null </returns>
        [Jig.Annotations.CanBeNull]
        [SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "It's tries to find the Http Module in web.config section and return to the user specified instance")]
        public static TObject FirstOrDefaultHttpModule<TObject>([Jig.Annotations.NotNull] this HttpApplication applicationInstance, [Jig.Annotations.NotNull] string moduleName) where TObject : class, IHttpModule
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (!string.IsNullOrWhiteSpace(moduleName) && applicationInstance != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                var module = applicationInstance.Modules[moduleName] as TObject;
                return module;
            }

            return default(TObject);
        }
    }
}
