﻿// -----------------------------------------------------------------------------
// <copyright file="ImageFormatHelper.cs" company="genuine">
//      Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------------
//   Author:     J.Baltika
//   Date:       10/29/2013
// -----------------------------------------------------------------------------
namespace Jig.Web
{
    using System.Drawing.Imaging;
    using System.Runtime.InteropServices;

    /// <summary>
    /// The image format helper.
    /// </summary>
    [Guid("AF7DBD28-52F1-4759-B056-ED9E7A03F879")]
    public static partial class ImageFormatHelper
    {
        #region Public Methods and Operators

        /// <summary>
        /// Gets the type of the image format by content.
        /// </summary>
        /// <param name="contentType">
        /// Type of the content.
        /// </param>
        /// <returns>
        /// The type of the image format by content
        /// </returns>
        [Jig.Annotations.CanBeNull]
        public static ImageFormat GetImageFormatByContentType([Jig.Annotations.NotNull] string contentType)
        {
            ImageFormat format = null;

            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            if (contentType != null)
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
            {
                if (contentType.Equals("image/gif"))
                {
                    format = ImageFormat.Gif;
                }
                else if (contentType.Equals("image/jpeg") || contentType.Equals("image/pjpeg"))
                {
                    format = ImageFormat.Jpeg;
                }
                else if (contentType.Equals("image/png"))
                {
                    format = ImageFormat.Png;
                }
                else if (contentType.Equals("image/bmp"))
                {
                    format = ImageFormat.Bmp;
                }
                else if (contentType.Equals("image/x-icon"))
                {
                    format = ImageFormat.Icon;
                }
                else if (contentType.Equals("image/tiff"))
                {
                    format = ImageFormat.Tiff;
                }
            }

            return format;
        }

        #endregion
    }
}