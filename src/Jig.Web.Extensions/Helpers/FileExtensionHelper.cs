﻿// -----------------------------------------------------------------------------
// <copyright file="FileExtensionHelper.cs" company="genuine">
//      Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------------
//   Author:     J.Baltika
//   Date:       09/10/2013
// -----------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The file extensions.
    /// </summary>
    public static partial class FileExtensionHelper
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets the type of the file extension by content.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        /// <returns>The type of the file extension by content</returns>
        [Jig.Annotations.NotNull]
        public static string GetImageFileExtensionByContentType([Jig.Annotations.NotNull] string contentType)
        {
            string ext = "bin";

            if (contentType.Equals("image/gif"))
            {
                ext = "gif";
            }
            else if (contentType.Equals("image/jpeg") || contentType.Equals("image/pjpeg"))
            {
                ext = "jpg";
            }
            else if (contentType.Equals("image/png"))
            {
                ext = "png";
            }
            else if (contentType.Equals("image/bmp"))
            {
                ext = "bmp";
            }
            else if (contentType.Equals("image/x-icon"))
            {
                ext = "ico";
            }
            else if (contentType.Equals("image/tiff"))
            {
                ext = "tif";
            }

            return ext;
        }

        /// <summary>
        /// Gets the common web file extensions.
        /// </summary>
        /// <param name="additionalExtensions">The additional extensions.</param>
        /// <returns>
        /// The common file extension collection
        /// </returns>
        [Jig.Annotations.NotNull]
        public static ICollection<string> GetMediaFileExtensions([Jig.Annotations.NotNull] params string[] additionalExtensions)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            ICollection<string> collection = new HashSet<string>(additionalExtensions ?? new string[] { }, StringComparer.InvariantCultureIgnoreCase);
            // ReSharper restore ConstantNullCoalescingCondition

            /* Images */
            collection.Add(".jpg");
            collection.Add(".jpeg");
            collection.Add(".png");
            collection.Add(".gif");
            collection.Add(".ico");

            /* Html */
            collection.Add(".htm");
            collection.Add(".html");
            collection.Add(".js");
            collection.Add(".css");

            /* Fonts */
            collection.Add(".eot");
            collection.Add(".svg");
            collection.Add(".ttf");
            collection.Add(".woff");

            /* Documents */
            collection.Add(".pdf");
            collection.Add(".doc");
            collection.Add(".docx");
            collection.Add(".xls");
            collection.Add(".xlsx");

            return collection;
        }

        /// <summary>
        /// Gets the image extensions.
        /// </summary>
        /// <param name="additionalExtensions">The additional extensions.</param>
        /// <returns>
        /// The common image extension collection
        /// </returns>
        [Jig.Annotations.NotNull]
        public static ICollection<string> GetImageFileExtensions([Jig.Annotations.NotNull] params string[] additionalExtensions)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            ICollection<string> collection = new HashSet<string>(additionalExtensions ?? new string[] { }, StringComparer.InvariantCultureIgnoreCase);
            // ReSharper restore ConstantNullCoalescingCondition

            /* Images */
            collection.Add(".jpg");
            collection.Add(".jpeg");
            collection.Add(".png");
            collection.Add(".gif");
            collection.Add(".ico");

            return collection;
        }

        /// <summary>
        /// Gets the HTML file extensions.
        /// </summary>
        /// <param name="additionalExtensions">The additional extensions.</param>
        /// <returns>The collection of the extensions</returns>
        [Jig.Annotations.NotNull]
        public static ICollection<string> GetHtmlFileExtensions([Jig.Annotations.NotNull] params string[] additionalExtensions)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            ICollection<string> collection = new HashSet<string>(additionalExtensions ?? new string[] { }, StringComparer.InvariantCultureIgnoreCase);
            // ReSharper restore ConstantNullCoalescingCondition

            /* Html */
            collection.Add(".htm");
            collection.Add(".html");
            collection.Add(".js");
            collection.Add(".css");

            return collection;
        }

        /// <summary>
        /// Gets the font file extensions.
        /// </summary>
        /// <param name="additionalExtensions">The additional extensions.</param>
        /// <returns>The collection of the extensions</returns>
        [Jig.Annotations.NotNull]
        public static ICollection<string> GetFontFileExtensions([Jig.Annotations.NotNull] params string[] additionalExtensions)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            ICollection<string> collection = new HashSet<string>(additionalExtensions ?? new string[] { }, StringComparer.InvariantCultureIgnoreCase);
            // ReSharper restore ConstantNullCoalescingCondition

            /* Fonts */
            collection.Add(".eot");
            collection.Add(".svg");
            collection.Add(".ttf");
            collection.Add(".woff");

            return collection;
        }

        /// <summary>
        /// Gets the document file extensions.
        /// </summary>
        /// <param name="additionalExtensions">The additional extensions.</param>
        /// <returns>The collection of the extensions</returns>
        [Jig.Annotations.NotNull]
        public static ICollection<string> GetDocumentFileExtensions([Jig.Annotations.NotNull] params string[] additionalExtensions)
        {
            // ReSharper disable ConstantNullCoalescingCondition
            ICollection<string> collection = new HashSet<string>(additionalExtensions ?? new string[] { }, StringComparer.InvariantCultureIgnoreCase);
            // ReSharper restore ConstantNullCoalescingCondition

            /* Documents */
            collection.Add(".pdf");
            collection.Add(".doc");
            collection.Add(".docx");
            collection.Add(".xls");
            collection.Add(".xlsx");

            return collection;
        }

        #endregion
    }
}