﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="USStateHelper.cs" company="genuine">
//     Copyright (c) Jig Web Team. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
//  Author:     J.Baltika
//  Date:       04/24/2008
// --------------------------------------------------------------------------------------------------------------------
namespace Jig.Web
{
    using System;
    using System.Collections.Generic;
    using System.Web;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.DocumentationRules", "SA1601:PartialElementsMustBeDocumented", Justification = "Extension methods partial class."), System.Runtime.InteropServices.GuidAttribute("F8F3D47C-4982-4C02-BEAD-1B38B941DF8C")]
    // ReSharper disable InconsistentNaming
    public static partial class USStateHelper
    // ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Gets the state collection.
        /// </summary>
        /// <returns>The State collection</returns>
        [Jig.Annotations.NotNull]
        public static IDictionary<string, string> GetStateCollection()
        {
            return new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
                       {
                           { "AL", "Alabama" },
                           { "AK", "Alaska" },
                           { "AZ", "Arizona" },
                           { "AR", "Arkansas" },
                           { "CA", "California" },
                           { "CO", "Colorado" },
                           { "CT", "Connecticut" },
                           { "DC", "District of Columbia" },
                           { "DE", "Delaware" },
                           { "FL", "Florida" },
                           { "GA", "Georgia" },
                           { "HI", "Hawaii" },
                           { "ID", "Idaho" },
                           { "IL", "Illinois" },
                           { "IN", "Indiana" },
                           { "IA", "Iowa" },
                           { "KS", "Kansas" },
                           { "KY", "Kentucky" },
                           { "LA", "Louisiana" },
                           { "ME", "Maine" },
                           { "MD", "Maryland" },
                           { "MA", "Massachusetts" },
                           { "MI", "Michigan" },
                           { "MN", "Minnesota" },
                           { "MS", "Mississippi" },
                           { "MO", "Missouri" },
                           { "MT", "Montana" },
                           { "NE", "Nebraska" },
                           { "NV", "Nevada" },
                           { "NH", "New Hampshire" },
                           { "NJ", "New Jersey" },
                           { "NM", "New Mexico" },
                           { "NY", "New York" },
                           { "NC", "North Carolina" },
                           { "ND", "North Dakota" },
                           { "OH", "Ohio" },
                           { "OK", "Oklahoma" },
                           { "OR", "Oregon" },
                           { "PA", "Pennsylvania" },
                           { "PR", "Puerto Rico" },
                           { "RI", "Rhode Island" },
                           { "SC", "South Carolina" },
                           { "SD", "South Dakota" },
                           { "TN", "Tennessee" },
                           { "TX", "Texas" },
                           { "UT", "Utah" },
                           { "VT", "Vermont" },
                           { "VA", "Virginia" },
                           { "WA", "Washington" },
                           { "WV", "West Virginia" },
                           { "WI", "Wisconsin" },
                           { "WY", "Wyoming" }
                       };
        }

        /// <summary>
        /// States the long name of the abbreviation to.
        /// </summary>
        /// <param name="stateAbbreviation">The state abbreviation.</param>
        /// <returns>The state long name.</returns>
        [Jig.Annotations.NotNull]
        public static string StateAbbreviationToLongName([Jig.Annotations.NotNull] string stateAbbreviation)
        {
            if (string.IsNullOrWhiteSpace(stateAbbreviation) || stateAbbreviation.Length != 2)
            {
                throw new HttpException(550, "The State Abbreviation must be two characters!");
            }

            return GetStateCollection()[stateAbbreviation];
        }
    }
}
